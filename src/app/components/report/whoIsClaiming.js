App.controller('whoIsClaiming', ['$scope','$state','$stateParams', '$rootScope', 'MetaInformation','$http', 'SETTINGS', 'GLOBALS', 'authService','toaster','$mdDialog','$filter','$timeout', function($scope,$state,$stateParams, $rootScope, MetaInformation,$http, SETTINGS, GLOBALS, authService,toaster,$mdDialog,$filter,$timeout) {

    $scope.claimingObj1 = {};
    $scope.claimingObj1.fromDate = null;
    $scope.claimingObj1.toDate = null;

    $scope.noData = "No data found";
    $scope.corpID = $scope.userDetails.corpID;  

    $scope.claimingObj = {
        DataTableCommon:{
            param:{
                Draw:1,
                Start:0,
                Length:10
            },
            ExportType:0
        },
        corpID:$scope.corpID,
        fromDate:($scope.claimingObj1.fromDate) ? $scope.claimingObj1.fromDate : "",
        toDate:($scope.claimingObj1.toDate) ? $scope.claimingObj1.toDate : ""
    };
    
    // $scope.claimingObj.claimingCapacity = "ccd";
    $scope.claimingObj.cc = "ccd";


    /* ===== CallBack Global Left panel View ===== */
    $scope.copyObj = angular.copy($scope.claimingObj);
    
    $scope.$emit('callSelectedFieldByPage',$scope.claimingObj);

    $scope.$on('callSelectedField', function(evnt,obj) { 
        var finalObj = angular.copy($scope.copyObj);
        // $scope.claimingObj = Object.assign(finalObj,obj); 
        $scope.claimingObj = $.extend(finalObj, obj);
        $scope.callFilterData(true);
    }); 

    function filterDate(obj){
        if(obj.deposit_date){
            obj.deposit_date = new Date(obj.deposit_date);
        }
        if(obj.claimCreateDate){
            obj.claimCreateDate = new Date(obj.claimCreateDate);
        }
        if(obj.claimDate){
            obj.claimDate = new Date(obj.claimDate);
        }
        return obj;
    }

    $scope.checkValidDate = function(fromDate,toDate){
        if(toDate && fromDate > toDate){
            $scope.claimingObj1.toDate = fromDate;
        }
    }

    $scope.callFilterData = function(valid){
        if($scope.whoisClaim.$valid){
            $timeout(function(){
                if(valid){
                    $scope.claimingObj.DataTableCommon.ExportType = 0;
                    $scope.claimingObj.DataTableCommon.param.Start = 0;
                    $scope.claimingObj.DataTableCommon.param.Length = 10;
                    
                    if($scope.claimingObj.DataTableCommon.param.Columns){
                        delete $scope.claimingObj.DataTableCommon.param.Columns;
                    }
                }
                $scope.accountListLoading = true;
                if(!$scope.claimingObj.DataTableCommon.ExportType){
                    $scope.bookingList = [];
                    $scope.claimingObj.fromDate = $filter('date')($scope.claimingObj1.fromDate, "dd/MM/yyyy");
                    $scope.claimingObj.toDate = $filter('date')($scope.claimingObj1.toDate, "dd/MM/yyyy");
                    $scope.claimingObj.claimingCapacity = $scope.claimingObj.cc;
                    $scope.$emit('callSelectedFieldByPage',$scope.claimingObj);
                    if($scope.claimingObj.pageStart){
                        $scope.query.page = 1;
                    }
                }
                $http({
                    url: SETTINGS[GLOBALS.ENV].apiUrl+"Claim/GetWhoIsClaiming",
                    method: 'POST',
                    data: $scope.claimingObj,
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
                    }
                }).success(function(response) {
                    $scope.accountListLoading = false;
                    if(response.ResposeCode == 2){
                        if(response.DataList.data){
                            $scope.bookingList = response.DataList.data.filter(filterDate);
                            $scope.bookingListTotal = response.DataList.recordsTotal;

                            /*===== For Fixed header =====*/
                            $timeout(function () {

                                if ($("md-table-container .md-table:first").length > 0) {
                                    $("md-table-container .md-table:first").floatThead('destroy');
                                    $("md-table-container .md-table:first").floatThead({ scrollingTop: 60 });
                                }

                            }, 2);

                        }
                        if(response.DataList.fileName){
                            /*window.open(
                                SETTINGS[GLOBALS.ENV].apiUrl+response.DataList.fileName,
                                '_blank'
                                );*/
                                window.open(
                                    response.DataList.fileName,
                                    '_blank'
                                    );
                            }
                        // $scope.bookingListTotal = response.DataList.recordsTotal;
                    }else{
                        $scope.bookingList = "";
                    }
                    if($scope.claimingObj.pageStart){
                        delete $scope.claimingObj.pageStart;
                    }
                }).error(function(err,status) {
                    /*===== For Fixed header =====*/
                    var $table = $('.md-table:first');
                    $table.floatThead({
                      responsiveContainer: function($table){
                          return $table.closest('.full-height');
                      }

                  });
                    $scope.errorView(err,status);
                });
            },300);
        }
    }



    $scope.getPDFLink = function(expot){
        $scope.claimingObj.DataTableCommon.ExportType = Number(expot);
        if($scope.claimingObj.DataTableCommon.param.Columns){
            delete $scope.claimingObj.DataTableCommon.param.Columns;
        }
        $scope.callFilterData();
    }

    /* ==== Pagination Click function ==== */
    $scope.logOrder = function (order) {
     $scope.claimingObj.DataTableCommon.param.Columns = [{}];
     if(order.charAt(0) == '-'){
        $scope.claimingObj.DataTableCommon.param.Columns[0].Name = order.slice(1);    
        $scope.claimingObj.DataTableCommon.param.Columns[0].Dir = "desc";    
    }else{
        $scope.claimingObj.DataTableCommon.param.Columns[0].Name = order;    
        $scope.claimingObj.DataTableCommon.param.Columns[0].Dir = "asc";    
    }
    $scope.claimingObj.DataTableCommon.ExportType = 0;
    $scope.callFilterData();
};

$scope.logPagination = function (page, limit) {
    var startPage = (limit * (page - 1));
    $scope.claimingObj.DataTableCommon.param.Start = startPage;
    $scope.claimingObj.DataTableCommon.param.Length = limit;
    $scope.claimingObj.DataTableCommon.ExportType = 0;
    $scope.callFilterData();
}

}]);