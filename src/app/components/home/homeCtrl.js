App.controller('homeCtrl', ['$scope','$state','$stateParams', '$rootScope', 'MetaInformation','$http', 'SETTINGS', 'GLOBALS', 'authService','toaster','$mdDialog','$filter','$timeout', function($scope,$state,$stateParams, $rootScope, MetaInformation,$http, SETTINGS, GLOBALS, authService,toaster,$mdDialog,$filter,$timeout) {
	// --- 1 ---
	/*$scope.bookingReportParms = {
		matched: "1",
		unmatched: "1",
		unmatched1: "1"
	};*/


	/*$scope.copyObj = angular.copy($scope.bookingReportParms);*/

	// --- 2 ---
	// $scope.$emit('callSelectedFieldByPage', $scope.bookingReportParms);

	
	/*$scope.$on('callSelectedField', function(evnt,obj) { 
		var finalObj = angular.copy($scope.copyObj);
		$scope.bookingReportParms = Object.assign(finalObj,obj); 
	
	});*/

	// $scope.$emit('callSelectedFieldByPage',{});

  // $scope.limitOptions = [10,25,50];
  $scope.limitOptions = [50, 100];
  $scope.query = {};
  // $scope.query.limit = 10;
  $scope.query.limit = 50;
  $scope.query.page = 1;


  $scope.claimingObj1 = {};
  $scope.claimingObj1.fromDate = null;
  $scope.claimingObj1.toDate = null;

  $scope.noData = "No data found";
  $scope.corpID = $scope.userDetails.corpID;  
  $scope.showPage = false;

  $scope.claimingObj = {
    DataTableCommon: {
     param: {
      Draw: 1,
      Start: 0,
      // Length: 10
      Length: 50
    },
    ExportType: 0
  },
  corpID: $scope.corpID,
  unit: ($scope.userDetails.unit)?$scope.userDetails.unit: '',
  branch: ($scope.userDetails.branch)?$scope.userDetails.branch: '',
  account: ($scope.userDetails.account)?$scope.userDetails.account: '',
  iata: ($scope.userDetails.iata)?$scope.userDetails.iata: '',
  // agent: ($scope.userDetails.agent)?$scope.userDetails.agent: '',
  agent: '',
  fromDate: '',
  toDate: '',
  dateSelect: '',
  propertyBrand: '',
  propertyChain: '',
  propertyName: '',
  propertyAddress: '',
  propertyCity: '',
  propertyState: '',
  propertyZip: '',
  propertyPhone: '',
  propertyPreferred: false,
  lastName: '',
  firstName: '',
  type: '',
  status: '',
  claimed: '',
  amount: '',
  amountSign: '',
  amountDollar: '',
  checkNo: '',
  confirmation: '',
  recordNo: '',
        /*bookingNo: '',
        paymentNo: '',*/
        batchNo: 0,
        recordSelect: '',
        bookingSource: '',
        accountGroup: '',
        pnr: '',
        rateCode: '',
        vendor: '',
        matched: "1",
        unmatched: "1",
        unmatched1: "1"
      };

      $scope.copObj = angular.copy($scope.claimingObj);
      /* ===== CallBack Global Left panel View ===== */
      $scope.$emit('callSelectedFieldByPage',$scope.claimingObj);
      $scope.$on('callSelectedField', function(evnt,obj) { 
        //var gettt = angular.copy(obj);
        // obj = $rootScope.bkupData;
        /*if(obj.amountSign){
        	obj.amountSign = ''+($scope.amountSelection.indexOf(obj.amountSign) + 1)+'';
        }*/
        if(obj.TAID){
        	obj.TAID = ($scope.userDetails.ta_id) ? $scope.userDetails.ta_id : '';
        }
        if(obj.propertyPreferred){
        	obj.propertyPreferred = true;
        }else{
        	obj.propertyPreferred = false;
        }
        /*if(obj.unmatched){
            obj.unmatched = '1';
        }else{
            obj.unmatched = '0';
        }
        if(obj.matched){
            obj.matched = '1';
        }else{
            obj.matched = '0';
        }
        if(obj.unmatched1){
            obj.unmatched1 = '1';
        }else{
            obj.unmatched1 = '0';
        }
        obj.recordSelect = obj.unmatched+obj.matched+obj.unmatched1;*/
        obj.amountDollar = (obj.amountDollar) ? ''+obj.amountDollar+'' : '0';
        var finalObj = angular.copy($scope.copObj);
        // $scope.claimingObj = Object.assign(finalObj, obj); 
        $scope.claimingObj = $.extend(finalObj, obj);
        $scope.callFilterData(true);
        // $scope.limitOptions = [10,25,50];
        $scope.limitOptions = [50, 100];
        $scope.query = {};
        // $scope.query.limit = 10;
        $scope.query.limit = 50;
        $scope.query.page = 1;
      }); 

      function filterDate(obj){
        if(obj.deposit_date){
          if(obj.deposit_date != '0001-01-01T00:00:00'){
            obj.deposit_date = new Date(obj.deposit_date);
          }
        }
        if(obj.in_date){
          obj.in_date = new Date(obj.in_date);
        }
        if(obj.claimDate){
          obj.claimDate = new Date(obj.claimDate);
        }
        return obj;
      }

      $scope.checkValidDate = function(fromDate,toDate){
       if(toDate && fromDate > toDate){
        $scope.claimingObj1.toDate = fromDate;
      }
    }

    $scope.callFilterData = function(valid){
     $timeout(function(){
      if(valid){
       $scope.claimingObj.DataTableCommon.ExportType = 0;
     }
     $scope.accountListLoading = true;
     if(!$scope.claimingObj.DataTableCommon.ExportType){
       $scope.bookingList = [];
       $scope.$emit('callSelectedFieldByPage',$scope.claimingObj);
     }
     $http({
       url: SETTINGS[GLOBALS.ENV].apiUrl+"Payments/GetPaymentsFromFilter",
       method: 'POST',
       data: $scope.claimingObj,
       headers: {
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
      }
    }).success(function(response) {
     $scope.showPage = true;
                //return false;
                $scope.accountListLoading = false;
                if(response.ResposeCode == 2){
                	if(response.DataList && response.DataList.data){
                		$scope.bookingList = response.DataList.data.filter(filterDate);
                        // $rootScope.allowSaveReport = false;
                      }
                      if(response.DataList.fileName){
                    	/*window.open(
                    		SETTINGS[GLOBALS.ENV].apiUrl+response.DataList.fileName,
                    		'_blank'
                    		);*/
                        window.open(
                          response.DataList.fileName,
                          '_blank'
                          );
                      }
                      $scope.bookingListTotal = response.DataList.recordsTotal;
                    }else{
                      $scope.bookingList = [];
                    }
                  }).error(function(err,status) {
                   $scope.errorView(err,status);
                 });
                },300);
   }
    //$scope.callFilterData(true);



    $scope.getPDFLink = function(expot){
    	$scope.claimingObj.DataTableCommon.ExportType = Number(expot);
      /*if($scope.batchDataParam.dataTableCommon.param.Columns){
            delete $scope.batchDataParam.dataTableCommon.param.Columns;
          }*/
          $scope.callFilterData();
        }

        /* ==== Pagination Click function ==== */
        $scope.logOrder = function (order) {

        };

        $scope.logPagination = function (page, limit) {
         var startPage = (limit * (page - 1));
         $scope.claimingObj.DataTableCommon.param.Start = startPage;
         $scope.claimingObj.DataTableCommon.param.Length = limit;
         $scope.claimingObj.DataTableCommon.ExportType = 0;
         $scope.callFilterData();
       }


     }]);