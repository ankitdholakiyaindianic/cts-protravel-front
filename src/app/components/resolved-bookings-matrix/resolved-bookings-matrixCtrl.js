App.controller('resolvedbookingsmatrixCtrl', ['$scope','$state','$stateParams', '$rootScope', 'MetaInformation','$http', 'SETTINGS', 'GLOBALS', 'authService','toaster','$mdDialog','$filter','$timeout', function($scope,$state,$stateParams, $rootScope, MetaInformation,$http, SETTINGS, GLOBALS, authService,toaster,$mdDialog,$filter,$timeout) {

    /* ==== pagination options ==== */
    $scope.limitOptions = [10,25,50];
    $scope.query = {};
    $scope.query.limit = 10;
    $scope.query.page = 1;

    $scope.commissionByAcountObj = {};
    $scope.commissionByAcountObj.fromDate = null;
    $scope.commissionByAcountObj.toDate = null;
    $scope.commissionByAcountObj.selectedDate = 0;
    $scope.setselectedDate = 0;

    $scope.noData = "No data found";
    $scope.corpID = $scope.userDetails.corpID;

    $scope.getMonthName = ["Jan","Fab","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];

    $scope.getData = {
        recordsTotalView : 0,
        recordsTotalViewTop : 0
    }

    $scope.horizontalData = [];
    $scope.verticalData = [];
    $scope.setCounter = 0;
    $scope.exportDisbaled = true;

    var date = new Date();
    var toDate = new Date(date.getFullYear(), date.getMonth() + 1, 0);
    var currentDate = new Date();
    currentDate.setMonth((toDate.getMonth() - 11));

    $scope.commissionByAcountObj = {
        DataTableCommon:{
            param:{Draw:1,Start:0,Length:10},
            ExportType:0
        },
        corpid: $scope.corpID,
        fromDate:($scope.commissionByAcountObj.fromDate) ? $scope.commissionByAcountObj.fromDate : "",
        toDate:($scope.commissionByAcountObj.toDate) ? $scope.commissionByAcountObj.toDate : "",
        dateSelect: "0",
        unit:"",branch:"",agent:"",iata:"",
        account:"",propertyBrand:"",propertyChain:"",
        propertyName:"",propertyAddress:"",propertyCity:"",
        propertyState:"",propertyZip:"",propertyPhone:"",propertyPreferred:"",
        lastName:"",firstName:"",type:"1",status:"",claimed:"",amount:"",
        amountsign:"",amountdollar:"",checkno:"",batchNo:"",recordselect:"",
        bookingsource:"",accountgroup:"",pnr:"",rateCode:""
    };
    $scope.sendTheHTMLDataParams = {
        DataTableCommon:{
            param:{Draw:1,Start:0,Length:10},
            ExportType:0
        }
    };

    if((toDate.getMonth() + 1) == 12){
        $scope.commissionByAcountObj.fromDate = 1 + "/" + (currentDate.getMonth()+1) + "/" + (toDate.getFullYear());
    }else{
        $scope.commissionByAcountObj.fromDate = 1 + "/" + (currentDate.getMonth()+1) + "/" + (toDate.getFullYear()-1);    
    }
    $scope.commissionByAcountObj.toDate = (toDate.getDate()) + "/" + (toDate.getMonth() + 1) + "/" + toDate.getFullYear();

    
    /* ===== CallBack Global Left panel View ===== */
    $scope.copyObj = angular.copy($scope.commissionByAcountObj);
    $scope.$emit('callSelectedFieldByPage',$scope.commissionByAcountObj);

    $scope.$on('callSelectedField', function(evnt,obj) { 
        var finalObj = angular.copy($scope.copyObj);
        // $scope.commissionByAcountObj = Object.assign(finalObj, obj); 
        $scope.commissionByAcountObj = $.extend(finalObj, obj);
        $scope.callFiterData(true);
    });

    function returnObj(val) {
        return function(element) {
            if(element.wk_arrivalYear === val.year){
                return element;    
            }
        }
    }

    function returnObjTop(val) {
        return function(element) {
            if(element.wk_resolvedYear === val.year){
                return element;    
            }
        }
    }

    $scope.callBackCounter = function(val){
        val = $scope.setCounter;
        $scope.setCounter++;
        return val;
    }

    /* ===== Left side Year month View ====== */
    $scope.filterTeams = function(player) {
        var teamIsNew = indexedTeams.indexOf(player.wk_arrivalYear) == -1;
        if (teamIsNew) {
            indexedTeams.push(player.wk_arrivalYear);
        }
        return teamIsNew;
    }

    filterMonth = function(player) {
        var teamIsNew = indexedTeamsB.indexOf(player.wk_arrivalMonth) == -1;
        if (teamIsNew) {
            indexedTeamsB.push(player.wk_arrivalMonth);
        }
        return teamIsNew;
    }

    $scope.getMonthNameData = function(query) {
        var objData = {"year":query.wk_arrivalYear,"month":query.wk_arrivalMonth};
        var filterData = $scope.bookingList.filter(returnObj(objData));
        indexedTeamsB = [];
        var filterData1 = filterData.filter(filterMonth);
        return filterData1;
    }

    /* ===== Top side Year month View ===== */
    $scope.filterTeamsTop = function(player) {
        var teamIsNew = indexedTeamsTop.indexOf(player.wk_resolvedYear) == -1;
        if (teamIsNew) {
            indexedTeamsTop.push(player.wk_resolvedYear);
        }
        return teamIsNew;
    }

    filterMonthTop = function(player) {
        var teamIsNew = indexedTeamsBTop.indexOf(player.wk_resolvedMonth) == -1;
        if (teamIsNew) {
            indexedTeamsBTop.push(player.wk_resolvedMonth);
        }
        return teamIsNew;
    }

    $scope.getMonthNameDataTop = function(query) {
        var objData = {"year":query.wk_resolvedYear,"month":query.wk_resolvedMonth};
        var filterData = $scope.bookingList.filter(returnObjTop(objData));
        indexedTeamsBTop = [];
        var filterData1 = filterData.filter(filterMonthTop);
        return filterData1;
    }

    function createFilterFor(query) {
        var teamIsNew = $scope.indexedTeamsA.indexOf(query.account) == -1;
        if (teamIsNew) {
            $scope.indexedTeamsA.push(query.account);
        }
        return teamIsNew;
    }

    function createFilterForChild(query) {
        var teamIsNew = $scope.indexedTeamsB.indexOf(query.cts_type) == -1;
        if (teamIsNew) {
            $scope.indexedTeamsB.push(query.cts_type);
        }
        return teamIsNew;
    }

    function filterDate(obj){
        if(obj.date){
            obj.date = new Date(obj.date);
        }
        if(obj.in_date){
            obj.in_date = new Date(obj.in_date);
        }
        return obj;
    }

    $scope.checkValidDate = function(fromDate,toDate){
        if(toDate && fromDate > toDate){
            $scope.commissionByAcountObj.toDate = fromDate;
        }
    }

    function returnObjData(val) {
        return function(element) {
            if(element.wk_resolvedYear === val.top.year && element.wk_arrivalYear === val.left.year && element.wk_resolvedMonth === val.top.month && element.wk_arrivalMonth === val.left.month){
                return element;    
            }
        }
    }

    $scope.callBackBindYearMonthView = function(id,leftData,ind){
        var loopLimit = $scope.verticalData.length;
        var verData = $scope.verticalData;
        if(1){
            var html = '';
            var totalCnt = 0;
            var wk2_bookingCount = 0;
            for(var j=0;j<loopLimit;j++){
                var crrObj = {};
                crrObj.left = leftData;
                crrObj.top = verData[j];
                var getCount = '';
                var getObj = $scope.bookingList.filter(returnObjData(crrObj));
                if(getObj.length){
                    getCount = getObj[0].wk_resolvedCount;
                    wk2_bookingCount = getObj[0].wk2_bookingCount;
                    totalCnt = totalCnt + getCount;
                }
                html +='<th class="md-cell removeData md-column text-center text-right bg-color-rm">'+((getCount) ? $filter('number')(getCount, 0) : getCount)+'</th>';   
            } 
            html +='<th class="md-cell removeData md-column text-center text-right bg-color-rm">'+($filter('number')(totalCnt, 0))+'</th>';   
            $('#getSetYearMonth'+ind).find('.removeData').remove();
            $timeout(function(){           
                var currentInd = ind;
                var FinalHTML = '<th class="md-cell removeData md-column text-right text-center bg-color-gray">'+($filter('number')(wk2_bookingCount, 0))+'</th>'+html;
                $('#getSetYearMonth'+currentInd).append(FinalHTML);   
            },true);
        }        
    }

    $scope.callFiterData = function(valid){
        if(valid){
            $scope.commissionByAcountObj.DataTableCommon.ExportType = 0;
        }
        if(!$scope.commissionByAcountObj.DataTableCommon.ExportType){
            $scope.accountListLoading = true;
            $scope.bookingList = [];
            $scope.parentResults = [];
            $scope.subChildResults = [];
            $scope.indexedTeamsA = [];
            $scope.indexedTeamsB = [];
            $scope.getFullYearArrTop = [];
            $scope.getFullYearArr = [];
            $scope.horizontalData = [];
            $scope.verticalData = [];
            $scope.commissionByAcountObj.fromDate = $filter('date')($scope.commissionByAcountObj.fromDate, "dd/MM/yyyy");
            $scope.commissionByAcountObj.toDate = $filter('date')($scope.commissionByAcountObj.toDate, "dd/MM/yyyy");
            $scope.setselectedDate = $scope.commissionByAcountObj.selectedDate;
            $scope.$emit('callSelectedFieldByPage',$scope.commissionByAcountObj);

        }
        $http({
            url: SETTINGS[GLOBALS.ENV].apiUrl+"GetBookingsResolution/GetBookingsResolution",
            method: 'POST',
            data: $scope.commissionByAcountObj,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
            }
        }).success(function(response) {
            $scope.accountListLoading = false;
            if(response.ResposeCode == 2){
                if(response.DataList.data){
                    $scope.exportDisbaled = false;
                    $scope.bookingList = response.DataList.data;
                    /* ===== reset Value ===== */
                    $scope.getData = {
                        recordsTotalView : 0,
                        recordsTotalViewTop : 0
                    }
                    $scope.setCounter = 0;
                    indexedTeams = [];
                    indexedTeamsTop = [];
                    $scope.getFullYearArr = $scope.bookingList.filter($scope.filterTeams);
                    $scope.TopListView = $filter('orderBy')($scope.bookingList, 'wk_resolvedYear');
                    $scope.getFullYearArrTop = $scope.TopListView.filter($scope.filterTeamsTop);
                }
                if(response.DataList.fileName){
                    /*window.open(
                        SETTINGS[GLOBALS.ENV].apiUrl+response.DataList.fileName,
                        '_blank'
                        );*/
                        window.open(
                            response.DataList.fileName,
                            '_blank'
                            );
                    }
                    $scope.bookingListTotal = response.DataList.recordsTotal;
                }else{
                    $scope.bookingList = [];
                }
            }).error(function(err,status) {
                $scope.errorView(err,status);
            });
        }


        $scope.sendTheHTMLData = function(){
         $http({
            url: SETTINGS[GLOBALS.ENV].apiUrl+"ExportData/ExportFiles",
            method: 'POST',
            data: $scope.sendTheHTMLDataParams,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
            }
        }).success(function(response) {
            if(response.DataList.fileName){
                window.open(
                    SETTINGS[GLOBALS.ENV].apiUrl+response.DataList.fileName,
                    '_blank'
                    );
            }
        }).error(function(err,status) {
            $scope.errorView(err,status);
        });
    }


    /* ==== call function page onload ==== */
    $scope.callFiterData(true);

    /* ==== Get the HTML content ==== */
    $scope.getPDFLinkData = function(exportType){
        var HTMLContent = document.getElementById("reslovedBookingTable");
        $scope.sendTheHTMLDataParams.title = "Resolved Bookings Matrix";
        $(HTMLContent).find('.dataclasstemp').remove();
        if(HTMLContent){
            $scope.sendTheHTMLDataParams.data = HTMLContent.outerHTML;    
        }
        $scope.sendTheHTMLDataParams.DataTableCommon.ExportType = exportType;
        $scope.sendTheHTMLData();
    }

    /* ==== Pagination Click function ==== */

    $scope.getPDFLink = function(expot){
        $scope.commissionByAcountObj.DataTableCommon.ExportType = Number(expot);
        $scope.callFiterData();
    }

    $scope.logOrder = function (order) {
        
    };

    $scope.logPagination = function (page, limit) {
        var startPage = (limit * (page - 1));
        $scope.commissionByAcountObj.DataTableCommon.param.Start = startPage;
        $scope.commissionByAcountObj.DataTableCommon.param.Length = limit;
        $scope.commissionByAcountObj.DataTableCommon.ExportType = 0;
        $scope.callFiterData();
    }

}]);