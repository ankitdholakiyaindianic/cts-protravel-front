App.controller('unmatchedPaymentsCtrl', ['$scope','$state','$stateParams', '$rootScope', 'MetaInformation','$http', 'SETTINGS', 'GLOBALS', 'authService','toaster','$mdDialog','$filter','$timeout', function($scope,$state,$stateParams, $rootScope, MetaInformation,$http, SETTINGS, GLOBALS, authService,toaster,$mdDialog,$filter,$timeout) {

    $scope.unmatchedPaymentRpt = {};
    $scope.unmatchedPaymentRpt.fromDate = null;
    $scope.unmatchedPaymentRpt.toDate = null;
    $scope.unmatchedPaymentRpt.selectedDate = 1;

    $scope.noData = "No data found";
    $scope.corpID = $scope.userDetails.corpID;
    $scope.unmatchedPaymentLoading = false;

    $scope.limitOptions = [10,25,50];
    $scope.query = {};
    $scope.query.limit = 10;
    $scope.query.page = 1;

    var finalTotal = 0;
    $scope.totalCommission = function(obj){
        finalTotal += Number(obj.net_act_comm);
        return finalTotal;
    }
    function filterDate(obj){
        if(obj.in_date){
            obj.in_date = new Date(obj.in_date);
        }
        if(obj.out_date){
            obj.out_date = new Date(obj.out_date);
        }
        if(obj.check_date){
            obj.check_date = new Date(obj.check_date);
        }
        if(obj.deposit_date){
            obj.deposit_date = new Date(obj.deposit_date);
        }
        if(obj.date){
            obj.date = new Date(obj.date);
        }
        return obj;
    }

    $scope.bookingToFilter = function() {
        indexedTeams = [];
        return $scope.unmatchedPayment;
    }


    $scope.filterTeams = function(player) {
        var teamIsNew = indexedTeams.indexOf(player.cts_type) == -1;
        if (teamIsNew) {
            indexedTeams.push(player.cts_type);
        }
        return teamIsNew;
    }

    $scope.bookingReportParms = {
        DataTableCommon:{
            param:{
                Draw:1,
                Start:0,
                Length:10
            },
            ExportType:0
        },
        corpID: $scope.corpID,
        fromDate: ($scope.unmatchedPaymentRpt.fromDate) ? $scope.unmatchedPaymentRpt.fromDate : "",
        toDate: ($scope.unmatchedPaymentRpt.toDate) ? $scope.unmatchedPaymentRpt.toDate : "",
        unit: "",
        branch: "",
        agent: "",
        iata: "",
        account: "",
        propertybrand: "",
        propertychain: "",
        propertyname: "",
        propertyaddress: "",
        propertycity: "",
        propertystate: "",
        propertyzip: "",
        propertyphone: "",
        propertypreferred: "false",
        lastName: "",
        firstName: "",
        type: "",
        checkno: "",
        accountgroup: ""
    };


    /* ===== CallBack Global Left panel View ===== */
    $scope.copyObj = angular.copy($scope.bookingReportParms);
    $scope.$emit('callSelectedFieldByPage',$scope.bookingReportParms);

    $scope.$on('callSelectedField', function(evnt,obj) { 
        var finalObj = angular.copy($scope.copyObj);
        // $scope.bookingReportParms = Object.assign(finalObj,obj); 
        $scope.bookingReportParms = $.extend(finalObj, obj);
        $scope.callFiterData(true);
    });

    $scope.checkValidDate = function(fromDate,toDate){
        if(toDate && fromDate > toDate){
            $scope.unmatchedPaymentRpt.toDate = fromDate;
        }
    }

    $scope.callFiterData = function(valid){
        if($scope.umnatchedPaymemtsFrm.$valid){
            $timeout(function(){
                if(valid){
                    $scope.bookingReportParms.DataTableCommon.ExportType = 0;
                }
                if(!$scope.bookingReportParms.DataTableCommon.ExportType){
                    $scope.unmatchedPayment = [];
                    $scope.bookingReportParms.fromDate = $filter('date')($scope.unmatchedPaymentRpt.fromDate, "dd/MM/yyyy");
                    $scope.bookingReportParms.toDate = $filter('date')($scope.unmatchedPaymentRpt.toDate, "dd/MM/yyyy");
                    $scope.bookingReportParms.dateSelect = $scope.unmatchedPaymentRpt.selectedDate;
                    $scope.unmatchedPaymentLoading = true;
                    $scope.$emit('callSelectedFieldByPage',$scope.bookingReportParms);
                    if($scope.bookingReportParms.pageStart){
                        $scope.query.page = 1;
                    }
                }
                $http({
                    url: SETTINGS[GLOBALS.ENV].apiUrl+"Payment/GetUnmatchedPayments",
                    method: 'POST',
                    data: $scope.bookingReportParms,
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
                    }
                }).success(function(response) {
                    $scope.unmatchedPaymentLoading = false;
                    if(response.ResposeCode == 2){
                        if(response.DataList.data){
                            $scope.unmatchedPayment = response.DataList.data.filter(filterDate);
                        }
                        if(response.DataList.fileName){
                            /*window.open(
                                SETTINGS[GLOBALS.ENV].apiUrl+response.DataList.fileName,
                                '_blank'
                                );*/
                                window.open(
                                    response.DataList.fileName,
                                    '_blank'
                                    );
                            }
                            $scope.unmatchedPaymentTotal = response.DataList.recordsTotal;
                        }else{
                            $scope.unmatchedPayment = [];
                        }
                        if($scope.bookingReportParms.DataTableCommon.param.Columns){
                            delete $scope.bookingReportParms.DataTableCommon.param.Columns;
                        }
                        if($scope.bookingReportParms.pageStart){
                            delete $scope.bookingReportParms.pageStart;
                        }
                    }).error(function(err,status) {
                        $scope.errorView(err,status);
                        if($scope.bookingReportParms.DataTableCommon.param.Columns){
                            delete $scope.bookingReportParms.DataTableCommon.param.Columns;
                        }
                    });
                },300);
        }        
    }

    $scope.totalAvailableCommsion = function(obj){
        var total = {};
        total.net_act_comm = 0;
        for(var i = 0; i<obj.length;i++){
            if(obj[i].net_act_comm){
                total.net_act_comm =  Number(total.net_act_comm) + Number(obj[i].net_act_comm); 
            }
        }
        return total;
    }

    $scope.getPDFLink = function(expot){
        $scope.bookingReportParms.DataTableCommon.ExportType = Number(expot);
        if($scope.bookingReportParms.DataTableCommon.param.Columns){
            delete $scope.bookingReportParms.DataTableCommon.param.Columns;
        }
        $scope.callFiterData();
    }

    /* ==== Pagination Click function ==== */
    $scope.logOrder = function (order) {
      $scope.bookingReportParms.DataTableCommon.param.Columns = [{}];
      if(order.charAt(0) == '-'){
        $scope.bookingReportParms.DataTableCommon.param.Columns[0].Name = order.slice(1);    
        $scope.bookingReportParms.DataTableCommon.param.Columns[0].Dir = "desc";    
    }else{
        $scope.bookingReportParms.DataTableCommon.param.Columns[0].Name = order;    
        $scope.bookingReportParms.DataTableCommon.param.Columns[0].Dir = "asc";    
    }
    $scope.bookingReportParms.DataTableCommon.ExportType = 0;
    $scope.callFiterData();

};

$scope.logPagination = function (page, limit) {
    var startPage = (limit * (page - 1));
    $scope.bookingReportParms.DataTableCommon.param.Start = startPage;
    $scope.bookingReportParms.DataTableCommon.param.Length = limit;
    $scope.bookingReportParms.DataTableCommon.ExportType = 0;
    $scope.callFiterData();
}

}]);