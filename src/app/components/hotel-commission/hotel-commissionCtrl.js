App.controller('hotel-commissionCtrl', ['$scope','$state','$stateParams', '$rootScope', 'MetaInformation','$http', 'SETTINGS', 'GLOBALS', 'authService','toaster','$mdDialog','$filter','$timeout', function($scope,$state,$stateParams, $rootScope, MetaInformation,$http, SETTINGS, GLOBALS, authService,toaster,$mdDialog,$filter,$timeout) {

    $scope.commissionByAcountObj = {};
    $scope.commissionByAcountObj.fromDate = null;
    $scope.commissionByAcountObj.toDate = null;
    $scope.commissionByAcountObj.selectedDate = 1;
    $scope.setselectedDate = 0;

    /* ==== pagination options ==== */
    $scope.limitOptions = [10,25,50];
    $scope.query = {};
    $scope.query.limit = 10;
    $scope.query.page = 1;

    $scope.noData = "No data found";
    $scope.corpID = $scope.userDetails.corpID;
    
    $scope.bookingReportParms = {
        DataTableCommon:{
            param:{
                Draw:1,
                Start:0,
                Length:10
            },
            ExportType:0
        },
        corpID:$scope.corpID,
        fromDate:($scope.commissionByAcountObj.fromDate) ? $scope.commissionByAcountObj.fromDate : "",
        toDate:($scope.commissionByAcountObj.toDate) ? $scope.commissionByAcountObj.toDate : "",
        group:"",
        description:"",
        accountCode:"",
        active:"",
        type:"1"
    };

    /* ===== CallBack Global Left panel View ===== */
    $scope.copyObj = angular.copy($scope.bookingReportParms);
    $scope.$emit('callSelectedFieldByPage',$scope.bookingReportParms);

    $scope.$on('callSelectedField', function(evnt,obj) { 
        var finalObj = angular.copy($scope.copyObj);
        // $scope.bookingReportParms = Object.assign(finalObj,obj); 
        $scope.bookingReportParms = $.extend(finalObj, obj);
        $scope.callFiterData(true);
    });


    function createFilterFor(query) {
        var teamIsNew = $scope.indexedTeamsA.indexOf(query.branch) == -1;
        if (teamIsNew) {
            $scope.indexedTeamsA.push(query.branch);
        }
        return teamIsNew;
    }

    function createFilterForChild(query) {
        var teamIsNew = $scope.indexedTeamsB.indexOf(query.cts_type) == -1;
        if (teamIsNew) {
            $scope.indexedTeamsB.push(query.cts_type);
        }
        return teamIsNew;
    }

    function filterDate(obj){
        if(obj.date){
            obj.date = new Date(obj.date);
        }
        if(obj.in_date){
            obj.in_date = new Date(obj.in_date);
        }
        return obj;
    }

    $scope.checkValidDate = function(fromDate,toDate){
        if(toDate && fromDate > toDate){
            $scope.commissionByAcountObj.toDate = fromDate;
        }
    }

    $scope.callFiterData = function(valid){
        if($scope.commisHotel.$valid){
            $timeout(function(){
                if(valid){
                    $scope.bookingReportParms.DataTableCommon.ExportType = 0;
                }
                if(!$scope.bookingReportParms.DataTableCommon.ExportType){
                    $scope.accountListLoading = true;
                    $scope.bookingList = [];
                    $scope.bookingReportParms.fromDate = $filter('date')($scope.commissionByAcountObj.fromDate, "dd/MM/yyyy");
                    $scope.bookingReportParms.toDate = $filter('date')($scope.commissionByAcountObj.toDate, "dd/MM/yyyy");
                    $scope.bookingReportParms.dateSelect = $scope.commissionByAcountObj.selectedDate;
                    $scope.setselectedDate = $scope.commissionByAcountObj.selectedDate;
                    // dateSelect:$scope.commissionByAcountObj.selectedDate,
                    $scope.$emit('callSelectedFieldByPage',$scope.bookingReportParms);
                    
                    if($scope.bookingReportParms.pageStart){
                        $scope.query.page = 1;
                    }
                }
                $http({
                    url: SETTINGS[GLOBALS.ENV].apiUrl+"Commission/GetHotelCommissions",
                    method: 'POST',
                    data: $scope.bookingReportParms,
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
                    }
                }).success(function(response) {
                    $scope.accountListLoading = false;
                    if(response.ResposeCode == 2){
                        if(response.DataList.data){
                            $scope.bookingList = response.DataList.data;
                        }
                        if(response.DataList.fileName){
                            /*window.open(
                                SETTINGS[GLOBALS.ENV].apiUrl+response.DataList.fileName,
                                '_blank'
                                );*/
                                window.open(
                                    response.DataList.fileName,
                                    '_blank'
                                    );
                            }
                            $scope.bookingListTotal = response.DataList.recordsTotal;
                        }else{
                            $scope.bookingList = [];
                        }
                        if($scope.bookingReportParms.pageStart){
                            delete $scope.bookingReportParms.pageStart;
                        }
                    }).error(function(err,status) {
                        $scope.errorView(err,status);
                    });
                },300);
        }    
    }

    $scope.getPDFLink = function(expot){
        $scope.bookingReportParms.DataTableCommon.ExportType = Number(expot);
        $scope.callFiterData();
    }

    /* ==== Pagination Click function ==== */
    $scope.logOrder = function (order) {
        
    };

    $scope.logPagination = function (page, limit) {
        var startPage = (limit * (page - 1));
        $scope.bookingReportParms.DataTableCommon.param.Start = startPage;
        $scope.bookingReportParms.DataTableCommon.param.Length = limit;
        $scope.bookingReportParms.DataTableCommon.ExportType = 0;
        $scope.callFiterData();
    }

}]);