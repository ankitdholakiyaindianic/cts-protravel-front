App.controller('importReassignmentReport', ['$scope', '$state', '$stateParams', '$rootScope', 'MetaInformation', '$http', 'SETTINGS', 'GLOBALS', 'authService', 'toaster', '$mdDialog', '$filter', '$timeout', function ($scope, $state, $stateParams, $rootScope, MetaInformation, $http, SETTINGS, GLOBALS, authService, toaster, $mdDialog, $filter, $timeout) {

    // $scope.limitOptions = [10, 25, 50];
    $scope.limitOptions = [50, 100];
    $scope.query = {};
    // $scope.query.limit = 10;
    $scope.query.limit = 50;
    $scope.query.page = 1;

    $scope.seo = {
        "seo_title": "Home page",
        "seo_description": "Home page description",
        "seo_keywords": "Home page Keywords"
    };
    setMetaData($scope, $rootScope, MetaInformation, $scope.seo);
    $scope.variable = 'Variable Name';

    $scope.reportDetails = {
        DataTableCommon: {
            param: {
                Draw: 1,
                Start: 0,
                // Length: 10
                Length: 50
            },
            ExportType: 0
        }
    };

    $scope.reportLoading = false;
    $scope.noData = "No data found";

    function createFilterFor(query) {
        var teamIsNew = $scope.indexedTeamsA.indexOf(query.changeDate) == -1;
        if (teamIsNew) {
            $scope.indexedTeamsA.push(query.changeDate);
        }
        return teamIsNew;
    }
    $scope.createFilterForChild = function (query) {
        var teamIsNew = $scope.indexedTeamsB.indexOf(query.businessUnit) == -1;
        if (teamIsNew) {
            $scope.indexedTeamsB.push(query.businessUnit);
        }
        return teamIsNew;
    }
    $scope.createFilterForChildForRuledesc = function (query) {
        var teamIsNew = $scope.indexedTeamsC.indexOf(query.ruleDescription) == -1;
        if (teamIsNew) {
            $scope.indexedTeamsC.push(query.ruleDescription);
        }
        return teamIsNew;
    }

    function filterDate(obj) {
        if (obj.changeDate) {
            obj.changeDate = new Date(obj.changeDate);
        }
        return obj;
    }

    $scope.checkValidDate = function (fromDate, toDate) {
        if (toDate && fromDate > toDate) {
            $scope.commissionByAcountObj.toDate = fromDate;
        }
    }

    function returnObj(val) {
        return function (element) {
            if (element.changeDate === val.a && element.businessUnit === val.b && element.ruleDescription === val.c) {
                return element;
            }
        }
    }

    function returnObjFirst(val) {
        return function (element) {
            if (element.changeDate === val.a && element.businessUnit === val.b) {
                return element;
            }
        }
    }


    $scope.GroupFilterData = function (obj) {
        $scope.indexedTeamsB = [];
        return obj;
    }
    $scope.GroupFilterData1 = function (obj) {
        $scope.indexedTeamsC = [];
        return obj;
    }


    $scope.bookingListData = function (a, b, c) {
        var objData = { 'a': a, 'b': b, 'c': c };
        var finalData = $scope.reassignmentList.filter(returnObj(objData));
        if (finalData.length) {
            return finalData[0].list;
        }
    }
    $scope.bookingListDataFirst = function (a, b, c) {
        var objData = { 'a': a, 'b': b };
        var finalData = $scope.reassignmentList.filter(returnObjFirst(objData));
        if (finalData.length) {
            return finalData[0].list;
        }
    }

    $scope.callFiterData = function (valid) {
        $scope.reportDetails.corpID = $scope.userDetails.corpID;
        if ($scope.importReassReport.$valid) {
            $timeout(function () {
                if (valid) {
                    $scope.reportDetails.DataTableCommon.ExportType = 0;
                    // $scope.limitOptions = [10,25,50];
                    $scope.limitOptions = [50, 100];
                    $scope.query = {};
                    // $scope.query.limit = 10;
                    $scope.query.limit = 50;
                    $scope.query.page = 1;
                    $scope.reportDetails.DataTableCommon.param.Start = 0;
                    // $scope.reportDetails.DataTableCommon.param.Length = 10;
                    $scope.reportDetails.DataTableCommon.param.Length = 50;
                }
                if (!$scope.reportDetails.DataTableCommon.ExportType) {

                    //if(reportDetails.fromDate != null && reportDetails.toDate != null){
                        $scope.reportDetails.fromDate = $filter('date')($scope.report.fromDate, "dd/MM/yyyy");
                        $scope.reportDetails.toDate = $filter('date')($scope.report.toDate, "dd/MM/yyyy");
                        $scope.reportLoading = true;
                        $scope.subChildResults = [];
                        $scope.indexedTeamsA = [];
                        $scope.indexedTeamsB = [];
                        $scope.indexedTeamsC = [];
                        $scope.reassignmentList = [];
                    }
                    $http({
                        url: SETTINGS[GLOBALS.ENV].apiUrl + "Payment_Updates/ImportReassignmentList",
                        method: 'POST',
                        data: $scope.reportDetails,
                        headers: {
                            'Content-Type': 'application/json',
                            'Authorization': 'bearer ' + authService.getAuthToken('AuthToken')
                        }
                    }).success(function (response) {
                        $scope.reportLoading = false;
                        if (response.ResposeCode == 2) {
                            if (response.DataList.data) {
                                $scope.reassignmentList = response.DataList.data.filter(filterDate);
                            /*$scope.parentResults = $scope.reassignmentList.filter(createFilterFor);
                            $scope.subChildResults = $scope.reassignmentList.filter(createFilterForChild);
                            $scope.subChildResultsForRuledesc = $scope.reassignmentList.filter(createFilterForChildForRuledesc);*/
                            $scope.bookingListTotal = response.DataList.recordsTotal;

                            /*===== For Fixed header =====*/
                            $timeout(function () {

                                if ($("md-table-container .md-table:first").length > 0) {
                                    $("md-table-container .md-table:first").floatThead('destroy');
                                    $("md-table-container .md-table:first").floatThead({ scrollingTop: 60 });
                                }

                            }, 2);

                        }
                        if (response.DataList.fileName) {
                            /*window.open(
                                SETTINGS[GLOBALS.ENV].apiUrl + response.DataList.fileName,
                                '_blank'
                                );*/
                                window.open(
                                    response.DataList.fileName,
                                    '_blank'
                                    );
                            }
                        // $scope.bookingListTotal = response.DataList.recordsTotal;
                    }else{
                        $scope.reassignmentList = [];
                    }
                }).error(function (err, status) {
                    /*===== For Fixed header =====*/
                    var $table = $('.md-table:first');
                    $table.floatThead({
                        responsiveContainer: function ($table) {
                            return $table.closest('.full-height');
                        }

                    });
                    $scope.errorView(err, status);
                });
            }, 300);
}
}

$scope.getPDFLink = function (expot) {
    $scope.reportDetails.DataTableCommon.ExportType = Number(expot);
    $scope.callFiterData();
}

/* ==== Pagination Click function ==== */
$scope.logOrder = function (order) {
    $scope.bookingReportParms.DataTableCommon.param.Columns = [{}];
    if (order.charAt(0) == '-') {
        $scope.bookingReportParms.DataTableCommon.param.Columns[0].Name = order.slice(1);
        $scope.bookingReportParms.DataTableCommon.param.Columns[0].Dir = "desc";
    } else {
        $scope.bookingReportParms.DataTableCommon.param.Columns[0].Name = order;
        $scope.bookingReportParms.DataTableCommon.param.Columns[0].Dir = "asc";
    }
        // $scope.reportDetails.DataTableCommon.ExportType = 0;
        $scope.callFiterData();
    };

    $scope.logPagination = function (page, limit) {
        var startPage = (limit * (page - 1));
        $scope.reportDetails.DataTableCommon.param.Start = startPage;
        $scope.reportDetails.DataTableCommon.param.Length = limit;
        $scope.reportDetails.DataTableCommon.ExportType = 0;
        $scope.callFiterData();
    }

}]);