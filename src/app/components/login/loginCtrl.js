App.controller('loginCtrl', ['$scope','authService','$cookies','$cookieStore','$state','$stateParams', '$http', 'GLOBALS','SETTINGS', 'toaster','$window', '$rootScope', function($scope, authService,$cookies, $cookieStore, $state, $stateParams, $http, GLOBALS,SETTINGS,toaster, $window, $rootScope){
	
	var t;
	$scope.user = {
		UserName: "",
		Password: ""
	}, t = angular.copy($scope.user), $scope.revert = function() {
		$scope.user = angular.copy(t), $scope.login_form.$setPristine(), $scope.login_form.$setUntouched()
	}, $scope.canRevert = function() {
		return !angular.equals($scope.user, t) || !$scope.login_form.$pristine
	}, $scope.canSubmit = function() {
		return $scope.login_form.$valid && !angular.equals($scope.user, t)
	}, $scope.submitForm = function() {
		$scope.showInfoOnSubmit = !0, e.revert()
	}

	$scope.loading = false;
	
	$scope.loginDetailsParams = {};
	$scope.signin = function(loginDetails){
		$scope.loading = true;
		delete $scope.loginErrorMessage;

		/*$scope.loginDetailsParams.UserName = CryptoJS.AES.encrypt(loginDetails.UserName, GLOBALS.securityToken).toString();
		$scope.loginDetailsParams.Password = CryptoJS.AES.encrypt(loginDetails.Password, GLOBALS.securityToken).toString();*/

		var key = CryptoJS.enc.Utf8.parse('8080808080808080');  
		var iv = CryptoJS.enc.Utf8.parse('8080808080808080');  
		
		$scope.loginDetailsParams.UserName = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(loginDetails.UserName), key,  
		{
			keySize: 128 / 8,   
			iv: iv,  
			mode: CryptoJS.mode.CBC,  
			padding: CryptoJS.pad.Pkcs7 
		}).toString()
		$scope.loginDetailsParams.Password = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(loginDetails.Password), key,  
		{
			keySize: 128 / 8,   
			iv: iv,  
			mode: CryptoJS.mode.CBC,  
			padding: CryptoJS.pad.Pkcs7 
		}).toString();  

		$http({
			url: SETTINGS[GLOBALS.ENV].apiUrl+"login",
			method: 'POST',
			data: $.param($scope.loginDetailsParams),
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded',
				'Authorization': authService.getAuthToken('AuthToken')
			}
		}).success(function(response) { 
			$scope.loading = false;
			if(response.ResposeCode == 2){
				// authService.setAuthToken(response.DataModel.access_token);
				//$cookieStore.put('userInfo', response.DataModel.LoginResponse);
				// localStorage.setItem('userInfo', response.DataModel.LoginResponse);
				$window.sessionStorage.setItem('AuthToken', response.DataModel.access_token);
				$window.sessionStorage.setItem('userInfo', response.DataModel.LoginResponse);
				// $window.sessionStorage.setItem('Via', response.DataModel.Via);
				
				// $window.sessionStorage.setItem(id, JSON.stringify(json));sessionStorage.setItem('userInfo', response.DataModel.LoginResponse);
				$state.go("main.home");
				$scope.userInfo();

			}else{
				$scope.loginErrorMessage = response.ResponseMessage;
				// toaster.pop('error', '', $scope.loginErrorMessage);
				$state.go("login");
			}
		});
		// authService.setAuthToken();
		// $state.go("main.resolvedbookingsmatrix");
	};	
}]);

