var app = angular.module('authenticationServices', []);
app.service('authService', ['$cookies', '$cookieStore', '$http', '$state','$window', function($cookies, $cookieStore, $http, $state,$window) {
    var authService = {};
    authService.isAuthenticated = function() {
        //console.log(authService.authToken)
        // var authToken = $cookieStore.get('AuthToken');
        var authToken = $window.sessionStorage.getItem('AuthToken');
        authService.authToken = authToken ? authToken : '';
        return authService.authToken;
    };
    authService.setAuthToken = function(token) {
        //$cookieStore.put('AuthToken', token);
        $window.sessionStorage.setItem('AuthToken',token);
        // $cookieStore.put('AuthToken', "ABCDEFGHIJKLMNOPQRSTUVWXYZ");
    };
    authService.clearAuthentication = function() {
        //$cookieStore.remove('AuthToken');
        $window.sessionStorage.removeItem('AuthToken');
    };
    authService.clearAuthenticationBack = function(err) {
        if (err == 401) {
            //$cookieStore.remove('AuthToken');
            $window.sessionStorage.removeItem('AuthToken');
            //$state.go('login');
            //window.location = settings.url + '/login';
        }
    };
    authService.getAuthToken = function(tokenname) {
        return $window.sessionStorage.getItem('AuthToken');
        //return $cookieStore.get(tokenname);
    };

    return authService;
}]);