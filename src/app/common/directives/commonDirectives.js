var app = angular.module('commonDirectives', []);
app.directive('pageTitle', ['$rootScope', '$timeout', function($rootScope, $timeout){

//coommon directive
return {
  link: function(scope, element) {
    var listener = function(event, toState, toParams, fromState, fromParams) {
        // Default title - load on Dashboard 1
        var title = 'CTS-TZELL';
        // Create your own title pattern
        if (toState.data.pageTitle) {title = toState.data.pageTitle;}
        $timeout(function() {
          element.text(title);
        });
      };
      $rootScope.$on('$stateChangeStart', listener);
    }
  };
}]);
app.directive('ngSpinnerBar', ['$rootScope', '$state',
  function($rootScope, $state) {
    return {
      link: function(scope, element, attrs) {
                // by defult hide the spinner bar
                //element.addClass('hide'); // hide spinner bar by default

                // display the spinner bar whenever the route changes(the content part started loading)
                $rootScope.$on('$stateChangeStart', function() {
                    //element.removeClass('hide'); // show spinner bar
                  });

                // hide the spinner bar on rounte change success(after the content loaded)
                $rootScope.$on('$stateChangeSuccess', function() {
                    //element.addClass('hide'); // hide spinner bar
                    $('body').removeClass('page-on-load'); // remove page loading indicator
                    Layout.setAngularJsSidebarMenuActiveLink('match', null, $state); // activate selected link in the sidebar menu

                    // auto scorll to page top
                    setTimeout(function () {
                        themeApp.scrollTop(); // scroll to the top on content load
                        //$rootScope.settings.layout.pageAutoScrollOnLoad
                      }, 100);     
                  });

                // handle errors
                $rootScope.$on('$stateNotFound', function() {
                    element.addClass('hide'); // hide spinner bar
                  });

                // handle errors
                $rootScope.$on('$stateChangeError', function() {
                    element.addClass('hide'); // hide spinner bar
                  });
              }
            };
          }
          ])

// Handle global LINK click
app.directive('a', function() {
  return {
    restrict: 'E',
    link: function(scope, elem, attrs) {
      if (attrs.ngClick || attrs.href === '' || attrs.href === '#') {
        elem.on('click', function(e) {
                    e.preventDefault(); // prevent link click for above criteria
                  });
      }
    }
  };
});

// Handle Dropdown Hover Plugin Integration
app.directive('dropdownMenuHover', function () {
  return {
    link: function (scope, elem) {
      elem.dropdownHover();
    }
  };  
});

app.directive("uiPreloader", ["$rootScope", function(){
  return {
    restrict: "A",
    template: '<span class="bar"></span>',
    link: function(e, t, a) {
      t.addClass("preloaderbar"), e.$on("$stateChangeStart", function(e) {
        t.removeClass("hide").addClass("active")
      }), e.$on("$stateChangeSuccess", function(e, a, o, n) {
        e.targetScope.$watch("$viewContentLoaded", function() {
          //t.addClass("hide").removeClass("active");
        })
      }), e.$on("preloader:active", function(e) {
        t.removeClass("hide").addClass("active")
      }), e.$on("preloader:hide", function(e) {
        //t.addClass("hide").removeClass("active")
      })
    }
  }
}]);
app.directive("slimScroll", function(){
  return {
    restrict: "A",
    link: function(e, t, a) {
      var o = $(t[0]);
      if($(window).width() <= 944){
        o.slimScroll({
          height: a.scrollHeight || "100%"
        })
      }
    }
  }
});
app.directive("accordionNav", function(){
  return {
    restrict: "A",
    link: function(e, t, a) {
      var o, n, l, r;
      o = $(t[0]), n = 250, l = o.find("ul").parent("li"), l.append('<i class="material-icons icon-has-ul">arrow_drop_down</i>'), r = l.children("a"), r.on("click", function(e) {
        e.preventDefault()
      }), o.on("click", function(e) {
        var t = e.target,
        a = $(t).closest("li");
        if (a.length) {
          var l = a.children("ul"),
          r = a.parents().length + 1,
          i = o.find("ul").filter(function() {
            if ($(this).parents().length >= r && this !== l.get(0)) return !0
          });
          if($(window).width() <= 944){
            i.slideUp(n).closest("li").removeClass("open"), a.has("ul").length && a.toggleClass("open"), l.stop().slideToggle(n,function(){var crr = $(this).css('display'); $(this).attr('style','display:'+crr+'');})
          }
        }
      })
    }
  }
});
app.directive("highlightActive", function(){
  return {
    restrict: "A",
    controller: ["$scope", "$element", "$attrs", "$location", function(e, t, a, o) {
      var n, l, r;
      l = t.find("a"), r = function() {
        return o.path()
      }, n = function(e, t) {
        t = "#" + t, angular.forEach(e, function(e) {
          var a, o, n;
          o = angular.element(e), a = o.parent("li"), n = o.attr("href"), a.hasClass("active") && a.removeClass("active"), 0 === t.indexOf(n) && a.addClass("active")
        })
      }, n(l, o.path()), e.$watch(r, function(e, t) {
        e !== t && n(l, o.path())
      })
    }]
  }
});

app.directive("toggleSidebar", function(){
  return {
    restrict: "A",
    controller: ["$scope", "$element", "$attrs", "$location", function(e, t) {
      var a = $(t[0]),
      o = $("#body");
      a.on("click", function(e) {
        o.toggleClass("sidebar-mobile-open"), e.preventDefault()
      })
    }]
  }
});

/* ==== Limit bind Value ==== */
app.directive("limitTo", [function() {
  return {
    restrict: "A",
    link: function(scope, elem, attrs) {
      var limit = parseInt(attrs.limitTo);
      angular.element(elem).on("keydown", function(e) {
        angular.element(elem).attr('maxLength',limit);
      });
      angular.element(elem).on("keypress", function(e) {
        /*if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 || (e.keyCode == 65 && e.ctrlKey === true) || (e.keyCode >= 35 && e.keyCode <= 39)) {
          return;
        }
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
          e.preventDefault();
        }*/
      });
    }
  }
}]);

/* ==== Custom Auto Focus ==== */
app.directive("customautofocus", [function() {
  return {
    restrict: "AE",
    link: function(scope, elem, attrs) {
      setTimeout(function(){
        var val = attrs.customautofocus;
        if(val.length){
          elem.focus();
        }
      },1000);
    }
  }
}]);

/* ==== File Upload ==== */
app.directive('fileModel', ['$parse', function($parse) {
  return {
    restrict: 'A',
    link: function(scope, element, attrs) {
      var model = $parse(attrs.fileModel);
      var modelSetter = model.assign;

      element.bind('change', function() {
        scope.$apply(function() {
          modelSetter(scope, element[0].files[0]);
        });
      });
    }
  };
}]);


app.directive('draggable', function() {
  return {
    restrict: 'A',
    link: function(scope, element, attrs) {
      element.draggable({
        stop: function(event, ui) {
          event.stopPropagation();
        }
      });
    }
  };
});

