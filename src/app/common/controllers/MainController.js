var app = angular.module('MainController',['ngCookies','defaultServices', 'ngIdle']);

app.controller('MainController', ['$scope', '$rootScope','$cookies','$cookieStore','$state','authService','resourceService','$http','$mdDialog','GLOBALS','SETTINGS','toaster','$filter','$timeout', '$window', '$rootScope', function($scope, $rootScope,$cookies,$cookieStore,$state,authService,resourceService,$http,$mdDialog,GLOBALS,SETTINGS,toaster, $filter,$timeout, $window, $rootScope) {
    /* ==== Global Variables ==== */
    $scope.rootPath = GLOBALS.SITE_URL;
    $scope.navCollapsed = !1;
    $scope.app_brand = 'CTS-TZELL';
    $scope.app_year = new Date().getFullYear();
// $scope.amountSelection = ["=", ">", "<", "<>", ">=", "<="];
$scope.amountSelection = [{'id': 1, 'symbole':'='}, {'id': 2, 'symbole':'>'}, {'id': 3, 'symbole':'<'}, {'id': 4, 'symbole':'<>'}, {'id': 5, 'symbole':'>='}, {'id': 6, 'symbole':'<='}];

$scope.unitsObj = {};
$scope.insertPaymentDataParamsNew = {};
$scope.allowShowCTSNumber = false;
$rootScope.allowSaveReport = true;

$scope.showMaintainace = [{'report_id':42,web_menus:false},{'report_id':43,web_menus:false},{'report_id':28,web_menus:false},{'report_id':89,web_menus:false},{'report_id':91,web_menus:false},{'report_id':3,web_menus:false},{'report_id':130,web_menus:false},{'report_id':19,web_menus:false},{'report_id':31,web_menus:false},{'report_id':32,web_menus:false},{'report_id':39,web_menus:false},{'report_id':30,web_menus:false},{'report_id':54,web_menus:false},{'report_id':138,web_menus:false},{'report_id':33,web_menus:false},{'report_id':151,web_menus:false},{'report_id':41,web_menus:false},{'report_id':46,web_menus:false},{'report_id':40,web_menus:false},{'report_id':88,web_menus:false},{'report_id':45,web_menus:false},{'report_id':87,web_menus:false},{'report_id':68,web_menus:false},{'report_id':1,web_menus:false},{'report_id':4,web_menus:false},{'report_id':9,web_menus:false},{'report_id':8,web_menus:false},{'report_id':38,web_menus:false},{'report_id':21,web_menus:false},{'report_id':35,web_menus:false}];

$scope.showBookingList = [{'menu_id':9,web_menus:true},{'report_id':61,web_menus:false},{'report_id':60,web_menus:false},{'report_id':63,web_menus:false},{'report_id':62,web_menus:false}];

$scope.showlistByAgent = [{'report_id':10,web_menus:false},{'report_id':12,web_menus:false},{'report_id':11,web_menus:false},{'report_id':6,web_menus:false},{'report_id':14,web_menus:false},{'report_id':15,web_menus:false},{'report_id':16,web_menus:false},{'report_id':13,web_menus:false}];
$scope.showlistByAccount = [{'report_id':65,web_menus:false},{'report_id':66,web_menus:false},{'report_id':64,web_menus:false},{'report_id':67,web_menus:false}];
$scope.showBatchRelease = [{'report_id':20,web_menus:false},{'report_id':24,web_menus:false},{'report_id':25,web_menus:false},{'report_id':18,web_menus:false},{'report_id':23,web_menus:false}];
$scope.showReassignment = [{'report_id':17,web_menus:false}];
$scope.setAllUpdateParams = true;
$scope.showClaimMessage = false;

/* ==== Global Variables ==== */

/* ==== pagination options ==== */
// $scope.limitOptions = [10,25,50];
$scope.limitOptions = [50, 100];
$scope.query = {};
// $scope.query.limit = 10;
$scope.query.limit = 50;
$scope.query.page = 1;
$scope.query.AccountselectedItem = '';
$scope.query.descriptionText = '';
/* ==== pagination options ==== */
$scope.defaultSelectedParams = {
    DataTableCommon:{
        param:{
            Draw:1,
            Start:0,
            // Length:10
            Length:50
        },
        ExportType:0
    },
    corpID:"",fromDate:"",toDate: "",TAID:"",dateSelect:"",unit:"",branch:"",agent:"",iata:"",account:"",propertyBrand:"",propertyChain:"",propertyName:"",propertyAddress:"",propertyCity:"",propertyState:"",propertyZip:"",propertyPhone:"",propertyPreferred:"",lastName:"",firstName:"",type:"",status:"",claimed:"",amount:"",amountSign:"",amountDollar:"",checkNo:"",batchNo:0,recordSelect:"",bookingSource:"",accountGroup:"",pnr:"",rateCode:"",groupType:""
};

/* ==== toster Option ==== */
$scope.tosterOption = {
    'prevent-duplicates':true,
    'time-out': 2000,
    'limit':1,
};
/* ==== toster Option ==== */

/* ===== Access Page left menu ====== */
function returnObjAccess(val) {
    return function(element) {
        if(val.web_menus && element.menu_id === val.menu_id && element.web_menus === val.web_menus){
            return element;    
        }else if(element.report_id === val.report_id && element.web_menus === val.web_menus){
            return element;    
        }
    }
}

/* ==== Sidebar Close and show ==== */
$scope.toggleCollapsedNav = function() {
//if($(window).width() < 944){
    $scope.navCollapsed ? ($scope.navCollapsed = !1, $scope.togglerIconVal = "keyboard_arrow_down") : ($scope.navCollapsed = !0, $scope.togglerIconVal = "keyboard_arrow_right")
//}
}
$scope.$watch("navCollapsed", function(t, a) {
//$scope.navCollapsed ? $scope.togglerIconVal = "radio_button_unchecked" : $scope.togglerIconVal = "radio_button_checked"
$scope.navCollapsed ? $scope.togglerIconVal = "keyboard_arrow_right" : $scope.togglerIconVal = "keyboard_arrow_down"
});
/* ==== Sidebar Close and show ==== */

/* ==== Check Session expaire ==== */
$scope.errorView = function(err,status){
    if(status == 401){
        $scope.signout();
    }
}
/* ==== Check Session expaire ==== */

$scope.checkAccess = function(obj){
    if(sessionStorage.getItem('userInfo')){
        $scope.userDetails = JSON.parse(sessionStorage.getItem('userInfo'));
        if($scope.userDetails && $scope.userDetails.UserPermissionsResponseViewModel){
            var validPage = $scope.userDetails.UserPermissionsResponseViewModel.filter(returnObjAccess(obj));
            if(validPage.length){
                return true;
            }
            return false;
        }

        /*if($scope.userDetails && $scope.userDetails.superUser == true){
            return true;
        }
        else{
            if($scope.userDetails && $scope.userDetails.UserPermissionsResponseViewModel){
                var validPage = $scope.userDetails.UserPermissionsResponseViewModel.filter(returnObjAccess(obj));
                if(validPage.length){
                    return true;
                }
                return false;
            }
        }*/
    }
}

function selectstatusCode(obj){
    var teamIsNew = $scope.indexedstatusCode.indexOf(obj.statusCode) == -1;
    if (teamIsNew) {
        $scope.indexedstatusCode.push(obj.statusCode);
    }
    return teamIsNew;
}


/* ===== Maintainace label show hide function ===== */
$scope.checkMaintainanceShow = function(mainObj){
    if($scope.userDetails && $scope.userDetails.superUser == true){
        return true;
    }
    for(var i=0; i< mainObj.length;i++){
        var validPage = $scope.userDetails.UserPermissionsResponseViewModel.filter(returnObjAccess(mainObj[i]));
        if(validPage.length){
            return true;
        }
    }
    return false;
}

/* ===== Access Page left menu ====== */
$scope.navLeftView = function(idName){
    $timeout(function(){
        if($(window).width() >= 944){
            $('#navLeftView li').each(function(index, el) {
                $(this).hover(function(){
                    if($(this).find('ul').length){
                        var elem = $(this).find('ul');
                        var currentPos = $(this).offset().top;
                        elem.css('top',(currentPos - $(window).scrollTop())+'px');                             
                        $(window).scroll(function(){
                            elem.css('top',(currentPos - $(this).scrollTop())+'px');
                        });
                        $(this).find('ul').css('max-height',(($(window).height() - $(this).offset().top) - 50)+'px');
                    }
                });                
            });
            if($('#'+idName+'')){
                $('#'+idName+'').enscroll({
                    showOnHover: true,
                    verticalTrackClass: 'track3',
                    verticalHandleClass: 'handle3',
                    verticalScrollerSide:'left'
                });
            }
        }
    },true);
}

$scope.checkWindowSizeMobile = false;
$scope.checkWindowSizeDesktop = true;
$scope.checkWindowSize = function(name){
    if($(window).width() >= 944){
// return (name == 'desktop-view') ? true :false;
$scope.checkWindowSizeMobile = false;
$scope.checkWindowSizeDesktop = true;
}else{
// return (name == 'mobile-view') ? true :false;
$scope.checkWindowSizeMobile = true;
$scope.checkWindowSizeDesktop = false;
}
}

angular.element($window).bind('resize', function(){
    $scope.$apply(function(){
        $scope.checkWindowSize();    
    });    
});
$scope.checkWindowSize();

/* ==== Logout Function ==== */
$scope.signout = function() {
// $state.go('login');
$http({
    url: SETTINGS[GLOBALS.ENV].apiUrl+"logout",
    method: 'POST',
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
    }}).success(function(response) {
        authService.clearAuthentication();
        sessionStorage.removeItem("userInfo");
        $state.go('login');
        $window.location.reload();
    });
// authService.clearAuthentication();
}
/* ==== Logout Function ==== */

/* ==== User Info ==== */
$scope.userInfo = function(){
    if(sessionStorage.getItem('userInfo')){
        $scope.userDetails = JSON.parse(sessionStorage.getItem('userInfo'));
        $scope.getStatus();
        $scope.getUnits();
        $scope.GetBookingTypes();
        $scope.GetBookingTypesAgency();
        $scope.GetUserPermissions();
        $scope.GetPaymentsStatuses();
        $scope.getTodaysDate();
        if($scope.userDetails.logo){
            $scope.app_logo = SETTINGS[GLOBALS.ENV].apiUrl+'logos/'+$scope.userDetails.logo;
        }else{
            $scope.app_logo = 'src/assets/img/logo.png';
        }
    }
}
/* ==== User Info ==== */

/* ===== Preview PDF Link function ===== */
$scope.getDocIdNumber = function(doc_id){
    return (doc_id ) ? true : false;
}
$scope.GetControlData = function(obj){
    if(!obj.booking_id){
        obj.booking_id = null;
    }
    if(!obj.payment_id){
        obj.payment_id = null;
    }

    $http({
        url: SETTINGS[GLOBALS.ENV].apiUrl+"ReferenceDocument/GetReferenceDocument",
        method: 'POST',
        data: obj,
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
        }
    }).success(function(response) {
        if(response.ResposeCode == 2){
            if(response.DataModel.length){
                window.open(
                    response.DataModel[0],
                    '_blank'
                    );
            }
        }
    }).error(function(err,status) {
        $scope.errorView(err,status);
    });
}

/*===== Remove Release =====*/
$scope.updateBatchStatusParams = {
    // DataTableCommon:{param:{Draw:1,Start:0,Length:10},ExportType:0},
    DataTableCommon:{param:{Draw:1,Start:0,Length:50},ExportType:0},
    userID: ($scope.userDetails)?$scope.userDetails.userID:'',
    ta_id: ($scope.userDetails)?$scope.userDetails.ta_id:''
};
$scope.removeRelease = function(batchId, index){
    $scope.updateBatchStatusParams.status = 0;
    $scope.updateBatchStatusParams.batchID = [batchId];
    $scope.updateBatchStatusParams.date =  $filter('date')($rootScope.dateToRelease, "dd/MM/yyyy");

    $http({
        url: SETTINGS[GLOBALS.ENV].apiUrl+"Batch/UpdateBatchStatus",
        method: 'POST',
        data: $scope.updateBatchStatusParams,
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
        }
    }).success(function(response) {
        if(response.ResposeCode == 2){
            toaster.pop('Success', '', response.ResponseMessage);
            $rootScope.unmatchedPaymentList = $scope.unmatchedPaymentListTemp;
            $scope.$broadcast('refreshBatchReleaseUtilityOnRemove');
            $rootScope.unmatchedPaymentList.splice(index, 1);
        }else{
            toaster.pop('error', '', response.ResponseMessage);
        }
        $scope.callBackDialog('cancel');
    }).error(function(err,status) {
        $scope.errorView(err,status);
    });
}

$scope.querySearch = function(query) {

    var results = query ? $scope.GetTheAccountsForCorpMethod.filter($scope.createFilterFor(query) ) : $scope.GetTheAccountsForCorpMethod, deferred;
    if (false) {
        deferred = $q.defer();
        $timeout(function () { deferred.resolve( results ); }, Math.random() * 1000, false);
        return deferred.promise;
    } else {
        return results;
    }
}

$scope.createFilterFor = function(query) {
    var lowercaseQuery = angular.lowercase(query);
    return function filterFn(state) {
        var setval = state.accountCode + ' - ' + state.description + ", " + state.id;
        state['querySearch'] = setval;
        if(state.id && state.id.toString().toLowerCase().indexOf(lowercaseQuery) != -1){
            return (state.id && state.id.toString().toLowerCase().indexOf(lowercaseQuery) === 0);
        }
        if(state.accountCode && state.accountCode.toLowerCase().indexOf(lowercaseQuery) != -1){
            return (state.accountCode && state.accountCode.toLowerCase().indexOf(lowercaseQuery) === 0);
        }
        if(state.description && state.description.toLowerCase().indexOf(lowercaseQuery) != -1){
            return (state.description && state.description.toLowerCase().indexOf(lowercaseQuery) === 0);
        }
        return (state.accountCode && state.description && setval.toLowerCase().indexOf(lowercaseQuery) === 0);
    };
}


/* ===== To clear amount field from popup  ===== */
$scope.clearAmountField = function(value){
    if(!value){
        $scope.reportFilterRoot.amountDollar = "";
    }
}


/******************************************************************
==== Material Popups Function ==== 
*******************************************************************/

/* ==== Help & Support Popup ==== */
$scope.showAlert = function(ev) {
    $mdDialog.show({
        controller: 'DialogController',
        templateUrl: 'src/app/common/views/dialog1.tmpl.html',
        parent: angular.element(document.body),
        targetEvent: ev,
        clickOutsideToClose:true,
        fullscreen: false
    }).then(function(answer) {
        $scope.status = 'You said the information was "' + answer + '".';
    }, function() {
        $scope.status = 'You cancelled the dialog.';
    });
}
/* ==== Help & Support Popup ==== */

/* ==== Get Business Units List ==== */
$scope.getUnits = function(){
    $scope.unitsObj.id = ($scope.userDetails)?$scope.userDetails.corpID:'';
    $scope.unitsObj.unit = "";
    $http({
        url: SETTINGS[GLOBALS.ENV].apiUrl+"Corp_Agency/GetCorpBusinessUnits",
        method: 'POST',
        data: $.param($scope.unitsObj),
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
        }
    }).success(function(response) {             
        if(response.ResposeCode == 2){
            $scope.unitList = response.DataList.data;
        }
    }).error(function(err,status) {
        $scope.errorView(err,status);
    });
}
/* ==== Get Business Units List ==== */

/* ====  Get Type List ==== */
$scope.GetBookingTypes = function(){
    $http({
        url: SETTINGS[GLOBALS.ENV].apiUrl+"BookingTypes/GetBookingTypes",
        method: 'POST',
// data: $scope.bookingTypesParams,
headers: {
    'Content-Type': 'application/json',
    'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
}
}).success(function(response) {
    if(response.DataList && response.DataList.data.length){
        $scope.bookingTypes = response.DataList.data;

// for(var i = 0 ; i < $scope.bookingTypes.length ; i++)
}                   
}).error(function(err,status) {
    $scope.errorView(err,status);
});
}

$scope.GetBookingTypesAgency = function(){
    $http({
        url: SETTINGS[GLOBALS.ENV].apiUrl+"BookingTypes/GetBookingTypesAgency",
        method: 'POST',
        data: {ta_id:$scope.userDetails.ta_id},
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
        }
    }).success(function(response) {
        if(response.DataList && response.DataList.data.length){
            $scope.bookingTypesAgency = response.DataList.data;
        }                   
    }).error(function(err,status) {
        $scope.errorView(err,status);
    });
}

/* ====  Get Type List ==== */

/* ==== GetPaymentsStatuses ==== */
// $scope.GetPaymentsStatusesObj = {DataTableCommon:{param:{Draw:1,Start:0,Length:10},ExportType:0},corpID:''};
$scope.GetPaymentsStatusesObj = {DataTableCommon:{param:{Draw:1,Start:0,Length:50},ExportType:0},corpID:''};
$scope.GetPaymentsStatuses = function(){
    $scope.GetPaymentsStatusesObj.corpID = $scope.userDetails.corpID;
    $http({
        url: SETTINGS[GLOBALS.ENV].apiUrl+"Payments/GetPaymentsStatuses",
        method: 'POST',
        data: $scope.GetPaymentsStatusesObj,
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
        }
    }).success(function(response) {
        $scope.statusListView = [];
        $scope.indexedstatusCode = [];
        if(response.ResposeCode == 2){
            if(response.DataList && response.DataList.data.length){
                $scope.statusListView = response.DataList.data.filter(selectstatusCode);
            }
        }
    }).error(function(err,status) {
        $scope.errorView(err,status);
    });
}


/* ===== Get all the status for outstanding booking report hotel only ===== */

$scope.getStatus = function(obj){

    $http({
        url: SETTINGS[GLOBALS.ENV].apiUrl+"Status/GetAllStatus",
        method: 'GET',
            // data: $scope.saveOutstandingChangesParamsObj,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
            }
        }).success(function(response) {
            if(response.ResposeCode == 2){
                if(response.DataList && response.DataList.data.length){
                    $scope.commentListForOutstandingBookingRpt = response.DataList.data;
                }

            }else{

            }
        }).error(function(err,status) {
            $scope.errorView(err,status);
        });

    }
    // $scope.getStatus();

    /* =========================================================================== */



    /* ==== Get GetUserPermissions ==== */
    $scope.GetUserPermissions = function(){
        $scope.GetUserPermissionsData = {};
        $scope.GetUserPerObj = {};
        $scope.GetUserPerObj.id = ($scope.userDetails)? $scope.userDetails.user.id : '';
        $http({
            url: SETTINGS[GLOBALS.ENV].apiUrl+"User/GetUserPermissions",
            method: 'POST',
            data: $.param($scope.GetUserPerObj),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
            }
        }).success(function(response) {
            if(response.DataModel){
                $scope.GetUserPermissionsData = response.DataModel;

            }                   
        }).error(function(err,status) {
            $scope.errorView(err,status);
        });
    }

    /* ====================  Left panel Selected Search Box function ==================== */

    $scope.reportFilterRoot = {};
    $scope.disableDateRadioButtons = true;
    $scope.checkValidDate = function(fromDate,toDate){
        if(toDate && fromDate > toDate){
            $scope.toDate = fromDate;
        }
    }

    $scope.disableType = function(fromDate,toDate){
        if(toDate && fromDate){
            $scope.disableDateRadioButtons = false;
        }else{
            $scope.disableDateRadioButtons = true;
        }
    }

    function filterUnitNameNew(element){
        if(element.ta_id == $scope.selectedUnitname){
            return element;
        }
    }

    $scope.filterUnitName = function(obj){
        $scope.selectedUnitname = obj;
        if($scope.unitList){
            var getObj = $scope.unitList.filter(filterUnitNameNew);
            if(getObj.length){
                return getObj[0].businessUnitDescription;    
            }
        }
    }

    $scope.filteroutstandingStatusName = function(statusCode){
        if($scope.statusListView){
            for(var i = 0 ; i < $scope.statusListView.length ; i++){
                if($scope.statusListView[i].statusCode == statusCode){
                    return $scope.statusListView[i].description;
                }
            }
        }
    }
    $scope.disableAgentTextbox = false;
    /* ===== Call funciton after change UI Router View ====== */   
    $scope.$on('callSelectedFieldByPage', function(evnt,obj) {
        $rootScope.$on('$stateChangeSuccess', function (ev, to, toParams, from, fromParams) {
            if(to.name != from.name){
                /*var tempParams = $scope.reportFilterRoot;*/
                $scope.reportFilterRoot = {};
                /*$scope.reportFilterRoot.unit = tempParams.unit;*/
                $scope.defaultSelectedParams.inDate = "";
            }
        });

        if(obj.selectedDate || obj.selectedDate != undefined){
            obj.dateSelect = obj.selectedDate;
            $scope.reportFilterRoot.dateSelect = obj.dateSelect;
        }
// $scope.reportFilterRoot.dateSelect = obj.dateSelect;

$scope.filterDataSelected = obj;
$scope.disableAgentTextbox = false;
if($scope.userDetails && $scope.userDetails.agent){
    // if($state.current.name == "main.home" || $state.current.name == "main.claimed-AllExcludingHotelAccount" || $state.current.name == "main.claimedAllTypeAccount" || $state.current.name == "main.claimed-AllHotelCarAndOther" || $state.current.name == "main.claimedAllExcludingHotel" || $state.current.name == "main.claimed-AllExcludingHotel-Agent" || $state.current.name == "main.claimed-AllTypes-Agent" || $state.current.name == "main.claimed-AllHotelCarAndOther-Agent" || $state.current.name == "main.claimed-hotelonly-Agent" || $state.current.name == "main.outstandingBookingReport-HotelsOnly" || $state.current.name == "main.matureBookingsReportByAgent-CarsOnly" || $state.current.name == "main.matureBookingsReportByAgent-HotelsOnly" || $state.current.name == "main.matureBookingsReport"){
        if($state.current.name == "main.claimed-AllExcludingHotelAccount" || $state.current.name == "main.claimedAllTypeAccount" || $state.current.name == "main.claimed-AllHotelCarAndOther" || $state.current.name == "main.claimedAllExcludingHotel" || $state.current.name == "main.claimed-AllExcludingHotel-Agent" || $state.current.name == "main.claimed-AllTypes-Agent" || $state.current.name == "main.claimed-AllHotelCarAndOther-Agent" || $state.current.name == "main.claimed-hotelonly-Agent" || $state.current.name == "main.outstandingBookingReport-HotelsOnly" || $state.current.name == "main.matureBookingsReportByAgent-CarsOnly" || $state.current.name == "main.matureBookingsReportByAgent-HotelsOnly" || $state.current.name == "main.matureBookingsReport"){
            $scope.disableAgentTextbox = true;
        }
    }

    if($scope.filterDataSelected.matched || $scope.filterDataSelected.unmatched || $scope.filterDataSelected.unmatched1){
        $scope.filterDataSelected.recordSelect = (($scope.filterDataSelected.unmatched) ? '1' : '0')+""+(($scope.filterDataSelected.matched) ? '1' : '0')+""+(($scope.filterDataSelected.unmatched1) ? '1' : '0');
        if($scope.filterDataSelected.matched){delete $scope.filterDataSelected.matched};
        if($scope.filterDataSelected.unmatched){delete $scope.filterDataSelected.unmatched};
        if($scope.filterDataSelected.unmatched1){delete $scope.filterDataSelected.unmatched1};
        $scope.reportFilterRoot.recordSelect = $scope.filterDataSelected.recordSelect;

    }

/*if($scope.reportFilterRoot.matched || $scope.reportFilterRoot.unmatched || $scope.reportFilterRoot.unmatched1){
$scope.reportFilterRoot.recordSelect = (($scope.reportFilterRoot.unmatched) ? '1' : '0')+""+(($scope.reportFilterRoot.matched) ? '1' : '0')+""+(($scope.reportFilterRoot.unmatched1) ? '1' : '0');

if($scope.reportFilterRoot.matched){delete $scope.reportFilterRoot.matched};
if($scope.reportFilterRoot.unmatched){delete $scope.reportFilterRoot.unmatched};
if($scope.reportFilterRoot.unmatched1){delete $scope.reportFilterRoot.unmatched1};
$scope.reportFilterRoot.recordSelect = $scope.reportFilterRoot.recordSelect;
return;
}*/


if(obj && obj.lastName){
    $scope.reportFilterRoot.lastName = obj.lastName;
}
if(obj && obj.firstName){
    $scope.reportFilterRoot.firstName = obj.firstName;
}
$scope.reportFilterRoot.selectedDate = obj.dateSelect;            

if(obj && obj.amountSign){
    $scope.reportFilterRoot.amountSign = obj.amountSign;
}    
if(obj && obj.amountDollar){
    $scope.reportFilterRoot.amountDollar = obj.amountDollar;
}

if(obj.fromDate){
    if(obj.fromDate.indexOf('/') != -1){
        var check = obj.fromDate.split('/');
        var fDcheck = ''+check[1]+'/'+check[0]+'/'+check[2];
    }else{
        var fDcheck = obj.fromDate;
    }
    $scope.defaultSelectedParams.fromDate = new Date(fDcheck);
}
if(obj.toDate){
    if(obj.toDate.indexOf('/') != -1){
        var check1 = obj.toDate.split('/');
        var fDcheck1 = ''+check1[1]+'/'+check1[0]+'/'+check1[2];
    }else{
        var fDcheck1 = obj.toDate;
    }
    $scope.defaultSelectedParams.toDate = new Date(fDcheck1);
}
if(obj.fromdate){
    if(obj.fromdate.indexOf('/') != -1){
        var check12 = obj.fromdate.split('/');
        var fDcheck12 = ''+check12[1]+'/'+check12[0]+'/'+check12[2];
    }else{
        var fDcheck12 = obj.fromdate;
    }
    $scope.defaultSelectedParams.fromdate = new Date(fDcheck12);
}
if(obj.todate){
    if(obj.todate.indexOf('/') != -1){
        var check123 = obj.todate.split('/');
        var fDcheck123 = ''+check123[1]+'/'+check123[0]+'/'+check123[2];
    }else{
        var fDcheck123 = obj.todate;
    }
    $scope.defaultSelectedParams.todate = new Date(fDcheck123);
}


$scope.allowShowCTSNumber = false;

if(obj.type && obj.type.indexOf(',') != -1){
    var curType = obj.type.split(',');    
    $scope.reportFilterRoot.type = curType;
}else{
    if(obj.type){
        $scope.reportFilterRoot.type = [obj.type];    
    }

}

if($state.current.name == "main.commissionAnalysis"){
    $scope.reportFilterRoot.cts_type = $scope.reportFilterRoot.type;
    if($scope.reportFilterRoot.type){
        $scope.filterDataSelected.cts_type = $scope.reportFilterRoot.type.toString();
    }
}

if(obj.status && obj.status.indexOf(',') != -1){
    var curStatus = obj.status.split(',');    
    $scope.reportFilterRoot.status = curStatus;
}else{
    if(obj.status){
        $scope.reportFilterRoot.status = [obj.status];    
    }
}

if(obj && obj.claimed == "1"){
// $scope.reportFilterRoot.claimed = obj.claimed;
$scope.reportFilterRoot.claimed = true;
}

if($scope.reportFilterRoot.unclaimed != false){
    if(obj && obj.claimed == "0"){
// $scope.reportFilterRoot.claimed = obj.claimed;
$scope.reportFilterRoot.unclaimed = true;
$scope.filterDataSelected.unclaimed = $scope.reportFilterRoot.unclaimed;
}else if(obj && obj.claimed == "2"){
    $scope.reportFilterRoot.unclaimed = true;
    $scope.filterDataSelected.unclaimed = $scope.reportFilterRoot.unclaimed;
}
}

/*if($scope.reportFilterRoot.claimed ==  true){
$scope.filterDataSelected.claimed = "1";
}else{
$scope.filterDataSelected.claimed = "0";
}*/

if($state.current.name == 'main.claimed-AllExcludingHotelAccount' || $state.current.name == 'main.claimedAllTypeAccount' || $state.current.name == 'main.claimed-AllHotelCarAndOther' || $state.current.name == 'main.claimedAllExcludingHotel' || $state.current.name == 'main.claimed-AllExcludingHotel-Agent' || $state.current.name == 'main.claimed-AllTypes-Agent' || $state.current.name == 'main.claimed-AllHotelCarAndOther-Agent' || $state.current.name == 'main.claimed-hotelonly-Agent'){
    if($scope.reportFilterRoot.claimed ==  true){
        $scope.filterDataSelected.claimed = "1";
    }else{
        $scope.filterDataSelected.claimed = "0";
    }
}

if($scope.reportFilterRoot.recordSelect){
    for(var i = 0 ; i < $scope.reportFilterRoot.recordSelect.length ; i++){
        if($scope.reportFilterRoot.recordSelect[0] == "1"){
            $scope.reportFilterRoot.unmatched = true;
        }
        if($scope.reportFilterRoot.recordSelect[1] == "1"){
            $scope.reportFilterRoot.matched = true;
        }
        if($scope.reportFilterRoot.recordSelect[2] == "1"){
            $scope.reportFilterRoot.unmatched1 = true;
        }
    }
}

if($scope.filterDataSelected.agent){
    $scope.reportFilterRoot.agent = $scope.filterDataSelected.agent;
}
if($scope.filterDataSelected.account){
    $scope.reportFilterRoot.account = $scope.filterDataSelected.account;
}
if($scope.filterDataSelected.iata){
    $scope.reportFilterRoot.iata = $scope.filterDataSelected.iata;
}
if($scope.filterDataSelected.branch){
    $scope.reportFilterRoot.branch = $scope.filterDataSelected.branch;
}

if($scope.filterDataSelected.propertyBrand){
    $scope.reportFilterRoot.propertyBrand = $scope.filterDataSelected.propertyBrand;
}
if($scope.filterDataSelected.propertyChain){
    $scope.reportFilterRoot.propertyChain = $scope.filterDataSelected.propertyChain;
}
if($scope.filterDataSelected.propertyName){
    $scope.reportFilterRoot.propertyName = $scope.filterDataSelected.propertyName;
}
if($scope.filterDataSelected.propertyAddress){
    $scope.reportFilterRoot.propertyAddress = $scope.filterDataSelected.propertyAddress;
}
if($scope.filterDataSelected.propertyCity){
    $scope.reportFilterRoot.propertyCity = $scope.filterDataSelected.propertyCity;
}
if($scope.filterDataSelected.propertyState){
    $scope.reportFilterRoot.propertyState = $scope.filterDataSelected.propertyState;
}
if($scope.filterDataSelected.propertyZip){
    $scope.reportFilterRoot.propertyZip = $scope.filterDataSelected.propertyZip;
}
if($scope.filterDataSelected.propertyPhone){
    $scope.reportFilterRoot.propertyPhone = $scope.filterDataSelected.propertyPhone;
}

/*if($scope.reportFilterRoot && $scope.reportFilterRoot.propertyPreferred){
if($scope.reportFilterRoot.propertyPreferred == true){
$scope.reportFilterRoot.propertyPreferred = "1";
}else{
$scope.reportFilterRoot.propertyPreferred = "0";
}    
}*/
if($state.current.name == "main.outstandingBookingReport-HotelsOnly"){
    // if($scope.reportFilterRoot.status == ""){
        $scope.reportFilterRoot.status = ["O"];
    // }
}

$scope.filterDataSelected.branch = angular.lowercase($scope.filterDataSelected.branch);
$scope.filterDataSelected.account = angular.lowercase($scope.filterDataSelected.account);
$scope.filterDataSelected.agent = angular.lowercase($scope.filterDataSelected.agent);
$scope.filterDataSelected.iata = angular.lowercase($scope.filterDataSelected.iata);
$scope.filterDataSelected.propertyBrand = angular.lowercase($scope.filterDataSelected.propertyBrand);
$scope.filterDataSelected.propertyChain = angular.lowercase($scope.filterDataSelected.propertyChain);
$scope.filterDataSelected.propertyName = angular.lowercase($scope.filterDataSelected.propertyName);
$scope.filterDataSelected.propertyAddress = angular.lowercase($scope.filterDataSelected.propertyAddress);
$scope.filterDataSelected.propertyCity = angular.lowercase($scope.filterDataSelected.propertyCity);
$scope.filterDataSelected.propertyState = angular.lowercase($scope.filterDataSelected.propertyState);
$scope.filterDataSelected.propertyZip = angular.lowercase($scope.filterDataSelected.propertyZip);
$scope.filterDataSelected.propertyPhone = angular.lowercase($scope.filterDataSelected.propertyPhone);
$scope.filterDataSelected.lastName = angular.lowercase($scope.filterDataSelected.lastName);
$scope.filterDataSelected.firstName = angular.lowercase($scope.filterDataSelected.firstName);
$scope.filterDataSelected.bookingSource = angular.lowercase($scope.filterDataSelected.bookingSource);
$scope.filterDataSelected.accountGroup = angular.lowercase($scope.filterDataSelected.accountGroup);
$scope.filterDataSelected.checkNo = angular.lowercase($scope.filterDataSelected.checkNo);
$scope.filterDataSelected.batchNo = angular.lowercase($scope.filterDataSelected.batchNo);
$scope.filterDataSelected.rateCode = angular.lowercase($scope.filterDataSelected.rateCode);

});

/* ===== Call funciton after change UI Router View ====== */

$scope.clearDates = function(){
    $scope.fromDate = undefined;
    $scope.toDate = undefined;
    $scope.defaultSelectedParams.fromDate = "";
    $scope.defaultSelectedParams.toDate = "";
    $scope.reportFilterRoot.selectedDate = "";
} 

$scope.resetAllFieldsRoot = function(){
    $scope.defaultSelectedParams.inDate = "";
    if($state.current.name == "main.outstandingBookingReport-HotelsOnly"){
        $scope.tempVar = angular.copy($scope.reportFilterRoot);
    }
    if($scope.userDetails && $scope.userDetails.agent){
        if($state.current.name == "main.claimed-AllExcludingHotelAccount" || $state.current.name == "main.claimedAllTypeAccount" || $state.current.name == "main.claimed-AllHotelCarAndOther" || $state.current.name == "main.claimedAllExcludingHotel" || $state.current.name == "main.claimed-AllExcludingHotel-Agent" || $state.current.name == "main.claimed-AllTypes-Agent" || $state.current.name == "main.claimed-AllHotelCarAndOther-Agent" || $state.current.name == "main.claimed-hotelonly-Agent"){
            $scope.calimParamers = angular.copy($scope.reportFilterRoot);
        }
    }
    $scope.reportFilterRoot = {};
    $scope.filterDataSelected = {};
    $scope.defaultSelectedParams.fromDate = "";
    $scope.defaultSelectedParams.toDate = "";

    if($state.current.name == "main.outstandingBookingReport-HotelsOnly"){
        $scope.reportFilterRoot.type = $scope.tempVar.type;
        $scope.reportFilterRoot.status = $scope.tempVar.status;
        $scope.reportFilterRoot.agentStatus = $scope.tempVar.agentStatus;
        $scope.reportFilterRoot.agent = $scope.tempVar.agent;
    }
    if($scope.userDetails && $scope.userDetails.agent){
        if($state.current.name == "main.claimed-AllExcludingHotelAccount" || $state.current.name == "main.claimedAllTypeAccount" || $state.current.name == "main.claimed-AllHotelCarAndOther" || $state.current.name == "main.claimedAllExcludingHotel" || $state.current.name == "main.claimed-AllExcludingHotel-Agent" || $state.current.name == "main.claimed-AllTypes-Agent" || $state.current.name == "main.claimed-AllHotelCarAndOther-Agent" || $state.current.name == "main.claimed-hotelonly-Agent"){
            $scope.reportFilterRoot.agent = $scope.calimParamers.agent;
        }
    }
}

$scope.callBackDialog = function(data){
    $scope.showAgent = false;
    /*$scope.disputeData = {};
    $scope.disputeDataAgentList = [];
    $scope.disputeDataAccountList = [];
    $scope.agentClaimList = [];
    $scope.accountClaimList = [];
    $scope.email = "";*/
    $mdDialog.cancel();
}

$scope.ctsFormSubmit = function(ev, val){
    /*if($state.current.name == 'main.home'){
        $state.go('main.quickFindView');
    }
    $scope.ctsNumberObj = {
        'DataTableCommon':{param:{Draw:1,Start:0,length:500},ExportType:0},
        'corpID':($scope.userDetails)?$scope.userDetails.corpID:'',
        'invoiceNumber' : val
    };
    $http({
        url: SETTINGS[GLOBALS.ENV].apiUrl+"Invoice/GetInvoiceData",
        method: 'POST',
        data: $scope.ctsNumberObj,
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
        }
    }).success(function(response) {
        $scope.getInvoiceDataList = [];
        if(response.DataList){
            if(response.DataList.data.length){
                $scope.getInvoiceDataList = response.DataList.data;
            }
        }
        $scope.allowShowCTSNumber = true;

    }).error(function(err,status) {
        $scope.errorView(err,status);
    });*/
    $scope.paymentsearchRecord = {
        'id' : parseInt(val),
        'booking_id': 0
    }
    $scope.showConfirmPopup(ev,$scope.paymentsearchRecord,'member home');

}

$scope.ctsRecordSubmit = function(ev,searchRecord){
    $scope.paymentsearchRecord = {
        'id' : parseInt(searchRecord),
        'booking_id': 0
    }
    $scope.showConfirmPopup(ev,$scope.paymentsearchRecord,'Member home');
}

/*===== Edit Records =====*/
$scope.goRecordSubmit = function(ev, recordToEdit){
    $scope.goRecordObj = {
        'id' : parseInt(recordToEdit),
        'booking_id': 0
    }
    $scope.showConfirmPopup(ev,$scope.goRecordObj,'Member Home');
}

/* ===== Left Panel select Criteria popup function ===== */

$rootScope.$on('$stateChangeSuccess', function (ev, to, toParams, from, fromParams) {
    if(to.name == "main.home"){
        if($scope.defaultSelectedParams && ($scope.defaultSelectedParams.fromDate || $scope.defaultSelectedParams.toDate)){
            $scope.defaultSelectedParams.fromDate  = "";
            $scope.defaultSelectedParams.toDate = "";
        }
    }
});

$scope.leftPanelCommonFilter = function(ev) {
    $rootScope.allowSaveReport = false;
//$scope.reportFilterRoot = angular.copy($scope.defaultSelectedParams);
$mdDialog.show({
    contentElement: '#leftPanelCommonFilter',
    parent: angular.element(document.body),
    targetEvent: ev,
    clickOutsideToClose:false,
    fullscreen: false,
    skipHide: true
}).finally(function() {
});
// $("#leftPanelCommonFilter").draggable();
};

$scope.callSaveReports = function(){
    $scope.saveFilterRecord = {
        userID : $scope.userDetails.userID,
        corpID : $scope.userDetails.corpID,
        parameters : JSON.stringify($scope.reportFilterRoot)
    };        
    $http({
        url: SETTINGS[GLOBALS.ENV].apiUrl+"Report/GetReportData",
        method: 'POST',
        data: $.param($scope.saveFilterRecord),
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
        }
    }).success(function(response) {
        if(response.ResposeCode == 2){
            if(response.DataList && response.DataList.data.length){
                toaster.pop('info', '', "Report is already on file with selected parameters: " +response.DataList.data[0].WebReportData.title + "");
            }
        }             
    }).error(function(err,status) {
        $scope.errorView(err,status);
    });
}

$scope.checkOutValidType = function(obj,filterObj){
    filterObj = filterObj.toString();
    var splitObj = [];
    if(filterObj.indexOf(',') != -1){
        splitObj = filterObj.split(',');
    }else{
        splitObj = [filterObj];
    }
    var objStr =  obj.toString();
    if(splitObj.indexOf(objStr) != -1){
        return true;
    }
    return false;
}
$scope.applyFiltersRoot = function(){
    if($state.current.name == "main.extractReport" || $state.current.name == "main.home" || $state.current.name == "main.quickFindView"){
        if(!$scope.reportFilterRoot.unmatched && !$scope.reportFilterRoot.matched && !$scope.reportFilterRoot.unmatched1){
            $scope.recordSelectedValidationError = "Must have at least one record type selected.";
            return;
        }else{
            if($scope.recordSelectedValidationError){
                delete $scope.recordSelectedValidationError;    
            }
        }
    }    

    if($state.current.name == "main.home" || $state.current.name == "main.quickFindView"){
        if($scope.defaultSelectedParams.fromDate && $scope.defaultSelectedParams.toDate)
            if(!$scope.reportFilterRoot.selectedDate){
                $scope.dateTypeSelectedValidationError = "Date type must be selected.";
                toaster.pop('error', '', $scope.dateTypeSelectedValidationError);
                return;
            }else{
                if($scope.dateTypeSelectedValidationError){
                    delete $scope.dateTypeSelectedValidationError
                }
            }
        }

        if($scope.reportFilterRoot.unclaimed == true){
            $scope.reportFilterRoot.claimed = "2";
        }else{
            $scope.reportFilterRoot.claimed = "0";
        }

        $scope.reportFilterRoot.corpID = $scope.userDetails.corpID;
        $scope.reportFilterRoot.unit = ($scope.reportFilterRoot.unit) ? ($scope.reportFilterRoot.unit == 'All')?'':$scope.reportFilterRoot.unit:'';
// $scope.reportFilterRoot.unit = ($scope.reportFilterRoot.unit) ? $scope.reportFilterRoot.unit : ($scope.userDetails.unit) ? $scope.userDetails.unit : '';
$scope.reportFilterRoot.status = ($scope.reportFilterRoot.status) ? $scope.reportFilterRoot.status.toString() : ($scope.userDetails.status) ? $scope.userDetails.status.toString() : '';
$scope.reportFilterRoot.type = ($scope.reportFilterRoot.type) ? $scope.reportFilterRoot.type.toString() : ($scope.userDetails.type) ? $scope.userDetails.type.toString() : '';
$scope.reportFilterRoot.fromDate = ($scope.defaultSelectedParams.fromDate) ? $filter('date')(new Date($scope.defaultSelectedParams.fromDate), "dd/MM/yyyy") : ($scope.userDetails.fromDate) ? $filter('date')(new Date($scope.userDetails.fromDate), "dd/MM/yyyy") : '';
$scope.reportFilterRoot.toDate = ($scope.defaultSelectedParams.toDate) ? $filter('date')(new Date($scope.defaultSelectedParams.toDate), "dd/MM/yyyy") : ($scope.userDetails.toDate) ? $filter('date')(new Date($scope.userDetails.toDate), "dd/MM/yyyy") : '';
$scope.reportFilterRoot.confirmation = ($scope.reportFilterRoot.confirmation)?$scope.reportFilterRoot.confirmation:"";
$scope.reportFilterRoot.payment_id = ($scope.reportFilterRoot.payment_id)?Number($scope.reportFilterRoot.payment_id):null;
$scope.reportFilterRoot.inDate = ($scope.defaultSelectedParams.inDate) ? $filter('date')(new Date($scope.defaultSelectedParams.inDate), "dd/MM/yyyy") : '';
$rootScope.allowSaveReport = false;
$scope.reportFilterRoot.pageStart = 1;
$scope.reportFilterRoot.userId = ($scope.userDetails)?$scope.userDetails.userID:'';

if($scope.reportFilterRoot.amountDollar){
    $scope.reportFilterRoot.amountDollar = $scope.reportFilterRoot.amountDollar.replace(/\$/g, '');
}
if($scope.reportFilterRoot.account){
    $scope.reportFilterRoot.account = $scope.reportFilterRoot.account.replace(/\%/g, '');
}
if($scope.reportFilterRoot.agent){
    $scope.reportFilterRoot.agent = $scope.reportFilterRoot.agent.replace(/\%/g, '');
}
if($scope.reportFilterRoot.amountDollar){
    $scope.reportFilterRoot.amountDollar = $scope.reportFilterRoot.amountDollar.replace(/\%/g, '');
}
if($scope.reportFilterRoot.batchNo){
    $scope.reportFilterRoot.batchNo = $scope.reportFilterRoot.batchNo.replace(/\%/g, '');
}
if($scope.reportFilterRoot.bookingSource){
    $scope.reportFilterRoot.bookingSource = $scope.reportFilterRoot.bookingSource.replace(/\%/g, '');
}
if($scope.reportFilterRoot.checkNo){
    $scope.reportFilterRoot.checkNo = $scope.reportFilterRoot.checkNo.replace(/\%/g, '');
}
if($scope.reportFilterRoot.firstName){
    $scope.reportFilterRoot.firstName = $scope.reportFilterRoot.firstName.replace(/\%/g, '');
}
if($scope.reportFilterRoot.lastName){
    $scope.reportFilterRoot.lastName = $scope.reportFilterRoot.lastName.replace(/\%/g, '');
}
if($scope.reportFilterRoot.iata){
    $scope.reportFilterRoot.iata = $scope.reportFilterRoot.iata.replace(/\%/g, '');
}
if($scope.reportFilterRoot.propertyAddress){
    $scope.reportFilterRoot.propertyAddress = $scope.reportFilterRoot.propertyAddress.replace(/\%/g, '');
}
if($scope.reportFilterRoot.propertyBrand){
    $scope.reportFilterRoot.propertyBrand = $scope.reportFilterRoot.propertyBrand.replace(/\%/g, '');
}
if($scope.reportFilterRoot.propertyChain){
    $scope.reportFilterRoot.propertyChain = $scope.reportFilterRoot.propertyChain.replace(/\%/g, '');
}
if($scope.reportFilterRoot.propertyCity){
    $scope.reportFilterRoot.propertyCity = $scope.reportFilterRoot.propertyCity.replace(/\%/g, '');
}
if($scope.reportFilterRoot.propertyName){
    $scope.reportFilterRoot.propertyName = $scope.reportFilterRoot.propertyName.replace(/\%/g, '');
}
if($scope.reportFilterRoot.propertyPhone){
    $scope.reportFilterRoot.propertyPhone = $scope.reportFilterRoot.propertyPhone.replace(/\%/g, '');
}
if($scope.reportFilterRoot.propertyState){
    $scope.reportFilterRoot.propertyState = $scope.reportFilterRoot.propertyState.replace(/\%/g, '');
}
if($scope.reportFilterRoot.propertyZip){
    $scope.reportFilterRoot.propertyZip = $scope.reportFilterRoot.propertyZip.replace(/\%/g, '');
}
if($scope.reportFilterRoot.rateCode){
    $scope.reportFilterRoot.rateCode = $scope.reportFilterRoot.rateCode.replace(/\%/g, '');
}

if($state.current.name == "main.outstandingBookingReport-HotelsOnly"){
    var sendName = $scope.filteroutstandingStatusName($scope.reportFilterRoot.agentStatus);
    $scope.reportFilterRoot.status = $scope.reportFilterRoot.agentStatus + "|" + sendName;
}
if($state.current.name == "main.notProvidedReport"){
    $scope.reportFilterRoot.fromDate = "";
    $scope.reportFilterRoot.toDate = "";

}
/*if($state.current.name == 'main.home'){
$state.go('main.quickFindView');
if($scope.reportFilterRoot.unmatched){
$scope.reportFilterRoot.unmatched = "1";
}else{
$scope.reportFilterRoot.unmatched = "0";
}
if($scope.reportFilterRoot.matched){
$scope.reportFilterRoot.matched = "1";
}else{
$scope.reportFilterRoot.matched = "0";
}
if($scope.reportFilterRoot.unmatched1){
$scope.reportFilterRoot.unmatched1 = "1";
}else{
$scope.reportFilterRoot.unmatched1 = "0";
}
$scope.reportFilterRoot.recordSelect = $scope.reportFilterRoot.unmatched + $scope.reportFilterRoot.matched + $scope.reportFilterRoot.unmatched1;
$rootScope.bkupData = $scope.reportFilterRoot;
$timeout(function(){
$scope.$broadcast('callSelectedField',$scope.reportFilterRoot);
},500);
}*/

$scope.$broadcast('callSelectedField',$scope.reportFilterRoot);
$scope.filterDataSelected = angular.copy($scope.reportFilterRoot);

// -------------
/*if($state.current.name == "main.outstandingBookingReport-HotelsOnly"){
    // $scope.reportFilterRoot.status = $scope.reportFilterRoot.agentStatus;
    $scope.reportFilterRoot.status = ["O"];
}*/
// -------------
$scope.callBackDialog();
}  
/* ===== Left Panel select Criteria popup function ===== */

$scope.helpPopupFn = function(ev) {
    $mdDialog.show({
        contentElement: '#helpPopup',
        parent: angular.element(document.body),
        targetEvent: ev,
        clickOutsideToClose:true,
        fullscreen: false,
        skipHide: true
    }).finally(function() {
    });
};

/* ====================  Left panel Selected Search Box function ==================== */

$scope.DisableFieldUpdates = function(){
    $scope.allowRecordUpdate = false;

    $scope.dropDownList_Type_Enabled = false;
    $scope.dropDownList_IATA_Enabled = false;
    $scope.dropDownList_Branch_Enabled = false;
    $scope.dropDownList_Agent_Enabled = false;
    $scope.dropDownList_Account_Enabled = false;

    $scope.textBox_LastName_ReadOnly = true;
    $scope.textBox_FirstName_ReadOnly = true;
    $scope.textBox_InDate_ReadOnly = true;
    $scope.textBox_OutDate_ReadOnly = true;
    $scope.textBox_Nights_ReadOnly = true;
    $scope.textBox_Rooms_ReadOnly = true;
    $scope.textBox_Rate_ReadOnly = true;
    $scope.textBox_RateCurrency_ReadOnly = true;
    $scope.textBox_RateCode_ReadOnly = true;
    $scope.textBox_CorporateDiscount_ReadOnly = true;
    $scope.textBox_Confirmation_ReadOnly = true;

    $scope.textBox_PropertyChain_ReadOnly = true;
    $scope.textBox_PropertyName_ReadOnly = true;
    $scope.textBox_PropertyAddress1_ReadOnly = true;
    $scope.textBox_PropertyAddress2_ReadOnly = true;
    $scope.textBox_PropertyCity_ReadOnly = true;
    $scope.textBox_PropertyState_ReadOnly = true;
    $scope.textBox_PropertyZip_ReadOnly = true;
    $scope.textBox_PropertyCountry_ReadOnly = true;
    $scope.textBox_PropertyPhone_ReadOnly = true;
    $scope.textBox_PropertyFax_ReadOnly = true;

    $scope.textBox_HotelID_ReadOnly = true;
    $scope.textBox_CityCode_ReadOnly = true;
    $scope.textBox_BookingPCC_ReadOnly = true;
    $scope.textBox_AgentPCC_ReadOnly = true;

    $scope.textBox_DepositDate_ReadOnly = true;
    $scope.textBox_CheckNo_ReadOnly = true;
    $scope.textBox_CheckDate_ReadOnly = true;
    $scope.textBox_Tax1_ReadOnly = true;
    $scope.textBox_Commissions_ReadOnly = true;
    $scope.textBox_PayCurrency_ReadOnly = true;
}

$scope.EnableFieldUpdates = function(){
    $scope.allowRecordUpdate = false;

    /*if (($scope.userDetails.allowRecordUpdate === false) || ($scope.travelList.length && $scope.travelList[0]["payment_id"].toString() == "0")){
        return;
    }*/
    if ($scope.userDetails.allowRecordUpdate === false){
        return;
    }

    /*if($state.current.name == 'main.claimed-AllExcludingHotelAccount' || $state.current.name == 'main.claimedAllTypeAccount' || $state.current.name == 'main.claimed-AllHotelCarAndOther' || $state.current.name == 'main.claimedAllExcludingHotel' || $state.current.name == 'main.claimed-AllExcludingHotel-Agent' || $state.current.name == 'main.claimed-AllTypes-Agent' || $state.current.name == 'main.claimed-AllHotelCarAndOther-Agent' || $state.current.name == 'main.claimed-hotelonly-Agent' || $state.current.name == 'main.unclaimed-AllExcludingHotel-Agent' || $state.current.name == 'main.unclaimed-AllTypes' || $state.current.name == 'main.unclaimed-AllTypesHotelCarAndOther-Agent' || $state.current.name == 'main.unclaimed-hotelonly-Agent'){
        return;           
    }

    if($scope.userDetails.superUser == true){
        return;
    }*/


    $scope.batchReleased = $scope.travelList[0]["webStatus"].toString() == "1" ? true : false;
    
    if ($scope.travelList.length){

// if($scope.userDetails.superUser == true){                
    if(false){                
// $scope.GetUserPermissionsData.updatePayments_PostRelease_LastName = true;
// $scope.GetUserPermissionsData.updatePayments_PostRelease_FirstName = true;
// $scope.GetUserPermissionsData.updatePayments_PostRelease_Type = true;
// $scope.GetUserPermissionsData.updatePayments_PostRelease_InDate = true;
// $scope.GetUserPermissionsData.updatePayments_PostRelease_OutDate = true;
// $scope.GetUserPermissionsData.updatePayments_PostRelease_Nights = true;
// $scope.GetUserPermissionsData.updatePayments_PostRelease_Rooms = true;
// $scope.GetUserPermissionsData.updatePayments_PostRelease_Rate = true;
// $scope.GetUserPermissionsData.updatePayments_PostRelease_RateCode = true;
// $scope.GetUserPermissionsData.updatePayments_PostRelease_CorporateDiscount = true;
// $scope.GetUserPermissionsData.updatePayments_PostRelease_Confirmation = true;
// $scope.GetUserPermissionsData.updatePayments_PostRelease_IATA = true;
// $scope.GetUserPermissionsData.updatePayments_PostRelease_Branch = true;
// $scope.GetUserPermissionsData.updatePayments_PostRelease_Agent = true;
// $scope.GetUserPermissionsData.updatePayments_PostRelease_Account = true;
// $scope.GetUserPermissionsData.updatePayments_PostRelease_PropertyChain = true;
// $scope.GetUserPermissionsData.updatePayments_PostRelease_PropertyName = true;
// $scope.GetUserPermissionsData.updatePayments_PostRelease_PropertyAddress = true;
// $scope.GetUserPermissionsData.updatePayments_PostRelease_PropertyCity = true;
// $scope.GetUserPermissionsData.updatePayments_PostRelease_PropertyState = true;
// $scope.GetUserPermissionsData.updatePayments_PostRelease_PropertyZip = true;
// $scope.GetUserPermissionsData.updatePayments_PostRelease_PropertyCountry = true;
// $scope.GetUserPermissionsData.updatePayments_PostRelease_PropertyPhone = true;
// $scope.GetUserPermissionsData.updatePayments_PostRelease_PropertyFax = true;
// $scope.GetUserPermissionsData.updatePayments_PostRelease_HotelID = true;
// $scope.GetUserPermissionsData.updatePayments_PostRelease_CityCode = true;
// $scope.GetUserPermissionsData.updatePayments_PostRelease_BookingPCC = true;
// $scope.GetUserPermissionsData.updatePayments_PostRelease_AgentPCC = true;
// $scope.GetUserPermissionsData.updatePayments_PostRelease_DepositDate = true;
// $scope.GetUserPermissionsData.updatePayments_PostRelease_CheckNo = true;
// $scope.GetUserPermissionsData.updatePayments_PostRelease_CheckDate = true;
// $scope.GetUserPermissionsData.updatePayments_PostRelease_Tax1 = true;
// $scope.GetUserPermissionsData.updatePayments_PostRelease_Commissions = true;

$scope.allowRecordUpdate = true;
$scope.dropDownList_Type_Enabled = true;
$scope.allowRecordUpdate = true;
$scope.textBox_LastName_ReadOnly = false;
$scope.allowRecordUpdate = true;
$scope.textBox_FirstName_ReadOnly = false;
$scope.allowRecordUpdate = true;
$scope.textBox_InDate_ReadOnly = false;
$scope.allowRecordUpdate = true;
$scope.textBox_OutDate_ReadOnly = false;
$scope.allowRecordUpdate = true;
$scope.textBox_Nights_ReadOnly = false;
$scope.allowRecordUpdate = true;
$scope.textBox_Rooms_ReadOnly = false;
$scope.allowRecordUpdate = true;
$scope.textBox_Rate_ReadOnly = false;
$scope.textBox_RateCurrency_ReadOnly = false;
$scope.allowRecordUpdate = true;
$scope.textBox_RateCode_ReadOnly = false;
$scope.allowRecordUpdate = true;
$scope.textBox_CorporateDiscount_ReadOnly = false;
$scope.allowRecordUpdate = true;
$scope.textBox_Confirmation_ReadOnly = false;
$scope.allowRecordUpdate = true;
$scope.dropDownList_IATA_Enabled = true;
$scope.allowRecordUpdate = true;
$scope.dropDownList_Branch_Enabled = true;
$scope.allowRecordUpdate = true;
$scope.dropDownList_Agent_Enabled = true;
$scope.allowRecordUpdate = true;
$scope.dropDownList_Account_Enabled = true;
$scope.allowRecordUpdate = true;
$scope.textBox_PropertyChain_ReadOnly = false;
$scope.allowRecordUpdate = true;
$scope.textBox_PropertyName_ReadOnly = false;
$scope.allowRecordUpdate = true;
$scope.textBox_PropertyAddress1_ReadOnly = false;
$scope.allowRecordUpdate = true;
$scope.textBox_PropertyCity_ReadOnly = false;
$scope.allowRecordUpdate = true;
$scope.textBox_PropertyState_ReadOnly = false;
$scope.allowRecordUpdate = true;
$scope.textBox_PropertyZip_ReadOnly = false;
$scope.allowRecordUpdate = true;
$scope.textBox_PropertyCountry_ReadOnly = false;
$scope.allowRecordUpdate = true;
$scope.textBox_PropertyPhone_ReadOnly = false;
$scope.allowRecordUpdate = true;
$scope.textBox_PropertyFax_ReadOnly = false;
$scope.allowRecordUpdate = true;
$scope.textBox_HotelID_ReadOnly = false;
$scope.allowRecordUpdate = true;
$scope.textBox_CityCode_ReadOnly = false;
$scope.allowRecordUpdate = true;
$scope.textBox_BookingPCC_ReadOnly = false;
$scope.allowRecordUpdate = true;
$scope.textBox_AgentPCC_ReadOnly = false;
$scope.allowRecordUpdate = true;
$scope.textBox_DepositDate_ReadOnly = false;
$scope.allowRecordUpdate = true;
$scope.textBox_CheckNo_ReadOnly = false;
$scope.allowRecordUpdate = true;
$scope.textBox_CheckDate_ReadOnly = false;
$scope.allowRecordUpdate = true;
$scope.textBox_Tax1_ReadOnly = false;
$scope.allowRecordUpdate = true;
$scope.textBox_Commissions_ReadOnly = false;
$scope.textBox_PropertyAddress2_ReadOnly = false;
$scope.textBox_PayCurrency_ReadOnly = false;


}else{
    if ((!$scope.batchReleased && $scope.GetUserPermissionsData.updatePayments_Type) || ($scope.batchReleased && $scope.GetUserPermissionsData.updatePayments_PostRelease_Type)){
        $scope.allowRecordUpdate = true;
        $scope.dropDownList_Type_Enabled = true;
    }
    if ((!$scope.batchReleased && $scope.GetUserPermissionsData.updatePayments_LastName) || ($scope.batchReleased && $scope.GetUserPermissionsData.updatePayments_PostRelease_LastName)){
        $scope.allowRecordUpdate = true;
        $scope.textBox_LastName_ReadOnly = false;
    }
    if ((!$scope.batchReleased && $scope.GetUserPermissionsData.updatePayments_FirstName) || ($scope.batchReleased && $scope.GetUserPermissionsData.updatePayments_PostRelease_FirstName)){
        $scope.allowRecordUpdate = true;
        $scope.textBox_FirstName_ReadOnly = false;
    }
    if ((!$scope.batchReleased && $scope.GetUserPermissionsData.updatePayments_InDate) || ($scope.batchReleased && $scope.GetUserPermissionsData.updatePayments_PostRelease_InDate)){
        $scope.allowRecordUpdate = true;
        $scope.textBox_InDate_ReadOnly = false;
    }
    if ((!$scope.batchReleased && $scope.GetUserPermissionsData.updatePayments_OutDate) || ($scope.batchReleased && $scope.GetUserPermissionsData.updatePayments_PostRelease_OutDate)){
        $scope.allowRecordUpdate = true;
        $scope.textBox_OutDate_ReadOnly = false;
    }
    if ((!$scope.batchReleased && $scope.GetUserPermissionsData.updatePayments_Nights) || ($scope.batchReleased && $scope.GetUserPermissionsData.updatePayments_PostRelease_Nights)){
        $scope.allowRecordUpdate = true;
        $scope.textBox_Nights_ReadOnly = false;
    }
    if ((!$scope.batchReleased && $scope.GetUserPermissionsData.updatePayments_Rooms) || ($scope.batchReleased && $scope.GetUserPermissionsData.updatePayments_PostRelease_Rooms)){
        $scope.allowRecordUpdate = true;
        $scope.textBox_Rooms_ReadOnly = false;
    }
    if ((!$scope.batchReleased && $scope.GetUserPermissionsData.updatePayments_Rate) || ($scope.batchReleased && $scope.GetUserPermissionsData.updatePayments_PostRelease_Rate)){
        $scope.allowRecordUpdate = true;
        $scope.textBox_Rate_ReadOnly = false;
        $scope.textBox_RateCurrency_ReadOnly = false;
    }
    if ((!$scope.batchReleased && $scope.GetUserPermissionsData.updatePayments_RateCode) || ($scope.batchReleased && $scope.GetUserPermissionsData.updatePayments_PostRelease_RateCode)){
        $scope.allowRecordUpdate = true;
        $scope.textBox_RateCode_ReadOnly = false;
    }
    if ((!$scope.batchReleased && $scope.GetUserPermissionsData.updatePayments_CorporateDiscount) || ($scope.batchReleased && $scope.GetUserPermissionsData.updatePayments_PostRelease_CorporateDiscount)){
        $scope.allowRecordUpdate = true;
        $scope.textBox_CorporateDiscount_ReadOnly = false;
    }
    if ((!$scope.batchReleased && $scope.GetUserPermissionsData.updatePayments_Confirmation) || ($scope.batchReleased && $scope.GetUserPermissionsData.updatePayments_PostRelease_Confirmation)){
        $scope.allowRecordUpdate = true;
        $scope.textBox_Confirmation_ReadOnly = false;
    }
    if ((!$scope.batchReleased && $scope.GetUserPermissionsData.updatePayments_IATA) || ($scope.batchReleased && $scope.GetUserPermissionsData.updatePayments_PostRelease_IATA)){
        $scope.allowRecordUpdate = true;
        $scope.dropDownList_IATA_Enabled = true;
    }
    if ((!$scope.batchReleased && $scope.GetUserPermissionsData.updatePayments_Branch) || ($scope.batchReleased && $scope.GetUserPermissionsData.updatePayments_PostRelease_Branch)){
        $scope.allowRecordUpdate = true;
        $scope.dropDownList_Branch_Enabled = true;
    }
    if ((!$scope.batchReleased && $scope.GetUserPermissionsData.updatePayments_Agent) || ($scope.batchReleased && $scope.GetUserPermissionsData.updatePayments_PostRelease_Agent)){
        $scope.allowRecordUpdate = true;
        $scope.dropDownList_Agent_Enabled = true;
    }
    if ((!$scope.batchReleased && $scope.GetUserPermissionsData.updatePayments_Account) || ($scope.batchReleased && $scope.GetUserPermissionsData.updatePayments_PostRelease_Account)){
        $scope.allowRecordUpdate = true;
        $scope.dropDownList_Account_Enabled = true;
    }
    if ((!$scope.batchReleased && $scope.GetUserPermissionsData.updatePayments_PropertyChain) || ($scope.batchReleased && $scope.GetUserPermissionsData.updatePayments_PostRelease_PropertyChain)){
        $scope.allowRecordUpdate = true;
        $scope.textBox_PropertyChain_ReadOnly = false;
    }
    if ((!$scope.batchReleased && $scope.GetUserPermissionsData.updatePayments_PropertyName) || ($scope.batchReleased && $scope.GetUserPermissionsData.updatePayments_PostRelease_PropertyName)){
        $scope.allowRecordUpdate = true;
        $scope.textBox_PropertyName_ReadOnly = false;
    }
    if ((!$scope.batchReleased && $scope.GetUserPermissionsData.updatePayments_PropertyAddress) || ($scope.batchReleased && $scope.GetUserPermissionsData.updatePayments_PostRelease_PropertyAddress)){
        $scope.allowRecordUpdate = true;
        $scope.textBox_PropertyAddress1_ReadOnly = false;
        $scope.textBox_PropertyAddress2_ReadOnly = false;

    }
    if ((!$scope.batchReleased && $scope.GetUserPermissionsData.updatePayments_PropertyCity) || ($scope.batchReleased && $scope.GetUserPermissionsData.updatePayments_PostRelease_PropertyCity)){
        $scope.allowRecordUpdate = true;
        $scope.textBox_PropertyCity_ReadOnly = false;
    }
    if ((!$scope.batchReleased && $scope.GetUserPermissionsData.updatePayments_PropertyState) || ($scope.batchReleased && $scope.GetUserPermissionsData.updatePayments_PostRelease_PropertyState)){
        $scope.allowRecordUpdate = true;
        $scope.textBox_PropertyState_ReadOnly = false;
    }
    if ((!$scope.batchReleased && $scope.GetUserPermissionsData.updatePayments_PropertyZip) || ($scope.batchReleased && $scope.GetUserPermissionsData.updatePayments_PostRelease_PropertyZip)){
        $scope.allowRecordUpdate = true;
        $scope.textBox_PropertyZip_ReadOnly = false;
    }
    if ((!$scope.batchReleased && $scope.GetUserPermissionsData.updatePayments_PropertyCountry) || ($scope.batchReleased && $scope.GetUserPermissionsData.updatePayments_PostRelease_PropertyCountry)){
        $scope.allowRecordUpdate = true;
        $scope.textBox_PropertyCountry_ReadOnly = false;
    }
    if ((!$scope.batchReleased && $scope.GetUserPermissionsData.updatePayments_PropertyPhone) || ($scope.batchReleased && $scope.GetUserPermissionsData.updatePayments_PostRelease_PropertyPhone)){
        $scope.allowRecordUpdate = true;
        $scope.textBox_PropertyPhone_ReadOnly = false;
    }
    if ((!$scope.batchReleased && $scope.GetUserPermissionsData.updatePayments_PropertyFax) || (($scope.batchReleased) && $scope.GetUserPermissionsData.updatePayments_PostRelease_PropertyFax)){
        $scope.allowRecordUpdate = true;
        $scope.textBox_PropertyFax_ReadOnly = false;
    }
    if (((!$scope.batchReleased) && $scope.GetUserPermissionsData.updatePayments_HotelID) || (($scope.batchReleased) && $scope.GetUserPermissionsData.updatePayments_PostRelease_HotelID)){
        $scope.allowRecordUpdate = true;
        $scope.textBox_HotelID_ReadOnly = false;
    }
    if (((!$scope.batchReleased) && $scope.GetUserPermissionsData.updatePayments_CityCode) || (($scope.batchReleased) && $scope.GetUserPermissionsData.updatePayments_PostRelease_CityCode)){
        $scope.allowRecordUpdate = true;
        $scope.textBox_CityCode_ReadOnly = false;
    }
    if (((!$scope.batchReleased) && $scope.GetUserPermissionsData.updatePayments_BookingPCC) || (($scope.batchReleased) && $scope.GetUserPermissionsData.updatePayments_PostRelease_BookingPCC)){
        $scope.allowRecordUpdate = true;
        $scope.textBox_BookingPCC_ReadOnly = false;
    }
    if (((!$scope.batchReleased) && $scope.GetUserPermissionsData.updatePayments_AgentPCC) || (($scope.batchReleased) && $scope.GetUserPermissionsData.updatePayments_PostRelease_AgentPCC)){
        $scope.allowRecordUpdate = true;
        $scope.textBox_AgentPCC_ReadOnly = false;
    }
    if (((!$scope.batchReleased) && $scope.GetUserPermissionsData.updatePayments_DepositDate) || (($scope.batchReleased) && $scope.GetUserPermissionsData.updatePayments_PostRelease_DepositDate)){
        $scope.allowRecordUpdate = true;
        $scope.textBox_DepositDate_ReadOnly = false;
    }
    if (((!$scope.batchReleased) && $scope.GetUserPermissionsData.updatePayments_CheckNo) || (($scope.batchReleased) && $scope.GetUserPermissionsData.updatePayments_PostRelease_CheckNo)){
        $scope.allowRecordUpdate = true;
        $scope.textBox_CheckNo_ReadOnly = false;
    }
    if (((!$scope.batchReleased) && $scope.GetUserPermissionsData.updatePayments_CheckDate) || (($scope.batchReleased) && $scope.GetUserPermissionsData.updatePayments_PostRelease_CheckDate)){
        $scope.allowRecordUpdate = true;
        $scope.textBox_CheckDate_ReadOnly = false;
    }
    if (((!$scope.batchReleased) && $scope.GetUserPermissionsData.updatePayments_Tax1) || (($scope.batchReleased) && $scope.GetUserPermissionsData.updatePayments_PostRelease_Tax1)){
        $scope.allowRecordUpdate = true;
        $scope.textBox_Tax1_ReadOnly = false;
    }
    if (((!$scope.batchReleased) && $scope.GetUserPermissionsData.updatePayments_Commissions) || (($scope.batchReleased) && $scope.GetUserPermissionsData.updatePayments_PostRelease_Commissions)){
        $scope.allowRecordUpdate = true;
        $scope.textBox_Commissions_ReadOnly = false;
        $scope.textBox_PayCurrency_ReadOnly = false;
    }

}
}
}

/* ====================  Report ID Popup (Payments, Claims, Transfer and Dispote) function ==================== */

/* ====== Report ID page Global Variables  ===== */
$scope.api_url;
$scope.claimApiUrl = "";
$scope.updatePaymentApi;

$scope.editPaymentDataParams = {DataTableCommon:{param:{Draw:1,Start:0,length:500},ExportType:0}};

$scope.claimsHistoryParams = {
    DataTableCommon:{param:{Draw:1,Start:0,length:500},ExportType:0},
    useSplitDataFiles: ($scope.userDetails)?$scope.userDetails.useSplitDataFiles:'',
    userCapacity: $scope.capacity
};

$scope.paymentDataParams = {
    dataTableCommon:{param:{Draw:1,Start:0,length:500},ExportType:0},
    agent:($scope.userDetails)?$scope.userDetails.agent:'',
    account:($scope.userDetails)?$scope.userDetails.account:''
};

$scope.paymentHistoryDataParams = {DataTableCommon:{param:{Draw:1,Start:0,length:500},ExportType:0}};

// $scope.insertPaymentDataParams = {DataTableCommon:{param:{Draw:1,Start:0,length:10},ExportType:0}};
$scope.insertPaymentDataParams = {DataTableCommon:{param:{Draw:1,Start:0,length:50},ExportType:0}};

$scope.updatePaymentParams = {DataTableCommon:{param:{Draw:1,Start:0,length:500},ExportType:0}};

/* ===== Get Today's Date ===== */
$scope.getTodaysDate = function(){
    $http({
        url: SETTINGS[GLOBALS.ENV].apiUrl+"GetTodaysDate",
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
        }
    }).success(function(response) {
        $scope.toDaysDate = response;
        $rootScope.globalDateAndTime = response;
        d = new Date();
        var offset = -7;
        utc = d.getTime() + (d.getTimezoneOffset() * 60000);
        nd = new Date(utc + (3600000*offset));
        $rootScope.USTime = nd.toLocaleString();
        $rootScope.USTime = $rootScope.USTime.split(',');
        $rootScope.USTime = $rootScope.USTime[1];

    }).error(function(err,status) {
        $scope.errorView(err,status);
    });
}
/* ===== Get Today's Date ===== */

/* ====== Report ID page Global Variables  ===== */

/* ===== filter Date Format function ===== */
function filterDate(obj){

    var d = new Date();
    var tzName = d.toLocaleString('en', {timeZoneName:'short'}).split(' ').pop();

    var offset = new Date().getTimezoneOffset()*60*1000;

    var browserName = navigator.vendor;

    if(browserName == "Google Inc." || browserName == "Apple Computer, Inc."){
        var inDate = $filter('date')(obj.in_date, "MM/dd/yyyy");
        var outDate = $filter('date')(obj.out_date, "MM/dd/yyyy");
        var checkDate = $filter('date')(obj.check_date, "MM/dd/yyyy");
        var depositDate = $filter('date')(obj.deposit_date, "MM/dd/yyyy");
        var date = $filter('date')(obj.date, "MM/dd/yyyy");
        var claimDate = $filter('date')(obj.claimDate, "MM/dd/yyyy");


        if(tzName == "GMT+5:30"){
           /* if(obj.in_date){
                obj.in_date = new Date(Date.parse(obj.in_date)+offset);

            }
            if(obj.out_date){
                obj.out_date = new Date(Date.parse(obj.out_date)+offset);
            }
            if(obj.check_date){
                obj.check_date = new Date(Date.parse(obj.check_date)+offset);
            }
            if(obj.deposit_date){
                obj.deposit_date = new Date(Date.parse(obj.deposit_date)+offset);
            }
            if(obj.date){
                obj.date = new Date(Date.parse(obj.date)+offset);
            }
            if(obj.claimDate){
                obj.claimDate = new Date(Date.parse(obj.claimDate)+offset);
            }*/

            if(obj.in_date){
                obj.in_date = new Date(obj.in_date);
            }
            if(obj.out_date){
                obj.out_date = new Date(obj.out_date);
            }
            if(obj.check_date){
                obj.check_date = new Date(obj.check_date);
            }
            if(obj.deposit_date){
                obj.deposit_date = new Date(obj.deposit_date);
            }
            if(obj.date){
                obj.date = new Date(obj.date);
            }
            if(obj.claimDate){
                obj.claimDate = new Date(obj.claimDate);
            }

        }else{
            if(obj.in_date){
                obj.in_date = new Date(Date.parse(inDate)+offset);
            }
            if(obj.out_date){
                obj.out_date = new Date(Date.parse(outDate)+offset);
            }
            if(obj.check_date){
                obj.check_date = new Date(Date.parse(checkDate)+offset);
            }
            if(obj.deposit_date){
                obj.deposit_date = new Date(Date.parse(depositDate)+offset);
            }
            if(obj.date){
                obj.date = new Date(Date.parse(date)+offset);
            }
            if(obj.claimDate){
                obj.claimDate = new Date(Date.parse(claimDate)+offset);
            }
        }

    }else{
        if(obj.in_date){
            obj.in_date = new Date(obj.in_date);
        }
        if(obj.out_date){
            obj.out_date = new Date(obj.out_date);
        }
        if(obj.check_date){
            obj.check_date = new Date(obj.check_date);
        }
        if(obj.deposit_date){
            obj.deposit_date = new Date(obj.deposit_date);
        }
        if(obj.date){
            obj.date = new Date(obj.date);
        }
        if(obj.claimDate){
            obj.claimDate = new Date(obj.claimDate);
        }
    }

    return obj;
}
/* ===== filter Date Format function ===== */

/*$scope.GetClaimsHistory = function(){
$http({
url: SETTINGS[GLOBALS.ENV].apiUrl+"ClaimsHistory/GetClaimsHistory",
method: 'POST',
data: $scope.claimsHistoryParams,
headers: {
'Content-Type': 'application/json',
'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
}
}).success(function(response) {
if(response.ResposeCode == 2){
if(response.DataList.data){
$scope.historyDataList = response.DataList.data.filter(filterDate);
}
}
}).error(function(err,status) {
$scope.errorView(err,status);
});
}*/

$scope.updatePayments = function(){    
    $http({
        url: SETTINGS[GLOBALS.ENV].apiUrl+$scope.updatePaymentApi,
        method: 'POST',
        data: $scope.updatePaymentParams,
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
        }
    }).success(function(response) {
        $scope.callBackDialog('cancel');
        if(response.ResposeCode == 2){
            if(!$scope.showClaimMessage){
                toaster.pop('Success', '', response.ResponseMessage);
            }
        }else{
            toaster.pop('error', '', response.ResponseMessage);
        }
    }).error(function(err,status) {
        $scope.errorView(err,status);
    });
}

function deleteSomeCommentForClaimAndUnclaime(obj){
    if(obj.description){
        var containsDesc = obj.description.indexOf("Payment record data change:");
        if(containsDesc == -1){
            return obj;
        }        
    }
}

/* ===== GetPaymentsHistory for Getting Payment History ===== */

$scope.GetPaymentsHistory = function(){
    $scope.historyDataList = [];
    $scope.historyListPayment = [];
    $scope.showHistoryLoading = true;
    $http({
        url: SETTINGS[GLOBALS.ENV].apiUrl+"Payments/GetPaymentsHistory",
        method: 'POST',
        data: $scope.paymentHistoryDataParams,
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
        }
    }).success(function(response) {
        $scope.showHistoryLoading = false;
        if(response.DataList && response.DataList.data){
            $scope.historyListPayment = response.DataList.data;

            /*if($state.current.name == 'main.claimed-AllExcludingHotelAccount' || $state.current.name == 'main.claimedAllTypeAccount' || $state.current.name == 'main.claimed-AllHotelCarAndOther' || $state.current.name == 'main.claimedAllExcludingHotel' || $state.current.name == 'main.claimed-AllExcludingHotel-Agent' || $state.current.name == 'main.claimed-AllTypes-Agent' || $state.current.name == 'main.claimed-AllHotelCarAndOther-Agent' || $state.current.name == 'main.claimed-hotelonly-Agent' || $state.current.name == 'main.unclaimed-AllExcludingHotel-Agent' || $state.current.name == 'main.unclaimed-AllTypes' || $state.current.name == 'main.unclaimed-AllTypesHotelCarAndOther-Agent' || $state.current.name == 'main.unclaimed-hotelonly-Agent' || $state.current.name == 'main.home'){
                $scope.historyListPayment = response.DataList.data.filter(deleteSomeCommentForClaimAndUnclaime);
            }*/
        }
    }).error(function(err,status) {
        $scope.errorView(err,status);
    });
}

$scope.historyPopupFn = function(ev, id) {
//$scope.claimsHistoryParams.id = id;

// $scope.paymentHistoryDataParams.id = 2445804405;
// $scope.paymentHistoryDataParams.pid = 1;
// if(!$scope.mainObj.id){
//     $scope.travelList
// }

if($scope.mainObj.id){
    $scope.paymentHistoryDataParams.id = $scope.mainObj.id;    
}
else if($scope.mainObj.ID){
    $scope.paymentHistoryDataParams.id = $scope.mainObj.ID;    
}
if($scope.mainObj.payment_id){
    $scope.paymentHistoryDataParams.pid = $scope.mainObj.payment_id;    
}else if($scope.mainObj.Payment_ID){
    $scope.paymentHistoryDataParams.pid = $scope.mainObj.Payment_ID;    
}

if(!$scope.mainObj.payment_id){
    $scope.paymentHistoryDataParams.pid = $scope.travelList[0].payment_id;    
}


// $scope.paymentHistoryDataParams.pid = $scope.mainObj.Payment_ID;

$scope.GetPaymentsHistory();
$mdDialog.show({
    contentElement: '#history',
    parent: angular.element(document.body),
    targetEvent: ev,
    clickOutsideToClose:true,
    fullscreen: false,
    skipHide: true
}).finally(function() {
});
// $("#history").draggable();
};

/* ===== GetPaymentsHistory for Getting Payment History ===== */

$scope.checkAgentAccountObj = function(obj){
    var getID = (obj.agentID) ? obj.agentID.toString() :  obj.id.toString();
    if($scope.selectedPayments.length){
        return (getID === $scope.selectedPayments[0].agent.toString());
    }
}
function getAgentId(obj){
    if($scope.selectedPayments.length){
        if(obj.agentCode == $scope.selectedPayments[0].agent){
            return obj.id;
        }
    }
}
function getAccountIdClaim(obj){
    if($scope.selectedPayments.length){
        if(obj.accountCode == $scope.selectedPayments[0].account){
            return obj.id;
        }
    }
}
function getagentIdForTransfer(obj){
    if(obj.agentCode == $scope.toAgentModel[0].agentCode){
        return obj.id;
    }
}
function getacctIdForTransfer(obj){
    if(obj.accountCode == $scope.toAccountModel[0].accountCode){
        return obj.id;
    }   
}

/* ===== Insert Claims Records ===== */
$scope.insertClaimData = function(){
    $scope.disableDisputeAfterInstantClaim = true;
    var valid = false;
    $scope.getTodaysDate();
    $scope.showClaimMessage = true;
    $scope.insertPaymentDataParams.type = ($scope.transferOrClaim) ? 'C' : 'T';
    $scope.insertPaymentDataParamsNew.type = ($scope.transferOrClaim) ? 'C' : 'T';
// var depositeDate = $filter('date')($scope.selectedPayments[0].deposit_date, "dd/MM/yyyy");
var depositeDate = new Date($scope.selectedPayments[0].deposit_date);
var depositeDate1 = depositeDate;
depositeDate = Date.parse(depositeDate);
$scope.toDaysDate = new Date($scope.toDaysDate);
$scope.toDaysDate1 = new Date($scope.toDaysDate);
$scope.toDaysDate1 = $filter('date')($scope.toDaysDate1, "dd/MM/yyyy")
$scope.toDaysDate = Date.parse($scope.toDaysDate);

if($scope.businessCalListObj && $scope.businessCalListObj.DataList && $scope.businessCalListObj.DataList.data.length){
    for(var i = 0 ; i < $scope.businessCalListObj.DataList.data.length ; i++){
        var toDate = new Date($scope.businessCalListObj.DataList.data[i].toDate);
        var toDate1 = new Date($scope.businessCalListObj.DataList.data[i].toDate);
        toDate1 = $filter('date')(toDate1, "dd/MM/yyyy"); 
        toDate = Date.parse(toDate);
        $scope.insertPaymentDataParams.date = toDate1;
        $scope.insertPaymentDataParamsNew.date = toDate1;

        if(depositeDate < toDate){
            if($scope.businessCalListObj.DataList.data[i].agentClose == false || $scope.userDetails.capacity == 2 && $scope.businessCalListObj.DataList.data[i].accountingClose == false){
                if($scope.toDaysDate < toDate){
                    $scope.insertPaymentDataParams.date = $scope.toDaysDate1;
                    $scope.insertPaymentDataParamsNew.date = $scope.toDaysDate1;
                }else{
                    $scope.insertPaymentDataParams.date = toDate1;
                    $scope.insertPaymentDataParamsNew.date = toDate1;
                }
                break;
            }
        }
    }
}

/*===== Remove '$' sign. =====*/
if($scope.selectedPayments[0].claimAmount){
    $scope.selectedPayments[0].claimAmount = $scope.selectedPayments[0].claimAmount.replace(/\$/g, '');            
}
if($scope.selectedPayments[0].available){
    $scope.selectedPayments[0].available = $scope.selectedPayments[0].available.replace(/\$/g, '');            
}
if($scope.selectedPayments[0].amount){
    $scope.selectedPayments[0].amount = $scope.selectedPayments[0].amount.replace(/\$/g, '');
}

/*===== Request Parameters for insert claim(start) =====*/
// $scope.insertPaymentDataParams.currency = $scope.mainObj.net_pay_currency;
// $scope.insertPaymentDataParams.availamount = ($scope.selectedPayments.length && $scope.selectedPayments[0].available)?$scope.selectedPayments[0].available:($scope.selectedPayments[0].amount)?$scope.selectedPayments[0].amount:'';

//($scope.selectedPayments[0].agent.id) ? $scope.selectedPayments[0].agent.id :        
if($scope.insertPaymentDataParams.type == 'C'){
    var agntId = $scope.agentClaimList.filter(getAgentId);
    var accId = $scope.accountClaimList.filter(getAccountIdClaim);
    $scope.insertPaymentDataParams.agentID = ($scope.selectedPayments.length && $scope.selectedPayments[0].agent) ? agntId[0].id :'';
    $scope.insertPaymentDataParams.accountID = ($scope.selectedPayments.length && $scope.selectedPayments[0].account)?accId[0].id:'';
    $scope.insertPaymentDataParams.amount = ($scope.selectedPayments.length && $scope.selectedPayments[0].claimAmount)?$scope.selectedPayments[0].claimAmount:0;
    // $scope.insertPaymentDataParams.availamount = ($scope.selectedPayments.length && $scope.selectedPayments[0].available)?$scope.selectedPayments[0].available:($scope.selectedPayments[0].amount)?$scope.selectedPayments[0].amount:'';
    

    /*---- This below 1 parameter is removed from the request ----*/
    // $scope.insertPaymentDataParams.availamount = ($scope.selectedPayments.length && $scope.selectedPayments[0].available)?$scope.selectedPayments[0].available:($scope.selectedPayments[0].amount)?$scope.selectedPayments[0].amount:'';

}
if($scope.insertPaymentDataParams.type == 'T'){
    var agntIdForTransfer = $scope.toAgentList.filter(getagentIdForTransfer);
    var accIdForTransfer = $scope.toAccountList.filter(getacctIdForTransfer);
    $scope.insertPaymentDataParams.agentID = ($scope.selectedPayments.length && $scope.selectedPayments[0].agent) ?agntIdForTransfer[0].id :'';
    $scope.insertPaymentDataParams.accountID = ($scope.selectedPayments.length && $scope.selectedPayments[0].account)?accIdForTransfer[0].id:'';
    $scope.insertPaymentDataParams.amount = ($scope.selectedPayments.length && $scope.selectedPayments[0].amount)?$scope.selectedPayments[0].amount:0;
    $scope.insertPaymentDataParams.amount = $scope.insertPaymentDataParams.amount.replace(/\-/g, '');
    var amntNew = ($scope.selectedPayments.length && $scope.selectedPayments[0].amount)?$scope.selectedPayments[0].amount:0;
    // amntNew = amntNew.replace(/\-/g, '');
    $scope.insertPaymentDataParamsNew.amount = amntNew;
    
}

// return false;
/* -----------------------------------------------------------------New parameters------------------------------------------------------------------------------------------------------------ */
// $scope.insertPaymentDataParams.agentID = ($scope.selectedPayments.length && $scope.selectedPayments[0].agent) ? $scope.selectedPayments[0].agent :'';
// $scope.insertPaymentDataParams.accountID = ($scope.selectedPayments.length && $scope.selectedPayments[0].account)?($scope.selectedPayments[0].account.id) ? $scope.selectedPayments[0].account.id:$scope.selectedPayments[0].account:'';
/* ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */

/* -----------------------------------------------------------------Old parameters------------------------------------------------------------------------------------------------------------ */
/*$scope.insertPaymentDataParams.agentID = ($scope.formAgentAccountList.length && $scope.formAgentAccountList[0].agentCode) ? $scope.formAgentAccountList[0].agentCode :'';

$scope.insertPaymentDataParams.accountID = ($scope.formAgentAccountList.length && $scope.formAgentAccountList[0].accountCode)? $scope.formAgentAccountList[0].accountCode:'';*/
/* ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */

// $scope.insertPaymentDataParams.comment = ($scope.selectedPayments.length && $scope.selectedPayments[0].comments != "Select Comment")?$scope.selectedPayments[0].comments:($scope.selectedPayments[0].enteredComments.length)?$scope.selectedPayments[0].enteredComments:'';
$scope.insertPaymentDataParams.comment = ($scope.selectedPayments.length && $scope.selectedPayments[0].enteredComments)?$scope.selectedPayments[0].enteredComments:$scope.selectedPayments[0].comments;
$scope.insertPaymentDataParams.userID = $scope.userDetails.userID;
$scope.insertPaymentDataParams.useSplitDataFiles = $scope.userDetails.useSplitDataFiles;
$scope.insertPaymentDataParams.id = $scope.mainObj.id;


$scope.insertPaymentDataParamsNew.comment = ($scope.selectedPayments.length && $scope.selectedPayments[0].comments != "Select Comment")?$scope.selectedPayments[0].comments:($scope.selectedPayments[0].enteredComments.length)?$scope.selectedPayments[0].enteredComments:'';
$scope.insertPaymentDataParamsNew.userID = $scope.userDetails.userID;
$scope.insertPaymentDataParamsNew.useSplitDataFiles = $scope.userDetails.useSplitDataFiles;
$scope.insertPaymentDataParamsNew.id = $scope.mainObj.id;
$scope.insertPaymentDataParamsNew.currency = $scope.selectedPayments[0].net_pay_currency;

/*---- This below 1 parameter("$scope.insertPaymentDataParams.userCapacity") is removed from the request ----*/
// $scope.insertPaymentDataParams.userCapacity = $scope.userDetails.capacity;

// $scope.insertPaymentDataParams.availamount = ($scope.selectedPayments.length && $scope.selectedPayments[0].available)?$scope.selectedPayments[0].available:($scope.selectedPayments[0].amount)?$scope.selectedPayments[0].amount:0;
$scope.insertPaymentDataParams.currency = $scope.selectedPayments[0].net_pay_currency;

// return false;


/*===== Request Parameters for insert claim(end) =====*/   
var claimAmountTemp = Number($scope.selectedPayments[0].claimAmount);
var availableAmountTemp = Number($scope.selectedPayments[0].available);        
var transferAmount = Number($scope.selectedPayments[0].amount);

if($scope.insertPaymentDataParams.type == "C"){
    if(claimAmountTemp == undefined || claimAmountTemp == 'NaN' || claimAmountTemp == 0){
        toaster.pop('error', '', 'Enter An Amount To Claim.');
        $scope.callBackDialog('cancel');

        $scope.selectedPayments[0].claimAmount = "$"+$scope.selectedPayments[0].claimAmount;
        $scope.selectedPayments[0].available = "$"+$scope.selectedPayments[0].available;
        $scope.selectedPayments[0].amount = "$"+$scope.selectedPayments[0].amount;

        return;
    }
    
    var claimAmountDecimal = $scope.selectedPayments[0].claimAmount;
    claimAmountDecimal = claimAmountDecimal.replace(/\,/g, '');
    claimAmountDecimal = claimAmountDecimal.toString();
    //if(claimAmountDecimal.charAt(0) == '.'){

        //var count = (claimAmountDecimal.match(/./g) || []).length;
        var regexp = /^(\d+)?(\.\d{1,2})?$/i;
        var result = regexp.test(claimAmountDecimal);
        if(!result) {
            toaster.pop('error', '', 'Claim amount is not valid for money. Please re-enter.');
            $scope.callBackDialog('cancel');

            $scope.selectedPayments[0].claimAmount = "$"+$scope.selectedPayments[0].claimAmount;
            $scope.selectedPayments[0].available = "$"+$scope.selectedPayments[0].available;
            $scope.selectedPayments[0].amount = "$"+$scope.selectedPayments[0].amount;

            return;
        }
    //}
    //return false;
    // if(claimAmountDecimal.charAt(0) == '-'){
    //     toaster.pop('error', '', 'Negative amount.');
    //     $scope.callBackDialog('cancel');
    //     return;
    // }
// var claimAmountDoller = $scope.selectedPayments[0].claimAmount.indexOf('$');
var claimAmountDoller = claimAmountDecimal.indexOf('$');
if(claimAmountDoller != -1){
    toaster.pop('error', '', 'Claim amount is not valid for money. Please re-enter.');
    $scope.callBackDialog('cancel');

    $scope.selectedPayments[0].claimAmount = "$"+$scope.selectedPayments[0].claimAmount;
    $scope.selectedPayments[0].available = "$"+$scope.selectedPayments[0].available;
    $scope.selectedPayments[0].amount = "$"+$scope.selectedPayments[0].amount;

    return;
}
switch($scope.userDetails.capacity){
    case 1: 
    if(availableAmountTemp > 0){
        if(claimAmountTemp < 0){
            toaster.pop('error', '', 'Claim amount cannot be less than zero.');
            $scope.callBackDialog('cancel');

            $scope.selectedPayments[0].claimAmount = "$"+$scope.selectedPayments[0].claimAmount;
            $scope.selectedPayments[0].available = "$"+$scope.selectedPayments[0].available;
            $scope.selectedPayments[0].amount = "$"+$scope.selectedPayments[0].amount;

            return;
        }
        if(claimAmountTemp > availableAmountTemp){
            toaster.pop('error', '', 'Claim amount cannot be greater than available amount.');
            $scope.callBackDialog('cancel');

            $scope.selectedPayments[0].claimAmount = "$"+$scope.selectedPayments[0].claimAmount;
            $scope.selectedPayments[0].available = "$"+$scope.selectedPayments[0].available;
            $scope.selectedPayments[0].amount = "$"+$scope.selectedPayments[0].amount;

            return;
        }
    }

    if(availableAmountTemp < 0){
        if(claimAmountTemp < availableAmountTemp){
            toaster.pop('error', '', 'Can not claim more than the refund.');
            $scope.callBackDialog('cancel');

            $scope.selectedPayments[0].claimAmount = "$"+$scope.selectedPayments[0].claimAmount;
            $scope.selectedPayments[0].available = "$"+$scope.selectedPayments[0].available;
            $scope.selectedPayments[0].amount = "$"+$scope.selectedPayments[0].amount;

            return;
        }  
        if(claimAmountTemp > 0){
            toaster.pop('error', '', 'Claim amount can not be greater than zero.');
            $scope.callBackDialog('cancel');

            $scope.selectedPayments[0].claimAmount = "$"+$scope.selectedPayments[0].claimAmount;
            $scope.selectedPayments[0].available = "$"+$scope.selectedPayments[0].available;
            $scope.selectedPayments[0].amount = "$"+$scope.selectedPayments[0].amount;

            return;
        } 
    }

    if(availableAmountTemp == 0){
        toaster.pop('error', '', 'Available amount is zero. Claim cannot be made.');
        $scope.callBackDialog('cancel');

        $scope.selectedPayments[0].claimAmount = "$"+$scope.selectedPayments[0].claimAmount;
        $scope.selectedPayments[0].available = "$"+$scope.selectedPayments[0].available;
        $scope.selectedPayments[0].amount = "$"+$scope.selectedPayments[0].amount;

        return;
    }  
    break;  

    case 2:
    if(availableAmountTemp > 0){
        if(claimAmountTemp > availableAmountTemp){
            toaster.pop('error', '', 'Claim amount cannot be greater than available amount.');
            $scope.callBackDialog('cancel');

            $scope.selectedPayments[0].claimAmount = "$"+$scope.selectedPayments[0].claimAmount;
            $scope.selectedPayments[0].available = "$"+$scope.selectedPayments[0].available;
            $scope.selectedPayments[0].amount = "$"+$scope.selectedPayments[0].amount;

            return;
        }
    }    

    if(availableAmountTemp < 0){
        if(claimAmountTemp < availableAmountTemp){
            toaster.pop('error', '', 'Can not claim more than the refund.');
            $scope.callBackDialog('cancel');

            $scope.selectedPayments[0].claimAmount = "$"+$scope.selectedPayments[0].claimAmount;
            $scope.selectedPayments[0].available = "$"+$scope.selectedPayments[0].available;
            $scope.selectedPayments[0].amount = "$"+$scope.selectedPayments[0].amount;

            return;
        }
        if(claimAmountTemp > 0){
            toaster.pop('error', '', 'Claim amount can not be greater than zero.');
            $scope.callBackDialog('cancel');
            return;
        } 
    }    
    if(availableAmountTemp == 0){
        if(claimAmountTemp > 0){
            toaster.pop('error', '', 'Claim amount can not be greater than zero.');
            $scope.callBackDialog('cancel');

            $scope.selectedPayments[0].claimAmount = "$"+$scope.selectedPayments[0].claimAmount;
            $scope.selectedPayments[0].available = "$"+$scope.selectedPayments[0].available;
            $scope.selectedPayments[0].amount = "$"+$scope.selectedPayments[0].amount;

            return;
        } 
    }
    break;
    default:
    toaster.pop('error', '', 'Claiming is not allowed. Please contact your system representive for assistance.');
    $scope.callBackDialog('cancel');
    return;
}

if(!$scope.selectedPayments[0].agent){
    toaster.pop('error', '', 'Select an agent.');
    $scope.callBackDialog('cancel');

    $scope.selectedPayments[0].claimAmount = "$"+$scope.selectedPayments[0].claimAmount;
    $scope.selectedPayments[0].available = "$"+$scope.selectedPayments[0].available;
    $scope.selectedPayments[0].amount = "$"+$scope.selectedPayments[0].amount;

    return;
}else{
    $scope.selectedPayments[0].agent = Number($scope.selectedPayments[0].agent);
}

if(!$scope.selectedPayments[0].account){
    toaster.pop('error', '', 'Select an account.');
    $scope.callBackDialog('cancel');

    $scope.selectedPayments[0].claimAmount = "$"+$scope.selectedPayments[0].claimAmount;
    $scope.selectedPayments[0].available = "$"+$scope.selectedPayments[0].available;
    $scope.selectedPayments[0].amount = "$"+$scope.selectedPayments[0].amount;

    return;
}/*else{
$scope.getAgentAccountObj = $scope.accountClaimList.filter($scope.checkAgentAccountObj);
if(!$scope.getAgentAccountObj.length){
toaster.pop('error', '', 'Invalid account for agent.');
$scope.callBackDialog('cancel');
return;
}
}*/

if($scope.insertPaymentDataParams && $scope.insertPaymentDataParams.date){
    if($scope.insertPaymentDataParams.date.indexOf('/') != -1){
        var checkDate = $scope.insertPaymentDataParams.date.split('/');
        var checkDateSel = new Date(''+checkDate[1]+'/'+checkDate[0]+'/'+checkDate[2]+'');
    }else{
        var checkDateSel = new Date($scope.insertPaymentDataParams.date);
    }
}

if($scope.getfromDate){
    if($scope.getfromDate.indexOf('/') != -1){
        var checkFromDate = $scope.getfromDate.split('/');
        var checkDateFromSel = new Date(''+checkFromDate[1]+'/'+checkFromDate[0]+'/'+checkFromDate[2]+'');
    }else{
        var checkDateFromSel = new Date($scope.getfromDate);
    }
}

if(!$scope.insertPaymentDataParams.date){
    toaster.pop('error', '', 'Claim Date is not a valid date string.');
    $scope.callBackDialog('cancel');

    $scope.selectedPayments[0].claimAmount = "$"+$scope.selectedPayments[0].claimAmount;
    $scope.selectedPayments[0].available = "$"+$scope.selectedPayments[0].available;
    $scope.selectedPayments[0].amount = "$"+$scope.selectedPayments[0].amount;

    return;
}
if (Date.parse(checkDateSel) < Date.parse(checkDateFromSel)){
    toaster.pop('error', '', 'Claiming is closed for the date entered. Please re-enter.');
    $scope.callBackDialog('cancel');

    $scope.selectedPayments[0].claimAmount = "$"+$scope.selectedPayments[0].claimAmount;
    $scope.selectedPayments[0].available = "$"+$scope.selectedPayments[0].available;
    $scope.selectedPayments[0].amount = "$"+$scope.selectedPayments[0].amount;

    return;
}
}else{
    var transferAmount = $scope.selectedPayments[0].amount;
    transferAmount = transferAmount.replace(/\,/g, '');
    var claimAmountTemp = $scope.selectedPayments[0].claimed;
    claimAmountTemp = claimAmountTemp.replace(/\$/g, '');
    claimAmountTemp = Number(claimAmountTemp);

    transferAmount = transferAmount.toString();
    if(transferAmount == undefined || transferAmount == 'NaN' || transferAmount == 0){
        toaster.pop('error', '', 'Enter An Amount To transfer.');
        $scope.callBackDialog('cancel');

        $scope.selectedPayments[0].amount = "$"+$scope.selectedPayments[0].amount;
        $scope.selectedPayments[0].claimed = "$"+$scope.selectedPayments[0].claimed;
        return;
    }

    var regexp = /^(\d+)?(\.\d{1,2})?$/i;
    var resultTransfer = regexp.test(transferAmount);
    if(!resultTransfer){
    //if(transferAmount.charAt(0) == '.'){
        toaster.pop('error', '', 'Transfer amount is not valid for money. Please re-enter.');
        $scope.callBackDialog('cancel');

        $scope.selectedPayments[0].amount = "$"+$scope.selectedPayments[0].amount;
        $scope.selectedPayments[0].claimed = "$"+$scope.selectedPayments[0].claimed;

        return;
    }
    var transferAmountDoller = transferAmount.indexOf('$');
    if(transferAmountDoller != -1){
        toaster.pop('error', '', 'Transfer amount is not valid for money. Please re-enter.');
        $scope.callBackDialog('cancel');

        $scope.selectedPayments[0].amount = "$"+$scope.selectedPayments[0].amount;
        $scope.selectedPayments[0].claimed = "$"+$scope.selectedPayments[0].claimed;

        return;
    }
    switch($scope.userDetails.capacity){
        case 1: 
        if(claimAmountTemp > 0){
            if(transferAmount < 0){
                toaster.pop('error', '', 'Transfer amount cannot be less than zero.');
                $scope.callBackDialog('cancel');

                $scope.selectedPayments[0].amount = "$"+$scope.selectedPayments[0].amount;
                $scope.selectedPayments[0].claimed = "$"+$scope.selectedPayments[0].claimed;

                return;
            }
            if(transferAmount > claimAmountTemp){
                toaster.pop('error', '', 'Transfer amount cannot be greater than available amount.');
                $scope.callBackDialog('cancel');

                $scope.selectedPayments[0].amount = "$"+$scope.selectedPayments[0].amount;
                $scope.selectedPayments[0].claimed = "$"+$scope.selectedPayments[0].claimed;

                return;
            }
        }
        if(claimAmountTemp < 0){
            if(transferAmount < claimAmountTemp){
                toaster.pop('error', '', 'Can not transfer more than the refund.');
                $scope.callBackDialog('cancel');

                $scope.selectedPayments[0].amount = "$"+$scope.selectedPayments[0].amount;
                $scope.selectedPayments[0].claimed = "$"+$scope.selectedPayments[0].claimed;

                return;
            }
            if(transferAmount > 0){
                toaster.pop('error', '', 'Transfer amount cannot be greater than zero.');
                $scope.selectedPayments[0].amount = "$"+$scope.selectedPayments[0].amount;
                $scope.callBackDialog('cancel');


                if($scope.selectedPayments[0].amount.indexOf('$') == -1){
                    $scope.selectedPayments[0].amount = "$"+$scope.selectedPayments[0].amount;
                }
                $scope.selectedPayments[0].claimed = "$"+$scope.selectedPayments[0].claimed;
                
                return;
            }
        }
        if(claimAmountTemp == 0){
            toaster.pop('error', '', 'Claimed amount is zero. Transfer cannot be made.');
            $scope.callBackDialog('cancel');

            $scope.selectedPayments[0].amount = "$"+$scope.selectedPayments[0].amount;
            $scope.selectedPayments[0].claimed = "$"+$scope.selectedPayments[0].claimed;

            return;
        }
        break;
        case 2:
        /*if(claimAmountTemp > 0){
            if(transferAmount < claimAmountTemp){
                toaster.pop('error', '', 'Can not transfer more than the refund.');
                $scope.callBackDialog('cancel');
                return;
            }
            if(transferAmount > 0){
                toaster.pop('error', '', 'Transfer amount cannot be greater than zero.');
                $scope.selectedPayments[0].amount = "$"+$scope.selectedPayments[0].amount;
                $scope.callBackDialog('cancel');
                return;
            }
            if(claimAmountTemp == 0){
                if(transferAmount > 0){
                    toaster.pop('error', '', 'Transfer amount cannot be greater than zero.');
                    $scope.selectedPayments[0].amount = "$"+$scope.selectedPayments[0].amount;
                    $scope.callBackDialog('cancel');
                    return;
                }
            }
        }*/

        if(claimAmountTemp > 0){
            if(transferAmount > claimAmountTemp){
                toaster.pop('error', '', 'Transfer amount cannot be greater than claimed amount.');
                $scope.callBackDialog('cancel');

                $scope.selectedPayments[0].amount = "$"+$scope.selectedPayments[0].amount;
                // $scope.selectedPayments[0].claimed = "$"+$scope.selectedPayments[0].claimed;

                return;
            }
        }
        if(claimAmountTemp < 0){
            if(transferAmount < claimAmountTemp){
                toaster.pop('error', '', 'Can not transfer more than the refund.');
                $scope.callBackDialog('cancel');

                $scope.selectedPayments[0].amount = "$"+$scope.selectedPayments[0].amount;
                // $scope.selectedPayments[0].claimed = "$"+$scope.selectedPayments[0].claimed;

                return;
            }
            if(transferAmount > 0){
                toaster.pop('error', '', 'Claim amount cannot be greater than zero.');
                $scope.callBackDialog('cancel');

                $scope.selectedPayments[0].amount = "$"+$scope.selectedPayments[0].amount;
                // $scope.selectedPayments[0].claimed = "$"+$scope.selectedPayments[0].claimed;

                return;
            }
        }
        /*console.log("claimAmountTemp 2246 : ", claimAmountTemp);
        console.log("transferAmount 2247 : ", transferAmount);*/
        if(claimAmountTemp == 0){
            if(transferAmount > 0){
                toaster.pop('error', '', 'Transfer amount cannot be greater than zero.');
                $scope.selectedPayments[0].amount = "$"+$scope.selectedPayments[0].amount;
                $scope.callBackDialog('cancel');
                if($scope.selectedPayments[0].amount.indexOf('$') == -1){
                    $scope.selectedPayments[0].amount = "$"+$scope.selectedPayments[0].amount;
                }
                // $scope.selectedPayments[0].claimed = "$"+$scope.selectedPayments[0].claimed;

                return;
            }
        }

        break;
        default:
        toaster.pop('error', '', 'Transfers is not allowed. Please contact your system representive for assistance.');
        $scope.callBackDialog('cancel');

        $scope.selectedPayments[0].amount = "$"+$scope.selectedPayments[0].amount;
        // $scope.selectedPayments[0].claimed = "$"+$scope.selectedPayments[0].claimed;

        return;
    }  

    if(!$scope.selectedPayments[0].agent){
        toaster.pop('error', '', 'Select an agent.');
        $scope.callBackDialog('cancel');

        $scope.selectedPayments[0].amount = "$"+$scope.selectedPayments[0].amount;
        // $scope.selectedPayments[0].claimed = "$"+$scope.selectedPayments[0].claimed;

        return;
    }else{
        $scope.selectedPayments[0].agent = Number($scope.selectedPayments[0].agent);
    }

    /*agent1*/
/*if(!$scope.selectedPayments[0].agent1){
toaster.pop('error', '', 'Select an agent.');
$scope.callBackDialog('cancel');
return;
}else{
$scope.selectedPayments[0].agent1 = Number($scope.selectedPayments[0].agent1);
}*/

if(!$scope.selectedPayments[0].account){
    toaster.pop('error', '', 'Select an account.');
    $scope.callBackDialog('cancel');

    $scope.selectedPayments[0].amount = "$"+$scope.selectedPayments[0].amount;
    // $scope.selectedPayments[0].claimed = "$"+$scope.selectedPayments[0].claimed;

    return;
}/*else{
$scope.getAgentAccountObj = $scope.formAgentAccountList.filter($scope.checkAgentAccountObj);
if(!$scope.getAgentAccountObj.length){
toaster.pop('error', '', 'Invalid account for agent.');
$scope.callBackDialog('cancel');
return;
}
}*/


if($scope.insertPaymentDataParams && $scope.insertPaymentDataParams.date){
    if($scope.insertPaymentDataParams.date.indexOf('/') != -1){
        var checkDate = $scope.insertPaymentDataParams.date.split('/');
        var checkDateSel = new Date(''+checkDate[1]+'/'+checkDate[0]+'/'+checkDate[2]+'');
    }else{
        var checkDateSel = new Date($scope.insertPaymentDataParams.date);
    }
}

if($scope.getfromDate){
    if($scope.getfromDate.indexOf('/') != -1){
        var checkFromDate = $scope.getfromDate.split('/');
        var checkDateFromSel = new Date(''+checkFromDate[1]+'/'+checkFromDate[0]+'/'+checkFromDate[2]+'');
    }else{
        var checkDateFromSel = new Date($scope.getfromDate);
    }
}    

if(!$scope.insertPaymentDataParams.date){
    toaster.pop('error', '', 'Transfer Date is not a valid date string.');
    $scope.callBackDialog('cancel');

    $scope.selectedPayments[0].amount = "$"+$scope.selectedPayments[0].amount;
    // $scope.selectedPayments[0].claimed = "$"+$scope.selectedPayments[0].claimed;

    return;
}

if (Date.parse(checkDateSel) < Date.parse(checkDateFromSel)){
    toaster.pop('error', '', 'Claiming is closed for the date entered. Please re-enter.');
    $scope.callBackDialog('cancel');

    $scope.selectedPayments[0].amount = "$"+$scope.selectedPayments[0].amount;
    // $scope.selectedPayments[0].claimed = "$"+$scope.selectedPayments[0].claimed;

    return;
}

// -----
}

/*if($scope.insertPaymentDataParams.type == 'T'){

    var checkMinuseSign = $scope.insertPaymentDataParams.amount.toString();
    if(checkMinuseSign.charAt(0) != '-'){
        $scope.insertPaymentDataParams.amount = "-" + $scope.insertPaymentDataParams.amount;
    }
    
}*/

if($scope.insertPaymentDataParamsNew.type == 'T'){

    var checkMinuseSign = $scope.insertPaymentDataParamsNew.amount.toString();
    if(checkMinuseSign.charAt(0) != '-'){
        $scope.insertPaymentDataParamsNew.amount = "-" + $scope.insertPaymentDataParamsNew.amount;
    }
    
}

$http({
    url: SETTINGS[GLOBALS.ENV].apiUrl+"ClaimsHistory/InsertClaimData",
    method: 'POST',
    data: $scope.insertPaymentDataParams,
    headers: {
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
    }
}).success(function(response) {
    $scope.callBackDialog('cancel');
    if(response.ResposeCode == 2){
        if($scope.showClaimMessage){
            toaster.pop('Success', '', response.ResponseMessage);    
        }
        

// ****
// $scope.updatePaymentParams.id = 2446167595;
// $scope.updatePaymentParams.payment_id = 271678043;


/*===== Request Parameters for update payment(start) =====*/


// $scope.updatePaymentParams.payment_id = $scope.mainObj.payment_id;
var net_acc_comm = Number($scope.selectedPayments[0].net_act_comm);
$scope.updatePaymentParams.original_net_act_comm = null;

// $scope.updatePaymentParams.original_net_act_comm = ($scope.selectedPayments.length && $scope.selectedPayments[0].net_act_comm)?net_acc_comm:'';
// $scope.updatePaymentParams.agent = ($scope.selectedPayments.length && $scope.selectedPayments[0].agent)?$scope.selectedPayments[0].agent:'';
// $scope.updatePaymentParams.account = ($scope.selectedPayments.length && $scope.selectedPayments[0].account)?$scope.selectedPayments[0].account:'';

$scope.updatePaymentParams.id = $scope.mainObj.id;
/*if($state.current.name == "main.home"){
    $scope.updatePaymentParams.id = $scope.mainObj.id;
}*/
$scope.updatePaymentParams.userID = ($scope.userDetails)?$scope.userDetails.userID:null;
$scope.updatePaymentParams.first_name = ($scope.selectedPayments.length && $scope.selectedPayments[0].first_name)?$scope.selectedPayments[0].first_name:null;
$scope.updatePaymentParams.last_name = ($scope.selectedPayments.length && $scope.selectedPayments[0].last_name)?$scope.selectedPayments[0].last_name:null;
$scope.updatePaymentParams.prop_name = ($scope.selectedPayments.length && $scope.selectedPayments[0].prop_name)?$scope.selectedPayments[0].prop_name:null;

/* ---- Send below parameter blank in updatePayment / updatePayments ---- */

$scope.updatePaymentParams.in_date = null;
$scope.updatePaymentParams.out_date = null;
$scope.updatePaymentParams.confirmation = null;
$scope.updatePaymentParams.net_act_comm = null;
$scope.updatePaymentParams.nights = null;
$scope.updatePaymentParams.rooms = null;
$scope.updatePaymentParams.prop_chain = null;
$scope.updatePaymentParams.prop_address1 = null;
$scope.updatePaymentParams.prop_address2 = null;
$scope.updatePaymentParams.prop_city = null;
$scope.updatePaymentParams.prop_state = null;
$scope.updatePaymentParams.prop_zip = null;
$scope.updatePaymentParams.prop_country = null;
$scope.updatePaymentParams.prop_phone = null;
$scope.updatePaymentParams.prop_fax = null;
$scope.updatePaymentParams.invoice_num = null;
$scope.updatePaymentParams.ta_sys_id = null;
$scope.updatePaymentParams.ta_prop_id = null;
$scope.updatePaymentParams.bkg_src = null;
$scope.updatePaymentParams.net_rate = null;
$scope.updatePaymentParams.branch = null;
$scope.updatePaymentParams.agent = null;
$scope.updatePaymentParams.iata = null;
$scope.updatePaymentParams.pay_source = null;
$scope.updatePaymentParams.net_pay_currency = null;
$scope.updatePaymentParams.deposit_date = null;
$scope.updatePaymentParams.check_date = null;
$scope.updatePaymentParams.booking_id = null;
$scope.updatePaymentParams.payment_id = null;
$scope.updatePaymentParams.tax1 = null;
$scope.updatePaymentParams.prop_city_code = null;
$scope.updatePaymentParams.hotel_id = null;
$scope.updatePaymentParams.comments = null;
$scope.updatePaymentParams.check_num = null;
$scope.updatePaymentParams.pnr_id = null;
$scope.updatePaymentParams.bookingPCC = null;
$scope.updatePaymentParams.agentPCC = null;
$scope.updatePaymentParams.gdsStatus = null;
$scope.updatePaymentParams.rateCode = null;
$scope.updatePaymentParams.cd = null;
$scope.updatePaymentParams.rateCurrency = null;
$scope.updatePaymentParams.type = null;
$scope.updatePaymentParams.dept_id = null;
$scope.updatePaymentParams.prop_email = null;
$scope.updatePaymentParams.prop_id = null;
$scope.updatePaymentParams.back_office = null;
$scope.updatePaymentParams.account = null;



$scope.setAllUpdateParams = false;

// $scope.updatePaymentParams.account = ($scope.selectedPayments.length && $scope.selectedPayments[0].account)?$scope.selectedPayments[0].account:'';

/*===== Request Parameters for update payment(end) =====*/

if($scope.userDetails.useSplitDataFiles == true){
    $scope.updatePaymentApi = "Payment/UpdatePayment";
}else{
    $scope.updatePaymentApi = "Payments/UpdatePayments";
}
// return false;
// $scope.updatePayments();
$scope.updatePaymentData();
$scope.callBackDialog('cancel');
$scope.callBackDialog('cancel');
$scope.setAllUpdateParams = true;
$scope.GetPaymentDataForEdit();

}else{
    toaster.pop('error', '', response.ResponseMessage);
    $scope.callBackDialog('cancel');
}

}).error(function(err,status) {
    $scope.errorView(err,status);
});



/*---------------------------- New Insert claim data --------------------------------------*/

if($scope.insertPaymentDataParams.type == "T"){

    $http({
        url: SETTINGS[GLOBALS.ENV].apiUrl+"ClaimsHistory/InsertClaimData",
        method: 'POST',
        data: $scope.insertPaymentDataParamsNew,
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
        }
    }).success(function(response) {
        $scope.callBackDialog('cancel');
        if(response.ResposeCode == 2){
            if($scope.showClaimMessage){
                toaster.pop('Success', '', response.ResponseMessage);    
            }
        }
    });    

}    
/*-------------------------------------------------------------------*/

}

/***** Update Payment Data Main Popup *****/
$scope.updatePaymentData = function(){
    if($scope.setAllUpdateParams == true){
        if($scope.travelList[0].net_act_comm){
            $scope.travelList[0].net_act_comm = $scope.travelList[0].net_act_comm.replace(/\$/g, '');
        }
        if($scope.travelList[0].net_rate){
            $scope.travelList[0].net_rate = $scope.travelList[0].net_rate.replace(/\$/g, '');
        }
        if($scope.travelList[0].tax1){
            $scope.travelList[0].tax1 = $scope.travelList[0].tax1.replace(/\$/g, '');
        }
        if($scope.travelList[0].available){
            $scope.travelList[0].available = $scope.travelList[0].available.replace(/\$/g, '');
        }

        /* ===== Comman parameters for both updatePayment and updatePayments  =====*/

        $scope.updatePaymentParams.id = $scope.mainObj.id;
        $scope.updatePaymentParams.last_name = ($scope.travelList[0].last_name)?$scope.travelList[0].last_name:null;
        $scope.updatePaymentParams.first_name = ($scope.travelList[0].first_name)?$scope.travelList[0].first_name:null;
        $scope.updatePaymentParams.in_date = $filter('date')($scope.travelList[0].in_date, "dd/MM/yyyy");
        $scope.updatePaymentParams.out_date = $filter('date')($scope.travelList[0].out_date, "dd/MM/yyyy");
        $scope.updatePaymentParams.confirmation = ($scope.travelList[0].confirmation)?$scope.travelList[0].confirmation:null;
        $scope.updatePaymentParams.net_act_comm = ($scope.travelList[0].net_act_comm)?$scope.travelList[0].net_act_comm:null;
        /*if($scope.updatePaymentParams.net_act_comm.indexOf(",") != -1){
            $scope.updatePaymentParams.net_act_comm = $scope.updatePaymentParams.net_act_comm.replace(/\,/g, '');
            $scope.updatePaymentParams.net_act_comm = ($scope.updatePaymentParams.net_act_comm)?$scope.updatePaymentParams.net_act_comm:null;
        }else{
            $scope.updatePaymentParams.net_act_comm = ($scope.updatePaymentParams.net_act_comm)?$scope.updatePaymentParams.net_act_comm:null;
        }*/
        $scope.updatePaymentParams.nights = ($scope.travelList[0].nights)?$scope.travelList[0].nights:null;
        $scope.updatePaymentParams.rooms = ($scope.travelList[0].rooms)?$scope.travelList[0].rooms:null;
        $scope.updatePaymentParams.prop_chain = ($scope.travelList[0].prop_chain)?$scope.travelList[0].prop_chain:null;
        $scope.updatePaymentParams.prop_name = ($scope.travelList[0].prop_name)?$scope.travelList[0].prop_name:null;
        $scope.updatePaymentParams.prop_address1 = ($scope.travelList[0].prop_address1)?$scope.travelList[0].prop_address1:null;
        $scope.updatePaymentParams.prop_address2 = ($scope.travelList[0].prop_address2)?$scope.travelList[0].prop_address2:null;
        $scope.updatePaymentParams.prop_city = ($scope.travelList[0].prop_city)?$scope.travelList[0].prop_city:null;
        $scope.updatePaymentParams.prop_state = ($scope.travelList[0].prop_state)?$scope.travelList[0].prop_state:null;
        $scope.updatePaymentParams.prop_zip = ($scope.travelList[0].prop_zip)?$scope.travelList[0].prop_zip:null;
        $scope.updatePaymentParams.prop_country = ($scope.travelList[0].prop_country)?$scope.travelList[0].prop_country:null;
        $scope.updatePaymentParams.prop_phone = ($scope.travelList[0].prop_phone)?$scope.travelList[0].prop_phone:'';
        $scope.updatePaymentParams.prop_fax = ($scope.travelList[0].prop_fax)?$scope.travelList[0].prop_fax:'';
        $scope.updatePaymentParams.userID = ($scope.userDetails)?$scope.userDetails.userID:null;
        $scope.updatePaymentParams.invoice_num = ($scope.travelList[0].invoice_num)?$scope.travelList[0].invoice_num:null;
        $scope.updatePaymentParams.ta_sys_id = ($scope.travelList[0].ta_sys_id)?$scope.travelList[0].ta_sys_id:null;
        $scope.updatePaymentParams.ta_prop_id = ($scope.travelList[0].hotel_id)?$scope.travelList[0].hotel_id:null;
        $scope.updatePaymentParams.bkg_src = ($scope.travelList[0].bkg_src)?$scope.travelList[0].bkg_src:null;
        $scope.updatePaymentParams.net_rate = ($scope.travelList[0].net_rate)?$scope.travelList[0].net_rate:0;
        $scope.updatePaymentParams.branch = ($scope.travelList[0].branch)?$scope.travelList[0].branch:null;
        $scope.updatePaymentParams.agent = ($scope.travelList[0].agent)?$scope.travelList[0].agent:null;
        $scope.updatePaymentParams.iata = ($scope.travelList[0].iata)?$scope.travelList[0].iata:null;
        $scope.updatePaymentParams.pay_source = ($scope.travelList[0].pay_source)?$scope.travelList[0].pay_source:null;
        $scope.updatePaymentParams.net_pay_currency = ($scope.travelList[0].net_pay_currency)?$scope.travelList[0].net_pay_currency:null;
        $scope.updatePaymentParams.deposit_date = $filter('date')($scope.travelList[0].deposit_date, "dd/MM/yyyy");
        $scope.updatePaymentParams.check_date = $filter('date')($scope.travelList[0].check_date, "dd/MM/yyyy");            
        $scope.updatePaymentParams.booking_id = ($scope.travelList[0].booking_id)?$scope.travelList[0].booking_id:null;
        $scope.updatePaymentParams.payment_id = $scope.mainObj.payment_id;
        $scope.updatePaymentParams.tax1 = ($scope.travelList[0].tax1)?$scope.travelList[0].tax1:null;
        $scope.updatePaymentParams.prop_city_code = ($scope.travelList[0].cityCode)?$scope.travelList[0].cityCode:null;
        $scope.updatePaymentParams.hotel_id = ($scope.travelList[0].hotel_id)?$scope.travelList[0].hotel_id:null;
        $scope.updatePaymentParams.comments = null;
        $scope.updatePaymentParams.check_num = ($scope.travelList[0].check_num)?$scope.travelList[0].check_num:null;
        $scope.updatePaymentParams.pnr_id = ($scope.travelList[0].pnr_id)?$scope.travelList[0].pnr_id:null;

        // $scope.updatePaymentParams.extra1 = ($scope.travelList[0].bookingPCC)?$scope.travelList[0].bookingPCC:null;
        $scope.updatePaymentParams.bookingPCC = ($scope.travelList[0].bookingPCC)?$scope.travelList[0].bookingPCC:null;

        //$scope.updatePaymentParams.extra2 = ($scope.travelList[0].agentPCC)?$scope.travelList[0].agentPCC:null;
        $scope.updatePaymentParams.agentPCC = ($scope.travelList[0].agentPCC)?$scope.travelList[0].agentPCC:null;

        // $scope.updatePaymentParams.extra3 = ($scope.travelList[0].gdsStatus)?$scope.travelList[0].gdsStatus:null;
        $scope.updatePaymentParams.gdsStatus = ($scope.travelList[0].gdsStatus)?$scope.travelList[0].gdsStatus:null;

        // $scope.updatePaymentParams.extra4 = ($scope.travelList[0].rateCode)?$scope.travelList[0].rateCode:null;
        $scope.updatePaymentParams.rateCode = ($scope.travelList[0].rateCode)?$scope.travelList[0].rateCode:null;

        // $scope.updatePaymentParams.extra5 = ($scope.travelList[0].cd)?$scope.travelList[0].cd:null;
        $scope.updatePaymentParams.cd = ($scope.travelList[0].cd)?$scope.travelList[0].cd:null;

        // $scope.updatePaymentParams.local_currency = ($scope.travelList[0].rateCurrency)?$scope.travelList[0].rateCurrency:null;
        $scope.updatePaymentParams.rateCurrency = ($scope.travelList[0].rateCurrency)?$scope.travelList[0].rateCurrency:null;

        // $scope.updatePaymentParams.cts_type = $scope.reportFilterRoot.unit;
        // $scope.updatePaymentParams.type = $scope.reportFilterRoot.unit;
        $scope.updatePaymentParams.type = $scope.travelList[0].cts_type;
        $scope.updatePaymentParams.cts_type = $scope.travelList[0].cts_type;

        $scope.updatePaymentParams.dept_id = null;
        $scope.updatePaymentParams.prop_email = null;
        $scope.updatePaymentParams.prop_id = null;
        $scope.updatePaymentParams.back_office = null;

        if($state.current.name == "main.matureBookingsReport" || $state.current.name == "main.matureBookingsReportByAgent-CarsOnly" || $state.current.name == "main.matureBookingsReportByAgent-HotelsOnly"){
            $scope.updatePaymentParams.bookingID = ($scope.travelList[0].bookingID)?$scope.travelList[0].bookingID:null;         
        }


        /* ======================================================================= */


// $scope.updatePaymentParams.pay_remarks = ($scope.travelList[0].pay_remarks)?$scope.travelList[0].pay_remarks:null;

$scope.updatePaymentParams.account = null;

/* -- Old code for account dropdown -- */
/*if(!$scope.query.AccountselectedItem){
    console.log("2597 new updated : ", $scope.query.descriptionText);
    if($scope.getDataSelected.length){
        console.log("2599");
        console.log("$scope.getDataSelected : ", $scope.getDataSelected);
        $scope.updatePaymentParams.account = $scope.getDataSelected[0].accountCode;
    }else{
        return false;    
    }        
}else{
    console.log("2606");
    console.log("$scope.getDataSelected : ", $scope.query.AccountselectedItem.accountCode);
    $scope.updatePaymentParams.account = $scope.query.AccountselectedItem.accountCode;
}*/
/* -- New code for account dropdown -- */
var setAccCodeAccount = $scope.query.descriptionText;
if(!setAccCodeAccount){
    toaster.pop('error', '', 'Please select an account.');
    return false;
}else{
    setAccCodeAccount = setAccCodeAccount.split(" - ");
    setAccCodeAccount = setAccCodeAccount[0];
    $scope.updatePaymentParams.account = setAccCodeAccount;
}

}
if($scope.userDetails.useSplitDataFiles == true){
// $scope.updatePaymentParams.user_id = ($scope.userDetails)?$scope.userDetails.userID:'';
$scope.updatePaymentParams.net_est_comm = null; 
$scope.updatePaymentParams.payment_last_name = null;
$scope.updatePaymentParams.payment_first_name = null;
$scope.updatePaymentParams.tax2 = null;
$scope.updatePaymentParams.tax3 = null;
$scope.updatePaymentApi = "Payment/UpdatePayment";
}else{
// $scope.updatePaymentParams.userid = ($scope.userDetails)?$scope.userDetails.userID:'';
$scope.updatePaymentParams.bkg_prop_chain = null;
$scope.updatePaymentParams.checkName = null;
$scope.updatePaymentApi = "Payments/UpdatePayments";
}

$scope.updatePayments();
$scope.callBackDialog('cancel');

}

function getFullAgentName(obj){
    if($scope.selectedPayments.length){
        if(obj.agentCode == $scope.selectedPayments[0].agent){
            return obj;
        }
    }
}
function getFullAccountName(obj){
    if($scope.selectedPayments.length){
        if(obj.accountCode == $scope.selectedPayments[0].account){
            return obj;
        }
    }
}
function getFullAgentNameTransfer(obj){
    if(obj.agentCode == $scope.toAgentModel[0].agentCode){
        return obj;
    }
}
function getFullAccountNameTransfer(obj){
    if(obj.accountCode == $scope.toAccountModel[0].accountCode){
        return obj;
    }
}

function selectDefaultCommentValueClaims(obj){
    if(obj.useAsDefault == true){
        return obj.comment;   
    }
}

function addCommas(nStr){

    if(nStr.charAt(0) == "$"){
        nStr = nStr.replace(/\$/g, '');
    }
    if(nStr < 0){
        var offset1 = nStr.length % 4;
        return nStr.substring(0, offset1) + nStr.substring(offset1).replace(/([0-9]{4})/g, ",$1");
    }
    var offset = nStr.length % 3;
    if (offset == 0)
        return nStr.substring(0, offset) + nStr.substring(offset).replace(/([0-9]{3})(?=[0-9]+)/g, "$1,");
    else
        return nStr.substring(0, offset) + nStr.substring(offset).replace(/([0-9]{3})/g, ",$1");
}


$scope.getClaims = function(){
    $scope.commentList = [];
    $scope.selectedPayments = [];
    $scope.agentClaimList = [];
    $scope.accountClaimList = [];
    $scope.paymentDataParams.userID=($scope.userDetails && $scope.userDetails.userID)?$scope.userDetails.userID:'';
    $http({
        url: SETTINGS[GLOBALS.ENV].apiUrl+$scope.claimApiUrl,
        method: 'POST',
        data: $scope.paymentDataParams,
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
        }
    }).success(function(response) {
        if(response.DataModel.GetPaymentDataForClaims_selected_payment.DataList){
            if(response.DataModel.GetPaymentDataForClaims_selected_payment.DataList && response.DataModel.GetPaymentDataForClaims_selected_payment.DataList.data && response.DataModel.GetPaymentDataForClaims_selected_payment.DataList.data.length){
                $scope.selectedPayments = response.DataModel.GetPaymentDataForClaims_selected_payment.DataList.data;  
// $scope.selectedPayments[0].comments = "Claimed by Agent";
if($scope.selectedPayments.length){
    if($scope.selectedPayments[0].net_act_comm){
        if($scope.selectedPayments[0].net_act_comm.charAt(0) == "$"){
            $scope.selectedPayments[0].net_act_comm = $scope.selectedPayments[0].net_act_comm.replace(/\$/g, '');
            $scope.selectedPayments[0].net_act_comm = Number($scope.selectedPayments[0].net_act_comm).toFixed(2);
            $scope.selectedPayments[0].net_act_comm = addCommas($scope.selectedPayments[0].net_act_comm);
            $scope.selectedPayments[0].net_act_comm = "$"+$scope.selectedPayments[0].net_act_comm;
        }
    }
    if($scope.selectedPayments[0].available || $scope.selectedPayments[0].available == ""){
        $scope.selectedPayments[0].available = Number($scope.selectedPayments[0].available).toFixed(2);
        $scope.selectedPayments[0].available = addCommas($scope.selectedPayments[0].available);
        $scope.selectedPayments[0].available = "$"+$scope.selectedPayments[0].available;

    }
    
    if($scope.selectedPayments[0].claimAmount){
        $scope.selectedPayments[0].claimAmount = Number($scope.selectedPayments[0].claimAmount).toFixed(2);
        $scope.selectedPayments[0].claimAmount = addCommas($scope.selectedPayments[0].claimAmount);
        $scope.selectedPayments[0].claimAmount = "$"+$scope.selectedPayments[0].claimAmount;
    }

    if($scope.selectedPayments[0].agent){
        $scope.selectedPayments[0].agent = ""
    }
    if($scope.selectedPayments[0].account){
        $scope.selectedPayments[0].account = "";
    }
}   
}
if(response.DataModel.GetPaymentDataForClaims_comment_entries.DataList){
    if(response.DataModel.GetPaymentDataForClaims_comment_entries.DataList.data.length){
        $scope.commentList = response.DataModel.GetPaymentDataForClaims_comment_entries.DataList.data;
        var selectCommentForClaim = response.DataModel.GetPaymentDataForClaims_comment_entries.DataList.data.filter(selectDefaultCommentValueClaims);
        
        if($scope.selectedPayments.length){
            if(selectCommentForClaim.length){
                $scope.selectedPayments[0].comments = selectCommentForClaim[0].comment;
            }
        }
    }   
}
if(response.DataModel.GetPaymentDataForClaims_list_of_agents.DataList){
    if(response.DataModel.GetPaymentDataForClaims_list_of_agents.DataList.data.length){
        $scope.agentClaimList = [];
        $scope.agentClaimList = response.DataModel.GetPaymentDataForClaims_list_of_agents.DataList.data;  
        if($scope.agentClaimList.length == 1){
            $scope.selectedPayments[0].agent = $scope.agentClaimList[0].agentCode;
        }else{
            $scope.selectedPayments[0].agent = "";
        }
        $scope.tempAgentList = angular.copy($scope.agentClaimList);
    }   
    if($scope.tempAgentList){
        if($scope.selectedPayments.length){
            $scope.displayAgentName = $scope.tempAgentList.filter(getFullAgentName);    
            if($scope.displayAgentName.length){
                $scope.showAgentName = $scope.displayAgentName[0].agentCode + " - " + $scope.displayAgentName[0].lastName + ", " + $scope.displayAgentName[0].firstName;
            }
        }

    }
    
    if($scope.selectedPayments.length){
        if($scope.selectedPayments[0].agent){
            $scope.getAccountAccordingToAgent($scope.selectedPayments[0].agent);    
        }
    }
    
}

if(response.DataModel.GetPaymentDataForClaims_list_of_accounts.DataModel){
    $scope.accountClaimList = [];
    $scope.accountClaimList = response.DataModel.GetPaymentDataForClaims_list_of_accounts.DataModel.agentAccountData;  
    $scope.tempAccountList = angular.copy($scope.accountClaimList);
    if($scope.tempAccountList){
        $scope.displayAccountName = $scope.tempAccountList.filter(getFullAccountName);
        if($scope.displayAccountName.length){
            $scope.showAccountName = $scope.displayAccountName[0].accountCode + " - " + $scope.displayAccountName[0].description;
        }
    }
}

if(response.DataModel.GetPaymentDataForClaims_business_calendar && response.DataModel.GetPaymentDataForClaims_business_calendar.DataList){

    $scope.businessCalListObj = response.DataModel.GetPaymentDataForClaims_business_calendar;
    if($scope.selectedPayments.length){
        var depositeDate = new Date($scope.selectedPayments[0].deposit_date);
        if(!$scope.selectedPayments[0].claimAmount){
            $scope.selectedPayments[0].available = $scope.selectedPayments[0].available.replace(/\$/g, '');
            $scope.selectedPayments[0].available = $scope.selectedPayments[0].available.replace(/\,/g, '');
            $scope.selectedPayments[0].claimAmount = Number($scope.selectedPayments[0].available).toFixed(2);
            $scope.selectedPayments[0].claimAmount = addCommas($scope.selectedPayments[0].claimAmount);
            $scope.selectedPayments[0].available = addCommas($scope.selectedPayments[0].available);
            $scope.selectedPayments[0].claimAmount = "$"+$scope.selectedPayments[0].claimAmount;
            $scope.selectedPayments[0].available = "$"+$scope.selectedPayments[0].available;

        }
    }
    var depositeDate1 = depositeDate;
    $scope.claimMonth = '';

    depositeDate = Date.parse(depositeDate);
    $scope.toDaysDate = new Date($scope.toDaysDate);
    $scope.toDaysDate1 = new Date($scope.toDaysDate);
    $scope.toDaysDate1 = $filter('date')($scope.toDaysDate1, "dd/MM/yyyy")
    $scope.toDaysDate = Date.parse($scope.toDaysDate);
    for(var i = 0 ; i < $scope.businessCalListObj.DataList.data.length ; i++){
// var toDate = $filter('date')($scope.businessCalListObj.DataList.data[i].toDate, "dd/MM/yyyy");
var toDate = new Date($scope.businessCalListObj.DataList.data[i].toDate);
var toDate1 = new Date($scope.businessCalListObj.DataList.data[i].toDate);
var fromDate = new Date($scope.businessCalListObj.DataList.data[i].fromDate);
fromDate = $filter('date')(fromDate, "MM/dd/yyyy");
$scope.getfromDate = angular.copy(fromDate);
toDate1 = $filter('date')(toDate1, "MM/dd/yyyy");

toDate = Date.parse(toDate);

$scope.insertPaymentDataParams.date = toDate1;

if(depositeDate > toDate){
    continue;
}
// if(depositeDate > toDate){

    if($scope.businessCalListObj.DataList.data[i].agentClose == false || ($scope.userDetails.capacity == 2 && $scope.businessCalListObj.DataList.data[i].accountingClose == false)){
        $scope.claimMonth = $scope.businessCalListObj.DataList.data[i].description;
        $scope.currentPeriod = $scope.claimMonth + ":" + fromDate + " - " + toDate1;

        if($scope.toDaysDate < toDate){
            $scope.insertPaymentDataParams.date = $scope.toDaysDate1;
        }else{
            $scope.insertPaymentDataParams.date = toDate1;
        }
        break;
    }
// }
} 
}
}

}).error(function(err,status) {
    $scope.errorView(err,status);
});
}

function getAccountId(obj){
    if($scope.agentClaimList.length > 1){
        if(obj.agentCode == $scope.value){
            return obj.id;
        }
    }else{
        return obj.id;
    }
}

$scope.getFullAccountNameForAgentItSelf = function(value){
    $scope.tempAccountList = angular.copy($scope.accountClaimList);
    if($scope.tempAccountList){
        $scope.displayAccountName1 = $scope.tempAccountList.filter(getFullAccountName);
        if($scope.displayAccountName1.length){
            $scope.showAccountName = $scope.displayAccountName1[0].accountCode + " - " + $scope.displayAccountName1[0].description;
        }    
    }
}

$scope.AccountForAgentObj = {
    DataTableCommon:{param:{Draw:1,Start:0,Length:50},ExportType:0},
};
$scope.getAccountAccordingToAgent = function(value){
    $scope.value = value;
    var agentId = "";
    if($scope.agentClaimList.length){
        agentId = $scope.agentClaimList.filter(getAccountId);
    }
    $scope.showAgentName = "";

    if($scope.tempAgentList){
        $scope.displayAgentName = $scope.tempAgentList.filter(getFullAgentName);
        if($scope.displayAgentName.length){
            $scope.showAgentName = $scope.displayAgentName[0].agentCode + " - " + $scope.displayAgentName[0].lastName + ", " + $scope.displayAgentName[0].firstName;
        }
    }

    if(value){
        if(agentId && agentId.length){
            $scope.AccountForAgentObj.agentID = agentId[0].id;
        }
    }else{
        $scope.AccountForAgentObj.corpID = ($scope.userDetails && $scope.userDetails.corpID)?$scope.userDetails.corpID:'';
        $scope.AccountForAgentObj.agent =  ($scope.userDetails && $scope.userDetails.agent)?$scope.userDetails.agent:'';    
    }

    $http({
        url: SETTINGS[GLOBALS.ENV].apiUrl+"Agent/GetAgentAccountsForClaims",
        method: 'POST',
        data: $scope.AccountForAgentObj,
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
        }
    }).success(function(response) {
        if(response.DataModel && response.DataModel.agentAccountData.length){

            $scope.accountClaimList = response.DataModel.agentAccountData;
            for(var i = 0 ; i < response.DataModel.agentAccountData.length ; i++){
                if(response.DataModel.agentAccountData[i].defaultAccountForAgent == 1){
                    if($scope.selectedPayments.length){
                        $scope.selectedPayments[0].account = response.DataModel.agentAccountData[i].accountCode;        
                    }
                }else{
                    if($scope.selectedPayments.length){
                        $scope.selectedPayments[0].account = response.DataModel.agentAccountData[0].accountCode;    
                    }
                }
            }
            if($scope.accountClaimList){
                $scope.showAccountName = "";
                $scope.displayAccountName = $scope.accountClaimList.filter(getFullAccountName);
                if($scope.displayAccountName){
                    $scope.showAccountName = $scope.displayAccountName[0].accountCode + " - " + $scope.displayAccountName[0].description;
                }
            }
        }else{
            $scope.selectedPayments[0].account = "";
            $scope.showAccountName = "";
        }
    }).error(function(err,status) {
        $scope.errorView(err,status);
    });
}

function getAccountIdTransfer(obj){
    if(obj.agentCode == $scope.valueForTransfer){
        return obj.id;
    }
}

$scope.AccountForAgentObjTransfer = {
    DataTableCommon:{param:{Draw:1,Start:0,Length:50},ExportType:0},
};

$scope.getAccountAccordingToAgentTransfer = function(value){
    $scope.valueForTransfer = value;
    var agentId = $scope.toAgentList.filter(getAccountIdTransfer);
    $scope.showAgentName = "";
    if($scope.tempAgentListTransfer){
        $scope.displayAgentName = $scope.tempAgentListTransfer.filter(getFullAgentNameTransfer);
        if($scope.displayAgentName.length){
            $scope.showAgentName = $scope.displayAgentName[0].agentCode + " - " + $scope.displayAgentName[0].lastName + ", " + $scope.displayAgentName[0].firstName;
        }
    }

    if(value){
        $scope.AccountForAgentObjTransfer.agentID = agentId[0].id;
    }else{
        $scope.AccountForAgentObjTransfer.corpID = ($scope.userDetails && $scope.userDetails.corpID)?$scope.userDetails.corpID:'';
        $scope.AccountForAgentObjTransfer.agent =  ($scope.userDetails && $scope.userDetails.agent)?$scope.userDetails.agent:'';    
    }

    $http({
        url: SETTINGS[GLOBALS.ENV].apiUrl+"Agent/GetAgentAccountsForClaims",
        method: 'POST',
        data: $scope.AccountForAgentObjTransfer,
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
        }
    }).success(function(response) {
        if(response.DataModel && response.DataModel.agentAccountData.length){
            $scope.toAccountList = [];
            $scope.toAccountList = response.DataModel.agentAccountData;
            for(var i = 0 ; i < response.DataModel.agentAccountData.length ; i++){
                if(response.DataModel.agentAccountData[i].defaultAccountForAgent == 1){
                    $scope.toAccountModel[0].accountCode = response.DataModel.agentAccountData[i].accountCode;    
                }else{
                    $scope.toAccountModel[0].accountCode = response.DataModel.agentAccountData[0].accountCode;    
                }
            }
            if($scope.toAccountList){
                $scope.showAccountName = "";
                $scope.displayAccountName = $scope.toAccountList.filter(getFullAccountNameTransfer);
                if($scope.displayAccountName.length){
                    $scope.showAccountName = $scope.displayAccountName[0].accountCode + " - " + $scope.displayAccountName[0].description;
                }
            }
        }
    }).error(function(err,status) {
        $scope.errorView(err,status);
    });
}

$scope.getClaimsHistory = function(){
    $scope.reponseHistory = [];
    $scope.CTHistoryLoading = true;
    $http({
        url: SETTINGS[GLOBALS.ENV].apiUrl+"ClaimsHistory/GetClaimsHistory",
        method: 'POST',
        data: $scope.claimsHistoryParams,
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
        }
    }).success(function(response) {
        $scope.CTHistoryLoading = false;
        if(response.DataList){
            if(response.DataList.data.length){
                $scope.reponseHistory = response.DataList.data.filter(filterDate);
            }
        }
    }).error(function(err,status) {
        $scope.errorView(err,status);
    });
}
$scope.transferOrClaim = "";
$scope.insertClaimDataPopup = function(ev, flag){

    /*if(!$scope.selectedPayments[0].agent){
        toaster.pop('error', '', 'Select an agent.');
        $scope.callBackDialog('cancel');

        $scope.selectedPayments[0].claimAmount = "$"+$scope.selectedPayments[0].claimAmount;
        $scope.selectedPayments[0].available = "$"+$scope.selectedPayments[0].available;
        $scope.selectedPayments[0].amount = "$"+$scope.selectedPayments[0].amount;

        return;
    }*/

    $scope.transferOrClaim = flag;
    
    if($scope.transferOrClaim == false){
        $scope.selectedPayments[0].claimAmount = $scope.selectedPayments[0].amount;
    }
// $scope.insertClaimDataPopupTitle = "A payment of " + $scope.selectedPayments[0].claimAmount + " is being claimed to agent: " + $scope.selectedPayments[0].agent + ", account: " + $scope.selectedPayments[0].account + ". This will be applied to the " + $scope.claimMonth + " statement.";

if($scope.transferOrClaim == false){

    if(!$scope.selectedPayments[0].claimAmount){
        toaster.pop('error', '', 'Please Enter Amount.');
        return;
    }

    var AC = ($rootScope.selectedPaymentsForAgentSelected && $rootScope.selectedPaymentsForAgentSelected.agentCode)?$rootScope.selectedPaymentsForAgentSelected.agentCode : "";
    var LN = ($rootScope.selectedPaymentsForAgentSelected && $rootScope.selectedPaymentsForAgentSelected.lastName)?$rootScope.selectedPaymentsForAgentSelected.lastName : "";
    var FN = ($rootScope.selectedPaymentsForAgentSelected && $rootScope.selectedPaymentsForAgentSelected.firstName)?$rootScope.selectedPaymentsForAgentSelected.firstName : "";
    $scope.fromAgentLabelTransfer = AC + " - " + LN + ", " + FN;
    
    if($scope.fromAgentLabelForTransfer){
        // if($scope.selectedPayments[0].claimAmount){

            if(!$scope.fromAgentLabelForTransfer){
                toaster.pop('error', '', 'Please Select From Agent.');
                return;
            }

            if(!$scope.selectedPayments[0].accountCode){
                toaster.pop('error', '', 'Please Select From Account.');
                return;
            }

            if(!$scope.showAgentName){
                toaster.pop('error', '', 'Please Select To Agent.');
                return;
            }

            if(!$scope.showAccountName){
                toaster.pop('error', '', 'Please Select To Account.');
                return;
            }

            $scope.insertClaimDataPopupTitle = "A Transfer of " + (($scope.selectedPayments.length && $scope.selectedPayments[0].claimAmount)?$scope.selectedPayments[0].claimAmount:'$0.00') + " from agent: " + $scope.fromAgentLabelForTransfer + ", account: " + $scope.selectedPayments[0].accountCode + " to agent: " + $scope.showAgentName + ", account: " + $scope.showAccountName +  ". This will be applied to the " + $scope.claimMonth + " statement.";            
        // }
    }else{

        // if($scope.selectedPayments[0].claimAmount){
            if(!$scope.fromAgentLabelTransfer){
                toaster.pop('error', '', 'Please Select From Agent.');
                return;
            }

            if(AC == "" || LN == "" || FN == ""){
                toaster.pop('error', '', 'Please Select From Agent.');
                return;
            }

            if(!$scope.selectedPayments[0].accountCode){
                toaster.pop('error', '', 'Please Select From Account.');
                return;
            }

            if(!$scope.showAgentName){
                toaster.pop('error', '', 'Please Select To Agent.');
                return;
            }

            if(!$scope.showAccountName){
                toaster.pop('error', '', 'Please Select To Account.');
                return;
            }

            $scope.insertClaimDataPopupTitle = "A Transfer of " + (($scope.selectedPayments.length && $scope.selectedPayments[0].claimAmount)?$scope.selectedPayments[0].claimAmount:'$0.00') + " from agent: " + $scope.fromAgentLabelTransfer + ", account: " + $scope.selectedPayments[0].accountCode + " to agent: " + $scope.showAgentName + ", account: " + $scope.showAccountName +  ". This will be applied to the " + $scope.claimMonth + " statement.";        
        // }
    }

}else{

    if($scope.selectedPayments.length && !$scope.selectedPayments[0].claimAmount){
        toaster.pop('error', '', 'Please Enter Amount.');
        return;
    }

    if(!$scope.showAgentName){
        toaster.pop('error', '', 'Please Select Agent.');
        return;
    }

    if(!$scope.showAccountName){
        toaster.pop('error', '', 'Please Select Account.');
        return;
    }

    var cmLine = ($scope.claimMonth)?$scope.claimMonth:'';
    $scope.insertClaimDataPopupTitle = "A payment of " + (($scope.selectedPayments.length && $scope.selectedPayments[0].claimAmount)?$scope.selectedPayments[0].claimAmount:'$0.00') + " is being claimed to agent: " + $scope.showAgentName + ", account: " + $scope.showAccountName + ". This will be applied to the " + cmLine + " statement.";
}



$mdDialog.show({
    contentElement: '#insertConfirmPopup',
    parent: angular.element(document.body),
    targetEvent: ev,
    clickOutsideToClose:true,
    fullscreen: false,
    skipHide: true
}).finally(function() {
});
// $("#insertConfirmPopup").draggable();
}
/* ===== Insert Claims Records ===== */

/* ===== Get Claims Records ===== */
$scope.claimAndTransferFn = function(ev,transfer) {
// $scope.paymentDataParams.id = 2446164847;

$scope.paymentDataParams.id = $scope.mainObj.id;
$scope.paymentDataParams.pid = $scope.mainObj.paymentID;

if($state.current.name == "main.home"){
    $scope.paymentDataParams.id = $scope.mainObj.id;
    $scope.paymentDataParams.pid = $scope.mainObj.paymentID;    
}


$scope.paymentDataParams.account = ($scope.userDetails && $scope.userDetails.account)?$scope.userDetails.account:'';
$scope.paymentDataParams.agent = ($scope.userDetails && $scope.userDetails.agent)?$scope.userDetails.agent:'';
// $scope.paymentDataParams.account = "101";
// $scope.paymentDataParams.agent = "101";

if(transfer){
    if($scope.userDetails.useSplitDataFiles == true){
        $scope.claimApiUrl = "Payment/GetPaymentDataForEdit";
    }else{
        $scope.claimApiUrl = "Payments/GetPaymentsDataForEdit";
    }
}else{
    if($scope.userDetails.useSplitDataFiles == true){
        $scope.claimApiUrl = "Payment/GetPaymentDataForClaims";
    }else{
        $scope.claimApiUrl = "Payments/GetPaymentsDataForClaims";
    }    
}


// $scope.claimsHistoryParams.id = 2445804405;
if($scope.mainObj.id){
    $scope.claimsHistoryParams.id = $scope.mainObj.id;    
}else if($scope.mainObj.ID || $scope.mainObj.ID >= 0){
    $scope.claimsHistoryParams.id = $scope.mainObj.ID;    
}

$scope.claimsHistoryParams.useSplitDataFiles = $scope.userDetails.useSplitDataFiles;
$scope.claimsHistoryParams.userCapacity = $scope.userDetails.capacity;



$scope.getClaims();
$scope.getClaimsHistory();

$mdDialog.show({
    contentElement: '#claimAndTransfer',
    parent: angular.element(document.body),
    targetEvent: ev,
    clickOutsideToClose:true,
    fullscreen: false,
    skipHide: true
}).finally(function() {
});
// $("#claimAndTransfer").draggable();
};

$scope.transferApiUrl;
$scope.transferDataParams = {DataTableCommon:{param:{Draw:1,Start:0,length:500},ExportType:0}};

function selectDefaultCommentValueTransfer(obj){
    if(obj.useAsDefault == true){
        return obj.comment;   
    }
}

function getAgentCodeFromList(obj){
    /*if(obj.agentCode == $scope.selectedPayments[0].agentCode){
        return obj.agentCode;
    }*/
    if($scope.mainObj){
        if(obj.agentCode == $scope.mainObj.agentCode){
            return obj.agentCode;
        }
    }
}

function filterForSelectAgent(obj){

    if($scope.selectedPayments.length){

        if(obj.agentCode == $scope.selectedPayments[0].claimedAgent){

            return obj.agentCode;
        }
        else{

            if(obj.agentCode == $scope.selectedPayments[0].agent){

                return obj.agentCode;
            }
        }
    }
}
$scope.getTransfers = function(){
    $scope.commentList = [];
    $scope.selectedPayments = [];
    $scope.formAgentAccountList = [];
    $scope.toAgentList = [];
    $scope.toAccountList = [];
    $scope.showAgent = true;
    $rootScope.selectedPaymentsForAgentSelected = "";
    $scope.transferDataParams.userID=($scope.userDetails && $scope.userDetails.userID)?$scope.userDetails.userID:'';
    $http({
        url: SETTINGS[GLOBALS.ENV].apiUrl+$scope.transferApiUrl,
        method: 'POST',
        data: $scope.transferDataParams,
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
        }
    }).success(function(response) {

        if(response.DataModel.GetSelectedPaymentForTransfers.DataList){
            if(response.DataModel.GetSelectedPaymentForTransfers.DataList.data.length){
                $scope.selectedPayments = response.DataModel.GetSelectedPaymentForTransfers.DataList.data;
                var commentsObj = response.DataModel.GetSelectedPaymentForTransfers.DataList.data;  
// $scope.selectedPayments[0].comments = 'Claimed by Agent';
}   
}
if(response.DataModel.GetCommentEntries.DataList){
    if(response.DataModel.GetCommentEntries.DataList.data.length){
        $scope.commentList = response.DataModel.GetCommentEntries.DataList.data;
        var selectCommentForTransfer = response.DataModel.GetCommentEntries.DataList.data.filter(selectDefaultCommentValueTransfer);
        
        if(selectCommentForTransfer.length){
            if($scope.selectedPayments.length){
                $scope.selectedPayments[0].comments = selectCommentForTransfer[0].comment;
            }
        }
    }   
}
if(response.DataModel.GetListOfAgentsAccountsClaimed.DataList){
    if(!response.DataModel.GetListOfAgentsAccountsClaimed.DataList.data.length){
        // console.log("3302 in if");
        if(response.DataModel.GetSelectedPaymentForTransfers.DataList.data.length){
            var dtToSelectAgent = response.DataModel.GetSelectedPaymentForTransfers.DataList.data;
            $rootScope.selectedPaymentsForAgentSelected = dtToSelectAgent[0].claimedAgentDetail;
            $scope.selectedPayments[0].accountCode = dtToSelectAgent[0].claimedAccountDetail;
            var psh = {'agentCode':dtToSelectAgent[0].claimedAgentDetail};
            $scope.formAgentAccountList.push(psh);
            $rootScope.selectedPaymentsForAgentSelected = $scope.formAgentAccountList[0];
            
        }
    }
    if(response.DataModel.GetListOfAgentsAccountsClaimed.DataList.data.length){
        $scope.formAgentAccountList = response.DataModel.GetListOfAgentsAccountsClaimed.DataList.data;  
        // $scope.selectedPaymentsForAgentSelected = angular.copy($scope.selectedPayments[0]);
        // console.log('3317 $scope.selectedPaymentsForAgentSelected : ', $scope.selectedPaymentsForAgentSelected, " | ", $scope.formAgentAccountList);
        $scope.sumOfClaimedAmountTransfer = 0;

        for(var i=0; i< $scope.formAgentAccountList.length;i++){
            if($scope.formAgentAccountList[i].amount) {
                $scope.sumOfClaimedAmountTransfer = $scope.sumOfClaimedAmountTransfer + $scope.formAgentAccountList[i].amount;
            } 
        }
        
        /* Set the value of agent dropdown if claimedAgent is not NULL*/
        if($scope.selectedPayments.length){
            if($scope.selectedPayments[0].claimedAgent){
                if($scope.sumOfClaimedAmountTransfer == 0) {
                    $rootScope.selectedPaymentsForAgentSelected = $scope.selectedPayments[0].agent;
                } else {
                    var agnt = $scope.selectedPayments[0].claimedAgent.split(" - ");
                    $rootScope.selectedPaymentsForAgentSelected = agnt[0];
                }
            } else {
                $rootScope.selectedPaymentsForAgentSelected = $scope.selectedPayments[0].agent
            }

            $scope.selectedPayments[0].agentCode = $scope.formAgentAccountList[0].agentCode;
            $scope.selectedPayments[0].accountCode = $scope.formAgentAccountList[0].accountCode + " - " + $scope.formAgentAccountList[0].description;
        // $scope.selectedPayments[0].amount = $scope.formAgentAccountList[0].amount;
    }
    if($scope.formAgentAccountList.length){

        var testVarAcc = "";
            // testVarAcc = $scope.formAgentAccountList.filter(getAgentCodeFromList);
            // if($scope.selectedPayments.claimedAgent){
                testVarAcc = $scope.formAgentAccountList.filter(filterForSelectAgent);
            // }else{

            // }
            // testVarAcc = $scope.formAgentAccountList;
            
            if(testVarAcc.length){
                // delete testVarAcc[0]['$$hashKey'];
                // delete testVarAcc[0]['$$mdSelectId'];
                $rootScope.selectedPaymentsForAgentSelected = testVarAcc[0];
                $rootScope.terstTmp = $rootScope.selectedPaymentsForAgentSelected;
                
                if($rootScope.selectedPaymentsForAgentSelected){
                    $scope.getFromAccountAccordingtoAgent($rootScope.selectedPaymentsForAgentSelected);
                }
            }
        }
        
        /*if($scope.selectedPayments.length){
            console.log("3228 : ", $scope.selectedPayments[0].net_act_comm);
            if($scope.selectedPayments[0].net_act_comm){
                console.log("3230 : ", $scope.selectedPayments[0].net_act_comm);
                if($scope.selectedPayments[0].net_act_comm.charAt(0) == "$"){
                    $scope.selectedPayments[0].net_act_comm = $scope.selectedPayments[0].net_act_comm.replace(/\$/g, '');
                    $scope.selectedPayments[0].net_act_comm = Number($scope.selectedPayments[0].net_act_comm).toFixed(2);
                    $scope.selectedPayments[0].net_act_comm = addCommas($scope.selectedPayments[0].net_act_comm);
                    $scope.selectedPayments[0].net_act_comm = "$" + $scope.selectedPayments[0].net_act_comm;
                }
            }
            if($scope.selectedPayments[0].claimed){
                if($scope.selectedPayments[0].claimed.charAt(0) == "$"){
                    $scope.selectedPayments[0].claimed = $scope.selectedPayments[0].claimed.replace(/\$/g, '');
                    $scope.selectedPayments[0].claimed = Number($scope.selectedPayments[0].claimed).toFixed(2);
                    $scope.selectedPayments[0].amount =  $scope.selectedPayments[0].claimed;
                    $scope.selectedPayments[0].claimed = addCommas($scope.selectedPayments[0].claimed);
                    $scope.selectedPayments[0].amount = addCommas($scope.selectedPayments[0].amount);
                    $scope.selectedPayments[0].amount = "$"+$scope.selectedPayments[0].amount;    
                    $scope.selectedPayments[0].claimed = "$" + $scope.selectedPayments[0].claimed;
                }
            }
        }*/
        
        if($scope.formAgentAccountList.length){
            for(var i = 0 ; i < $scope.formAgentAccountList.length ; i++){
                if($rootScope.selectedPaymentsForAgentSelected && $rootScope.selectedPaymentsForAgentSelected.agentCode){
                    if($rootScope.selectedPaymentsForAgentSelected.agentCode == $scope.formAgentAccountList[i].agentCode){
                    // if($scope.formAgentAccountList.length && $scope.formAgentAccountList[3] && $scope.formAgentAccountList[3].amount){
                        if($scope.selectedPayments.length){
                            $scope.selectedPayments[0].amount =  Number($scope.formAgentAccountList[i].amount).toFixed(2);
                            $scope.selectedPayments[0].amount = addCommas($scope.selectedPayments[0].amount);
                            $scope.selectedPayments[0].amount = "$"+$scope.selectedPayments[0].amount;  
                        }  
                    // }
                }
            }
        }
    }


        // if($scope.selectedPayments[0].amount){
        //     $scope.selectedPayments[0].amount = Number($scope.selectedPayments[0].amount).toFixed(2);
        //     $scope.selectedPayments[0].amount = "$"+$scope.selectedPayments[0].amount;    
        // }

// response.DataModel.GetListOfAgentsAccountsClaimed.DataList.data = {"GetListOfAgentsAccountsClaimed":{"ResposeCode":2,"Exceptions":{},"DataList":{"draw":0,"recordsTotal":0,"fileName":null,"recordsFiltered":0,"data":[{"agentID":862,"agentCode":"101","lastName":"EVANS","firstName":"ANN","accountID":7621,"accountCode":"1004601","description":"INDV","amount":11.7400}],"dataEnumerable":null},"DataModel":null,"ResponseMessage":null}}


// var data = $scope.formAgentAccountList.filter($scope.createFilterFor($scope.agentDetails.defaultAccountID));
}   
}

if($scope.selectedPayments.length){
    if($scope.selectedPayments[0].net_act_comm){
        if($scope.selectedPayments[0].net_act_comm.charAt(0) == "$"){
            $scope.selectedPayments[0].net_act_comm = $scope.selectedPayments[0].net_act_comm.replace(/\$/g, '');
            $scope.selectedPayments[0].net_act_comm = Number($scope.selectedPayments[0].net_act_comm).toFixed(2);
            $scope.selectedPayments[0].net_act_comm = addCommas($scope.selectedPayments[0].net_act_comm);
            $scope.selectedPayments[0].net_act_comm = "$" + $scope.selectedPayments[0].net_act_comm;
        }
    }
    if($scope.selectedPayments[0].claimed){
        if($scope.selectedPayments[0].claimed.charAt(0) == "$"){
            $scope.selectedPayments[0].claimed = $scope.selectedPayments[0].claimed.replace(/\$/g, '');
            $scope.selectedPayments[0].claimed = Number($scope.selectedPayments[0].claimed).toFixed(2);
            $scope.selectedPayments[0].amount =  $scope.selectedPayments[0].claimed;
            $scope.selectedPayments[0].claimed = addCommas($scope.selectedPayments[0].claimed);
            $scope.selectedPayments[0].amount = addCommas($scope.selectedPayments[0].amount);
            $scope.selectedPayments[0].amount = "$"+$scope.selectedPayments[0].amount;    
            $scope.selectedPayments[0].claimed = "$" + $scope.selectedPayments[0].claimed;
        }
    }
}

if(response.DataModel.GetPaymentsDataForTransfers_list_of_accounts.DataModel.agentAccountData){
    if(response.DataModel.GetPaymentsDataForTransfers_list_of_accounts.DataModel.agentAccountData.length){
        $scope.toAccountList = response.DataModel.GetPaymentsDataForTransfers_list_of_accounts.DataModel.agentAccountData;  
        $scope.toAccountModel = angular.copy($scope.toAccountList);
        $scope.tempAccountListTransfer = angular.copy($scope.toAccountList);
    }   
    if($scope.tempAccountListTransfer){
        $scope.displayAccountNameTransfer = $scope.tempAccountListTransfer.filter(getFullAccountNameTransfer);
        if($scope.displayAccountNameTransfer.length){
            $scope.showAccountName = $scope.displayAccountNameTransfer[0].accountCode + " - " + $scope.displayAccountNameTransfer[0].description;
        }
    }
}
if(response.DataModel.GetListOfAgentsForTransfers.DataList){
    if(response.DataModel.GetListOfAgentsForTransfers.DataList.data.length){
        $scope.toAgentList = response.DataModel.GetListOfAgentsForTransfers.DataList.data;  
        $scope.toAgentModel = angular.copy($scope.toAgentList);
        $scope.tempAgentListTransfer = angular.copy($scope.toAgentModel);
        if(response.DataModel.agent && response.DataModel.agent.agentCode){
            // $scope.toAgentModel[0].agentCode = response.DataModel.agent.agentCode;
        }
        
        if($scope.toAgentModel){
            if($scope.toAgentModel.length){
                $scope.toAgentModel[0].agentCode = "";
            }
        }
        if($scope.toAccountModel){
            if($scope.toAccountModel.length){
                $scope.toAccountModel[0].accountCode = "";
            }
        }
    }   
    if($scope.tempAgentListTransfer){
        $scope.displayAgentNameTransfer = $scope.tempAgentListTransfer.filter(getFullAgentNameTransfer);
        if($scope.displayAgentNameTransfer.length){
            $scope.showAgentName = $scope.displayAgentNameTransfer[0].agentCode + " - " + $scope.displayAgentNameTransfer[0].lastName + ", " + $scope.displayAgentNameTransfer[0].firstName;
        }
    }

}

// $scope.businessCalListObj
// GetPaymentsDataForTransfers_business_calendar
if(response.DataModel.GetPaymentsDataForTransfers_business_calendar && response.DataModel.GetPaymentsDataForTransfers_business_calendar.DataList){
    $scope.businessCalListObj = response.DataModel.GetPaymentsDataForTransfers_business_calendar;
// if($scope.selectedPayments && $scope.selectedPayments[0].deposit_date){
    if($scope.selectedPayments && $scope.selectedPayments[0] && $scope.selectedPayments[0].deposit_date){
        var depositeDate = new Date($scope.selectedPayments[0].deposit_date);
    }
// if(!$scope.selectedPayments[0].claimAmount){
    if($scope.selectedPayments[0]  && !$scope.selectedPayments[0].claimAmount){
        $scope.selectedPayments[0].claimAmount = $scope.selectedPayments[0].available;
    }
    var depositeDate1 = depositeDate;
    $scope.claimMonth = '';
    var despoSiteDate = Date.parse(depositeDate);
    var assignDepoDate = $filter('date')(depositeDate, "MM/dd/yyyy");

    depositeDate = Date.parse(depositeDate);
    $scope.toDaysDate = new Date($scope.toDaysDate);
    $scope.toDaysDate1 = new Date($scope.toDaysDate);
    $scope.toDaysDate1 = $filter('date')($scope.toDaysDate1, "dd/MM/yyyy")
    $scope.toDaysDate = Date.parse($scope.toDaysDate);
    for(var i = 0 ; i < $scope.businessCalListObj.DataList.data.length ; i++){
// var toDate = $filter('date')($scope.businessCalListObj.DataList.data[i].toDate, "dd/MM/yyyy");
var toDate = new Date($scope.businessCalListObj.DataList.data[i].toDate);
var toDate1 = new Date($scope.businessCalListObj.DataList.data[i].toDate);
var fromDate = new Date($scope.businessCalListObj.DataList.data[i].fromDate);
fromDate = $filter('date')(fromDate, "MM/dd/yyyy");
toDate1 = $filter('date')(toDate1, "MM/dd/yyyy");

toDate = Date.parse(toDate);

$scope.insertPaymentDataParams.date = toDate1;

// if(depositeDate < toDate){
//     continue;
// }
// if(depositeDate < toDate){
    if($scope.businessCalListObj.DataList.data[i].agentClose == false || ($scope.userDetails.capacity == 2 && $scope.businessCalListObj.DataList.data[i].accountingClose == false)){

        $scope.claimMonth = $scope.businessCalListObj.DataList.data[i].description;
        $scope.currentPeriodTranster = $scope.claimMonth + ":" + fromDate + " - " + toDate1;
        if($scope.toDaysDate < toDate){
            $scope.insertPaymentDataParams.date = $scope.toDaysDate1;
        }else{
            $scope.insertPaymentDataParams.date = toDate1;

            /*-----------------------------------------------------------------*/
            if(toDate < despoSiteDate){
              $scope.insertPaymentDataParams.date = assignDepoDate;  
          }else{
            $scope.insertPaymentDataParams.date = toDate1;
        }
        /*-----------------------------------------------------------------*/
    }
    break;
}
// }
} 
}

$scope.disabledAccount = true;

if($scope.userDetails.capacity != 2){
    $scope.disblaedTransferFirstName = true;
    $scope.disblaedTransferLastName = true;
    $scope.disblaedTransferPropertyName = true;
}

}).error(function(err,status) {
    $scope.errorView(err,status);
});
}


function getFromAccountAccordingtoAgentFilter(obj){
    if(obj.agentCode == $scope.getAccAcoAgent){
        return obj.accountCode;
    }
}

$scope.setCommentDropDefaultValueFn = function(){
    $scope.selectedPayments[0].comments = "Select Comment";
}

$scope.getFromAccountAccordingtoAgent = function(id){
    $scope.getAccAcoAgent = id;
    if(id){
        $scope.insertPaymentDataParamsNew.agentID = id.agentID;
        $scope.insertPaymentDataParamsNew.accountID = id.accountID;
    }
    
    if($scope.getAccAcoAgent){
        $scope.fromAgentLabelForTransfer = $scope.getAccAcoAgent.agentCode + " - " + $scope.getAccAcoAgent.lastName + ", " + $scope.getAccAcoAgent.firstName; 
    }
    if($scope.getAccAcoAgent){
        $scope.selectedPayments[0].accountCode = $scope.getAccAcoAgent.accountCode + " - " + $scope.getAccAcoAgent.description;
        // $scope.selectedPayments[0].claimed = Number($scope.getAccAcoAgent.amount).toFixed(2);
        // $scope.selectedPayments[0].claimed = "$"+$scope.selectedPayments[0].claimed;
        $scope.selectedPayments[0].amount = Number($scope.getAccAcoAgent.amount).toFixed(2);
        $scope.selectedPayments[0].amount = addCommas($scope.selectedPayments[0].amount);
        $scope.selectedPayments[0].amount = "$"+$scope.selectedPayments[0].amount;
        /*  ==== 06/04/2017 ==== */
        $scope.selectedPayments[0].agent = $scope.getAccAcoAgent.agentCode;
    }
    /*if($scope.formAgentAccountList){
        var testAccount = $scope.formAgentAccountList.filter(getFromAccountAccordingtoAgentFilter);
        if(testAccount.length){
            $scope.selectedPayments[0].accountCode = testAccount[0].accountCode + " - " + testAccount[0].description;
            $scope.selectedPayments[0].claimed = Number(testAccount[0].amount).toFixed(2);
            $scope.selectedPayments[0].claimed = "$"+$scope.selectedPayments[0].claimed;
            $scope.selectedPayments[0].amount = $scope.selectedPayments[0].claimed;
        }
    }*/
}

/*===== Transfer Popup ===== */
$scope.transferPopup = function(ev, transfer){
    if($scope.userDetails.useSplitDataFiles == true){
        $scope.transferApiUrl = "Payment/GetPaymentDataForTransfers";
    }else{
        $scope.transferApiUrl = "Payments/GetPaymentsDataForTransfers";
    }

// $scope.transferDataParams.id = 2378405609;
$scope.transferDataParams.id = $scope.mainObj.id;
$scope.transferDataParams.agent = ($scope.userDetails.agent) ? $scope.userDetails.agent : '';
$scope.transferDataParams.account = ($scope.userDetails.account) ? $scope.userDetails.account : '';

// $scope.claimsHistoryParams.id = 2445804405;
$scope.claimsHistoryParams.id = $scope.mainObj.id;
$scope.claimsHistoryParams.useSplitDataFiles = $scope.userDetails.useSplitDataFiles;
$scope.claimsHistoryParams.userCapacity = $scope.userDetails.capacity;


$scope.getTransfers();
$scope.getClaimsHistory();

$mdDialog.show({
    contentElement: '#transfer',
    parent: angular.element(document.body),
    targetEvent: ev,
    clickOutsideToClose:true,
    fullscreen: false,
    skipHide: true
}).finally(function() {
});
// $("#transfer").draggable();

}

/* ===== Get Claims Records ===== */

$scope.selectedValueAccount = function(obj){
    return (obj.accountCode === $scope.travelList[0].account);
}

function gettheFullAcctName(obj){
    if(obj.accountCode == $scope.checkAccVar){
        return obj;
    }
}
function gettheFullAcctNameToSelect(obj){
    if(obj.id == $scope.checkAccVar){
        return obj;
    }
}

$scope.disableDisputeAfterInstantClaim = false;
/* ==== Report By ID (Payments, Claims and Transfers) ==== */
$scope.GetPaymentDataForEdit = function(ev){
    /* ==== Set Default Parameters ==== */
    $scope.showClaimMessage = false;
    $scope.corpClaims = false;
    $scope.allowClaims = false;
    $scope.allowTransfer = false;
    $scope.allowDispute = false;
    $scope.showClaimButton = false;
    $scope.showTransferButton = false;
    $scope.showDisputeButton = false;
    $scope.disableClaimButton = false;
    $scope.disableDisputeButton = false;

    $scope.showHistoryView = false;
    $scope.hideCommissionLabel = true;
    $scope.travelList = [];
    $scope.travelType = [];
    $scope.systemList = [];
    $scope.GetListOfAgentsMethod = [];
    $scope.GetTheAccountsForCorpMethod = [];
    $scope.GetListOfBranchesMethod = [];
    $scope.GetTheIatasForCorpMethod = [];
    $scope.GetListOfUnclaimedAgentsMethod = [];
    $scope.systemListOne = [];
    $scope.GetClaimDataForPaymentMethod = [];
    $scope.systemList = [];
    $scope.errorMsgClaims = "";
    $scope.textBox_ID = "";
    $scope.hiddenField_ID = "";
    $scope.hiddenField_PID = "";

    if($state.current.name != "main.unclaimed-AllTypes" && $state.current.name != "main.unclaimed-AllExcludingHotel-Agent" && $state.current.name != "main.unclaimed-AllTypesHotelCarAndOther-Agent" && $state.current.name != "main.unclaimed-hotelonly-Agent" && $state.current.name != "main.batchReleaseUtility"){
        $scope.editPaymentDataParamsData.userId = ($scope.userDetails)?$scope.userDetails.userID:'';
    }

    /* ==== Set Default Parameters ==== */
    $http({
        url: SETTINGS[GLOBALS.ENV].apiUrl+$scope.api_url,
        method: 'POST',
        data: $scope.editPaymentDataParamsData,
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
        }
    }).success(function(response) {
        $scope.PaymentData = response;
        $scope.query.AccountselectedItem = '';
        $scope.getDataSelected = [];
        if(response.ResposeCode == 2){
            /* ==== Get Selected Payment Records Method[0] ==== */
            if($scope.PaymentData.DataModel.GetSelectedPaymentRecordMethod.DataList){
                if($scope.PaymentData.DataModel.GetSelectedPaymentRecordMethod.DataList.data && $scope.PaymentData.DataModel.GetSelectedPaymentRecordMethod.DataList.data.length){
                    $scope.travelList = $scope.PaymentData.DataModel.GetSelectedPaymentRecordMethod.DataList.data.filter(filterDate);

                 /*   if($scope.userDetails && $scope.userDetails.superUser == true){
                        if($scope.travelList.length){
                            if($scope.travelList[0].claimButton == true){
                                $scope.showClaimButton = true;
                            }else{
                                $scope.showClaimButton = false;
                            }

                            if($scope.travelList[0].transferButton == true){
                                $scope.showTransferButton = true;
                            }
                            else{
                                $scope.showTransferButton = false;
                            }

                            if($scope.travelList[0].disputeButton == true){
                                $scope.showDisputeButton = true;
                                if($scope.travelList[0].disputegrey == true){
                                    $scope.disableDisputeButton = true;
                                }
                            }else{
                                $scope.showDisputeButton = false;
                            }
                        }

                    }else{*/
                        if($scope.travelList.length){
                            if($scope.userDetails.capacity == 1){
                                if($scope.travelList[0].claimButton == true){
                                    $scope.showClaimButton = true;
                                    if($scope.travelList[0].ClaimGrey == true){
                                        $scope.disableClaimButton = true;
                                    }
                                }
                                else{
                                    $scope.showClaimButton = false;
                                }

                                /*if($scope.travelList[0].transferButton == true){
                                    $scope.showTransferButton = true;
                                }
                                else{
                                    $scope.showTransferButton = false;
                                }*/

                                if($scope.travelList[0].disputeButton == true){
                                    $scope.showDisputeButton = true;
                                    if($scope.travelList[0].disputeGrey == true){
                                        $scope.disableDisputeButton = true;
                                        
                                    }
                                }else{
                                    $scope.showDisputeButton = false;
                                }
                            }

                            if($scope.userDetails.capacity == 2){
                                if($scope.travelList[0].transferButton == true){
                                    $scope.showTransferButton = true;
                                }
                                else{
                                    $scope.showTransferButton = false;
                                }

                                if($scope.travelList[0].claimButton == true){
                                    $scope.showClaimButton = true;
                                    if($scope.travelList[0].ClaimGrey == true){
                                        $scope.disableClaimButton = true;
                                    }
                                }
                                else{
                                    $scope.showClaimButton = false;
                                }

                                $scope.showDisputeButton = false;
                            }
                        }
                        /*}*/

                        $scope.getDataSelected = angular.copy($scope.PaymentData.DataModel.GetTheAccountsForCorpMethod.DataList.data.filter($scope.selectedValueAccount));
                        if($scope.getDataSelected.length){

                        // $scope.query.descriptionText = $scope.getDataSelected[0].accountCode + ' - ' + $scope.getDataSelected[0].description + ', ' + $scope.getDataSelected[0].id;
                        $scope.query.descriptionText = $scope.getDataSelected[0].accountCode + ' - ' + $scope.getDataSelected[0].description;
                        // console.log("$scope.query.descriptionText 3812 : ", $scope.query.descriptionText);
                        
                        $scope.sumOfClaimedAmount = 0;

                        if($scope.travelList.length){
                            var availAmutDrp = Number($scope.PaymentData.DataModel.GetSelectedPaymentRecordMethod.DataList.data[0].available);
                            var commissionDrp = Number($scope.PaymentData.DataModel.GetSelectedPaymentRecordMethod.DataList.data[0].net_act_comm);
                            var userRestriction = ($scope.userDetails)?$scope.userDetails.agent:'';
                            var payment_claimsDrp = false;

                            
                            if($scope.PaymentData.DataModel.GetClaimDataForPaymentMethod.DataList){
                                // if($scope.PaymentData.DataModel.GetClaimDataForPaymentMethod.DataList.data && $scope.PaymentData.DataModel.GetClaimDataForPaymentMethod.DataList.data.length){
                                    if($scope.PaymentData.DataModel.GetClaimDataForPaymentMethod.DataList.data){
                                        for(var i = 0 ; i < $scope.PaymentData.DataModel.GetClaimDataForPaymentMethod.DataList.data.length ; i++){
                                            if(angular.lowercase($scope.PaymentData.DataModel.GetClaimDataForPaymentMethod.DataList.data[i].claimType) == "c"){
                                                payment_claimsDrp = true;
                                            }
                                        }
                                    }
                                }
                           /*  if((availAmutDrp != commissionDrp) && userRestriction != '' && payment_claimsDrp){
                                $scope.travelList[0].agent = $scope.PaymentData.DataModel.GetSelectedPaymentRecordMethod.DataList.data[0].agent;
                                $scope.checkAccVar  = $scope.PaymentData.DataModel.GetSelectedPaymentRecordMethod.DataList.data[0].account;

                                if($scope.PaymentData.DataModel.GetTheAccountsForCorpMethod.DataList && $scope.PaymentData.DataModel.GetTheAccountsForCorpMethod.DataList.data.length){
                                    var selectAccountDrpObj = $scope.PaymentData.DataModel.GetTheAccountsForCorpMethod.DataList.data.filter(gettheFullAcctName);
                                    var selectAccountDrp = selectAccountDrpObj[0].accountCode + " - " + selectAccountDrpObj[0].description;
                                    $scope.query.descriptionText = selectAccountDrp;
                                    
                                }
                                
                            }*/
                            
                            if(availAmutDrp != commissionDrp){
                                if($scope.PaymentData.DataModel.GetSelectedPaymentRecordMethod.DataList.data && $scope.PaymentData.DataModel.GetSelectedPaymentRecordMethod.DataList.data.length){
                                    $scope.travelList[0].agent = $scope.PaymentData.DataModel.GetSelectedPaymentRecordMethod.DataList.data[0].agent;
                                    var agentToSelect = $scope.PaymentData.DataModel.GetSelectedPaymentRecordMethod.DataList.data[0].claimedAgent;
                                    if(agentToSelect.indexOf("-") != -1){
                                        agentToSelect = agentToSelect.split(' - ');
                                    }
                                    if(agentToSelect.length){
                                        agentToSelect = agentToSelect[0];
                                        $scope.travelList[0].agent = agentToSelect;
                                    }
                                }

                                if($scope.PaymentData.DataModel.GetClaimDataForPaymentMethod.DataList && $scope.PaymentData.DataModel.GetClaimDataForPaymentMethod.DataList.data){
                                    if($scope.PaymentData.DataModel.GetClaimDataForPaymentMethod.DataList && $scope.PaymentData.DataModel.GetClaimDataForPaymentMethod.DataList.data.length){
                                        var listLength = $scope.PaymentData.DataModel.GetClaimDataForPaymentMethod.DataList.data.length;
                                        var claimDataForPaymentMethodList = $scope.PaymentData.DataModel.GetClaimDataForPaymentMethod.DataList.data;

                                        var accToAppend = claimDataForPaymentMethodList[ listLength - 1 ];
                                        if(accToAppend){
                                            $scope.checkAccVar  = accToAppend.accountID;
                                        }
                                    }
                                }

                                if($scope.PaymentData.DataModel.GetTheAccountsForCorpMethod.DataList && $scope.PaymentData.DataModel.GetTheAccountsForCorpMethod.DataList.data.length){
                                    var selectAccountDrpObj = $scope.PaymentData.DataModel.GetTheAccountsForCorpMethod.DataList.data.filter(gettheFullAcctNameToSelect);
                                    // console.log("selectAccountDrpObj 3848 : ", selectAccountDrpObj); 
                                    if(selectAccountDrpObj.length){
                                        var selectAccountDrp = selectAccountDrpObj[0].accountCode + " - " + selectAccountDrpObj[0].description;
                                        // console.log("selectAccountDrp 3849 : ", selectAccountDrp);
                                        $scope.query.descriptionText = selectAccountDrp;
                                        // console.log("$scope.query.descriptionText 3878 : ", $scope.query.descriptionText);
                                    }
                                    
                                }
                                
                            }

                            else{
                                var largestClaimAmount = 0;
                                var position = 0;
                                for(var i=0; i< $scope.travelList.length;i++){
                                    if($scope.travelList[i].claimAmount) {
                                        if($scope.travelList[i].claimAmount > largestClaimAmount){
                                            position = i;
                                            largestClaimAmount = $scope.travelList[i].claimAmount;
                                        }
                                        if($scope.travelList[0].claimedAgent){
                                            var agentToSelect = $scope.travelList[position].claimedAgent.split(" - ");
                                        }
                                        $scope.travelList[0].agent = agentToSelect[0];
                                        if($scope.travelList[0].claimedAccount){
                                            $scope.query.descriptionText = $scope.travelList[0].claimedAccount;

                                            // console.log("$scope.query.descriptionText 3886 : ", $scope.query.descriptionText);
                                        }
                                    } 
                                }
                            }
                        }


                        /* Set the value of account dropdown if claimedAccount is not NULL*/


                        if($scope.travelList[0].net_act_comm){
                            $scope.travelList[0].net_act_comm = $scope.travelList[0].net_act_comm.toFixed(2);
                            $scope.travelList[0].net_act_comm = addCommas($scope.travelList[0].net_act_comm);
                            $scope.travelList[0].net_act_comm = "$"+$scope.travelList[0].net_act_comm;
                        }

                        if($scope.travelList[0].net_rate){
                            $scope.travelList[0].net_rate = $scope.travelList[0].net_rate.toFixed(2);
                            $scope.travelList[0].net_rate = addCommas($scope.travelList[0].net_rate);
                            $scope.travelList[0].net_rate = "$"+$scope.travelList[0].net_rate;
                        }

                        if($scope.travelList[0].tax1 || $scope.travelList[0].tax1 >= 0){
                            $scope.travelList[0].tax1 = $scope.travelList[0].tax1.toFixed(2);
                            $scope.travelList[0].tax1 = addCommas($scope.travelList[0].tax1);
                            $scope.travelList[0].tax1 = "$"+$scope.travelList[0].tax1;
                        }
                        if($scope.travelList[0].available || $scope.travelList[0].available >= 0){
                            $scope.travelList[0].available = Number($scope.travelList[0].available);
                            $scope.travelList[0].available = $scope.travelList[0].available.toFixed(2);
                            $scope.travelList[0].available = addCommas($scope.travelList[0].available);
                            $scope.travelList[0].available = "$"+$scope.travelList[0].available;
                        }




                        if($scope.travelList && $scope.travelList.length){
                            var cnt = 0;
                            var cnt1 = 0;
                            var cnt2 = 0;
                            var cnt3 = 0;
                            var cnt4 = 0;
                            var cnt5 = 0;
                            var cnt6 = 0;
                            var cnt7 = 0;
                            var cnt8 = 0;
                            var cnt9 = 0;
                            for(var i = 0 ; i < $scope.travelList.length; i++){
                                if($scope.travelList[i].PaymentID){
                                    cnt = 1;
                                    cntpos = i;
                                }
                                if($scope.travelList[i].paymentID){
                                    cnt1 = 1;
                                    cntpos1 = i;
                                }
                                if($scope.travelList[i].bookingID){
                                    cnt2 = 1;
                                    cntpos2 = i;
                                }
                                if($scope.travelList[i].BookingID){
                                    cnt3 = 1;
                                    cntpos3 = i;
                                }
                                if($scope.travelList[i].id){
                                    cnt4 = 1;
                                    cntpos4 = i;
                                }
                                if($scope.travelList[i].pay_source != ""){
                                    cnt5 = 1;
                                    cntpos5 = i;
                                }
                                if($scope.travelList[i].payment_id){
                                    cnt6 = 1;
                                    cntpos6 = i;
                                }

                                var DD = new Date($scope.travelList[i].deposit_date);
                                var compareDate = new Date('01/01/0001');
                                if(DD  > compareDate){
                                    cnt7 = 1;
                                    cntpos7 = i;
                                }else{
                                    $scope.travelList[0].deposit_date = "";
                                }
                                var CD = new Date($scope.travelList[i].check_date);
                                var compareCheckDate = new Date('01/01/0001');
                                if(CD  > compareCheckDate){
                                    cnt9 = 1;
                                    cntpos9 = i;
                                }else{
                                    $scope.travelList[0].check_date = "";
                                }
                                if($scope.travelList[i].check_num != ''){
                                    cnt8 = 1;
                                    cntpos8 = i;
                                }
                                if($scope.travelList[i].net_act_comm){

                                    var checkSign = $scope.travelList[0].net_act_comm;
                                    checkSign = checkSign.toString();

                                    if(checkSign.indexOf("$") != -1){
                                        $scope.travelList[0].net_act_comm = $scope.travelList[0].net_act_comm.replace(/\$/g, '');    
                                    }
                                    if(checkSign.indexOf(",") != -1){
                                        $scope.travelList[0].net_act_comm = $scope.travelList[0].net_act_comm.replace(/\,/g, '');    
                                    }
                                    $scope.travelList[0].net_act_comm = $scope.travelList[i].net_act_comm;
                                    $scope.travelList[0].net_act_comm = Number($scope.travelList[0].net_act_comm).toFixed(2);
                                    $scope.travelList[0].net_act_comm = addCommas($scope.travelList[0].net_act_comm);

                                }

                            }
                            if(cnt){
                                $scope.travelList[0].webId = $scope.travelList[cntpos].PaymentID;   
                            }else if(cnt1){
                                $scope.travelList[0].webId = $scope.travelList[cntpos1].paymentID;    
                            }else if(cnt2){
                                $scope.travelList[0].webId = $scope.travelList[cntpos2].bookingID;    
                            }else if(cnt3){
                                $scope.travelList[0].webId = $scope.travelList[cntpos3].BookingID;    
                            }else if(cnt4){
                                $scope.travelList[0].webId = $scope.travelList[cntpos4].id;    
                            }else{

                            }
                            if(cnt5){
                                $scope.travelList[0].pay_source = $scope.travelList[cntpos5].pay_source;
                            }
                            if(cnt6){
                                $scope.travelList[0].payment_id = $scope.travelList[cntpos6].payment_id;
                            }
                            if(cnt7){
                                $scope.travelList[0].deposit_date = $scope.travelList[cntpos7].deposit_date;
                            }
                            if(cnt8){
                                $scope.travelList[0].check_num = $scope.travelList[cntpos8].check_num;
                            }
                            if(cnt9){
                                $scope.travelList[0].check_date = $scope.travelList[cntpos9].check_date;
                            }
                        }

                    }

                    /*---------*/
                    if($scope.travelList && $scope.travelList.length){
                        for(var i = 0 ; i < $scope.travelList.length ; i++){
                            if($scope.travelList[i].batch_id > 0){
                                $scope.travelList[0].batch_id = $scope.travelList[i].batch_id;
                            }
                        }
                    }
                    /*---------*/
                }
            }
            /* ==== Get Selected Payment Records Method[1] ==== */
            if($scope.PaymentData.DataModel.GetBookingTypesForPaymentMethod.DataList){   
                if($scope.PaymentData.DataModel.GetBookingTypesForPaymentMethod.DataList.data && $scope.PaymentData.DataModel.GetBookingTypesForPaymentMethod.DataList.data.length){
                    $scope.travelType =  $scope.PaymentData.DataModel.GetBookingTypesForPaymentMethod.DataList.data;
                }
            }
            /* ==== Get  List of Agents Method[2] ==== */
            if($scope.PaymentData.DataModel.GetListOfAgentsMethod.DataList){   
                if($scope.PaymentData.DataModel.GetListOfAgentsMethod.DataList.data && $scope.PaymentData.DataModel.GetListOfAgentsMethod.DataList.data.length){
                    $scope.GetListOfAgentsMethod =  $scope.PaymentData.DataModel.GetListOfAgentsMethod.DataList.data;
                }
            }
            /* ==== Get The Accounts For Corp Method[3] ==== */
            if($scope.PaymentData.DataModel.GetTheAccountsForCorpMethod.DataList){   
                if($scope.PaymentData.DataModel.GetTheAccountsForCorpMethod.DataList.data && $scope.PaymentData.DataModel.GetTheAccountsForCorpMethod.DataList.data.length){
                    $scope.GetTheAccountsForCorpMethod =  $scope.PaymentData.DataModel.GetTheAccountsForCorpMethod.DataList.data;
                }
            }
            /* ==== Get List Of Branches Method[4] ==== */                
            if($scope.PaymentData.DataModel.GetListOfBranchesMethod.DataList){   
                if($scope.PaymentData.DataModel.GetListOfBranchesMethod.DataList.data && $scope.PaymentData.DataModel.GetListOfBranchesMethod.DataList.data.length){
                    $scope.GetListOfBranchesMethod =  $scope.PaymentData.DataModel.GetListOfBranchesMethod.DataList.data;
                }
            }
            /* ==== Get The Iatas For Corp Method[5] ==== */
            if($scope.PaymentData.DataModel.GetTheIatasForCorpMethod.DataList){   
                if($scope.PaymentData.DataModel.GetTheIatasForCorpMethod.DataList.data && $scope.PaymentData.DataModel.GetTheIatasForCorpMethod.DataList.data.length){
                    $scope.GetTheIatasForCorpMethod =  $scope.PaymentData.DataModel.GetTheIatasForCorpMethod.DataList.data;
                }
            }
            /* ==== Get List Of UnclaimedAgents Method[6] ==== */
            if($scope.PaymentData.DataModel.GetListOfUnclaimedAgentsMethod.DataList){   
                if($scope.PaymentData.DataModel.GetListOfUnclaimedAgentsMethod.DataList.data && $scope.PaymentData.DataModel.GetListOfUnclaimedAgentsMethod.DataList.data.length){
                    $scope.GetListOfUnclaimedAgentsMethod =  $scope.PaymentData.DataModel.GetListOfUnclaimedAgentsMethod.DataList.data;
                }
            }
            /* ==== Get Agency Data For Payment Method[7] ==== */
            if($scope.PaymentData.DataModel.GetAgencyDataForPaymentMethod.DataList){   
                if($scope.PaymentData.DataModel.GetAgencyDataForPaymentMethod.DataList.data && $scope.PaymentData.DataModel.GetAgencyDataForPaymentMethod.DataList.data.length){
                    $scope.systemListOne = $scope.PaymentData.DataModel.GetAgencyDataForPaymentMethod.DataList.data.filter(filterDate);
                }
            }
            $scope.sumClaimedAmount = 0;
            /* ==== Get Claim Data For Payment Method[8] ==== */
            if($scope.PaymentData.DataModel.GetClaimDataForPaymentMethod.DataList){   
                if($scope.PaymentData.DataModel.GetClaimDataForPaymentMethod.DataList.data && $scope.PaymentData.DataModel.GetClaimDataForPaymentMethod.DataList.data.length){
                    $scope.GetClaimDataForPaymentMethod =  $scope.PaymentData.DataModel.GetClaimDataForPaymentMethod.DataList.data.filter(filterDate);
                    for(var i=0; i< $scope.GetClaimDataForPaymentMethod.length;i++){
                        if($scope.GetClaimDataForPaymentMethod[i].claimAmount) {
                            $scope.sumClaimedAmount = $scope.sumClaimedAmount + $scope.GetClaimDataForPaymentMethod[i].claimAmount;
                        } 
                    }

                }
            }

            /* ==== Get Data About Id Method[9] ==== */
            if($scope.PaymentData.DataModel.GetDataAboutIdMethod.DataList){
                if($scope.PaymentData.DataModel.GetDataAboutIdMethod.DataList.data && $scope.PaymentData.DataModel.GetDataAboutIdMethod.DataList.data.length){
                    $scope.systemList = $scope.PaymentData.DataModel.GetDataAboutIdMethod.DataList.data.filter(filterDate);

                }
            }
// Return if no record for id or record does not belong to corp
if (($scope.travelList.length == 0) || ($scope.travelList[0]["corpID"].toString() != $scope.userDetails.corpID.toString())){
    toaster.pop('error', '', 'Invalid record number!');
    return;
}

// For dispute button
if($scope.userDetails.superUser == false && ($scope.travelList[0].net_act_comm != $scope.travelList[0].available) && ($scope.sumClaimedAmount!=0)) {
    $scope.allowDispute = true;
}


/* ===== set Show and Hide Functionlity ===== */
if($scope.userDetails.useSplitDataFiles == true){
    $scope.corpClaims = true;
    $scope.textBox_ID = ($scope.travelList.length && $scope.travelList[0]["paymentID"].toString() == "0") ? $scope.travelList[0]["bookingID"].toString() : $scope.travelList[0]["paymentID"].toString();
    $scope.hiddenField_ID = $scope.textBox_ID;
    $scope.hiddenField_PID = $scope.travelList[0]["payment_id"].toString();
}else{
    if($scope.travelList.length){
        $scope.textBox_ID = $scope.travelList[0]["id"].toString();
        $scope.hiddenField_ID = $scope.textBox_ID;
        $scope.hiddenField_PID = $scope.travelList[0]["payment_id"].toString();
    }
}

if(!$scope.corpClaims || ($scope.corpClaims && $scope.userDetails.capacity === 2)){
    $scope.showHistoryView = true;
}

if($scope.userDetails.capacity === 2){
    $scope.allowClaims = true;
    if($scope.travelList[0].net_act_comm) {
        $scope.travelList[0].net_act_comm = $scope.travelList[0].net_act_comm.replace(/\$/g, '');  
    }
    
    if($scope.travelList[0].available) {
        $scope.travelList[0].available = $scope.travelList[0].available.replace(/\$/g, '');
    } 
    
    if($scope.travelList.length && $scope.travelList[0].net_act_comm){
        if($scope.travelList[0].net_act_comm.indexOf(',') != -1){
            $scope.travelList[0].net_act_comm = $scope.travelList[0].net_act_comm.replace(/\,/g, '');
        }
        var nAccComm = Number($scope.travelList[0].net_act_comm);    
    }
    if($scope.travelList.length && $scope.travelList[0].available){
        if($scope.travelList[0].available.indexOf(',') != -1){
            $scope.travelList[0].available = $scope.travelList[0].available.replace(/\,/g, '');
        }
        var avaiComm = Number($scope.PaymentData.DataModel.GetSelectedPaymentRecordMethod.DataList.data[0].available);

    }

    if($scope.travelList.length  && Number($scope.travelList[0].net_act_comm) != Number($scope.travelList[0].available)){
        $scope.allowTransfer = true;
    }
    if($scope.travelList.length  && Number($scope.travelList[0].net_act_comm) == Number($scope.travelList[0].available)){
        $scope.allowTransfer = false;
    }
    
}else if($scope.userDetails.capacity === 1){

    if($scope.travelList.length && $scope.travelList[0].available){
        if($scope.travelList[0].available.indexOf('$') != -1){
            $scope.travelList[0].available = $scope.travelList[0].available.replace(/\$/g, '');
        }
        if($scope.travelList[0].available.indexOf(',') != -1){
            $scope.travelList[0].available = $scope.travelList[0].available.replace(/\,/g, '');
        }
        var avaiComm = Number($scope.PaymentData.DataModel.GetSelectedPaymentRecordMethod.DataList.data[0].available);
        
    }

    $scope.allowClaims = false;
    if($scope.systemList.length && $scope.systemList[0].suspendClaiming === true){
        $scope.errorMsgClaims = "Claiming has been temporarily suspended due to our month-end process.";
    }else{
        if($scope.travelList.length && $scope.travelList[0].claimAmount > 0){
            $scope.allowDispute = true;
        }
        if($scope.travelList[0].available != 0){
            if($scope.travelList[0].last_name.trim().toUpperCase() === 'NOTPROVIDED' || $scope.travelList[0].last_name.trim().toUpperCase() === 'NOT PROVIDED'){
                $scope.errorMsgClaims = "Guest name is NOTPROVIDED; Please contact " + ($scope.systemListOne[0]["claimAssistanceEmail"].toString().length > 0 ? "<a href=\"mailto:" + $scope.systemListOne[0]["claimAssistanceEmail"].toString() + "?subject=NOTPROVIDED%20Inquiry%20-%20Web%20ID:%20" + $scope.hiddenField_ID + "\" style=\"color:Red\"><strong>" + $scope.systemListOne[0]["claimAssistanceEmail"].toString() + "</strong></a>" : "your system administrator") + " for claiming.";
            }else{
// Are we within the time frame for claiming
if($scope.systemListOne.length && $scope.systemListOne[0]['unclaimedAvailability_Number'] > 0){
    var currentDateTime = $scope.toDaysDate;
    var dateCompare = $scope.toDaysDate;
// What date are we working with?  in date; out date; deposit date
if ($scope.systemListOne[0]["unclaimedAvailability_After"].toString().toLowerCase() == "i"){
    currentDateTime = new Date($scope.travelList[0]["in_date"]);
}else if ($scope.systemListOne[0]["unclaimedAvailability_After"].toString().toLowerCase() == "o"){
    currentDateTime = new Date($scope.travelList[0]["out_date"]);
}else if ($scope.systemListOne[0]["unclaimedAvailability_After"].toString().toLowerCase() == "d"){
    currentDateTime = new Date($scope.travelList[0]["deposit_date"]);
}
// Calculate compare date
switch ($scope.systemListOne[0]["unclaimedAvailability_Period"].toString()){
    case "d":
    dateCompare = currentDateTime.setDate(currentDateTime.getDate() + parseFloat($scope.systemListOne[0]["unclaimedAvailability_Number"].toString()));
    break;
    case "m":
    dateCompare = currentDateTime.setMonth(currentDateTime.getMonth() + parseInt($scope.systemListOne[0]["unclaimedAvailability_Number"].toString()));
    break;
    case "y":
    dateCompare = currentDateTime.setFullYear(currentDateTime.getFullYear() + parseInt($scope.systemListOne[0]["unclaimedAvailability_Number"].toString()));
    break;
}
// Determine if claiming is available
if (dateCompare >= Date.parse(currentDateTime)){
    $scope.allowClaims = true;
}
}
}
}
}   

}
/* ===== set Show and Hide Functionlity ===== */
/*if($scope.travelList.length){
    $scope.reportFilterRoot.unit = $scope.travelList[0]['cts_type'].toString();
}*/
$scope.travelList[0].net_act_comm = "$"+$scope.travelList[0].net_act_comm;
if($scope.travelList[0].available.indexOf('$') != -1){
    $scope.travelList[0].available = "$"+$scope.travelList[0].available;
}


if($scope.travelList.length){
    if($state.current.name != "main.home" && $state.current.name != "main.unclaimed-AllTypes" && $state.current.name != "main.unclaimed-AllExcludingHotel-Agent" && $state.current.name != "main.unclaimed-AllTypesHotelCarAndOther-Agent" && $state.current.name != "main.unclaimed-hotelonly-Agent"){
        //$scope.reportFilterRoot.unit = $scope.travelList[0]['cts_type'].toString();
    }
}

if(!$scope.userDetails.superUser){
    // if($scope.travelList[0].available.indexOf('$') != -1){
        $scope.travelList[0].available = $scope.travelList[0].available.replace(/\$/g, '');
    // }
    
    if($scope.travelList[0].available.indexOf(',') != -1){
        $scope.travelList[0].available = $scope.travelList[0].available.replace(/\,/g, '');    
    }
    $scope.travelList[0].available = Number($scope.travelList[0].available);
    $scope.travelList[0].available = $scope.travelList[0].available.toFixed(2);
    if($scope.travelList[0].available <= 0.00){
        $scope.allowClaims = false;
    }
    
    $scope.travelList[0].available = "$"+$scope.travelList[0].available;
    $scope.travelList[0].available = addCommas($scope.travelList[0].available);
    
    
}else{

    $scope.travelList[0].available = addCommas($scope.travelList[0].available);
    $scope.travelList[0].net_act_comm = addCommas($scope.travelList[0].net_act_comm);
    $scope.travelList[0].net_act_comm = "$"+$scope.travelList[0].net_act_comm;
    
}

if($state.current.name == "main.outstandingBookingReport-HotelsOnly" || $state.current.name == "main.batchReleaseUtility"){
    $scope.allowClaims = false;
    $scope.allowTransfer = false;
    $scope.hideCommissionLabel = false;
}

// if($scope.userDetails.capacity == 2){
    if($scope.travelList && $scope.travelList.length) {
        var availAmt = $scope.travelList[0].available;
        var claimAmt = $scope.travelList[0].claimAmount;
        if($scope.travelList[0].available.indexOf('$') == -1){

            $scope.travelList[0].available = "$"+$scope.travelList[0].available;
        }

        availAmt = availAmt.replace(/\$/g, '');
        availAmt = Number(availAmt).toFixed(2);

        /* Claim and dispite buttons */
        if($scope.userDetails.capacity == 1){

            if(availAmt && claimAmt){
                if(availAmt == claimAmt){
                    $scope.allowClaims = true;
                    $scope.allowDispute = false;
                }
                if(availAmt != claimAmt){
                    $scope.allowClaims = true;
                    $scope.allowDispute = true;
                }
            }
            if(availAmt == 0){
                $scope.allowClaims = false;
                $scope.allowDispute = true;
            }
            if($scope.travelList && $scope.travelList.length) {
                if($scope.travelList[0].payment_id == 0) {
                    $scope.allowClaims = false;
                    $scope.allowTransfer = false;
                } else {
                    $scope.allowClaims = true;  
                    $scope.allowTransfer = true;
                }  
            }
            if($scope.travelList && $scope.travelList.length){
                if(availAmt == 0){
                    $scope.allowClaims = false;
                }
            }
        }else{
            $scope.allowDispute = false;
        }


        /* Claim and transfer buttons */
        if($scope.userDetails.capacity == 2){
            if(availAmt && claimAmt){
                if(availAmt == claimAmt){
                    $scope.allowClaims = true;
                    $scope.allowTransfer = false;
                }
                if(availAmt != claimAmt){
                    $scope.allowClaims = true;
                    $scope.allowTransfer = true;
                }
            }   
            if(availAmt == 0){
                $scope.allowClaims = false;
                $scope.allowTransfer = true;
            }
            if($scope.travelList && $scope.travelList.length) {
                if($scope.travelList[0].payment_id == 0) {
                    $scope.allowClaims = false;
                    $scope.allowTransfer = false;
                } else {
                    $scope.allowClaims = true;  
                    $scope.allowTransfer = true;
                }  
            }
            if($scope.travelList && $scope.travelList.length){
                if(availAmt == 0){
                    $scope.allowClaims = false;
                }
            }

        }else{
            $scope.allowTransfer = false;
        }
    }
    
    if(nAccComm != undefined && avaiComm != undefined){
        if(nAccComm == avaiComm){
            $scope.allowTransfer = false;
        }
    }
    
    var useClaimingFunctionality = ($scope.userDetails)?$scope.userDetails.useClaimingFunctionality:'';
    if(useClaimingFunctionality == true){
        $scope.allowClaims = true;  
    }
    
    if(avaiComm == 0){
        $scope.allowClaims = false;
        // $scope.allowDispute = false;
    }
// }


// }

$scope.DisableFieldUpdates();
$scope.EnableFieldUpdates();

/* ======= show Popup After Get Response ======== */
$mdDialog.show({
    contentElement: '#reportFilterPopup',
    parent: angular.element(document.body),
    targetEvent: ev,
    clickOutsideToClose:false,
    fullscreen: false,
    skipHide: true
}).finally(function() {});

}else{
    toaster.pop('error', '', response.ResponseMessage);
}

/* ======= show Popup After Get Response ======== */

}).error(function(err,status) {
    $scope.errorView(err,status);
});
// $("#reportFilterPopup").draggable();

}

$scope.showConfirmPopup = function(ev, obj,title) {
    $scope.disableDisputeAfterInstantClaim = false;
    $scope.popupTitle = title;        
    $scope.mainObj = obj;
    $scope.editPaymentDataParamsData = angular.copy($scope.editPaymentDataParams);

    if($scope.userDetails.useSplitDataFiles == true){
/*$scope.editPaymentDataParamsData.paymentid = obj.id;
$scope.editPaymentDataParamsData.bookingid = obj.booking_id;*/
if(obj && obj.ID || obj.ID == 0){
    if(obj.ID != 0){
        $scope.editPaymentDataParamsData.paymentid = obj.ID;
    }else{
        $scope.editPaymentDataParamsData.paymentid = obj.paymentID;
    }
    if($state.current.name == "main.home"){
        if(obj && obj.payment_id){
            $scope.editPaymentDataParamsData.paymentid = obj.id;   
            // $scope.editPaymentDataParamsData.paymentid = obj.payment_id;   
        }
    }
}
else if(obj && obj.id || obj.id == 0){
    if(obj.id != 0){
        $scope.editPaymentDataParamsData.paymentid = obj.id;    
    }else{
        $scope.editPaymentDataParamsData.paymentid = obj.paymentID;
    }
    if($state.current.name == "main.home"){
        if(obj && obj.payment_id){
            $scope.editPaymentDataParamsData.paymentid = obj.id;   
            // $scope.editPaymentDataParamsData.paymentid = obj.payment_id;   
        }
    }
}

/*if($scope.popupTitle == "Member home"){
    $scope.editPaymentDataParamsData.ctsPaymentID = "0";
}
*/
if($scope.popupTitle == "member home"){
    $scope.editPaymentDataParamsData.ctsPaymentID = "1";
}

if(obj && obj.bookingID){
    $scope.editPaymentDataParamsData.bookingid = obj.bookingID;
}
/*if($state.current.name == "main.batchReleaseUtility"){
    if(obj && obj.booking_id){
        $scope.editPaymentDataParamsData.bookingid = obj.booking_id;
    }    
}*/

/*if(obj && obj.booking_id){
$scope.editPaymentDataParamsData.bookingid = obj.booking_id; 
}*/

$scope.api_url = "Payment/GetPaymentDataForEdit";

}else{
/*$scope.editPaymentDataParamsData.paymentid = obj.id;
$scope.editPaymentDataParamsData.bookingid = obj.booking_id;*/
if(obj && obj.ID || obj.ID == 0){
    if(obj.ID != 0){
        $scope.editPaymentDataParamsData.paymentid = obj.ID;
    }else{
        $scope.editPaymentDataParamsData.paymentid = obj.paymentID;
    }
    if($state.current.name == "main.home"){
        if(obj && obj.payment_id){
            $scope.editPaymentDataParamsData.paymentid = obj.payment_id;   
        }
    }
}
else if(obj && obj.id || obj.id == 0){
    if(obj.id != 0){
        $scope.editPaymentDataParamsData.paymentid = obj.id;    
    }else{
        $scope.editPaymentDataParamsData.paymentid = obj.paymentID;
    }
    if($state.current.name == "main.home"){
        if(obj && obj.payment_id){
            $scope.editPaymentDataParamsData.paymentid = obj.payment_id;   
        }
    }
}

if(obj && obj.bookingID){
    $scope.editPaymentDataParamsData.bookingid = obj.bookingID;
}
/*if(obj && obj.booking_id){
$scope.editPaymentDataParamsData.bookingid = obj.booking_id; 
}*/

$scope.api_url = "Payments/GetPaymentsDataForEdit";
}
$scope.GetPaymentDataForEdit(ev);

};
/* ==== Report By ID (Payments, Claims and Transfers) ==== */

$scope.userInfo();



/* ===== Dispute Start  =====*/

$scope.disputeApiUrl;
// $scope.disputeDataParams1 = {dataTableCommon:{param:{Draw:1,Start:0,Length:10},ExportType:0}};
$scope.disputeDataParams1 = {dataTableCommon:{param:{Draw:1,Start:0,Length:50},ExportType:0}};
$scope.disputeDataParams2 = {dataTableCommon:{param:{Draw:1,Start:0,Length:500},ExportType:0}};
$scope.insertDisputeDataParams = {};

$scope.getDisputeData = function(){

    $http({
        url: SETTINGS[GLOBALS.ENV].apiUrl+$scope.disputeApiUrl,
        method: 'POST',
        data: $scope.disputeDataParams1,
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
        }
    }).success(function(response) {
        if(response.ResposeCode == 2){
            $scope.disputeDataAgentList = [];
            $scope.disputeDataAccountList = [];
            $scope.disputeData = {};
            if(response.DataModel){
                if(response.DataModel.dataForDisputes){
                    $scope.disputeData = response.DataModel.dataForDisputes.filter(filterDate);
                    if($scope.disputeData.length){
                        $scope.disputeData[0].guest = ($scope.disputeData[0].last_name && $scope.disputeData[0].first_name) ? ($scope.disputeData[0].last_name + "/" + $scope.disputeData[0].first_name) : ($scope.disputeData[0].last_name ? $scope.disputeData[0].last_name : $scope.disputeData[0].first_name);
                    }
                }
                if(response.DataModel.agentList){
                    $scope.disputeDataAgentList = response.DataModel.agentList;
                    
                    if($scope.disputeDataAgentList.length){
                        if($scope.disputeData.length){
                            $scope.disputeData[0].agent = "";
                            $scope.disputeData[0].agent = $scope.disputeDataAgentList[0].agentCode;
                        }
                        $scope.getEmail($scope.disputeDataAgentList);
                    }
                }
                if(response.DataModel.agentAccountData.agentAccountData){
                    $scope.disputeDataAccountList = response.DataModel.agentAccountData.agentAccountData;
                    if($scope.disputeDataAccountList.length){
                        if($scope.disputeData.length){
                            $scope.disputeData[0].account = "";
                            $scope.disputeData[0].account = $scope.disputeDataAccountList[0].accountCode;
                        }
                    }
                }   
                if($scope.disputeData.length){
                    if($scope.disputeData[0] && $scope.disputeData[0].net_act_comm){
                        $scope.disputeData[0].net_act_comm = $scope.disputeData[0].net_act_comm.replace(/\$/g, '');
                        $scope.disputeData[0].net_act_comm = Number($scope.disputeData[0].net_act_comm).toFixed(2);
                        $scope.disputeData[0].net_act_comm = "$"+$scope.disputeData[0].net_act_comm;
                    }
                }
            }
        }    

    }).error(function(err,status) {
        $scope.errorView(err,status);
    });
}

$scope.getDisputeDataForAgentAccountComment = function(){

    $http({
        url: SETTINGS[GLOBALS.ENV].apiUrl+"Payments/GetDisputeData",
        method: 'POST',
        data: $scope.disputeDataParams2,
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
        }
    }).success(function(response) {
        if(response.ResposeCode == 2){
            if(response.DataList){
                if(response.DataList.data){
                    $scope.disputeDataForAgentAccComment = response.DataList.data;
                }
            }
        }    

    }).error(function(err,status) {
        $scope.errorView(err,status);
    });

}

$scope.insertDisputeData = function(){

// $scope.insertDisputeDataParams.paymentsID = ($scope.mainObj && $scope.mainObj.paymentID)?$scope.mainObj.paymentID:'',
// $scope.insertDisputeDataParams.agentID = $scope.disputeData[0].agent;
// $scope.insertDisputeDataParams.accountID = $scope.disputeData[0].account;
// $scope.insertDisputeDataParams.comment  = $scope.disputeData[0].comment;
// $scope.insertDisputeDataParams.useSplitDataFiles =  $scope.userDetails.useSplitDataFiles;

if(!$scope.disputeDataAgentList.length){
    if($scope.disputeData.length){
        $scope.disputeData[0].agent = "";
    }
}

if(!$scope.disputeDataAccountList.length){
    if($scope.disputeData.length){
        $scope.disputeData[0].account = "";
    }
}

if($scope.disputeData.length && !$scope.disputeData[0].agent){
    toaster.pop('error', '', "Please Select Agent");
    return
}
if($scope.disputeData.length && !$scope.disputeData[0].account){
    toaster.pop('error', '', "Please Select Account");
    return
}
if($scope.disputeData.length){
    $scope.disputeData[0].net_act_comm = $scope.disputeData[0].net_act_comm.replace(/\$/g, '');

// Looping to get the selected agent id so as to send on insertion with dispute data
for(var i = 0 ; i < $scope.disputeDataAgentList.length; i++) {
    if($scope.disputeData[0].agent == $scope.disputeDataAgentList[i].agentCode) {
        var disputeAgentID = $scope.disputeDataAgentList[i].id;
    }
}

// Looping to get the selected account id so as to send on insertion with dispute data
for(var j = 0 ; j < $scope.disputeDataAccountList.length; j++) {
    if($scope.disputeData[0].account == $scope.disputeDataAccountList[j].accountCode) {
        var disputeAccountId = $scope.disputeDataAccountList[j].id;
    }
}

var fd = new FormData(); 
fd.append('paymentsID', $scope.mainObj.payment_id);
fd.append('id', $scope.disputeData[0].id);
fd.append('depositdate', $filter('date')($scope.disputeData[0].deposit_date, "dd/MM/yyyy"));
fd.append('checkNum', $scope.disputeData[0].check_num);
fd.append('guest', $scope.disputeData[0].guest);
fd.append('indate', $filter('date')($scope.disputeData[0].in_date, "dd/MM/yyyy"));
fd.append('property', $scope.disputeData[0].prop_name);
fd.append('commAmt', $scope.disputeData[0].net_act_comm);
fd.append('agent', $scope.disputeData[0].agent);
fd.append('account', $scope.disputeData[0].account);
fd.append('comment', ($scope.disputeData[0].comment)?$scope.disputeData[0].comment:"");
fd.append('useSplitDataFiles', $scope.userDetails.useSplitDataFiles);
fd.append('fileName', $scope.disputeData[0].myFile);
fd.append('ta_id', $scope.userDetails.ta_id);
fd.append('agentID', disputeAgentID);
fd.append('accountID', disputeAccountId);


/* $http({
url: SETTINGS[GLOBALS.ENV].apiUrl+"Dispute/InsertDisputeData",
method: 'POST',
data: fd,
headers: {
'Content-Type': undefined,
'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
}
})*/
$http.post(SETTINGS[GLOBALS.ENV].apiUrl+"Dispute/InsertDisputeData", fd, {
    transformRequest: angular.identity,
    headers: {
        'Content-Type': undefined,
        'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
    }
}).success(function(response) {

    if(response.ResposeCode == 2){
        toaster.pop('Success', '', response.ResponseMessage);
    }else{
        toaster.pop('error', '', response.ResponseMessage);
    }
    $scope.callBackDialog('cancel');

}).error(function(err,status) {
    $scope.errorView(err,status);
});
$scope.callBackDialog('cancel');
}
}

$scope.email = "";
$scope.getEmail = function(obj){
    if(!obj.length) {
        $scope.email = obj.email;
    } else {
        $scope.email = obj[0].email;
    }
    
}

$scope.disputePopup = function(ev){

    $scope.disputeDataParams1.id = $scope.mainObj.id;
    $scope.disputeDataParams1.agent = ($scope.userDetails && $scope.userDetails.agent)?$scope.userDetails.agent:''

// $scope.disputeDataParams1.id = 2445804405;
// $scope.disputeDataParams1.agent = 102;
$scope.disputeDataParams1.account = ($scope.userDetails && $scope.userDetails.account)?$scope.userDetails.account:''

if($scope.userDetails.useSplitDataFiles == true){
    $scope.disputeApiUrl = "Payment/GetPaymentDataForDisputes";
}else{
    $scope.disputeApiUrl = "Payments/GetPaymentsDataForDisputes";                
}

$scope.getDisputeData();

$scope.disputeDataParams2.paymentsID = $scope.mainObj.paymentID;
$scope.disputeDataParams2.agentID = ($scope.userDetails && $scope.userDetails.agent)?$scope.userDetails.agent:'';
// $scope.disputeDataParams2.paymentsID = 2410896800;
// $scope.disputeDataParams2.agentID = '1322';

$scope.getDisputeDataForAgentAccountComment();

$mdDialog.show({
    contentElement: '#dispute',
    parent: angular.element(document.body),
    targetEvent: ev,
    clickOutsideToClose:true,
    fullscreen: false,
    skipHide: true
}).finally(function() {
});
}

$scope.started = false;

function closeModals() {

}

$scope.$on('IdleStart', function() {
 // console.log("**-");
});

$scope.$on('IdleEnd', function() {

});

$scope.$on('IdleTimeout', function() {
    $scope.signout();
});

$scope.start = function() {
    Idle.watch();
    $scope.started = true;
};

$scope.stop = function() {
    Idle.unwatch();
    $scope.started = false;
};

/* ===== Dispute End  =====*/

}]);

app.controller('DialogController', ['$scope', '$rootScope','$cookies','$cookieStore','$state','authService','resourceService','$http','$mdDialog',function($scope, $rootScope,$cookies,$cookieStore,$state,authService,resourceService,$http,$mdDialog) {
    $scope.hide = function() {
        $mdDialog.hide();
    };

    $scope.cancel = function() {
        $mdDialog.cancel();
    };

    $scope.answer = function(answer) {
        $mdDialog.hide(answer);
    };
}]);