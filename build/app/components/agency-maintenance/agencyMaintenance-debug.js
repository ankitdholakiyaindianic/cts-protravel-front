App.controller('agencyMaintenanceCtrl', ['$scope','authService','$cookies','$cookieStore','$state','$stateParams', '$http', 'GLOBALS','SETTINGS', 'toaster','$mdDialog', function($scope, authService,$cookies, $cookieStore, $state, $stateParams, $http, GLOBALS,SETTINGS,toaster, $mdDialog){
	
	var t;
	$scope.reportData = {
		report: "",
	}, t = angular.copy($scope.reportData), $scope.revert = function() {
		$scope.reportData = angular.copy(t), $scope.report_form.$setPristine(), $scope.report_form.$setUntouched()
	}, $scope.canRevert = function() {
		return !angular.equals($scope.reportData, t) || !$scope.report_form.$pristine
	}, $scope.canSubmit = function() {
		return $scope.report_form.$valid && !angular.equals($scope.reportData, t)
	}, $scope.submitForm = function() {
		$scope.showInfoOnSubmit = !0, e.revert()
	}

	
	$scope.reportParams1 = {};
    $scope.reportParams2 = {};
    $scope.updateAgencyParams = {};
    $scope.updateMediaTypeParams = {};
    $scope.reportDetailsParams = {};
    $scope.updateReportDataParams = {};
    $scope.deleteReportParam = {};
    $scope.unitsObj = {};
    $scope.reportObj = {};
    $scope.loading = false;
    $scope.isReportData = false;
    $scope.reportListLoding = false;
    $scope.reportUpdateLoding = false;
    $scope.deleteReportLoading = false;


    /***** Get Report list *****/
    $scope.getAgencyList = function(){
      $scope.unitsObj.id = $scope.userDetails.corpID;
      $scope.unitsObj.unit = "";

      $scope.reportListLoding = true;

      $http({
        url: SETTINGS[GLOBALS.ENV].apiUrl+"Corp_Agency/GetCorpBusinessUnits",
        method: 'POST',
        data: $.param($scope.unitsObj),
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
        }
    }).success(function(response) { 

        if(response.ResposeCode == 2){
            $scope.reportListLoding = false;
            $scope.unitList = response.DataList.data;
        }else{

        }
    });
}

$scope.getAgencyList();
/***** Get Report list *****/




/***** Get Agency Data *****/
$scope.getAllAgencyData = function(report){

   $scope.reportParams1.str = report.report;
   if($scope.reportParams1.str){

    $scope.loading = true;

        // $scope.reportParams1.str = "ACENDAS";

        $http({
            url: SETTINGS[GLOBALS.ENV].apiUrl+"Agency/GetMediaTypes",
            method: 'POST',
            data: $scope.reportParams1,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
            }
        }).success(function(response) { 
        	$scope.loading = false;

            if(response.ResposeCode == 2){
                if(response.DataList.data){
                    $scope.isReportData = true;
                    $scope.mediaType = response.DataList.data;
                }
            }else{

            }
        });

        $scope.reportParams2.ta_id = report.report;
        $scope.reportParams2.active = "Y";

        $http({
            url: SETTINGS[GLOBALS.ENV].apiUrl+"Agency/GetAgencyPayLoc",
            method: 'POST',
            data: $.param($scope.reportParams2),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
            }
        }).success(function(response) { 
            $scope.loading = false;

            if(response.ResposeCode == 2){
                if(response.DataModel){
                    $scope.isReportData = true;
                    $scope.agency = response.DataModel;
                }else{
                    $scope.agency = "";
                }
            }else{
                $scope.agency = "";
            }
        });
    }else{
        $scope.isReportData = false;
    }
};
/***** Get Agency Data *****/
/*$scope.updateMediaTypeParams.ta_id = $scope.report; 
        $scope.updateMediaTypeParams.mediaCode = $scope.MediaCode; 
        $scope.updateMediaTypeParams.PayorID = $scope.PayorID; 
        $scope.updateMediaTypeParams.Web_AutoRelease = $scope.list.Web_AutoRelease; */
        function getOnlySelectedReports(obj){
            var finalObj = {};
            if(obj.Web_AutoRelease == true){
                finalObj.mediaCode = obj.mediaCode;
                finalObj.PayorID = obj.PayorID;
                finalObj.Web_AutoRelease = obj.Web_AutoRelease;

            }
            delete obj.de_ID;
            delete obj.MediaDesc;
            return finalObj;
        }


        /***** Update Agency Data *****/

        $scope.updateAgency = function(agencyDetails){
            $scope.updateAgencyParams.ta_id = $scope.report;
            $scope.updateAgencyParams.ta_name = agencyDetails.Agency_pay_loc.ta_name;
            $scope.updateAgencyParams.ta_address1 = agencyDetails.Agency_pay_loc.ta_address1;
            $scope.updateAgencyParams.ta_address2 = agencyDetails.Agency_pay_loc.ta_address2;
            $scope.updateAgencyParams.ta_city = agencyDetails.Agency_pay_loc.ta_city;
            $scope.updateAgencyParams.ta_state = agencyDetails.Agency_pay_loc.ta_state;
            $scope.updateAgencyParams.ta_zip = agencyDetails.Agency_pay_loc.ta_zip;
            $scope.updateAgencyParams.ta_phone = agencyDetails.Agency_pay_loc.ta_phone;
            $scope.updateAgencyParams.ta_fax = agencyDetails.Agency_pay_loc.ta_fax;
            $scope.updateAgencyParams.claimDisputeEmail = agencyDetails.Corp_Agency.claimDisputeEmail;
            $scope.updateAgencyParams.claimAssistanceEmail = agencyDetails.Corp_Agency.claimAssistanceEmail;
            $scope.updateAgencyParams.businessUnitCode = ($scope.agency && $scope.agency.Corp_Agency && $scope.agency.Corp_Agency.businessUnitCode)?$scope.agency.Corp_Agency.businessUnitCode:null;
            $scope.updateAgencyParams.businessUnitDescription = ($scope.agency && $scope.agency.Corp_Agency && $scope.agency.Corp_Agency.businessUnitDescription)?$scope.agency.Corp_Agency.businessUnitDescription:null;
            $scope.updateAgencyParams.ta_id_source = ($scope.agency && $scope.agency.Corp_Agency && $scope.agency.Corp_Agency.ta_id_source)?$scope.agency.Corp_Agency.ta_id_source:null;

            var mediaTypeDetails = $scope.mediaType.filter(getOnlySelectedReports);
            $scope.updateMediaTypeParams = mediaTypeDetails;

            $scope.reportUpdateLoding = true;

            $http({
                url: SETTINGS[GLOBALS.ENV].apiUrl+"Corp_Agency/UpdateAgencyData",
                method: 'POST',
                data: $scope.updateAgencyParams,
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
                }
            }).success(function(response) { 
                $scope.reportUpdateLoding = false;

                if(response.ResposeCode == 2){
                    $http({
                        url: SETTINGS[GLOBALS.ENV].apiUrl+"Agency/UpdateMediaType",
                        method: 'POST',
                        data: $scope.updateMediaTypeParams,
                        headers: {
                            'Content-Type': 'application/json',
                            'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
                        }
                    }).success(function(response) { 
                        $scope.reportUpdateLoding = false;
                        if(response.ResposeCode == 2){
                            toaster.pop('Success', '', response.ResponseMessage);
                        }else{
                            toaster.pop('error', '', response.ResponseMessage);
                        }
                    });
                }else{
                    toaster.pop('error', '', response.ResponseMessage);
                }
            });
        }
        /***** Update Agency Data *****/


        /***** Delete Report Data *****/
        $scope.deleteReport = function(){
            $scope.deleteReportLoading = true;
            $scope.deleteReportParam.id = $scope.id;

            $http({
                url: SETTINGS[GLOBALS.ENV].apiUrl+"Report/DeleteCustomReportData",
                method: 'POST',
                data: $.param($scope.deleteReportParam),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Authorization': 'bearer '+authService.getAuthToken('AuthToken')                

                }
            }).success(function(response) { 
                $scope.deleteReportLoading = false;
                if(response.ResposeCode == 2){
                    toaster.pop('Success', '', response.ResponseMessage);
                    $scope.getReportList();
                    $scope.cancelUpdate();
                }else{
                    toaster.pop('error', '', response.ResponseMessage);
                }
            });
        }
        /***** Delete Report Data *****/



        /***** Delete popup *****/
        $scope.popupTitle = 'Delete Report';
        $scope.pageDescription = 'Would you like to delete?';

        $scope.deleteReportConfirm = function(ev){
           $mdDialog.show({
            contentElement: '#conFirmPopup',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose:true,
            fullscreen: false
        }).finally(function() {
        });
    }

    $scope.callBackDialog = function(data){
        if(data == 'ok'){
            /*Call delete API here */
            $scope.deleteReport();
        }
        $mdDialog.cancel();
    }

    $scope.cancelUpdate = function(){
        $scope.isReportData = false;
        $scope.report = "selectAgency";
    }
    
}]);