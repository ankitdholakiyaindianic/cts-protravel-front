App.controller('batchReleaseUtilityCtrl', ['$scope','$state','$stateParams', '$rootScope', 'MetaInformation','$http', 'SETTINGS', 'GLOBALS', 'authService','toaster','$mdDialog','$filter', function($scope,$state,$stateParams, $rootScope, MetaInformation,$http, SETTINGS, GLOBALS, authService,toaster,$mdDialog,$filter) {

    $scope.unmatchedPaymentRpt = {};
    $scope.unmatchedPaymentRpt.fromDate = null;
    $scope.unmatchedPaymentRpt.toDate = null;
    $scope.unmatchedPaymentRpt.selectedDate = 0;

    $scope.noData = "No data found";
    $scope.corpID = $scope.userDetails.corpID;
    $scope.unmatchedPaymentLoading = false;

    var finalTotal = 0;
    $scope.totalCommission = function(obj){
        finalTotal += Number(obj.net_act_comm);
        return finalTotal;
    }
    function filterDate(obj){
        if(obj.deposit_date){
            obj.deposit_date = new Date(obj.deposit_date);
        }
        if(obj.check_date){
            obj.check_date = new Date(obj.check_date);
        }
        return obj;
    }

    $scope.bookingToFilter = function() {
        indexedTeams = [];
        return $scope.unmatchedPayment;
    }


    $scope.filterTeams = function(player) {
        var teamIsNew = indexedTeams.indexOf(player.cts_type) == -1;
        if (teamIsNew) {
            indexedTeams.push(player.cts_type);
        }
        return teamIsNew;
    }

   $scope.bookingReportParms = {
        dataTableCommon:{
            param:{
                Draw:1,
                Start:0,
                Length:10
            },
            ExportType:0
        },
        corpID:$scope.corpID,
        fromdate: ($scope.unmatchedPaymentRpt.fromDate) ? $scope.unmatchedPaymentRpt.fromDate : "",
        todate: ($scope.unmatchedPaymentRpt.toDate) ? $scope.unmatchedPaymentRpt.toDate : "",
        dateSelect:""
    };

   

    $scope.checkValidDate = function(fromDate,toDate){
        if(toDate && fromDate > toDate){
            $scope.unmatchedPaymentRpt.toDate = fromDate;
        }
    }

// $scope.unmatchedPayment = [{"Type":1,"PropertyName":"HOTEL METROPOLE MONTE CAR","PropertyCity":"","PropertyState":"","Bookings":1,"PaymentCount":0,"ExpectedCommission":0.0,"ActualCommission":0.0,"Pmt":0,"PaymentDays":0,"CXL":0,"DUE":0,"NC":0,"PP":0,"NS":0,"UnAns":0,"Other":0,"UnColl":0,"typeDescription":"Hotel","roomNights":1,"paymentPercent":0.0,"resolvedPercent":0.0,"commissionPercent":0.0,"averagePaymentDays":0.0},{"Type":1,"PropertyName":"THE LONDON NYC","PropertyCity":"NEW YORK","PropertyState":"","Bookings":3,"PaymentCount":0,"ExpectedCommission":0.0,"ActualCommission":0.0,"Pmt":0,"PaymentDays":0,"CXL":0,"DUE":0,"NC":0,"PP":0,"NS":0,"UnAns":0,"Other":0,"UnColl":0,"typeDescription":"Hotel","roomNights":6,"paymentPercent":0.0,"resolvedPercent":0.0,"commissionPercent":0.0,"averagePaymentDays":0.0},{"Type":1,"PropertyName":"HOTEL D ANGLETERRE","PropertyCity":"","PropertyState":"","Bookings":2,"PaymentCount":0,"ExpectedCommission":0.0,"ActualCommission":0.0,"Pmt":0,"PaymentDays":0,"CXL":0,"DUE":0,"NC":0,"PP":0,"NS":0,"UnAns":0,"Other":0,"UnColl":0,"typeDescription":"Hotel","roomNights":2,"paymentPercent":0.0,"resolvedPercent":0.0,"commissionPercent":0.0,"averagePaymentDays":0.0},{"Type":1,"PropertyName":"BABUINO 181","PropertyCity":"","PropertyState":"","Bookings":6,"PaymentCount":0,"ExpectedCommission":0.0,"ActualCommission":0.0,"Pmt":0,"PaymentDays":0,"CXL":0,"DUE":0,"NC":0,"PP":0,"NS":0,"UnAns":0,"Other":1,"UnColl":0,"typeDescription":"Hotel","roomNights":13,"paymentPercent":0.0,"resolvedPercent":0.0,"commissionPercent":0.0,"averagePaymentDays":0.0},{"Type":1,"PropertyName":"HOTEL MARLOWE","PropertyCity":"Cambridge","PropertyState":"","Bookings":3,"PaymentCount":0,"ExpectedCommission":0.0,"ActualCommission":0.0,"Pmt":0,"PaymentDays":0,"CXL":0,"DUE":0,"NC":0,"PP":0,"NS":0,"UnAns":0,"Other":0,"UnColl":0,"typeDescription":"Hotel","roomNights":3,"paymentPercent":0.0,"resolvedPercent":0.0,"commissionPercent":0.0,"averagePaymentDays":0.0},{"Type":1,"PropertyName":"CLAYTON HOTEL","PropertyCity":"LONDON","PropertyState":"","Bookings":3,"PaymentCount":0,"ExpectedCommission":0.0,"ActualCommission":0.0,"Pmt":0,"PaymentDays":0,"CXL":0,"DUE":0,"NC":0,"PP":0,"NS":0,"UnAns":0,"Other":0,"UnColl":0,"typeDescription":"Hotel","roomNights":3,"paymentPercent":0.0,"resolvedPercent":0.0,"commissionPercent":0.0,"averagePaymentDays":0.0},{"Type":1,"PropertyName":"1001 3RD STREET","PropertyCity":"West Hollywood","PropertyState":"","Bookings":2,"PaymentCount":0,"ExpectedCommission":0.0,"ActualCommission":0.0,"Pmt":0,"PaymentDays":0,"CXL":0,"DUE":0,"NC":0,"PP":0,"NS":0,"UnAns":0,"Other":0,"UnColl":0,"typeDescription":"Hotel","roomNights":2,"paymentPercent":0.0,"resolvedPercent":0.0,"commissionPercent":0.0,"averagePaymentDays":0.0},{"Type":1,"PropertyName":"HYATT HOTELS","PropertyCity":"Toronto","PropertyState":"","Bookings":2,"PaymentCount":0,"ExpectedCommission":0.0,"ActualCommission":0.0,"Pmt":0,"PaymentDays":0,"CXL":0,"DUE":0,"NC":0,"PP":0,"NS":1,"UnAns":0,"Other":0,"UnColl":0,"typeDescription":"Hotel","roomNights":2,"paymentPercent":0.0,"resolvedPercent":0.0,"commissionPercent":0.0,"averagePaymentDays":0.0},{"Type":1,"PropertyName":"RAFFLES INT HOTEL","PropertyCity":"DUBAI","PropertyState":"","Bookings":4,"PaymentCount":0,"ExpectedCommission":0.0,"ActualCommission":0.0,"Pmt":0,"PaymentDays":0,"CXL":0,"DUE":0,"NC":0,"PP":0,"NS":0,"UnAns":0,"Other":0,"UnColl":0,"typeDescription":"Hotel","roomNights":4,"paymentPercent":0.0,"resolvedPercent":0.0,"commissionPercent":0.0,"averagePaymentDays":0.0},{"Type":1,"PropertyName":"W MONTREAL","PropertyCity":"MONTREAL","PropertyState":"","Bookings":6,"PaymentCount":0,"ExpectedCommission":0.0,"ActualCommission":0.0,"Pmt":0,"PaymentDays":0,"CXL":0,"DUE":0,"NC":0,"PP":0,"NS":0,"UnAns":0,"Other":0,"UnColl":0,"typeDescription":"Hotel","roomNights":21,"paymentPercent":0.0,"resolvedPercent":0.0,"commissionPercent":0.0,"averagePaymentDays":0.0}]

    $scope.callFiterData = function(valid){
        if(valid){
            $scope.bookingReportParms.dataTableCommon.ExportType = 0;
        }
        if(!$scope.bookingReportParms.dataTableCommon.ExportType){
            $scope.unmatchedPayment = [];
            $scope.bookingReportParms.fromdate = $filter('date')($scope.unmatchedPaymentRpt.fromDate, "dd/MM/yyyy");
            $scope.bookingReportParms.todate = $filter('date')($scope.unmatchedPaymentRpt.toDate, "dd/MM/yyyy");
            // $scope.bookingReportParms.dateSelect = $scope.unmatchedPaymentRpt.selectedDate;
            $scope.unmatchedPaymentLoading = true;
        }
        $http({
            url: SETTINGS[GLOBALS.ENV].apiUrl+"Commission/GetCommissionAnalysis",
            method: 'POST',
            data: $scope.bookingReportParms,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
            }
        }).success(function(response) {

            console.log("New response CA : ", response);

            // return false;

            $scope.unmatchedPaymentLoading = false;
            if(response.ResposeCode == 2){
                if(response.DataList.data){
                    $scope.unmatchedPayment = response.DataList.data;
                }
                if(response.DataList.fileName){
                    window.open(
                        SETTINGS[GLOBALS.ENV].apiUrl+response.DataList.fileName,
                        '_blank'
                        );
                }
                $scope.unmatchedPaymentTotal = response.DataList.recordsTotal;
            }
        }).error(function(err,status) {
            $scope.errorView(err,status);
        });
    }

     if($stateParams.fromdate && $stateParams.todate){
        var fromDate = $stateParams.fromdate;
        var toDate = $stateParams.todate;
        var id = $stateParams.id;
        $scope.unmatchedPaymentRpt.fromDate = new Date(fromDate);
        $scope.unmatchedPaymentRpt.toDate = new Date(toDate);
        $scope.bookingReportParms.prop_chain = id;
        $scope.callFiterData(true);        
    }


    $scope.getPDFLink = function(expot){
        $scope.bookingReportParms.dataTableCommon.ExportType = Number(expot);
        $scope.callFiterData();
    }

    /* ==== Pagination Click function ==== */
    $scope.logOrder = function (order) {
        //console.log('order: ', order);
    };

    $scope.logPagination = function (page, limit) {
        var startPage = (limit * (page - 1));
        $scope.bookingReportParms.dataTableCommon.param.Start = startPage;
        $scope.bookingReportParms.dataTableCommon.param.Length = limit;
        $scope.bookingReportParms.dataTableCommon.ExportType = 0;
        $scope.callFiterData();
    }

}]);