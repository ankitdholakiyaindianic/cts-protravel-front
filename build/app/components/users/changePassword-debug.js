App.controller('changePassword', ['$scope','authService','$cookies','$cookieStore','$state','$stateParams', '$http', 'GLOBALS','SETTINGS', 'toaster',function($scope, authService,$cookies, $cookieStore, $state, $stateParams, $http, GLOBALS,SETTINGS,toaster){
	
	var t;
	$scope.user = {
		userID: "",
	}, t = angular.copy($scope.user), $scope.revert = function() {
		$scope.user = angular.copy(t), $scope.login_form.$setPristine(), $scope.login_form.$setUntouched()
	}, $scope.canRevert = function() {
		return !angular.equals($scope.user, t) || !$scope.login_form.$pristine
	}, $scope.canSubmit = function() {
		return $scope.login_form.$valid && !angular.equals($scope.user, t)
	}, $scope.submitForm = function() {
		$scope.showInfoOnSubmit = !0, e.revert()
	}

	/***** Get the User list *****/
	$scope.getUserList = function(){

		$scope.userListLoding = true;

		$http({
            url: SETTINGS[GLOBALS.ENV].apiUrl+"User/GetUserList",
            method: 'POST',
            data: $.param($scope.getUserDetails),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
            }
        }).success(function(response) { 
        	
        });
	}


}]);