App.controller('userMaintenance', ['$scope','authService','$cookies','$cookieStore','$state','$stateParams', '$http', 'GLOBALS','SETTINGS', 'toaster',function($scope, authService,$cookies, $cookieStore, $state, $stateParams, $http, GLOBALS,SETTINGS,toaster){
	
	var t;
	$scope.userData = {
		userID: "",
	}, t = angular.copy($scope.userData), $scope.revert = function() {
		$scope.userData = angular.copy(t), $scope.login_form.$setPristine(), $scope.login_form.$setUntouched()
	}, $scope.canRevert = function() {
		return !angular.equals($scope.userData, t) || !$scope.login_form.$pristine
	}, $scope.canSubmit = function() {
		return $scope.login_form.$valid && !angular.equals($scope.userData, t)
	}, $scope.submitForm = function() {
		$scope.showInfoOnSubmit = !0, e.revert()
	}

	
	$scope.userDetail = {};
	$scope.getUserDetails = {};
	$scope.unitsObj = {};
	$scope.reportObj = {};
	$scope.displayUserDetails = false;
	$scope.loading = false;
	$scope.userListLoding = false;
	$scope.userUpdateLoding = false;
	$scope.getUserDetails.ta_id = $scope.userDetails.ta_id;
	$scope.userid = 15;


    $scope.querySearch = function(query) {
        var results = query ? $scope.userListSearch.filter($scope.createFilterFor(query) ) : $scope.userListSearch, deferred;
        if (false) {
            deferred = $q.defer();
            $timeout(function () { deferred.resolve( results ); }, Math.random() * 1000, false);
            return deferred.promise;
        } else {
            return results;
        }
    }

    $scope.createFilterFor = function(query) {
      var lowercaseQuery = angular.lowercase(query);
      return function filterFn(state) {
        var setval = state.userID + ' - ' + state.name;
        // var setval = state.accountCode;
        state['querySearch'] = setval;
        if(state.userID && state.userID.toString().toLowerCase().indexOf(lowercaseQuery) != -1){
            return (state.userID && state.userID.toString().toLowerCase().indexOf(lowercaseQuery) === 0);
        }
        if(state.name && state.name.toLowerCase().indexOf(lowercaseQuery) != -1){
            return (state.name && state.name.toLowerCase().indexOf(lowercaseQuery) === 0);
        }
        /*if(state.description && state.description.toLowerCase().indexOf(lowercaseQuery) != -1){
            return (state.description && state.description.toLowerCase().indexOf(lowercaseQuery) === 0);
        }*/
        return (state.userID && state.name && setval.toLowerCase().indexOf(lowercaseQuery) === 0);
        // return (state.accountCode && setval.toLowerCase().indexOf(lowercaseQuery) === 0);

    };
}

/***** Get the User list *****/
$scope.getUserList = function(){

  $scope.userListLoding = true;

  $http({
    url: SETTINGS[GLOBALS.ENV].apiUrl+"User/GetUserList",
    method: 'POST',
    data: $.param($scope.getUserDetails),
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
    }
}).success(function(response) { 
   $scope.userListLoding = false;
   if(response.ResposeCode == 2){
    $scope.userList = response.DataList.data;
    $scope.userListSearch = response.DataList.data;
    
}else{

}
});
}

$scope.getUserList();

/***** Restrict user by -> Unit*****/

$scope.getUnits = function(){

  $scope.unitsObj.id = $scope.userDetails.corpID;
  $scope.unitsObj.unit = "";

  $http({
    url: SETTINGS[GLOBALS.ENV].apiUrl+"Corp_Agency/GetCorpBusinessUnits",
    method: 'POST',
    data: $.param($scope.unitsObj),
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
    }
}).success(function(response) { 

    if(response.ResposeCode == 2){
        $scope.unitList = response.DataList.data;
    }else{

    }
});
}

$scope.getUnits();

/***** Restrict user by -> Unit*****/


/***** Get reports data *****/

$scope.getReports = function(){

		// $scope.reportObj.userid = $scope.userid;
        $scope.reportObj.userid = $scope.userDetails.userID;

        $http({
            url: SETTINGS[GLOBALS.ENV].apiUrl+"Report/GetReportList",
            method: 'POST',
            data: $.param($scope.reportObj),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
            }
        }).success(function(response) { 
            // if(response.ResposeCode == 2){

            // }else{

            // }
        });
    }

    $scope.getReports();

    /***** Get reports data *****/


    function filterBoolean(obj,key){
        angular.forEach(obj, function(value, key) {
        	if(typeof obj[key] == 'boolean'){
        		if(obj[key]){
        			obj[key] = 1;
        		}else{
        			obj[key] = 0;
        		}
        	}
        });
        return obj;
    }

    function enableCheckboxFn(obj,key){
    	
        if(obj.updatePayments_Type == true){
        	$scope.userData.user_Permissions.paymentRecordupdate = true;	
        }else{
        	$scope.userData.user_Permissions.paymentRecordupdate = false;
        }
        // return obj;
    }

    $scope.selectedTab = 0;
    /***** Get the User Data by userID *****/
    $scope.getUserData = function(userDetail, flag){
        console.log("flag : ", flag);

        if(flag == true){
            // $scope.selectedItem = "";
            console.log("If flag");
        }else{
            console.log("Else flag");
            $scope.userID = 'select';
        }
        if(userDetail){
            $scope.selectedTab = 0;

            $scope.loading = true;

            $scope.userDetail.userID = userDetail.userID;

            $http({
                url: SETTINGS[GLOBALS.ENV].apiUrl+"User/GetUserData_ForProtravel",
            // url:"http://10.2.5.25/cst.WebApi/User/GetUserData",
            method: 'POST',
            data: $.param($scope.userDetail),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
                // 'Authorization': 'bearer HCKLHb7Y1Zb-bDGyaCtK_Xpm330XMklEUezxhBYpHQhjcXWUNT5Kcs9f43plcvV9Hj9y9Fdxh7LP0Vgq6BBmBXcPJebozFRo1nbfRXgVQEu-_C_C93rpgmAHbM1e28oN5RMpw-gNJXhymACRDiglbuqAz40orh665bGVnZF_b_tNQKN9U85kojFpAZDWiAtNWU0zJ0dEX8pwpf7WwZVYMjV9lJ1u7LDrxBZXfFhHJApKWlr72juxuEcx7NiX8YczgJ9VAE7Ab10HrkaaQLmReQ'
            }
        }).success(function(response) { 
        	$scope.loading = false;
        	$scope.displayUserDetails = true;
            if(response.ResposeCode == 2){

                $scope.userData = response.DataModel;
                if($scope.userData.user.password){
                    $scope.userData.user.password = "";
                }
                
                if($scope.userData){
                    if($scope.userData.user.active == true){
                        $scope.userData.user.active = 1;
                    }else{
                        $scope.userData.user.active = 0;
                    }
                }
                
                if($scope.userData){
                    if($scope.userData.user_Permissions){
                        if($scope.userData.user_Permissions.maintainBusinessCalendar_Dates != null || $scope.userData.user_Permissions.maintainBusinessCalendar_Close != null){
                            if($scope.userData.user_Permissions.maintainBusinessCalendar_Dates == true || $scope.userData.user_Permissions.maintainBusinessCalendar_Close == true){
                             $scope.userData.user_Permissions.businessCalendar = true;
                         }else{
                             $scope.userData.user_Permissions.businessCalendar = false;
                         }
                     }

                     if($scope.userData.user_Permissions.updatePayments_Type == true || $scope.userData.user_Permissions.updatePayments_PostRelease_Confirmation == true || $scope.userData.user_Permissions.updatePayments_PropertyChain == true || $scope.userData.user_Permissions.updatePayments_DepositDate == true || $scope.userData.user_Permissions.updatePayments_LastName == true || $scope.userData.user_Permissions.updatePayments_PostRelease_IATA == true || $scope.userData.user_Permissions.updatePayments_PropertyName == true || $scope.userData.user_Permissions.updatePayments_CheckNo == true || $scope.userData.user_Permissions.updatePayments_FirstName == true || $scope.userData.user_Permissions.updatePayments_PostRelease_Branch == true || $scope.userData.user_Permissions.updatePayments_PropertyAddress == true || $scope.userData.user_Permissions.updatePayments_CheckDate == true || $scope.userData.user_Permissions.updatePayments_InDate == true || $scope.userData.user_Permissions.updatePayments_PostRelease_Agent == true || $scope.userData.user_Permissions.updatePayments_PropertyCity == true || $scope.userData.user_Permissions.updatePayments_Commissions == true || $scope.userData.user_Permissions.updatePayments_OutDate == true || $scope.userData.user_Permissions.updatePayments_PostRelease_Account == true || $scope.userData.user_Permissions.updatePayments_PropertyState == true || $scope.userData.user_Permissions.updatePayments_Tax1 == true || $scope.userData.user_Permissions.updatePayments_Nights == true || $scope.userData.user_Permissions.updatePayments_PropertyZip == true || $scope.userData.user_Permissions.updatePayments_Rooms == true || $scope.userData.user_Permissions.updatePayments_PropertyCountry == true || $scope.userData.user_Permissions.updatePayments_Rate == true || $scope.userData.user_Permissions.updatePayments_PropertyPhone == true || $scope.userData.user_Permissions.updatePayments_RateCode == true || $scope.userData.user_Permissions.updatePayments_PropertyFax == true || $scope.userData.user_Permissions.updatePayments_CorporateDiscount == true || $scope.userData.user_Permissions.updatePayments_HotelID == true || $scope.userData.user_Permissions.updatePayments_BookingPCC == true || $scope.userData.user_Permissions.updatePayments_AgentPCC == true){
                         $scope.userData.user_Permissions.paymentRecordupdate = true;
                     }else{
                         $scope.userData.user_Permissions.paymentRecordupdate = false;
                     }
                     if($scope.userData.user.password){
                        $scope.login_form.reEnterPassword.$dirty = true;
                    }
                }
            }    
        }else{
            $scope.userData = "";
        }
    });
}else{

}

};

/***** Get the User Data by userID *****/

/****** Check Password and reenter password matches! ******/
$scope.checkPassword = function(password, confirmPassword){
    if(password || confirmPassword){
        if(password != confirmPassword){
            $scope.login_form.$valid = false;
            return "Password and ReEnter password does not match.";
        }else{
                // $scope.login_form.$valid = true;
                $scope.login_form.reEnterPassword.$error = true;
                $scope.login_form.reEnterPassword.$dirty = true;
            }
        }
    }

    /***** Update User Data *****/
    $scope.updateUserData = function(userMaintenanceDetails){

      var useData = [userMaintenanceDetails.user];


      if(userMaintenanceDetails.user_Permissions && userMaintenanceDetails.user_Permissions.businessCalendar){
        if(userMaintenanceDetails.user_Permissions.businessCalendar == false){
            userMaintenanceDetails.user_Permissions.maintainBusinessCalendar_Dates = false;
            userMaintenanceDetails.user_Permissions.maintainBusinessCalendar_Close = false;
        }
    }
    if(userMaintenanceDetails.user_Permissions && userMaintenanceDetails.user_Permissions.paymentRecordupdate){
        if(userMaintenanceDetails.user_Permissions.paymentRecordupdate == false){
            userMaintenanceDetails.user_Permissions.updatePayments_Type = false;
            userMaintenanceDetails.user_Permissions.updatePayments_PostRelease_Confirmation = false; 
            userMaintenanceDetails.user_Permissions.updatePayments_PropertyChain = false; 
            userMaintenanceDetails.user_Permissions.updatePayments_DepositDate = false;
            userMaintenanceDetails.user_Permissions.updatePayments_LastName = false;
            userMaintenanceDetails.user_Permissions.updatePayments_PostRelease_IATA = false;
            userMaintenanceDetails.user_Permissions.updatePayments_PropertyName = false; 
            userMaintenanceDetails.user_Permissions.updatePayments_CheckNo = false;
            userMaintenanceDetails.user_Permissions.updatePayments_FirstName = false;
            userMaintenanceDetails.user_Permissions.updatePayments_PostRelease_Branch = false;
            userMaintenanceDetails.user_Permissions.updatePayments_PropertyAddress = false;
            userMaintenanceDetails.user_Permissions.updatePayments_CheckDate = false;
            userMaintenanceDetails.user_Permissions.updatePayments_InDate = false;
            userMaintenanceDetails.user_Permissions.updatePayments_PostRelease_Agent = false;
            userMaintenanceDetails.user_Permissions.updatePayments_PropertyCity = false;
            userMaintenanceDetails.user_Permissions.updatePayments_Commissions = false;
            userMaintenanceDetails.user_Permissions.updatePayments_OutDate = false;
            userMaintenanceDetails.user_Permissions.updatePayments_PostRelease_Account = false;
            userMaintenanceDetails.user_Permissions.updatePayments_PropertyState = false;
            userMaintenanceDetails.user_Permissions.updatePayments_Tax1 = false;
            userMaintenanceDetails.user_Permissions.updatePayments_Nights = false;
            userMaintenanceDetails.user_Permissions.updatePayments_PropertyZip = false;
            userMaintenanceDetails.user_Permissions.updatePayments_Rooms = false;
            userMaintenanceDetails.user_Permissions.updatePayments_PropertyCountry = false;
            userMaintenanceDetails.user_Permissions.updatePayments_Rate = false;
            userMaintenanceDetails.user_Permissions.updatePayments_PropertyPhone = false;
            userMaintenanceDetails.user_Permissions.updatePayments_RateCode = false;
            userMaintenanceDetails.user_Permissions.updatePayments_PropertyFax = false;
            userMaintenanceDetails.user_Permissions.updatePayments_CorporateDiscount = false;
            userMaintenanceDetails.user_Permissions.updatePayments_HotelID = false;
            userMaintenanceDetails.user_Permissions.updatePayments_BookingPCC = false;
            userMaintenanceDetails.user_Permissions.updatePayments_AgentPCC = false;
        }
    }
    if(userMaintenanceDetails.user_Permissions){
        var userPermissionsData = [userMaintenanceDetails.user_Permissions];
    }

    var userObj = useData.filter(filterBoolean);
// if(userObj[0].reEnterPassword){
//     delete userObj[0].reEnterPassword;
// }

userObj[0].passwordNew = userObj[0].password;

var userObjNew = angular.copy(userObj[0]);
delete userObjNew.password;
if(userObjNew.passwordNew == ""){
    userObjNew.passwordNew = null;
}


		// var userPermissionsObj = userPermissionsData.filter(filterBoolean);
		var finalObj = {
			user:userObjNew,
			user_Permissions:(userPermissionsData)?userPermissionsData[0]:{},
            user_Reports: $scope.userData.userReports
        }

        if(finalObj.user_Permissions && finalObj.user_Permissions.businessCalendar == false){
            finalObj.user_Permissions.maintainBusinessCalendar_Dates = false;
            finalObj.user_Permissions.maintainBusinessCalendar_Close = false;
        }

        if(!finalObj.user.name){
          toaster.pop('error', '', "Please Enter Name");
          return;
      }

      if(!finalObj.user.email){
          toaster.pop('error', '', "Please Enter Email");
          return;
      }

      if(finalObj.user.passwordNew || finalObj.user.reEnterPassword){
          if(finalObj.user.passwordNew != finalObj.user.reEnterPassword){
            toaster.pop('error', '', "Password and ReEnter password does not match.");
            return;   
        }
    }

    if($scope.userData){
        if($scope.userData.user_Permissions){
            if($scope.userData.user_Permissions.paymentRecordupdate == true){
                finalObj.user.allowRecordUpdate = 1;
                finalObj.user_Permissions.User.allowRecordUpdate = true;
            }else{
                finalObj.user.allowRecordUpdate = 0;
                finalObj.user_Permissions.User.allowRecordUpdate = false;
            }
        }
    }

    $scope.userUpdateLoding = true;

    $http({
        url: SETTINGS[GLOBALS.ENV].apiUrl+"User/UpdateUserData",
        method: 'POST',
        data: finalObj,
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'bearer '+authService.getAuthToken('AuthToken')                

        }
    }).success(function(response) { 
     $scope.userUpdateLoding = false;
     if(response.ResposeCode == 2){
      toaster.pop('Success', '', response.ResponseMessage);
  }else{

  }
});

}

$scope.reDirectPage = function(){
    $state.go("main.addUserProfile");
}

}]);