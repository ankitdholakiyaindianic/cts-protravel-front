App.controller('forgetpasswordCtrl', ['$scope', '$rootScope', 'MetaInformation','$http', 'SETTINGS', 'GLOBALS', 'authService','toaster', '$state', function($scope, $rootScope, MetaInformation,$http, SETTINGS, GLOBALS, authService,toaster, $state) {
	
	var t;
	$scope.user = {
		Email: ""
	}, t = angular.copy($scope.user), $scope.revert = function() {
		$scope.user = angular.copy(t), $scope.reset_form.$setPristine(), $scope.reset_form.$setUntouched()
	}, $scope.canRevert = function() {
		return !angular.equals($scope.user, t) || !$scope.reset_form.$pristine
	}, $scope.canSubmit = function() {
		return $scope.reset_form.$valid && !angular.equals($scope.user, t)
	}, $scope.submitForm = function() {
		$scope.showInfoOnSubmit = !0, e.revert()
	}


	$scope.redirectToHomePage = function(){
		$state.go('login');
	}
	
	$scope.forgetpassword = function(email){		
		delete $scope.forgotPasswordErrorMessage;
		$http({
			url: SETTINGS[GLOBALS.ENV].apiUrl+"ForgotPassword",
			method: 'POST',
			data: $.param(email),
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function(response) { 
			if(response.ResposeCode == 2){
				$scope.forgotPasswordSucessMessage = response.ResponseMessage;
				toaster.pop('Success', '', $scope.forgotPasswordSucessMessage);
				$state.go("login");
			}else{
				$scope.forgotPasswordErrorMessage = response.ResponseMessage;
				if($scope.forgotPasswordErrorMessage == "Please enter valid Email"){
					$scope.forgotPasswordErrorMessage = "User not found, if you continue to have problems E-mail us at";
					$scope.linkError = "landcommissions@protravelinc.com";
				}
				// toaster.pop('error', '', $scope.forgotPasswordErrorMessage);
			}
			
		});
		
	};
}]);