App.controller('businessCalendar', ['$scope', '$rootScope', 'MetaInformation','$http', 'SETTINGS', 'GLOBALS', 'authService', 'toaster', '$state', '$timeout', function($scope, $rootScope, MetaInformation,$http, SETTINGS, GLOBALS, authService, toaster, $state, $timeout) {

    $scope.limitOptions = [10,25,50];
    $scope.query = {};
    $scope.query.limit = 10;
    $scope.query.page = 1;
    $scope.seo = {
        "seo_title": "Home page",
        "seo_description": "Home page description",
        "seo_keywords": "Home page Keywords"
    };
    setMetaData($scope, $rootScope, MetaInformation, $scope.seo);
    $scope.variable = 'Variable Name';

    $scope.businessCalendarLoading = false;
    $scope.updateLoading = false;

    $scope.loginDetails = {};
    $scope.loginDetails.year = new Date().getFullYear();

    $scope.previousCalenderYear = function(){
        $scope.loginDetails.year--;
        $scope.corpBusinessCalendar();
    }

    $scope.nextCalenderYear = function(){
        $scope.loginDetails.year++;
        $scope.corpBusinessCalendar();
    }
    function filterDate(obj){
        var offset = new Date().getTimezoneOffset()*60*1000;
        var browserName = navigator.vendor;
        if(browserName == "Google Inc." || browserName == "Apple Computer, Inc."){
            obj.fromDate = new Date(Date.parse(obj.fromDate)+offset);
            obj.toDate = new Date(Date.parse(obj.toDate)+offset);
        }else{
            obj.fromDate = new Date(obj.fromDate);
            obj.toDate = new Date(obj.toDate);
        }
        return obj;
    }
    $scope.corpBusinessCalendar = function(){
        $scope.loginDetails.corpID = $scope.userDetails.corpID;
        
        $scope.businessCalendarLoading = true;

        $http({
            url: SETTINGS[GLOBALS.ENV].apiUrl+"Corp_BusinessCalendar/GetCorpBusinessCalendar",
            method: 'POST',
            data: $.param($scope.loginDetails),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
            }
        }).success(function(response) { 
            $scope.businessCalendarLoading = false;
            if(response.ResposeCode == 2){
                if(response.DataList.data.length <= 0){
                    $scope.noData = "No data found";
                    $scope.businessCalendarData = "";
                }else{
                    delete $scope.noData;
                    $scope.businessCalendarData = response.DataList.data.filter(filterDate);
                    /*===== For Fixed header =====*/
                    $timeout(function () {

                        if ($("md-table-container .md-table:first").length > 0) {
                            $("md-table-container .md-table:first").floatThead('destroy');
                            $("md-table-container .md-table:first").floatThead({ scrollingTop: 60 });
                        }

                    }, 2);

                }
            }else{
                $scope.businessCalendarData = "";
                $scope.noData = "No data found";
            }
        }).error(function(err,status) {
            /*===== For Fixed header =====*/
            var $table = $('.md-table:first');
            $table.floatThead({
              responsiveContainer: function($table){
                  return $table.closest('.full-height');
              }

          });
            $scope.errorView(err,status);
        });
    };  

    if($scope.userDetails){
        if($scope.userDetails.maintainBusinessCalendar_Dates == true){
            $scope.disableFromAndToDate = false;
        }else{
            $scope.disableFromAndToDate = true;
        }

        if($scope.userDetails.maintainBusinessCalendar_Close == true){
            $scope.disableFromAndToCheckbox = false;
        }else{
            $scope.disableFromAndToCheckbox = true;
        }
    }

    $scope.corpBusinessCalendar();

    $scope.updateCalenderObjArray = [];
    
    $scope.selectedData = [];

    $scope.updateChanges = function(updateDetails){
        $scope.updateCalenderObj = {};

        $scope.updateCalenderObj.fromDate = updateDetails.fromDate.getDate() + "/" + (updateDetails.fromDate.getMonth()+1) + "/" + updateDetails.fromDate.getFullYear();
        $scope.updateCalenderObj.toDate = updateDetails.toDate.getDate() +  "/" + (updateDetails.toDate.getMonth()+1) + "/" +  updateDetails.toDate.getFullYear();
        $scope.updateCalenderObj.agentClose = (updateDetails.agentClose ? 1 : 0);
        $scope.updateCalenderObj.accountingClose = (updateDetails.accountingClose ? 1 : 0);
        $scope.updateCalenderObj.id = updateDetails.id;
        if($scope.businessCalendarData.accountingCloseUser == null && $scope.updateCalenderObj.accountingClose == 1){
            $scope.updateCalenderObj.accountingCloseUserID = $scope.userDetails.user.id;
        }  
        if($scope.businessCalendarData.agentCloseUser == null && $scope.updateCalenderObj.agentClose == 1){
            $scope.updateCalenderObj.agentCloseUserID = $scope.userDetails.user.id;
        } 
        
        if($scope.updateCalenderObj){
            $scope.updateCalenderObjArray.push($scope.updateCalenderObj);
        }    

    }

    $scope.updateBusinessCalendar = function(){

        if($scope.updateCalenderObjArray.length > 0){
            // $scope.updateCalenderObjArray.push($scope.updateCalenderObj);
            $scope.updateLoading = true;

            $http({
                url: SETTINGS[GLOBALS.ENV].apiUrl+"Corp_BusinessCalendar/UpdateCorpBusinessCalendarList",
                method: 'POST',
                data: JSON.stringify([$scope.updateCalenderObjArray[$scope.updateCalenderObjArray.length-1]]),
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
                }
            }).success(function(response) { 
                $scope.updateLoading = false;
                if(response.ResposeCode == 2){
                    $scope.corpBusinessCalendar();
                    toaster.pop('Success', '', response.ResponseMessage);
                }else{

                }
            });
        }
    }

    $scope.reDirectToPage = function(){
        $state.go('main.home');
    }

}]);