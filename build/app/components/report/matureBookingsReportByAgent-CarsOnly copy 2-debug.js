App.controller('matureBookingsReportByAgent-CarsOnly', ['$scope','$state','$stateParams', '$rootScope', 'MetaInformation','$http', 'SETTINGS', 'GLOBALS', 'authService','toaster','$mdDialog','$filter', function($scope,$state,$stateParams, $rootScope, MetaInformation,$http, SETTINGS, GLOBALS, authService,toaster,$mdDialog,$filter) {

    $scope.matureBookByAgentCarsOnly = {};
    $scope.matureBookByAgentCarsOnly.fromDate = null;
    $scope.matureBookByAgentCarsOnly.toDate = null;

    $scope.noData = "No data found";
    $scope.corpID = $scope.userDetails.corpID;


    function filterDate(obj){
        if(obj.deposit_date){
            obj.deposit_date = new Date(obj.deposit_date);
        }
        obj.in_date = new Date(obj.in_date);
        return obj;
    }

    $scope.bookingToFilter = function() {
        indexedTeams = [];
        return $scope.bookingList;
    }


     $scope.filterTeams = function(player) {
        var teamIsNew = indexedTeams.indexOf(player.agent) == -1;
        if (teamIsNew) {
            indexedTeams.push(player.agent);
        }
        return teamIsNew;
    }

    $scope.bookingReportParms = {
        DataTableCommon:{
                param:{
                    Draw:1,
                    Start:1,
                    Length:$scope.query.limit
                },
                ExportType:0
            },
        corpID:$scope.corpID,
        fromDate:($scope.matureBookByAgentCarsOnly.fromDate) ? $scope.matureBookByAgentCarsOnly.fromDate : "",
        toDate:($scope.matureBookByAgentCarsOnly.toDate) ? $scope.matureBookByAgentCarsOnly.toDate : "",
        TAID:($scope.userDetails.ta_id) ? $scope.userDetails.ta_id : "",
        dateSelect:"",
        unit:($scope.userDetails.unit) ? $scope.userDetails.unit :"",
        branch:($scope.userDetails.branch) ? $scope.userDetails.branch : "",
        agent:($scope.userDetails.agent) ? $scope.userDetails.agent : "",
        iata:($scope.userDetails.iata) ? $scope.userDetails.iata : "",
        account:($scope.userDetails.account) ? $scope.userDetails.account : "",
        propertyBrand:"",
        propertyChain:"",
        propertyName:"",
        propertyAddress:"",
        propertyCity:"",
        propertyState:"",
        propertyZip:"",
        propertyPhone:"",
        propertyPreferred:"",
        lastName:"",
        firstName:"",
        type:"",
        status:"",
        claimed:"",
        amount:"",
        amountSign:"",
        amountDollar:"",
        checkNo:"",
        batchNo:"",
        recordSelect:"",
        bookingSource:"",
        accountGroup:"",
        pnr:"",
        rateCode:""
    };


    $scope.checkValidDate = function(fromDate,toDate){
        if(toDate && fromDate > toDate){
            $scope.matureBookByAgentCarsOnly.toDate = fromDate;
        }
    }

    $scope.callFiterData = function(){
        $scope.bookingReportParms.fromDate = $filter('date')($scope.matureBookByAgentCarsOnly.fromDate, "dd/MM/yyyy");
        $scope.bookingReportParms.toDate = $filter('date')($scope.matureBookByAgentCarsOnly.toDate, "dd/MM/yyyy");
        $http({
            url: SETTINGS[GLOBALS.ENV].apiUrl+"BookingPayment/MatureBookingsReportByAgentCarsOnly",
            method: 'POST',
            data: $scope.bookingReportParms,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
            }
            }).success(function(response) {
            console.log(response); 
                $scope.accountListLoading = false;
                if(response.ResposeCode == 2){
                    if(response.DataList.data){
                        $scope.bookingList = response.DataList.data.filter(filterDate);
                    }
                    if(response.DataList.fileName){
                        window.open(
                            $scope.rootPath+response.DataList.fileName,
                            '_blank'
                        );
                    }
                    $scope.bookingListTotal = response.DataList.recordsTotal;
                }
            }).error(function(err,status) {
                $scope.errorView(err,status);
            });
    }

    $scope.getPDFLink = function(expot){
        $scope.setRequestList.DataTableCommon.ExportType = Number(expot);
        $scope.callFiterData();
    }

    /* ==== Pagination Click function ==== */
    $scope.logOrder = function (order) {
        //console.log('order: ', order);
    };

    $scope.logPagination = function (page, limit) {
        var startPage = (limit * (page - 1)) + 1;
        $scope.bookingReportParms.DataTableCommon.param.Start = startPage;
        $scope.bookingReportParms.DataTableCommon.param.Length = limit;
        $scope.bookingReportParms.DataTableCommon.ExportType = 0;
        $scope.callFiterData();
    }

    

}]);