App.controller('extractReportTM1', ['$scope','$state','$stateParams', '$rootScope', 'MetaInformation','$http', 'SETTINGS', 'GLOBALS', 'authService','toaster','$mdDialog','$filter','$timeout', function($scope,$state,$stateParams, $rootScope, MetaInformation,$http, SETTINGS, GLOBALS, authService,toaster,$mdDialog,$filter,$timeout) {

    $scope.commissionByAcountObj = {};
    $scope.corpBusinessCalParams = {};
    $scope.commissionByAcountObj.fromDate = null;
    $scope.commissionByAcountObj.toDate = null;
    $scope.xfireTm1ListLoading = false;
    $scope.corpBusinessListLoading = false;
    
    /* ==== pagination options ==== */
    // $scope.limitOptions = [10,25,50];
    $scope.limitOptions = [50, 100];
    $scope.query = {};
    // $scope.query.limit = 10;
    $scope.query.limit = 50;
    $scope.query.page = 1;

    $scope.noData = "No data found";
    $scope.corpID = $scope.userDetails.corpID;
    $scope.clearDateDropDown = true;
    
    $scope.bookingReportParms = {
        DataTableCommon:{
            param:{
                Draw:1,
                Start:0,
                // Length:10
                Length:50
            },
            ExportType:0
        },
        corpID:$scope.corpID,
        fromDate:($scope.commissionByAcountObj.fromDate) ? $scope.commissionByAcountObj.fromDate : "",
        toDate:($scope.commissionByAcountObj.toDate) ? $scope.commissionByAcountObj.toDate : "",
        TAID:"",
        dateSelect:"",
        unit:"",
        branch:"",
        agent:"",
        iata:"",
        account:"",
        propertyBrand:"",
        propertyChain:"",
        propertyName:"",
        propertyAddress:"",
        propertyCity:"",
        propertyState:"",
        propertyZip:"",
        propertyPhone:"",
        propertyPreferred:"",
        lastName:"",
        firstName:"",
        type:"",
        status:"",
        claimed:"",
        amount:"",
        amountSign:"",
        amountDollar:"",
        checkNo:"",
        batchNo:"",
        recordSelect:"",
        bookingSource:"",
        accountGroup:"",
        pnr:"",
        rateCode:"",
        groupType:"0"
    };

    $scope.clearDateDropDownSelectedFlag = false;

    /* ===== CallBack Global Left panel View ===== */
    $scope.copyObj = angular.copy($scope.bookingReportParms);
    $scope.$emit('callSelectedFieldByPage',$scope.bookingReportParms);

    $scope.$on('callSelectedField', function(evnt,obj) { 
        var finalObj = angular.copy($scope.copyObj);
        // $scope.bookingReportParms = Object.assign(finalObj,obj); 
        $scope.bookingReportParms = $.extend(finalObj, obj);
        $scope.callFiterData(true);
    });


    
    function filterDate(obj){

        var offset = new Date().getTimezoneOffset()*60*1000;
        
        var browserName = navigator.vendor;
        
        if(browserName == "Google Inc." || browserName == "Apple Computer, Inc."){
            if(obj.fromDate){
                obj.fromDate = new Date(Date.parse(obj.fromDate)+offset);
            }
            if(obj.toDate){
                obj.toDate = new Date(Date.parse(obj.toDate)+offset)
            }
            
        }else{

            if(obj.fromDate){
                obj.fromDate = new Date(obj.fromDate);
            }
            if(obj.toDate){
                obj.toDate = new Date(obj.toDate);
            }
        }


        /*if(obj.fromDate){
            obj.fromDate = new Date(obj.fromDate);
        }
        if(obj.toDate){
            obj.toDate = new Date(obj.toDate);
        }*/
        return obj;
    }

    $scope.checkValidDate = function(fromDate,toDate){
        if(toDate && fromDate > toDate){
            $scope.commissionByAcountObj.toDate = fromDate;
        }
    }

    function getFromAndToDate(obj){

        if(obj.id == $scope.dateId){
            // var offset = new Date().getTimezoneOffset()*60*1000;
            // $scope.commissionByAcountObj.fromDate = new Date(Date.parse(obj.fromDate)+offset);
            // $scope.commissionByAcountObj.toDate = new Date(Date.parse(obj.toDate)+offset);


            /*$scope.commissionByAcountObj.fromDate = new Date(obj.fromDate);
            $scope.commissionByAcountObj.toDate = new Date(obj.toDate);*/

            var d = new Date();
            var tzName = d.toLocaleString('en', {timeZoneName:'short'}).split(' ').pop();
            
            var offset = new Date().getTimezoneOffset()*60*1000;
            
            var browserName = navigator.vendor;
            if(browserName == "Google Inc." || browserName == "Apple Computer, Inc."){
                var fdt = $filter('date')(obj.fromDate, "MM/dd/yyyy");
                var tdt = $filter('date')(obj.toDate, "MM/dd/yyyy");
                if(tzName == "GMT+5:30"){
                    /*$scope.commissionByAcountObj.fromDate = new Date(Date.parse(obj.fromDate)+offset);
                    $scope.commissionByAcountObj.toDate = new Date(Date.parse(obj.toDate)+offset);    */
                    $scope.commissionByAcountObj.fromDate = new Date(obj.fromDate);
                    $scope.commissionByAcountObj.toDate = new Date(obj.toDate);
                }else{
                    $scope.commissionByAcountObj.fromDate = new Date(Date.parse(fdt)+offset);
                    $scope.commissionByAcountObj.toDate = new Date(Date.parse(tdt)+offset);
                }
                
            }else{
                $scope.commissionByAcountObj.fromDate = new Date(obj.fromDate);
                $scope.commissionByAcountObj.toDate = new Date(obj.toDate);
            }


            $scope.extReport.$valid = true;
            if(!$scope.clearDateDropDownSelectedFlag){
                $scope.callFiterData(true);
            }
        }
    }

    $scope.getDates = function(id){
        $scope.clearDateDropDown = false;
        $scope.dateId = id;
        $scope.corpBusinessList.filter(getFromAndToDate);
        // $scope.limitOptions = [10,25,50];
        $scope.limitOptions = [50, 100];
        $scope.query = {};
        // $scope.query.limit = 10;
        $scope.query.limit = 50;
        $scope.query.page = 1;
        $scope.bookingReportParms.DataTableCommon.param.Start = 0;        
    }

    function checkTheDropdownDate(obj){
        if($filter('date')(obj.fromDate, "dd/MM/yyyy") == $filter('date')($scope.commissionByAcountObj.fromDate, "dd/MM/yyyy") && $filter('date')(obj.toDate, "dd/MM/yyyy") == $filter('date')($scope.commissionByAcountObj.toDate, "dd/MM/yyyy")){
            $scope.clearDateDropDown = false;
            return "false";
        }else{
            $scope.clearDateDropDown = true;
        }
    }

    $scope.callFiterData = function(valid){
        if($scope.corpBusinessList.length){
            var checkDate = $scope.corpBusinessList.filter(checkTheDropdownDate);
            if(checkDate.length){
                $scope.clearDateDropDown = false;
            }else{
                $scope.clearDateDropDown = true;
            }
            if($scope.clearDateDropDownSelectedFlag){
                $scope.clearDateDropDown = false;
                $scope.extReport.$valid = true;
                $scope.corpBusinessList.filter(getFromAndToDate);
                $scope.clearDateDropDownSelectedFlag = false
            }
        }
        if($scope.clearDateDropDown == true){
           $scope.reportingPeriod = "SelectReportingPeriod";
       }
       if($scope.extReport.$valid){
        // $scope.clearDateDropDown = true;
        $timeout(function(){

            if(valid){
                $scope.bookingReportParms.DataTableCommon.ExportType = 0;
                // $scope.limitOptions = [10,25,50];
                $scope.limitOptions = [50, 100];
                $scope.query = {};
                // $scope.query.limit = 10;
                $scope.query.limit = 50;
                $scope.query.page = 1;
                $scope.bookingReportParms.DataTableCommon.param.Start = 0;
                // $scope.bookingReportParms.DataTableCommon.param.Length = 10;
                $scope.bookingReportParms.DataTableCommon.param.Length = 50;
            }
            if(!$scope.bookingReportParms.DataTableCommon.ExportType){
                $scope.xfireTm1ListLoading = true;
                $scope.bookingReportParms.fromDate = $filter('date')($scope.commissionByAcountObj.fromDate, "dd/MM/yyyy");
                $scope.bookingReportParms.toDate = $filter('date')($scope.commissionByAcountObj.toDate, "dd/MM/yyyy");

                $scope.$emit('callSelectedFieldByPage',$scope.bookingReportParms);
                if($scope.bookingReportParms.pageStart){
                    // $scope.limitOptions = [10,25,50];
                    $scope.limitOptions = [50, 100];
                    $scope.query = {};
                    // $scope.query.limit = 10;
                    $scope.query.limit = 50;
                    $scope.query.page = 1;
                }
            }
            $http({
                url: SETTINGS[GLOBALS.ENV].apiUrl+"GetExtractData/GetExtractData_TM1",
                method: 'POST',
                data: $scope.bookingReportParms,
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
                }
            }).success(function(response) {
                $scope.xfireTm1ListLoading = false;

                if(response.ResposeCode == 2){
                    if(response.DataList.data){
                        $scope.xfireTm1List = response.DataList.data;
                        $scope.bookingListTotal = response.DataList.recordsTotal;

                        /*===== For Fixed header =====*/
                        $timeout(function () {

                            if ($("md-table-container .md-table:first").length > 0) {
                                $("md-table-container .md-table:first").floatThead('destroy');
                                $("md-table-container .md-table:first").floatThead({ scrollingTop: 60 });
                            }

                        }, 2);
                    }
                    if(response.DataList.fileName){
                        /*window.open(
                            SETTINGS[GLOBALS.ENV].apiUrl+response.DataList.fileName,
                            '_blank'
                            );*/
                            window.open(
                                response.DataList.fileName,
                                '_blank'
                                );
                        }
                    // $scope.bookingListTotal = response.DataList.recordsTotal;
                }else{
                    $scope.xfireTm1List = "";
                }
            }).error(function(err,status) {
                /*===== For Fixed header =====*/
                var $table = $('.md-table:first');
                $table.floatThead({
                  responsiveContainer: function($table){
                      return $table.closest('.full-height');
                  }

              });
                $scope.errorView(err,status);
            });
        },300);
    }
}

function getDateToSelect(obj){
    if(obj){
        if(obj.currentMonth == true){
            return obj;
        }
    }
}

   // $scope.corpBusinessCalParams.year = new Date().getFullYear();
   $scope.corpBusinessCalendar = function(){
    $scope.corpBusinessCalParams.corpID = ($scope.userDetails)?$scope.userDetails.corpID:'';
    $scope.corpBusinessCalParams.userID = ($scope.userDetails)?$scope.userDetails.userID:'';
    $scope.corpBusinessCalParams.ta_id = ($scope.userDetails)?$scope.userDetails.ta_id:'';
    
    $scope.corpBusinessListLoading = true;

    $http({
        url: SETTINGS[GLOBALS.ENV].apiUrl+"Corp_BusinessCalendar/GetCorpBusinessCalendar",
        method: 'POST',
        data: $.param($scope.corpBusinessCalParams),
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
        }
    }).success(function(response) { 
        $scope.corpBusinessListLoading = false;
        if(response.ResposeCode == 2){
            if(response.DataList.data.length <= 0){
                $scope.corpBusinessList = "";
            }else{
                // $scope.corpBusinessList = response.DataList.data.filter(filterDate);
                $scope.corpBusinessList = response.DataList.data;
                var datetoSelect = response.DataList.data.filter(getDateToSelect);

                if(datetoSelect.length){
                    $scope.reportingPeriod = datetoSelect[0].id;
                    $scope.clearDateDropDownSelectedFlag = true;
                    $scope.dateId = datetoSelect[0].id;
                }
            }
        }else{
            $scope.corpBusinessList = "";
        }
    });
};

$scope.corpBusinessCalendar();

$scope.getPDFLink = function(expot){
    $scope.bookingReportParms.DataTableCommon.ExportType = Number(expot);
    $scope.callFiterData();
}

/* ==== Pagination Click function ==== */
$scope.logOrder = function (order) {
   $scope.bookingReportParms.DataTableCommon.param.Columns = [{}];
   if(order.charAt(0) == '-'){
    $scope.bookingReportParms.DataTableCommon.param.Columns[0].Name = order.slice(1);    
    $scope.bookingReportParms.DataTableCommon.param.Columns[0].Dir = "desc";    
}else{
    $scope.bookingReportParms.DataTableCommon.param.Columns[0].Name = order;    
    $scope.bookingReportParms.DataTableCommon.param.Columns[0].Dir = "asc";    
}

$scope.callFiterData();
};

$scope.logPagination = function (page, limit) {
    var startPage = (limit * (page - 1));
    $scope.bookingReportParms.DataTableCommon.param.Start = startPage;
    $scope.bookingReportParms.DataTableCommon.param.Length = limit;
    $scope.bookingReportParms.DataTableCommon.ExportType = 0;
    $scope.extReport.$valid = true;
    $scope.callFiterData();
}

}]);