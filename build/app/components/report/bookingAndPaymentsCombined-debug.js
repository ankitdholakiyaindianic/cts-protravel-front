App.controller('bookingAndPaymentsCombined', ['$scope','$state','$stateParams', '$rootScope', 'MetaInformation','$http', 'SETTINGS', 'GLOBALS', 'authService','toaster','$mdDialog','$filter','$timeout', function($scope,$state,$stateParams, $rootScope, MetaInformation,$http, SETTINGS, GLOBALS, authService,toaster,$mdDialog,$filter,$timeout) {

    $scope.commissionByAcountObj = {};
    $scope.commissionByAcountObj.fromDate = null;
    $scope.commissionByAcountObj.toDate = null;
    $scope.commissionByAcountObj.selectedDate = 0;
    $scope.setselectedDate = 0;
    /* ==== pagination options ==== */
    $scope.limitOptions = [10,25,50];
    $scope.query = {};
    $scope.query.limit = 10;
    $scope.query.page = 1;

    $scope.noData = "No data found";
    $scope.corpID = $scope.userDetails.corpID;

    $scope.accountGroupName = ["Matched Payments", "Matched Responses", "Open Bookings", "Unmatched Payments", "Unmatched Responses"];
    $scope.bookingReportParms = {
        DataTableCommon:{
            param:{
                Draw:1,
                Start:0,
                Length:10
            },
            ExportType:0
        },
        corpID:$scope.corpID,
        fromDate:($scope.commissionByAcountObj.fromDate) ? $scope.commissionByAcountObj.fromDate : "",
        toDate:($scope.commissionByAcountObj.toDate) ? $scope.commissionByAcountObj.toDate : "",
        TAID:"",
        unit:"",
        branch:"",
        agent:"",
        iata:"",
        account:"",
        propertyBrand:"",
        propertyChain:"",
        propertyName:"",
        propertyAddress:"",
        propertyCity:"",
        propertyState:"",
        propertyZip:"",
        propertyPhone:"",
        propertyPreferred:"",
        lastName:"",
        firstName:"",
        type:"",
        status:"",
        claimed:"",
        amount:"",
        amountSign:"",
        amountDollar:"",
        checkNo:"",
        batchNo:"",
        recordSelect:"",
        bookingSource:"",
        accountGroup:"",
        pnr:"",
        rateCode:""
    };


    /* ===== CallBack Global Left panel View ===== */
    $scope.copyObj = angular.copy($scope.bookingReportParms);
    $scope.$emit('callSelectedFieldByPage',$scope.bookingReportParms);

    $scope.$on('callSelectedField', function(evnt,obj) { 
        var finalObj = angular.copy($scope.copyObj);
        // $scope.bookingReportParms = Object.assign(finalObj,obj); 
        $scope.bookingReportParms = $.extend(finalObj, obj);
        $scope.callFiterData(true);
    });

    function filterDate(obj){
        if(obj.Deposit_Date){
            obj.Deposit_Date = new Date(obj.Deposit_Date);
        }
        if(obj.Arrival_Date){
            obj.Arrival_Date = new Date(obj.Arrival_Date);
        }
        return obj;
    }


    $scope.filterTeams = function(player) {
        var teamIsNew = indexedTeams.indexOf(player.Type) == -1;
        if (teamIsNew) {
            indexedTeams.push(player.Type);
        }
        return teamIsNew;
    }

    $scope.createFilterForChild = function(query) {
        var teamIsNew = $scope.indexedTeamsB.indexOf(query.Group) == -1;
        if (teamIsNew) {
            $scope.indexedTeamsB.push(query.Group);
        }
        return teamIsNew;
    }

    function returnObj(val){
        return function(element) {
            if(element.Type === val.a && element.Group === val.b){
                return element;    
            }
        }
    }

    $scope.GroupFilterData = function(obj){
        $scope.indexedTeamsB = [];
        return obj;
    }
    $scope.bookingListData = function(a,b){
        var objData = {'a':a,'b':b};
        var finalData = $scope.bookingList.filter(returnObj(objData));
        if(finalData.length){
            return finalData[0].list;
        }
    }

    $scope.checkValidDate = function(fromDate,toDate){
        if(toDate && fromDate > toDate){
            $scope.commissionByAcountObj.toDate = fromDate;
        }
    }


    $scope.callFiterData = function(valid){
        if($scope.bookingandpayments.$valid){
            $timeout(function(){
                if(valid){
                    $scope.bookingReportParms.DataTableCommon.ExportType = 0;

                }
                if(!$scope.bookingReportParms.DataTableCommon.ExportType){
                    $scope.accountListLoading = true;
                    $scope.bookingList = [];
                    indexedTeams = [];
                    $scope.indexedTeamsB = [];
                    $scope.bookingListParent = [];
                    $scope.bookingListChild = [];
                    $scope.bookingReportParms.fromDate = $filter('date')($scope.commissionByAcountObj.fromDate, "dd/MM/yyyy");
                    $scope.bookingReportParms.toDate = $filter('date')($scope.commissionByAcountObj.toDate, "dd/MM/yyyy");
                    $scope.bookingReportParms.dateSelect = $scope.commissionByAcountObj.selectedDate;
                    $scope.setselectedDate = $scope.commissionByAcountObj.selectedDate;
                    $scope.$emit('callSelectedFieldByPage',$scope.bookingReportParms);
                    if($scope.bookingReportParms.pageStart){
                        $scope.query.page = 1;
                    }
                }
                $http({
                    url: SETTINGS[GLOBALS.ENV].apiUrl+"BookingPayment/GetBookingsAndPaymentsCombined",
                    method: 'POST',
                    data: $scope.bookingReportParms,
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
                    }
                }).success(function(response) {

                    $scope.accountListLoading = false;
                    if(response.ResposeCode == 2){
                        if(response.DataList.data.length){
                            $scope.bookingList = response.DataList.data;
                            indexedTeams = [];
                            $scope.indexedTeamsB = [];
                        //$scope.bookingListParent = $scope.bookingList.filter($scope.filterTeams);
                        $scope.bookingListChild = $scope.bookingList.filter($scope.createFilterForChild);
                        $scope.bookingListTotal = response.DataList.recordsTotal;
                    }
                    if(response.DataList.fileName){
                        /*window.open(
                            SETTINGS[GLOBALS.ENV].apiUrl+response.DataList.fileName,
                            '_blank'
                            );*/
                            window.open(
                                response.DataList.fileName,
                                '_blank'
                                );
                        }
                    // $scope.bookingListTotal = response.DataList.recordsTotal;
                }else{
                    $scope.bookingList = "";
                }
                if($scope.bookingReportParms.pageStart){
                    delete $scope.bookingReportParms.pageStart;
                }
            }).error(function(err,status) {
                $scope.errorView(err,status);
            });
        },300);
        }     
    }

    $scope.totalAvailableCommsion = function(obj){
        var total = {};
        total.Actual_Commission = 0;
        total.Expected_Commission = 0;
        for(var i = 0; i<obj.length;i++){
            if(obj[i].Actual_Commission){
                total.Actual_Commission =  Number(total.Actual_Commission) + Number(obj[i].Actual_Commission); 
            }
            if(obj[i].Expected_Commission){
                total.Expected_Commission =  Number(total.Expected_Commission) + Number(obj[i].Expected_Commission); 
            }
        }
        return total;
    }

    if($stateParams.fromdate && $stateParams.todate){
        var fromDate = $stateParams.fromdate;
        var toDate = $stateParams.todate;
        var id = $stateParams.id;
        $scope.commissionByAcountObj.fromDate = new Date(fromDate);
        $scope.commissionByAcountObj.toDate = new Date(toDate);
        $scope.bookingReportParms.accountCode = id;
        $scope.callFiterData(true);        
    }

    $scope.getPDFLink = function(expot){
        $scope.bookingReportParms.DataTableCommon.ExportType = Number(expot);
        $scope.callFiterData();
    }

    /* ==== Pagination Click function ==== */
    $scope.logOrder = function (order) {
        //console.log('order: ', order);
    };

    $scope.logPagination = function (page, limit) {
        var startPage = (limit * (page - 1));
        $scope.bookingReportParms.DataTableCommon.param.Start = startPage;
        $scope.bookingReportParms.DataTableCommon.param.Length = limit;
        $scope.bookingReportParms.DataTableCommon.ExportType = 0;
        $scope.callFiterData();
    }

}]);