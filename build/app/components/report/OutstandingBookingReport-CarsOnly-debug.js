App.controller('outstandingBookingReport-CarsOnly', ['$scope','$state','$stateParams', '$rootScope', 'MetaInformation','$http', 'SETTINGS', 'GLOBALS', 'authService','toaster','$mdDialog','$filter','$timeout', function($scope,$state,$stateParams, $rootScope, MetaInformation,$http, SETTINGS, GLOBALS, authService,toaster,$mdDialog,$filter,$timeout) {

    $scope.matureBookByAgentCarsOnly = {};
    $scope.matureBookByAgentCarsOnly.fromDate = null;
    $scope.matureBookByAgentCarsOnly.toDate = null;

    $scope.noData = "No data found";
    $scope.corpID = $scope.userDetails.corpID;

    /* ==== pagination options ==== */
    $scope.limitOptions = [10,25,50];
    $scope.query = {};
    $scope.query.limit = 10;
    $scope.query.page = 1;


    $scope.totalCommission = function(obj){
        var finalTotal = 0;
        for(var i=0;i<obj.length;i++){
            finalTotal += Number(obj[i].net_act_comm);
        }
        return finalTotal;
    }
    function filterDate(obj){
        if(obj.deposit_date){
            obj.deposit_date = new Date(obj.deposit_date);
        }
        if(obj.in_date){
            obj.in_date = new Date(obj.in_date);
        }
        return obj;
    }

    $scope.bookingToFilter = function() {
        indexedTeams = [];
        return $scope.bookingList;
    }


    $scope.filterTeams = function(player) {
        var teamIsNew = indexedTeams.indexOf(player.agent) == -1;
        if (teamIsNew) {
            indexedTeams.push(player.agent);
        }
        return teamIsNew;
    }

    $scope.bookingReportParms = {
        DataTableCommon:{
            param:{
                Draw:1,
                Start:0,
                Length:10
            },
            ExportType:0
        },
        corpID:$scope.corpID,
        fromDate:($scope.matureBookByAgentCarsOnly.fromDate) ? $scope.matureBookByAgentCarsOnly.fromDate : "",
        toDate:($scope.matureBookByAgentCarsOnly.toDate) ? $scope.matureBookByAgentCarsOnly.toDate : "",
        TAID:($scope.userDetails.ta_id) ? $scope.userDetails.ta_id : "",
        dateSelect:"",
        unit:($scope.userDetails.unit) ? $scope.userDetails.unit :"",
        branch:($scope.userDetails.branch) ? $scope.userDetails.branch : "",
        agent:($scope.userDetails.agent) ? $scope.userDetails.agent : "",
        iata:($scope.userDetails.iata) ? $scope.userDetails.iata : "",
        account:($scope.userDetails.account) ? $scope.userDetails.account : "",
        propertyBrand:"",
        propertyChain:"",
        propertyName:"",
        propertyAddress:"",
        propertyCity:"",
        propertyState:"",
        propertyZip:"",
        propertyPhone:"",
        propertyPreferred:"",
        lastName:"",
        firstName:"",
        type:"2",
        status:"O",
        claimed:"",
        amount:"",
        amountSign:"",
        amountDollar:"",
        checkNo:"",
        batchNo:"",
        recordSelect:"",
        bookingSource:"",
        accountGroup:"",
        pnr:"",
        rateCode:""
    };

    /* ===== CallBack Global Left panel View ===== */
    $scope.copyObj = angular.copy($scope.bookingReportParms);
    $scope.$emit('callSelectedFieldByPage',$scope.bookingReportParms);

    $scope.$on('callSelectedField', function(evnt,obj) { 
        var finalObj = angular.copy($scope.copyObj);
        // $scope.bookingReportParms = Object.assign(finalObj,obj); 
        $scope.bookingReportParms = $.extend(finalObj, obj);
        $scope.callFiterData(true);
    });


    $scope.checkValidDate = function(fromDate,toDate){
        if(toDate && fromDate > toDate){
            $scope.matureBookByAgentCarsOnly.toDate = fromDate;
        }
    }

    $scope.callFiterData = function(valid){
      if($scope.Outcars.$valid){  
        $timeout(function(){
            if(valid){
                $scope.bookingReportParms.DataTableCommon.ExportType = 0;
                $scope.limitOptions = [10,25,50];
                $scope.query = {};
                $scope.query.limit = 10;
                $scope.query.page = 1;
                $scope.bookingReportParms.DataTableCommon.param.Start = 0;
                $scope.bookingReportParms.DataTableCommon.param.Length = 10;
            }
            if(!$scope.bookingReportParms.DataTableCommon.ExportType){
                $scope.bookingList = [];
                $scope.accountListLoading = true;
            }
            $scope.bookingReportParms.fromDate = $filter('date')($scope.matureBookByAgentCarsOnly.fromDate, "dd/MM/yyyy");
            $scope.bookingReportParms.toDate = $filter('date')($scope.matureBookByAgentCarsOnly.toDate, "dd/MM/yyyy");
            $scope.$emit('callSelectedFieldByPage',$scope.bookingReportParms);
            if($scope.bookingReportParms.pageStart){
                $scope.limitOptions = [10,25,50];
                $scope.query = {};
                $scope.query.limit = 10;
                $scope.query.page = 1;
            }

            $http({
                url: SETTINGS[GLOBALS.ENV].apiUrl+"BookingPayment/OutstandingBookingsReportCarsOnly",
                method: 'POST',
                data: $scope.bookingReportParms,
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
                }
            }).success(function(response) {
                $scope.accountListLoading = false;
                if(response.ResposeCode == 2){
                    if(response.DataList.data){
                        $scope.bookingList = response.DataList.data.filter(filterDate);
                        $scope.bookingListTotal = response.DataList.recordsTotal;

                        /*===== For Fixed header =====*/
                        $timeout(function () {

                            if ($("md-table-container .md-table:first").length > 0) {
                                $("md-table-container .md-table:first").floatThead('destroy');
                                $("md-table-container .md-table:first").floatThead({ scrollingTop: 60 });
                            }

                        }, 2);

                    }
                    if(response.DataList.fileName){
                        /*window.open(
                            SETTINGS[GLOBALS.ENV].apiUrl+response.DataList.fileName,
                            '_blank'
                            );*/
                            window.open(
                                response.DataList.fileName,
                                '_blank'
                                );
                        }
                    // $scope.bookingListTotal = response.DataList.recordsTotal;
                }else{
                    $scope.bookingList = "";
                }
                if($scope.bookingReportParms.pageStart){
                    delete $scope.bookingReportParms.pageStart;
                }
            }).error(function(err,status) {
             /*===== For Fixed header =====*/
             var $table = $('.md-table:first');
             $table.floatThead({
              responsiveContainer: function($table){
                  return $table.closest('.full-height');
              }

          });
             $scope.errorView(err,status);
         });
        },300);
    }    
}

$scope.getPDFLink = function(expot){
    $scope.bookingReportParms.DataTableCommon.ExportType = Number(expot);
    $scope.callFiterData();
}

/* ==== Pagination Click function ==== */
$scope.logOrder = function (order) {
  $scope.bookingReportParms.DataTableCommon.param.Columns = [{}];
  if(order.charAt(0) == '-'){
    $scope.bookingReportParms.DataTableCommon.param.Columns[0].Name = order.slice(1);    
    $scope.bookingReportParms.DataTableCommon.param.Columns[0].Dir = "desc";    
}else{
    $scope.bookingReportParms.DataTableCommon.param.Columns[0].Name = order;    
    $scope.bookingReportParms.DataTableCommon.param.Columns[0].Dir = "asc";    
}
$scope.bookingReportParms.DataTableCommon.ExportType = 0;
$scope.callFiterData();
};

$scope.logPagination = function (page, limit) {
    var startPage = (limit * (page - 1));
    $scope.bookingReportParms.DataTableCommon.param.Start = startPage;
    $scope.bookingReportParms.DataTableCommon.param.Length = limit;
    $scope.bookingReportParms.DataTableCommon.ExportType = 0;
    $scope.callFiterData();
}



}]);