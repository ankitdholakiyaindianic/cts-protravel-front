App.controller('corp-maintenanceCtrl', ['$scope', '$rootScope', 'MetaInformation','$http', 'SETTINGS', 'GLOBALS', 'authService','toaster','$state', function($scope, $rootScope, MetaInformation,$http, SETTINGS, GLOBALS, authService,toaster, $state) {
	$scope.name = 'corp-maintenance';
	$scope.seo = {
		"seo_title": "corp-maintenance",
		"seo_description": "corp-maintenance description",
		"seo_keywords": "corp-maintenance Keywords"
	};
	setMetaData($scope, $rootScope, MetaInformation, $scope.seo);

	$scope.corpID = $scope.userDetails.corpID;
	$scope.ta_id = $scope.userDetails.ta_id;
	/* ==== Get Corp Maintenance Data ==== */
	$scope.getAllData = function(){
		$scope.parseData = {};
		$scope.parseData.id = $scope.corpID;
		$http({
			url: SETTINGS[GLOBALS.ENV].apiUrl+"Corp/GetCorpData",
			method: 'POST',
			data: $.param($scope.parseData),
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded',
				'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
			}
		}).success(function(response) { 
			if(response.ResposeCode == 2){
				if(response.DataList){
					$scope.corpmainObj = response.DataList.data[0].CorpAccount;
				}
			}
		}).error(function(err,status) {
			$scope.errorView(err,status);
		});
	}
	
	/* ==== get Bussiness Units List ==== */
	$scope.getBussinessUnitList = function(){
		$scope.getBuzz = {};
		$scope.getBuzz.id = $scope.corpID;		
		$scope.getBuzz.unit = $scope.userDetails.unit;		
		$http({
			url: SETTINGS[GLOBALS.ENV].apiUrl+"Corp_Agency/GetCorpBusinessUnits",
			method: 'POST',
			data: $.param($scope.getBuzz),
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded',
				'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
			}
		}).success(function(response) { 
			if(response.DataList){
				$scope.getBussinessUnitData = response.DataList.data;
				$scope.getBussinessUnitDataOld = angular.copy($scope.getBussinessUnitData);
				$scope.callBuzzFun();
			}
		}).error(function(err,status) {
			$scope.errorView(err,status);
		});
	}
	/* ==== get Agency List ==== */
	$scope.Corp_Agency = function(){
		$scope.cpAg = {};
		$scope.cpAg.id = $scope.corpID;		
		$http({
			url: SETTINGS[GLOBALS.ENV].apiUrl+"Corp_Agency/GetCorp_AgencyDataById",
			method: 'POST',
			data: $.param($scope.cpAg),
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded',
				'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
			}
		}).success(function(response) { 
			
		}).error(function(err,status) {
			$scope.errorView(err,status);
		});
	}

	/* ==== watch function to check dyanamic model value ==== */
	$scope.callBuzzFun = function(){
		angular.forEach($scope.getBussinessUnitData, function(value, key) {
			$scope.$watch('getBussinessUnitData['+key+']',function( newValue, oldValue ) {
				if($scope.getBussinessUnitDataOld[key].businessUnitCode != newValue.businessUnitCode || $scope.getBussinessUnitDataOld[key].businessUnitDescription != newValue.businessUnitDescription){
					$scope.getBussinessUnitData[key]['ngchanged'] = true;
				}else{
					$scope.getBussinessUnitData[key]['ngchanged'] = false;
				}
			},true);			
		});

	}
/*
	function deleteUserField(obj){
		delete obj;
		delte
		return obj;
	}*/

	/* ==== update corp maintenance  ==== */
	$scope.updateCorpMain = function(obj){

		var cobj = angular.copy(obj);

		delete cobj.Corp_BusinessCalendar;

		$http({
			url: SETTINGS[GLOBALS.ENV].apiUrl+"Corp/UpdateCorp",
			method: 'POST',
			data: $.param(cobj),
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded',
				'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
			}
		}).success(function(response) { 
			if(response.ResposeCode == 2){
				toaster.pop('Success', '', response.ResponseMessage);
				$scope.updateBuzzUnit();
			}else{
				toaster.pop('error', '', response.ResponseMessage);
			}
		}).error(function(err,status) {
			$scope.errorView(err,status);
		});
		
	}

	/* ==== update Business units contents  ==== */
	function checkBuzzChangeVal(obj){
		return obj.ngchanged == true;
	}
	$scope.updateBuzzUnit = function(){
		$scope.checkData = $scope.getBussinessUnitData.filter(checkBuzzChangeVal);
		var cnt = 0;
		if($scope.checkData.length){			
			callBack();
		}
		function callBack(){
			var obj = $scope.checkData[cnt];
			obj.corpID = $scope.corpID;
			//obj.ta_id = $scope.ta_id;
			delete obj.$$hashKey;
			delete obj.ngchanged;
			$http({
				url: SETTINGS[GLOBALS.ENV].apiUrl+"Corp_Agency/UpdateCorp_Agency",
				method: 'POST',
				data: $.param(obj),
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded',
					'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
				}
			}).success(function(response) { 
				if(response.ResposeCode == 2){
					cnt++;
					if(cnt < $scope.checkData.length){
						callBack();
					}
				}else{
					toaster.pop('error', '', response.ResponseMessage);
				}
			}).error(function(err,status) {
				$scope.errorView(err,status);
			});
		}
	}

	$scope.reDirectToHomePage = function(){
		$state.go('main.home');
	}

	/* ==== Get Onload Data ==== */
	$scope.getAllData();
	//$scope.Corp_Agency();
	$scope.getBussinessUnitList();


}]);