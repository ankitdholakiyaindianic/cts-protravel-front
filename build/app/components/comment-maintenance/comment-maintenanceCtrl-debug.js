App.controller('comment-maintenanceCtrl', ['$scope','$state','$stateParams', '$rootScope', 'MetaInformation','$http', 'SETTINGS', 'GLOBALS', 'authService','toaster','$mdDialog', '$timeout', function($scope,$state,$stateParams, $rootScope, MetaInformation,$http, SETTINGS, GLOBALS, authService,toaster,$mdDialog, $timeout) {
	
	$scope.name = 'comment-maintenance';
	$scope.seo = {
		"seo_title": "comment-maintenance",
		"seo_description": "comment-maintenance description",
		"seo_keywords": "comment-maintenance Keywords"
	};
	setMetaData($scope, $rootScope, MetaInformation, $scope.seo);
	/* ==== Onload Variables ==== */
	var t;
	// $scope.limitOptions = [10,25,50];
	$scope.limitOptions = [50, 100];
	$scope.query = {};
	// $scope.query.limit = 10;
	$scope.query.limit = 50;
	$scope.query.page = 1;
	$scope.stateName = '';
	$scope.getClaimComments = {};
	$scope.getClaimComments.comment = '';
	$scope.getClaimComments.active = true;
	$scope.getClaimComments.useAsDefault = true;
	t = angular.copy($scope.getClaimComments), $scope.revert = function() {
		$scope.getClaimComments = angular.copy(t), $scope.commentmain.$setPristine(), $scope.commentmain.$setUntouched()
	}, $scope.canRevert = function() {
		return !angular.equals($scope.getClaimComments, t) || !$scope.commentmain.$pristine
	}, $scope.canSubmit = function() {
		return $scope.commentmain.$valid && !angular.equals($scope.getClaimComments, t)
	}, $scope.submitForm = function() {
		$scope.showInfoOnSubmit = !0, e.revert()
	}
	$scope.getClaimComments = {

		DataTableCommon:{
			param:{
				Draw:1,
				Start:0,
				// Length:10
				Length:50
			},
			ExportType:0
		}
	}

	$scope.corpID = $scope.userDetails.corpID;
	$scope.noData = "No data found";
	$scope.query.order = '-id';
	$scope.commentLoading = false;

	if($state.current.name == 'main.commentMaintenance'){
		$scope.commentLoading = true;
		$scope.stateName = 'list';
		$scope.getClaimComments = {};
		$scope.getClaimComments.corpID = $scope.corpID;
		$http({
			url: SETTINGS[GLOBALS.ENV].apiUrl+"Payments_ClaimComments/GetPayments_ClaimCommentsData",
			method: 'POST',
			data: $.param($scope.getClaimComments),
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded',
				'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
			}
		}).success(function(response) { 
			$scope.commentLoading = false;
			if(response.ResposeCode == 2){
				if(response.DataList){
					$scope.commentmainObjList = response.DataList.data;
					/*===== For Fixed header =====*/
					$timeout(function () {

						if ($("md-table-container .md-table:first").length > 0) {
							$("md-table-container .md-table:first").floatThead('destroy');
							$("md-table-container .md-table:first").floatThead({ scrollingTop: 60 });
						}

					}, 2);
				}
			}else{
				$scope.commentmainObjList = [];
			}
		}).error(function(err,status) {
			/*===== For Fixed header =====*/
			var $table = $('.md-table:first');
			$table.floatThead({
				responsiveContainer: function($table){
					return $table.closest('.full-height');
				}

			});

			$scope.errorView(err,status);
		});

	}else{
		if($stateParams.id){
			$scope.stateName = 'update';
			$scope.commentID = $stateParams.id;
			$http({
				url: SETTINGS[GLOBALS.ENV].apiUrl+"Payments_ClaimComments/GetPayments_ClaimCommentsDataById",
				method: 'POST',
				data: $.param({'id':$scope.commentID}),
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded',
					'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
				}
			}).success(function(response) { 
				if(response.ResposeCode == 2){
					if(!response.DataList){
						$scope.getClaimComments = response.DataModel;
					}
				}
			}).error(function(err,status) {
				$scope.errorView(err,status);
			});
		}else{
			if($state.current.name == 'main.commentMaintenanceAdd'){
				$scope.stateName = 'add';
			}
		}
	}

	/* ==== Add Comment API ==== */
	$scope.addAndUpdate = function(obj){

		if($scope.getClaimComments.active == "false" && $scope.getClaimComments.useAsDefault == "true"){
			// toaster.pop('error', '', 'Can not select as the default when comment is not active.');
			$scope.errorMessage = "Can not select as the default when comment is not active.";
			return;
		}else{
			delete $scope.errorMessage;
		}

		var API_Name = 'Payments_ClaimComments/InsertPayments_ClaimComments';
		if(obj.id){
			API_Name = 'Payments_ClaimComments/UpdatePayments_ClaimComments';
		}else{
			obj['id'] = 0;
		}
		obj.corpID = $scope.corpID;
		$http({
			url: SETTINGS[GLOBALS.ENV].apiUrl+API_Name,
			method: 'POST',
			data: $.param(obj),
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded',
				'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
			}
		}).success(function(response) { 
			if(response.ResposeCode == 2){
				toaster.pop('Success', '', response.ResponseMessage);
				$scope.reDirectPage();
			}else{
				toaster.pop('error', '', response.ResponseMessage);
			}
		}).error(function(err,status) {
			$scope.errorView(err,status);
		});
	}

	/* ==== Delete Comment API ==== */	
	$scope.deleteComment = function(){
		var obj = {};
		obj.id = $stateParams.id;
		$http({
			url: SETTINGS[GLOBALS.ENV].apiUrl+'Payments_ClaimComments/DeletePayments_ClaimComments',
			method: 'POST',
			data: $.param(obj),
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded',
				'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
			}
		}).success(function(response) { 
			if(response.ResposeCode == 2){
				toaster.pop('Success', '', response.ResponseMessage);
				$scope.reDirectPage();
			}else{
				toaster.pop('error', '', response.ResponseMessage);
			}
		}).error(function(err,status) {
			$scope.errorView(err,status);
		});
	}

	/* ==== Redirect Page in add comment page ==== */
	$scope.callAddComment = function(){
		$state.go('main.commentMaintenanceAdd');
	}
	/* ==== redirect Page by click cancel button ==== */
	$scope.reDirectPage = function(){
		$state.go('main.commentMaintenance');
	}

	/* ==== Confirm Popup ==== */
	$scope.popupTitle = 'Delete Claim Comment';
	$scope.pageDescription = 'Would you like to delete?';
	$scope.showConfirmPopup = function(ev,id) {
		$mdDialog.show({
			contentElement: '#conFirmPopup',
			parent: angular.element(document.body),
			targetEvent: ev,
			clickOutsideToClose:true,
			fullscreen: false
		}).finally(function() {
		});
	};
	$scope.callBackDialog = function(data){
		if(data == 'ok'){
			$scope.deleteComment();
		}
		$mdDialog.cancel();
	}
}]);