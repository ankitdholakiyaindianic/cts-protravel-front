App.controller('registerCtrl', ['$scope','authService','$cookies','$cookieStore','$state','$stateParams', function($scope, authService,$cookies, $cookieStore, $state, $stateParams){
	
	var t;
	$scope.user = {
		email: "",
		passowrd: ""
	}, t = angular.copy($scope.user), $scope.revert = function() {
		$scope.user = angular.copy(t), $scope.signup_form.$setPristine(), $scope.signup_form.$setUntouched()
	}, $scope.canRevert = function() {
		return !angular.equals($scope.user, t) || !$scope.signup_form.$pristine
	}, $scope.canSubmit = function() {
		return $scope.signup_form.$valid && !angular.equals($scope.user, t)
	}, $scope.submitForm = function() {
		$scope.showInfoOnSubmit = !0, e.revert()
	}
	$scope.register = function(){
		$state.go("login");
	};	
}]);