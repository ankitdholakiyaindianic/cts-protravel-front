App.controller('dunningActivityDetailCtrl', ['$scope','$state','$stateParams', '$rootScope', 'MetaInformation','$http', 'SETTINGS', 'GLOBALS', 'authService','toaster','$mdDialog','$filter', function($scope,$state,$stateParams, $rootScope, MetaInformation,$http, SETTINGS, GLOBALS, authService,toaster,$mdDialog,$filter) {

    $scope.commissionByAcountObj = {};
    $scope.commissionByAcountObj.fromDate = null;
    $scope.commissionByAcountObj.toDate = null;
    $scope.commissionByAcountObj.selectedDate = 0;
    $scope.setselectedDate = 0;

    $scope.noData = "No data found";
    $scope.corpID = $scope.userDetails.corpID;
    $scope.accountListLoading = false;
    $scope.bookingReportParms = {
        DataTableCommon:{
            param:{
                Draw:1,
                Start:0,
                Length:10
            },
            ExportType:0
        },
        corpID:$scope.corpID,
        fromDate:($scope.commissionByAcountObj.fromDate) ? $scope.commissionByAcountObj.fromDate : "",
        toDate:($scope.commissionByAcountObj.toDate) ? $scope.commissionByAcountObj.toDate : "",
        status:"",
        bookingSource:""
    };

    /* ===== CallBack Global Left panel View ===== */
    $scope.copyObj = angular.copy($scope.bookingReportParms);
    $scope.$emit('callSelectedFieldByPage',$scope.bookingReportParms);

    $scope.$on('callSelectedField', function(evnt,obj) { 
        var finalObj = angular.copy($scope.copyObj);
        // $scope.bookingReportParms = Object.assign(finalObj,obj); 
        $scope.bookingReportParms = $.extend(finalObj, obj);
        $scope.callFiterData(true);
    });

    function createFilterFor(query) {
        var teamIsNew = $scope.indexedTeamsA.indexOf(query.iata) == -1;
        if (teamIsNew) {
            $scope.indexedTeamsA.push(query.iata);
        }
        return teamIsNew;
    }

    function createFilterForChild(query) {
        var teamIsNew = $scope.indexedTeamsB.indexOf(query.cts_type) == -1;
        if (teamIsNew) {
            $scope.indexedTeamsB.push(query.cts_type);
        }
        return teamIsNew;
    }

    function filterDate(obj){
        if(obj.deposit_date){
            obj.deposit_date = new Date(obj.deposit_date);
        }
        if(obj.in_date){
            obj.in_date = new Date(obj.in_date);
        }
        if(obj.out_date){
            obj.out_date = new Date(obj.out_date);
        }
        if(obj.trans_date){
            obj.trans_date = new Date(obj.trans_date);
        }
        if(obj.inv_date){
            obj.inv_date = new Date(obj.inv_date);
        }
        return obj;
    }

    $scope.checkValidDate = function(fromDate,toDate){
        if(toDate && fromDate > toDate){
            $scope.commissionByAcountObj.toDate = fromDate;
        }
    }

    $scope.callFiterData = function(valid){
        if($scope.dunningAct.$valid){
            if(valid){
                $scope.bookingReportParms.DataTableCommon.ExportType = 0;
            }
            if(!$scope.bookingReportParms.DataTableCommon.ExportType){
                $scope.accountListLoading = true;
                $scope.bookingReportParms.fromDate = $filter('date')($scope.commissionByAcountObj.fromDate, "dd/MM/yyyy");
                $scope.bookingReportParms.toDate = $filter('date')($scope.commissionByAcountObj.toDate, "dd/MM/yyyy");
                $scope.bookingReportParms.dateSelect = "3";
                $scope.$emit('callSelectedFieldByPage',$scope.bookingReportParms);
                if($scope.bookingReportParms.pageStart){
                    $scope.query.page = 1;
                }
            }
            $http({
                url: SETTINGS[GLOBALS.ENV].apiUrl+"DunningActivity/GetDunningActivity",
                method: 'POST',
                data: $scope.bookingReportParms,
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
                }
            }).success(function(response) {
                $scope.accountListLoading = false;
                if(response.ResposeCode == 2){
                	if(response.DataList.data){
                        $scope.dunningActivityList = response.DataList.data.filter(filterDate);
                    }
                    if(response.DataList.fileName){
                        /*window.open(
                            SETTINGS[GLOBALS.ENV].apiUrl+response.DataList.fileName,
                            '_blank'
                            );*/
                            window.open(
                                response.DataList.fileName,
                                '_blank'
                                );
                        }
                        $scope.bookingListTotal = response.DataList.recordsTotal;
                    }else{
                        $scope.dunningActivityList = [];
                    }
                }).error(function(err,status) {
                    $scope.errorView(err,status);
                });
            }    
        }

        $scope.getPDFLink = function(expot){
            $scope.bookingReportParms.DataTableCommon.ExportType = Number(expot);
            $scope.callFiterData();
        }

        /* ==== Pagination Click function ==== */
        $scope.logOrder = function (order) {
            
        };

        $scope.logPagination = function (page, limit) {
            var startPage = (limit * (page - 1));
            $scope.bookingReportParms.DataTableCommon.param.Start = startPage;
            $scope.bookingReportParms.DataTableCommon.param.Length = limit;
            $scope.bookingReportParms.DataTableCommon.ExportType = 0;
            $scope.callFiterData();
        }

    }]);
