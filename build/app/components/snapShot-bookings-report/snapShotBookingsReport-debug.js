App.controller('snapShotBookingReportCtrl', ['$scope','$state','$stateParams', '$rootScope', 'MetaInformation','$http', 'SETTINGS', 'GLOBALS', 'authService','toaster','$mdDialog','$filter','$timeout', function($scope,$state,$stateParams, $rootScope, MetaInformation,$http, SETTINGS, GLOBALS, authService,toaster,$mdDialog,$filter,$timeout) {

    $scope.matureBookByAgentCarsOnly = {};
    $scope.matureBookByAgentCarsOnly.fromDate = null;
    $scope.matureBookByAgentCarsOnly.toDate = null;
    /* ==== pagination options ==== */
    $scope.limitOptions = [10,25,50];
    $scope.query = {};
    $scope.query.limit = 10;
    $scope.query.page = 1;

    $scope.noData = "No data found";
    $scope.corpID = $scope.userDetails.corpID;

    $scope.bookingReportParms = {
        DataTableCommon:{
            param:{
                Draw:1,
                Start:0,
                Length:10
            },
            ExportType:0
        },
        corpID:$scope.corpID,
        fromDate:($scope.matureBookByAgentCarsOnly.fromDate) ? $scope.matureBookByAgentCarsOnly.fromDate : "",
        toDate:($scope.matureBookByAgentCarsOnly.toDate) ? $scope.matureBookByAgentCarsOnly.toDate : "",
        fromDate:($scope.matureBookByAgentCarsOnly.fromDate) ? $scope.matureBookByAgentCarsOnly.fromDate : "",
        toDate:($scope.matureBookByAgentCarsOnly.toDate) ? $scope.matureBookByAgentCarsOnly.toDate : "",
        // dateSelect:2,
        type:"",
        unit:($scope.userDetails.unit) ? $scope.userDetails.unit :"",
        branch:($scope.userDetails.branch) ? $scope.userDetails.branch : "",
        agent:($scope.userDetails.agent) ? $scope.userDetails.agent : "",
        iata:($scope.userDetails.iata) ? $scope.userDetails.iata : "",
        account:($scope.userDetails.account) ? $scope.userDetails.account : "",
    };

    /* ===== CallBack Global Left panel View ===== */
    $scope.copyObj = angular.copy($scope.bookingReportParms);
    
    $scope.$emit('callSelectedFieldByPage',$scope.bookingReportParms);

    $scope.$on('callSelectedField', function(evnt,obj) { 
        var finalObj = angular.copy($scope.copyObj);
        // $scope.bookingReportParms = Object.assign(finalObj,obj); 
        $scope.bookingReportParms = $.extend(finalObj, obj);
        $scope.callFiterData(true);
    });
    
    function filterDate(obj){
        if(obj.trans_date){
            obj.trans_date = new Date(obj.trans_date);
        }
        if(obj.out_date){
            obj.out_date = new Date(obj.out_date);
        }
        if(obj.in_date){
            obj.in_date = new Date(obj.in_date);
        }
        return obj;
    }

    $scope.filterTeams = function(player) {
        var teamIsNew = indexedTeams.indexOf(player.ta_id) == -1;
        if (teamIsNew) {
            indexedTeams.push(player.ta_id);
        }
        return teamIsNew;
    }

    $scope.createFilterForChild = function(query) {
        var teamIsNew = $scope.indexedTeamsB.indexOf(query.bkg_src) == -1;
        if (teamIsNew) {
            $scope.indexedTeamsB.push(query.bkg_src);
        }
        return teamIsNew;
    }

    $scope.GroupFilterData = function(obj){
        $scope.indexedTeamsB = [];
        return obj;
    }

    function returnObj(val){
        return function(element) {
            if(element.ta_id === val.a && element.bkg_src === val.b){
                return element;    
            }
        }
    }

    $scope.bookingListData = function(a,b){
        var objData = {'a':a,'b':b};
        var finalData = $scope.bookingList.filter(returnObj(objData));
        if(finalData.length){
            return finalData[0].olist;
        }
    }

    $scope.checkValidDate = function(fromDate,toDate){
        if(toDate && fromDate > toDate){
            $scope.matureBookByAgentCarsOnly.toDate = fromDate;
        }
    }
    
    $scope.callFiterData = function(valid){
        if($scope.SnapShotBooking.$valid){
            $timeout(function(){
                if(valid){
                    $scope.bookingReportParms.DataTableCommon.ExportType = 0;
                }        
                if(!$scope.bookingReportParms.DataTableCommon.ExportType){
                    $scope.bookingList = [];
                    indexedTeams = [];
                    $scope.indexedTeamsB = [];
                    $scope.bookingListParent = [];
                    $scope.bookingListChild = [];
                    $scope.accountListLoading = true;
                }
                $scope.bookingReportParms.fromDate = $filter('date')($scope.matureBookByAgentCarsOnly.fromDate, "dd/MM/yyyy");
                $scope.bookingReportParms.toDate = $filter('date')($scope.matureBookByAgentCarsOnly.toDate, "dd/MM/yyyy");
                $scope.bookingReportParms.dateSelect = "2";
                $scope.$emit('callSelectedFieldByPage',$scope.bookingReportParms);
                if($scope.bookingReportParms.pageStart){
                    $scope.query.page = 1;
                }
                $http({
                    url: SETTINGS[GLOBALS.ENV].apiUrl+"GetBookingsResolution/GetMidOfficeCapturedBookings",
                    method: 'POST',
                    data: $scope.bookingReportParms,
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
                    }
                }).success(function(response) {
                    $scope.accountListLoading = false;
                    if(response.ResposeCode == 2){
                        if(response.DataList.data){
                            $scope.bookingList = response.DataList.data;
                        /*indexedTeams = [];
                        $scope.indexedTeamsB = [];*/
                        /*$scope.bookingListParent = $scope.bookingList.filter($scope.filterTeams);
                        $scope.bookingListChild = $scope.bookingList.filter($scope.createFilterForChild);*/
                    }
                    if(response.DataList.fileName){
                        /*window.open(
                            SETTINGS[GLOBALS.ENV].apiUrl+response.DataList.fileName,
                            '_blank'
                            );*/
                            window.open(
                                response.DataList.fileName,
                                '_blank'
                                );
                        }
                        $scope.bookingListTotal = response.DataList.recordsTotal;
                    }else{
                        $scope.bookingList = [];
                    }
                    if($scope.bookingReportParms.pageStart){
                        delete $scope.bookingReportParms.pageStart;
                    }
                }).error(function(err,status) {
                    $scope.errorView(err,status);
                });
            },300);
        }   
    }

    $scope.getPDFLink = function(expot){
        $scope.bookingReportParms.DataTableCommon.ExportType = Number(expot);
        $scope.callFiterData();
    }

    /* ==== Pagination Click function ==== */
    $scope.logOrder = function (order) {

    };

    $scope.logPagination = function (page, limit) {
        var startPage = (limit * (page - 1));
        $scope.bookingReportParms.DataTableCommon.param.Start = startPage;
        $scope.bookingReportParms.DataTableCommon.param.Length = limit;
        $scope.bookingReportParms.DataTableCommon.ExportType = 0;
        $scope.callFiterData();
    }

}]);