App.controller('houseBookingsReportCtrl', ['$scope','$state','$stateParams', '$rootScope', 'MetaInformation','$http', 'SETTINGS', 'GLOBALS', 'authService','toaster','$mdDialog','$location', function($scope,$state,$stateParams, $rootScope, MetaInformation,$http, SETTINGS, GLOBALS, authService,toaster,$mdDialog,$location) {
	
	$scope.name = 'houseBookingsReport';
	$scope.seo = {
		"seo_title": "houseBookingsReport",
		"seo_description": "houseBookingsReport description",
		"seo_keywords": "houseBookingsReport Keywords"
	};
	setMetaData($scope, $rootScope, MetaInformation, $scope.seo);

	/* ==== Onload Variables ==== */
	$scope.corpID = $scope.userDetails.corpID;
	$scope.noData = "No data found";

	/* ==== pagination options ==== */
	$scope.limitOptions = [10,25,50];
	$scope.query = {};
	$scope.query.limit = 10;
	$scope.query.page = 1;
	$scope.loadingData = false;

	function filterDate(obj){
		obj.Deposit_Date = new Date(obj.Deposit_Date);
		obj.Arrival_Date = new Date(obj.Arrival_Date);
		return obj;
	}

	/* ==== Get House Booking Report ==== */
	/* ==== Pagination Format List ====== */
	$scope.getHouseParam = {
		DataTableCommon:{
			param:{
				Draw:1,
				Start:0,
				Length:10
			},
			ExportType:0
		},
		corpID:$scope.corpID,type:"",lastName:"",firstName:"",propertyBrand:"",propertyChain:"",propertyName:"",propertyAddress:"",propertyCity:"",propertyState:"",propertyZip:"",propertyphone:"",unit:"",branch:"",account:"",iata:"",agent:"",amount:"",amountSign:"",amountDollar:""

	};

	/* ===== CallBack Global Left panel View ===== */
	$scope.copyObj = angular.copy($scope.getHouseParam);
	$scope.$emit('callSelectedFieldByPage',$scope.getHouseParam);

	$scope.$on('callSelectedField', function(evnt,obj) { 
		var finalObj = angular.copy($scope.copyObj);
		// $scope.getHouseParam = Object.assign(finalObj,obj); 
		$scope.getHouseParam = $.extend(finalObj, obj);
		$scope.getHouseBookingReportList(true);
	});


	$scope.getHouseBookingReportList = function(){

		$scope.loadingData = true;

		$http({
			url: SETTINGS[GLOBALS.ENV].apiUrl+"Bookings/GetHouseBookings",
			method: 'POST',
			data: $scope.getHouseParam,
			headers: {
				'Content-Type': 'application/json',
				'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
			}
		}).success(function(response) { 
			$scope.loadingData = false;
			if(response.ResposeCode == 2){
				if(response.DataList){
					if(response.DataList.data){
						$scope.houseBkReport = response.DataList.data.filter(filterDate);
						$scope.$emit('callSelectedFieldByPage',$scope.getHouseParam);
						if($scope.getHouseParam.pageStart){
							$scope.query.page = 1;
						}

					}
					if(response.DataList.fileName){
						/*window.open(
							SETTINGS[GLOBALS.ENV].apiUrl+response.DataList.fileName,
							'_blank'
							);*/
							window.open(
								response.DataList.fileName,
								'_blank'
								);
						}
						$scope.houseBookingsReportLength = response.DataList.recordsTotal;
					}
				}else{
					$scope.houseBkReport = [];
				}
				if($scope.getHouseParam.pageStart){
					delete $scope.getHouseParam.pageStart;
				}
			}).error(function(err,status) {
				$scope.errorView(err,status);
			});
		}

		$scope.getHouseBookingReportList();

		/* ==== get PDF Link ==== */
		$scope.getPDFLink = function(expot){
			$scope.getHouseParam.DataTableCommon.ExportType = Number(expot);
			$scope.getHouseBookingReportList();
		}

		/* ==== Pagination Click function ==== */
		$scope.logOrder = function (order) {
		// console.log('order: ', order);
	};

	$scope.logPagination = function (page, limit) {
		var startPage = (limit * (page - 1));
		$scope.getHouseParam.DataTableCommon.param.Start = startPage;
		$scope.getHouseParam.DataTableCommon.param.Length = limit;
		$scope.getHouseParam.DataTableCommon.ExportType = 0;
		$scope.getHouseBookingReportList();
	}

}]);