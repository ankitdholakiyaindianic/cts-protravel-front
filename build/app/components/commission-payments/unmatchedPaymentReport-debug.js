App.controller('unmatchedPaymentRptCtrl', ['$scope','$state','$stateParams', '$rootScope', 'MetaInformation','$http', 'SETTINGS', 'GLOBALS', 'authService','toaster','$mdDialog','$filter', function($scope,$state,$stateParams, $rootScope, MetaInformation,$http, SETTINGS, GLOBALS, authService,toaster,$mdDialog,$filter) {

    $scope.unmatchedPaymentRpt = {};
    $scope.unmatchedPaymentRpt.fromDate = null;
    $scope.unmatchedPaymentRpt.toDate = null;
    $scope.unmatchedPaymentRpt.selectedDate = 0;

    $scope.noData = "No data found";
    $scope.corpID = $scope.userDetails.corpID;
    $scope.unmatchedPaymentLoading = false;

    /* ==== pagination options ==== */
    $scope.limitOptions = [10,25,50];
    $scope.query = {};
    $scope.query.limit = 10;
    $scope.query.page = 1;

    var finalTotal = 0;
    $scope.totalCommission = function(obj){
        finalTotal += Number(obj.net_act_comm);
        return finalTotal;
    }
    function filterDate(obj){
        if(obj.deposit_date){
            obj.deposit_date = new Date(obj.deposit_date);
        }
        if(obj.check_date){
            obj.check_date = new Date(obj.check_date);
        }
        return obj;
    }

    $scope.bookingToFilter = function() {
        indexedTeams = [];
        return $scope.unmatchedPayment;
    }


    $scope.filterTeams = function(player) {
        var teamIsNew = indexedTeams.indexOf(player.Business_Unit) == -1;
        if (teamIsNew) {
            indexedTeams.push(player.Business_Unit);
        }
        return teamIsNew;
    }

    $scope.bookingReportParms = {
        DataTableCommon:{
            param:{
                Draw:1,
                Start:0,
                Length:10
            },
            ExportType:0
        },
        corpID:$scope.corpID,
        fromDate:"",toDate: "",TAID:"",dateSelect:"",
        unit:"",branch:"",agent:"",iata:"",account:"",
        propertyBrand:"",propertyChain:"",propertyName:"",propertyAddress:"",ppropertyState:"",propertyZip:"",
        propertyPhone:"",propertyPreferred:"",lastName:"",firstName:"",type:"",status:"",claimed:"0",
        amount:"",amountSign:"5",amountDollar:"1000",checkNo:"",batchNo:"",recordSelect:"",bookingSource:"",accountGroup:"",pnr:"",rateCode:"",groupType:""
    };

    /* ===== CallBack Global Left panel View ===== */
    $scope.copyObj = angular.copy($scope.bookingReportParms);
    $scope.$emit('callSelectedFieldByPage',$scope.bookingReportParms);

    $scope.$on('callSelectedField', function(evnt,obj) { 
        var finalObj = angular.copy($scope.copyObj);
        // $scope.bookingReportParms = Object.assign(finalObj,obj); 
        $scope.bookingReportParms = $.extend(finalObj, obj);
        $scope.callFiterData(true);
    });


    $scope.checkValidDate = function(fromDate,toDate){
        if(toDate && fromDate > toDate){
            $scope.unmatchedPaymentRpt.toDate = fromDate;
        }
    }

    $scope.callFiterData = function(valid){
        if(valid){
            $scope.bookingReportParms.DataTableCommon.ExportType = 0;
        }
        if(!$scope.bookingReportParms.DataTableCommon.ExportType){
            $scope.unmatchedPayment = [];
            $scope.unmatchedPaymentLoading = true;
        }
        $http({
            url: SETTINGS[GLOBALS.ENV].apiUrl+"GetNotMatched/GetNotMatched",
            method: 'POST',
            data: $scope.bookingReportParms,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
            }
        }).success(function(response) {
            $scope.unmatchedPaymentLoading = false;
            if(response.ResposeCode == 2){
                if(response.DataList.data){
                    $scope.unmatchedPayment = response.DataList.data.filter(filterDate);
                    $scope.$emit('callSelectedFieldByPage',$scope.bookingReportParms);
                    if($scope.bookingReportParms.pageStart){
                        $scope.query.page = 1;
                    }
                }
                if(response.DataList.fileName){
                    /*window.open(
                        SETTINGS[GLOBALS.ENV].apiUrl+response.DataList.fileName,
                        '_blank'
                        );*/
                        window.open(
                            response.DataList.fileName,
                            '_blank'
                            );
                    }
                    $scope.unmatchedPaymentTotal = response.DataList.recordsTotal;
                }else{
                    $scope.unmatchedPayment = [];
                }
                if($scope.bookingReportParms.pageStart){
                    delete $scope.bookingReportParms.pageStart;
                }
            }).error(function(err,status) {
                $scope.errorView(err,status);
            });
        }

        $scope.callFiterData();

        $scope.getPDFLink = function(expot){
            $scope.bookingReportParms.DataTableCommon.ExportType = Number(expot);
            $scope.callFiterData();
        }

        /* ==== Pagination Click function ==== */
        $scope.logOrder = function (order) {
            
        };

        $scope.logPagination = function (page, limit) {
            var startPage = (limit * (page - 1));
            $scope.bookingReportParms.DataTableCommon.param.Start = startPage;
            $scope.bookingReportParms.DataTableCommon.param.Length = limit;
            $scope.bookingReportParms.DataTableCommon.ExportType = 0;
            $scope.callFiterData();
        }

    }]);