App.controller('extractReportCtrl', ['$scope','$state','$stateParams', '$rootScope', 'MetaInformation','$http', 'SETTINGS', 'GLOBALS', 'authService','toaster','$mdDialog','$filter','$timeout', function($scope,$state,$stateParams, $rootScope, MetaInformation,$http, SETTINGS, GLOBALS, authService,toaster,$mdDialog,$filter,$timeout) {

    $scope.commissionByAcountObj = {};
    $scope.commissionByAcountObj.fromDate = null;
    $scope.commissionByAcountObj.toDate = null;
    $scope.xfireListLoading = false;
    
    $scope.noData = "No data found";
    $scope.corpID = $scope.userDetails.corpID;

    /* ==== pagination options ==== */
    // $scope.limitOptions = [10,25,50];
    $scope.limitOptions = [50, 100];
    $scope.query = {};
    // $scope.query.limit = 10;
    $scope.query.limit = 50;
    $scope.query.page = 1;
    
    $scope.bookingReportParms = {
        DataTableCommon:{
            param:{
                Draw:1,
                Start:0,
                // Length:10
                Length:50
            },
            ExportType:0
        },
        corpID:$scope.corpID,
        fromDate:($scope.commissionByAcountObj.fromDate) ? $scope.commissionByAcountObj.fromDate : "",
        toDate:($scope.commissionByAcountObj.toDate) ? $scope.commissionByAcountObj.toDate : "",
        matched: "1",
        unmatched: "1",
        unmatched1: "1",
    };
    
    /* ===== CallBack Global Left panel View ===== */
    $scope.copyObj = angular.copy($scope.bookingReportParms);
    $scope.$emit('callSelectedFieldByPage',$scope.bookingReportParms);

    $scope.$on('callSelectedField', function(evnt,obj) { 
        var finalObj = angular.copy($scope.copyObj);
        // $scope.bookingReportParms = Object.assign(finalObj,obj); 
        $scope.bookingReportParms = $.extend(finalObj, obj);
        $scope.callFiterData(true);
    });

    function filterDate(obj){
        if(obj.check_date){
            obj.check_date = new Date(obj.check_date);
        }
        if(obj.claimDate){
            obj.claimDate = new Date(obj.claimDate);
        }
        if(obj.create_date){
            obj.create_date = new Date(obj.create_date);
        }
        if(obj.deposit_date){
            obj.deposit_date = new Date(obj.deposit_date);
        }
        if(obj.in_date){
            obj.in_date = new Date(obj.in_date);
        }
        if(obj.out_date){
            obj.out_date = new Date(obj.out_date);
        }
        if(obj.pay_end){
            obj.pay_end = new Date(obj.pay_end);
        }
        if(obj.pay_start){
            obj.pay_start = new Date(obj.pay_start);
        }
        if(obj.report_datetime){
            obj.report_datetime = new Date(obj.report_datetime);
        }
        return obj;
    }

    $scope.checkValidDate = function(fromDate,toDate){
        if(toDate && fromDate > toDate){
            $scope.commissionByAcountObj.toDate = fromDate;
        }
    }


    $scope.callFiterData = function(valid){
        if($scope.xfire.$valid){
            $timeout(function(){
                if(valid){
                    $scope.bookingReportParms.DataTableCommon.ExportType = 0;
                    // $scope.limitOptions = [10,25,50];
                    $scope.limitOptions = [50, 100];
                    $scope.query = {};
                    // $scope.query.limit = 10;
                    $scope.query.limit = 50;
                    $scope.query.page = 1;
                    $scope.bookingReportParms.DataTableCommon.param.Start = 0;
                    // $scope.bookingReportParms.DataTableCommon.param.Length = 10;
                    $scope.bookingReportParms.DataTableCommon.param.Length = 50;
                }
                if(!$scope.bookingReportParms.DataTableCommon.ExportType){
                    $scope.xfireListLoading = true;
                    $scope.bookingReportParms.fromDate = $filter('date')($scope.commissionByAcountObj.fromDate, "dd/MM/yyyy");
                    $scope.bookingReportParms.toDate = $filter('date')($scope.commissionByAcountObj.toDate, "dd/MM/yyyy");
                    $scope.$emit('callSelectedFieldByPage',$scope.bookingReportParms);
                    if($scope.bookingReportParms.pageStart){
                        // $scope.limitOptions = [10,25,50];
                        $scope.limitOptions = [50, 100];
                        $scope.query = {};
                        // $scope.query.limit = 10;
                        $scope.query.limit = 50;
                        $scope.query.page = 1;
                    }
                }

                $http({
                    url: SETTINGS[GLOBALS.ENV].apiUrl+"Reports/GetExtraReportXfire",
                    method: 'POST',
                    data: $scope.bookingReportParms,
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
                    }
                }).success(function(response) {
                    $scope.xfireListLoading = false;

                    if(response.ResposeCode == 2){
                        if(response.DataList.data){
                            $scope.xfireList = response.DataList.data.filter(filterDate);
                            $scope.bookingListTotal = response.DataList.recordsTotal;
                            /*$(".md-table thead tr:first").css("width", "100%");
                            $(".md-table:first").floatThead({scrollingTop:60});*/

                            /*===== For Fixed header =====*/
                            $timeout(function () {

                                if ($("md-table-container .md-table:first").length > 0) {
                                    $("md-table-container .md-table:first").floatThead('destroy');
                                    $("md-table-container .md-table:first").floatThead({ scrollingTop: 60 });
                                }

                            }, 2);

                        }
                        if(response.DataList.fileName){
                            /*window.open(
                                SETTINGS[GLOBALS.ENV].apiUrl+response.DataList.fileName,
                                '_blank'
                                );*/
                                window.open(
                                    response.DataList.fileName,
                                    '_blank'
                                    );
                            }
                        // $scope.bookingListTotal = response.DataList.recordsTotal;
                    }else{
                        $scope.xfireList = [];
                    }
                    if($scope.bookingReportParms.pageStart){
                        delete $scope.bookingReportParms.pageStart;
                    }
                }).error(function(err,status) {
                    /*===== For Fixed header =====*/
                    var $table = $('.md-table:first');
                    $table.floatThead({
                      responsiveContainer: function($table){
                          return $table.closest('.full-height');
                      }

                  });
                    $scope.errorView(err,status);
                });
            },300);
}    
}


$scope.getPDFLink = function(expot){
    $scope.bookingReportParms.DataTableCommon.ExportType = Number(expot);
    $scope.callFiterData();
}

/* ==== Pagination Click function ==== */
$scope.logOrder = function (order) {
  $scope.bookingReportParms.DataTableCommon.param.Columns = [{}];
  if(order.charAt(0) == '-'){
    $scope.bookingReportParms.DataTableCommon.param.Columns[0].Name = order.slice(1);    
    $scope.bookingReportParms.DataTableCommon.param.Columns[0].Dir = "desc";    
}else{
    $scope.bookingReportParms.DataTableCommon.param.Columns[0].Name = order;    
    $scope.bookingReportParms.DataTableCommon.param.Columns[0].Dir = "asc";    
}
$scope.bookingReportParms.DataTableCommon.ExportType = 0;
$scope.callFilterData();
};

$scope.logPagination = function (page, limit) {
    var startPage = (limit * (page - 1));
    $scope.bookingReportParms.DataTableCommon.param.Start = startPage;
    $scope.bookingReportParms.DataTableCommon.param.Length = limit;
    $scope.bookingReportParms.DataTableCommon.ExportType = 0;
    $scope.callFiterData();
}


}]);