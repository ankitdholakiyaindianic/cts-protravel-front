App.controller('agentMaintenance', ['$scope','authService','$cookies','$cookieStore','$state','$stateParams', '$http', 'GLOBALS','SETTINGS', 'toaster','$mdDialog','$rootScope','$q','$timeout','$log','$filter', function($scope, authService,$cookies, $cookieStore, $state, $stateParams, $http, GLOBALS,SETTINGS,toaster, $mdDialog,$rootScope,$q,$timeout,$log,$filter){

    var t;
    $scope.stateName = '';
    $scope.agentDetails = {};
    // $scope.agentDetails.id = 0;
    // $scope.agentDetails.corpID = $scope.userDetails.corpID;
    $scope.agentDetails.active = true;
    $scope.agentListLoading = false;
    $scope.noData = "No data found";
    $scope.corpID = $scope.userDetails.corpID;
    $scope.ta_id = $scope.userDetails.ta_id;
    $scope.agentCode = "";
    $scope.disableAccountNo = false;
    $scope.dropdownLoading = false;
    $scope.savingData = false;

    t = angular.copy($scope.accountDetails), $scope.revert = function() {
        $scope.accountDetails = angular.copy(t), $scope.agentMaintenanceForm.$setPristine(), $scope.agentMaintenanceForm.$setUntouched()
    }, $scope.canRevert = function() {
        return !angular.equals($scope.accountDetails, t) || !$scope.agentMaintenanceForm.$pristine
    }, $scope.canSubmit = function() {
        return $scope.agentMaintenanceForm.$valid && !angular.equals($scope.agentDetails, t)
    }, $scope.submitForm = function() {
        $scope.showInfoOnSubmit = !0, e.revert()
    }
    // $scope.limitOptions = [10,25,50];
    $scope.limitOptions = [50, 100];
    $scope.query = {};
    // $scope.query.limit = 10;
    $scope.query.limit = 50;
    $scope.query.page = 1;
    
    $scope.getAccountParam = {
        DataTableCommon:{
            param:{
                Draw:1,
                Start:0,
                // Length:10
                Length:50
            },
            ExportType:0
        },
        ta_id:$scope.ta_id,agentCode:$scope.agentCode,group:"",description:"",accountCode:"",active:""
    };

    $scope.getBusinessUnits = function(){
        $http({
            url: SETTINGS[GLOBALS.ENV].apiUrl+"Corp_Agency/GetCorpBusinessUnits",
            method: 'POST',
            data: {ta_id: $scope.ta_id, id: $scope.corpID},
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
            }
        }).success(function(response) { 
            if(response.ResposeCode == 2){
                $scope.businessUnitList = response.DataList.data;
            }
        }).error(function(err,status) {
            $scope.errorView(err,status);
        });
    }

    $scope.getDefaultAccount = function(){

        $scope.dropdownLoading = true;

        $http({
            url: SETTINGS[GLOBALS.ENV].apiUrl+"Corp_Account/GetCorpAccountList",
            method: 'POST',
            data: {corpID: $scope.corpID},
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
            }
        }).success(function(response) { 
            $scope.dropdownLoading = false;
            if(response.ResposeCode == 2){
                $scope.defaultAccountList = response.DataList.data;


                if($stateParams.id){
                    $http({
                        url: SETTINGS[GLOBALS.ENV].apiUrl+"Agent/GetAgentData",
                        method: 'POST',
                        data: $.param($scope.obj),
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded',
                            'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
                        }
                    }).success(function(response) { 

                        if(response.ResposeCode == 2){
                            $scope.agentDetails = response.DataModel.agencyAgent;
                            // var data = $scope.defaultAccountList.filter($scope.createFilterFor($scope.agentDetails.defaultAccountID));
                            var data = $scope.defaultAccountList.filter($scope.createFilterForDisplay($scope.agentDetails.defaultAccountID));
                            
                            $scope.bkpselectedItem = data[0];
                            $scope.descriptionText = data[0].accountCode + ' - ' + data[0].description;
                            $scope.deleteAgentFlag = response.DataModel.deleteAgent;
                            
                            var xrefObj = {};
                            xrefObj.agentID = response.DataModel.agencyAgent.id;
                            /***** get associated data list  *****/
                            $http({
                                url: SETTINGS[GLOBALS.ENV].apiUrl+"XRef_Agent/GetAgentAccountXRef",
                                method: 'POST',
                                data: $.param(xrefObj),
                                headers: {
                                    'Content-Type': 'application/x-www-form-urlencoded',
                                    'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
                                }
                            }).success(function(res) { 
                                if(response.ResposeCode == 2){
                                    $scope.agentDetails.associatedAccounts = res.DataList.data;
                                            //$scope.contactsSelected = $scope.agentDetails.associatedAccounts;
                                            
                                            for(var i = 0 ; i < $scope.agentDetails.associatedAccounts.length ; i++){
                                                var data1 = $scope.defaultAccountList.filter($scope.createFilterForDisplay($scope.agentDetails.associatedAccounts[i].accountID));
                                                $scope.defaultAccountList.filter($scope.createFilterForBind($scope.agentDetails.associatedAccounts[i].accountID));
                                                $scope.contactsSelected.push(data1[0]);
                                                // console.log("$scope.contactsSelected 119 : ", $scope.contactsSelected);
                                            }
                                            // $scope.item.selectedval = $scope.contactsSelected;
                                            // $scope.selectedval = $scope.contactsSelected;
                                            // $scope.associatedText = $scope.contactsSelected
                                            

                                        }
                                        
                                    }).error(function(err,status) {
                                        $scope.errorView(err,status);
                                    });
                                    /***** get associated data list  *****/

                                }
                            }).error(function(err,status) {
                                $scope.errorView(err,status);
                            });

                        }
                    }else{

                    }
                }).error(function(err,status) {
                    $scope.errorView(err,status);
                });
            }

            $scope.getAgentMaintenanceList = function(valid){

                if(valid){
                    $scope.agentListLoading = true;
                    $scope.setRequestList.DataTableCommon.ExportType = 0;
                }
                $http({
                    url: SETTINGS[GLOBALS.ENV].apiUrl+"Agent/GetAgentList",
                    method: 'POST',
                    data: $scope.setRequestList,
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
                    }
                }).success(function(response) { 
                    $scope.agentListLoading = false;
                    if(response.ResposeCode == 2){
                        if(response.DataList.data && response.DataList.data.length){
                            $scope.agentList = response.DataList.data;
                            $scope.agentListTotal = response.DataList.recordsTotal;

                            /*===== For Fixed header =====*/
                            if($scope.stateName == 'list'){
                                $timeout(function () {

                                    if ($("md-table-container .md-table:first").length > 0) {
                                        $("md-table-container .md-table:first").floatThead('destroy');
                                        $("md-table-container .md-table:first").floatThead({ scrollingTop: 60 });
                                    }

                                }, 2);
                            }

                        }
                        if(response.DataList.fileName){
                            /*window.open(
                                SETTINGS[GLOBALS.ENV].apiUrl+response.DataList.fileName,
                                '_blank'
                                );*/
                                window.open(
                                    response.DataList.fileName,
                                    '_blank'
                                    );
                            }
                        // $scope.agentListTotal = response.DataList.recordsTotal;
                    }else{
                        $scope.agentList = [];
                    }
                    if($scope.setRequestList.DataTableCommon.param.Columns){
                        delete $scope.setRequestList.DataTableCommon.param.Columns;
                    }

                }).error(function(err,status) {
                 /*===== For Fixed header =====*/
                 var $table = $('.md-table:first');
                 $table.floatThead({
                  responsiveContainer: function($table){
                      return $table.closest('.full-height');
                  }

              });
                 $scope.errorView(err,status);
                 if($scope.setRequestList.DataTableCommon.param.Columns){
                    delete $scope.setRequestList.DataTableCommon.param.Columns;
                }

            });
            }

            if($state.current.name == 'main.agentMaintenance'){
                $scope.stateName = 'list';
                $scope.accountDetails = {};
                $scope.accountDetails.corpID = $scope.corpID;
                $scope.accountDetails.active = "";
                $scope.agentListLoading = true;
                $scope.setRequestList = angular.copy($scope.getAccountParam);
                $scope.setRequestList.searchText = "";
                $scope.getAgentMaintenanceList(true);

            }else{
                if($stateParams.id){
                    $scope.stateName = 'update';
                    $scope.accountID = $stateParams.id;
                    $scope.obj = {};
                    $scope.obj.agentCode = $stateParams.id;
                    $scope.obj.ta_id = $scope.ta_id;
                    $scope.obj.ta_id = $stateParams.ta_id;
                    $scope.getDefaultAccount();
                    $scope.getBusinessUnits();
                    $scope.setRequestList = angular.copy($scope.getAccountParam);
                    $scope.disableAccountNo = true;
                    $scope.getAgentMaintenanceList(true);
                    
                }else{
                    if($state.current.name == 'main.agentMaintenanceAdd'){
                        $scope.stateName = 'add';
                        $scope.setRequestList = angular.copy($scope.getAccountParam);
                        $scope.getAgentMaintenanceList(true);
                        $scope.getBusinessUnits();    
                        $scope.getDefaultAccount();
                    }
                }
            }

            $scope.callAddAgent = function(){
                $state.go('main.agentMaintenanceAdd');
            }

            $scope.reDirectPage = function(){
                $state.go('main.agentMaintenance');   
            }
            $scope.aDetails = {};
            $scope.accountID = [];
            function filterIds(obj){
                $scope.accountID.push(obj.id);
                return obj;
            }

            /***** Add and update agent *****/
            $scope.addAndUpdate = function(agentDetails){

                var DeffAccId = null;
                if($scope.bkpselectedItem && $scope.bkpselectedItem.id){
                    DeffAccId = $scope.bkpselectedItem.id;
                }

                $scope.accountID = [];
                $scope.aDetails.agentCode = (agentDetails.agentCode)?angular.uppercase(agentDetails.agentCode):'';
                $scope.aDetails.lastName = agentDetails.lastName;
                $scope.aDetails.firstName = agentDetails.firstName;
                $scope.aDetails.email = agentDetails.email;
                $scope.aDetails.ta_id = agentDetails.ta_id;
                $scope.aDetails.defaultAccountID = (!$scope.selectedItem) ? DeffAccId : $scope.selectedItem.id;
                $scope.aDetails.reassignedAgentID = agentDetails.reassignedAgentID;
                $scope.aDetails.active = agentDetails.active; 

                $scope.contactsSelected.filter(filterIds);

                if(!$scope.aDetails.agentCode){
                    toaster.pop('error', '', 'Please Enter Agent Code');
                    return false;
                }

                if(!$scope.aDetails.firstName){
                    toaster.pop('error', '', 'Please Enter First name');
                    return false;
                }

                if(!$scope.aDetails.lastName){
                    toaster.pop('error', '', 'Please Enter Last name');
                    return false;
                }

                if(!$scope.aDetails.email){
                    toaster.pop('error', '', 'Please Enter Email');
                    return false;
                }

                if(!$scope.aDetails.ta_id){
                    toaster.pop('error', '', 'Please Select Business Units');
                    return false;
                }

                if(!$scope.aDetails.ta_id){
                    toaster.pop('error', '', 'Please Select Business Units');
                    return false;
                }

                if(!$scope.aDetails.defaultAccountID){
                    toaster.pop('error', '', 'Please Select Default Account');
                    return false;
                }

                $scope.userListLoding = true;
                $scope.savingData = true;
                var API_Name = 'Agent/InsertAgentData';
                if(agentDetails.id){
                    API_Name = 'Agent/UpdateAgentData';
                }else{
                    agentDetails['id'] = 0;
                }

                agentDetails['corpID'] = $scope.corpID;
                // return false;
                $http({
                    url: SETTINGS[GLOBALS.ENV].apiUrl+API_Name,
                    method: 'POST',
                    data: $.param($scope.aDetails),
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded',
                        'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
                    }
                }).success(function(response) { 
                    $scope.savingData = false;
                    if(response.ResposeCode == 2){

                        var obj = [];
                        for(var i =0;i<$scope.accountID.length;i++){
                            var sel = {};
                            sel['accountID'] = $scope.accountID[i];
                            sel['agentID'] = response.DataModel.id;
                            obj.push(sel);
                        }
                        if(API_Name == "Agent/InsertAgentData"){
                            $http({
                                url: SETTINGS[GLOBALS.ENV].apiUrl+"XRef_Agent/InsertAgentAccountXRef",
                                method: 'POST',
                                data: JSON.stringify(obj),
                                headers: {
                                    'Content-Type': 'application/json',
                                    'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
                                }
                            }).success(function(res) { 
                                if(res.ResposeCode == 2){
                            // toaster.pop('Success', '', res.ResponseMessage);
                            // $scope.reDirectPage();
                        }else{
                            toaster.pop('error', '', res.ResponseMessage);
                        }
                    });
                        }else{

                            $http({
                                url: SETTINGS[GLOBALS.ENV].apiUrl+"XRef_Agent/UpdateAgentAccountXRef",
                                method: 'POST',
                                data: JSON.stringify(obj),
                                headers: {
                                    'Content-Type': 'application/json',
                                    'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
                                }
                            }).success(function(res) { 
                                if(res.ResposeCode == 2){

                                }else{
                            // toaster.pop('error', '', res.ResponseMessage);
                            // $scope.reDirectPage();
                        }
                    });
                            
                        }   

                        toaster.pop('Success', '', response.ResponseMessage);
                        $scope.reDirectPage();

                    }else{
                        toaster.pop('error', '', response.ResponseMessage);
                    }
                });
                
            }
            /***** Add and update agent *****/

            /***** Search Data *****/
            $scope.searchData = function(search){
                if(!search){
                    search = "";    
                }else{
                    search = angular.lowercase(search);
                    search = search.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
                }
                
                $scope.setRequestList.searchText = search;
                $scope.agentList = [];
                $scope.agentListLoading = true;

                $http({
                  url: SETTINGS[GLOBALS.ENV].apiUrl+"Agent/GetAgentList",
                  method: 'POST',
                  data: $scope.setRequestList,
                  headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'bearer '+authService.getAuthToken('AuthToken')                

                }
            }).success(function(response) { 
                $scope.agentListLoading = false;
                if(response.ResposeCode == 2){
                    $scope.agentList = response.DataList.data;
                    $scope.agentListTotal = response.DataList.recordsTotal;
                }else{      
                    $scope.agentList = [];
                    /*toaster.pop('error', '', response.ResponseMessage);*/
                }
            });
        }


        /***** Delete agent *****/    
        $scope.deleteAgent = function(){

            $scope.id = $stateParams.id;
            var obj = {};
            obj.agentCode = $scope.id;

            $http({
                url: SETTINGS[GLOBALS.ENV].apiUrl+"Agent/DeleteAgentData",
                method: 'POST',
                data: $.param(obj),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
                }
            }).success(function(response) { 
                if(response.ResposeCode == 2){
                    toaster.pop('Success', 'Delete', response.ResponseMessage);
                    $scope.reDirectPage();
                }else{
                    toaster.pop('error', 'Delete', response.ResponseMessage);
                }
            });
        }
        /***** Delete agent *****/    


        /* ==== get PDF Link ==== */
        $scope.getPDFLink = function(expot){
            $scope.setRequestList.DataTableCommon.ExportType = Number(expot);
            if($scope.setRequestList.DataTableCommon.param.Columns){
                delete $scope.setRequestList.DataTableCommon.param.Columns;
            }
            $scope.getAgentMaintenanceList();
        }

        /* ==== Pagination Click function ==== */
        $scope.logOrder = function (order) {
         $scope.setRequestList.DataTableCommon.param.Columns = [{}];
         if(order.charAt(0) == '-'){
            $scope.setRequestList.DataTableCommon.param.Columns[0].Name = order.slice(1);    
            $scope.setRequestList.DataTableCommon.param.Columns[0].Dir = "desc";    
        }else{
            $scope.setRequestList.DataTableCommon.param.Columns[0].Name = order;    
            $scope.setRequestList.DataTableCommon.param.Columns[0].Dir = "asc";    
        }

        $scope.storeColumns = $scope.setRequestList.DataTableCommon.param.Columns;
        $scope.setRequestList.DataTableCommon.ExportType = 0;
        $scope.getAgentMaintenanceList(true);

    };

    $scope.logPagination = function (page, limit) {
        var startPage = (limit * (page - 1));
        $scope.setRequestList = angular.copy($scope.getAccountParam);
        $scope.setRequestList.DataTableCommon.param.Columns = $scope.storeColumns;
        $scope.setRequestList.DataTableCommon.param.Start = startPage;
        $scope.setRequestList.DataTableCommon.param.Length = limit;
        $scope.setRequestList.DataTableCommon.ExportType = 0;
        $scope.getAgentMaintenanceList(true);
    }

    /***** Delete popup *****/
    $scope.popupTitle = 'Delete Account';
    $scope.pageDescription = 'Would you like to delete?';
    $scope.showConfirmPopup = function(ev,id) {
        $mdDialog.show({
            contentElement: '#conFirmPopup',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose:true,
            fullscreen: false
        }).finally(function() {
        });
    };

    $scope.callBackDialog = function(data){
        if(data == 'ok'){
            /*Call delete API here */
            $scope.deleteAgent();
        }
        $mdDialog.cancel();
    }

    /***** Delete popup *****/

    /* ==== NG - autocomplete material ==== */
    $scope.querySearch = function(query) {
      var results = query ? $scope.defaultAccountList.filter($scope.createFilterFor(query) ) : $scope.defaultAccountList, deferred;
      if (false) {
        deferred = $q.defer();
        $timeout(function () { deferred.resolve( results ); }, Math.random() * 1000, false);
        return deferred.promise;
    } else {
        return results;
    }
}

$scope.createFilterFor = function(query) {

    var lowercaseQuery = angular.lowercase(query);
    return function filterFn(state) {
        var setval = state.accountCode + ' - ' + state.description;
        // var setval = state.accountCode;
        
        state['querySearch'] = setval;
        if(state.id && state.id.toString().toLowerCase().indexOf(lowercaseQuery) != -1){

            return (state.id && state.id.toString().toLowerCase().indexOf(lowercaseQuery) == 0);
        }
        if(state.accountCode && state.accountCode.toLowerCase().indexOf(lowercaseQuery) != -1){

            return (state.accountCode && state.accountCode.toLowerCase().indexOf(lowercaseQuery) == 0);
        }
        /*if(state.description && state.description.toLowerCase().indexOf(lowercaseQuery) != -1){
            return (state.description && state.description.toLowerCase().indexOf(lowercaseQuery) === 0);
        }*/
        // return (state.accountCode && state.description && setval.toLowerCase().indexOf(lowercaseQuery) === 0);
        return (state.accountCode && setval.toLowerCase().indexOf(lowercaseQuery) == 0);

    };
}



$scope.createFilterForDisplay = function(query) {

    var lowercaseQuery = angular.lowercase(query);
    return function filterFn(state) {
        var setval = state.accountCode + ' - ' + state.description;
        // var setval = state.accountCode;
        
        state['querySearch'] = setval;
        if(state.id && state.id.toString().toLowerCase() == lowercaseQuery){
            // console.log("state : ", state);
            return (state.id && state.id.toString().toLowerCase() == lowercaseQuery);
        }

        if(state.accountCode && state.accountCode.toLowerCase() == lowercaseQuery){
            return (state.accountCode && state.accountCode.toLowerCase() == lowercaseQuery);
        }
        /*if(state.description && state.description.toLowerCase().indexOf(lowercaseQuery) != -1){
            return (state.description && state.description.toLowerCase().indexOf(lowercaseQuery) === 0);
        }*/
        // return (state.accountCode && state.description && setval.toLowerCase().indexOf(lowercaseQuery) === 0);
        return (state.accountCode && setval.toLowerCase() == lowercaseQuery);

    };
}

$scope.createFilterForBind = function(query) {
  return function filterFn(state) {
    if(state.id && state.id === query){
        return (state['selectedval'] = true);
    }
};
}



/* ===== NG multiple autocomplete material ===== */
$scope.contactsSelected = [];
$scope.selectedItemChange = function(item) {
  if(item){
    if($filter('filter')($scope.contactsSelected, function (d) {
        return d.accountCode === item.accountCode && d.description === item.description;})[0]){
        $scope.contactsSelected.splice($scope.contactsSelected.indexOf(item),1);
    }else{
        $scope.contactsSelected.push(item);    
    }
}
}


$scope.removeSelectedElm = function(item) {
    if(item){
        $scope.contactsSelected.splice($scope.contactsSelected.indexOf(item),1);
        $scope.defaultAccountList[$scope.defaultAccountList.indexOf(item)].selectedval = false;
    }
}


}]);