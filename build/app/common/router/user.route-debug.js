var ModuleName = 'DEMO';
var app = angular.module('user.route',[
    'ngCookies',
    'defaultServices',
    'constants',
    'ui.router'
    ]);

app.config(['$stateProvider', '$urlRouterProvider', '$locationProvider','$httpProvider','GLOBALS','SETTINGS', function($stateProvider, $urlRouterProvider, $locationProvider,$httpProvider,GLOBALS,SETTINGS) {
    $stateProvider
    .state('main.resolvedbookingsmatrix', {
        url: "/resolved-bookings-matrix",
        data: {
            pageTitle: 'Resolved Bookings Matrix',
            accessAllow: {'report_id':28,'web_menus':false}
        },
        views: {
            'home': {
                templateUrl: "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/resolved-bookings-matrix/resolved-bookings-matrixView.html",
                controller: "resolvedbookingsmatrixCtrl",
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: ModuleName,
                            insertBefore: '#load_plugins_before',
                            files: [
                            "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/resolved-bookings-matrix/resolved-bookings-matrixCtrl.js" 
                            ]
                        });
                    }]
                }
            }
        }

    })
    .state('main.addUserProfile', {
        url: "/addUserProfile",
        data: {
            pageTitle: 'Add user profile',
            accessAllow: {'report_id':42,'web_menus':false}
        },
        views: {
            'home': {
                templateUrl: "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/users/addUserProfile.html",
                controller: "userCtrl",
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: ModuleName,
                            insertBefore: '#load_plugins_before',
                            files: [
                            "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/users/userCtrl.js" 
                            ]
                        });
                    }]
                }
            }
        }

    })
    .state('main.component2', {
        url: "/component2",
        data: {
            pageTitle: 'Component Two'
        },
        views: {
            'home': {
                templateUrl: "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/component2/component2View.html",
                controller: "component2Ctrl",
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: ModuleName,
                            insertBefore: '#load_plugins_before',
                            files: [
                            "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/component2/component2Ctrl.js" ,
                            "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/component2/component2Service.js" 
                            ]
                        });
                    }],
                    user:['userApiService',function(userApiService){
                        return userApiService.GetAllCountry();
                    }],
                    user1:['userApiService',function(userApiService){
                        return userApiService.GetAllCountry();
                    }],
                    user2:['userApiService',function(userApiService){
                        return userApiService.GetAllCountry();
                    }]
                }
            }
        }

    })
    .state('main.extractReportTM1', {
        url: "/extractReportTM1",
        data: {
            pageTitle: 'Report: Extract Report - TM1',
            accessAllow: {'report_id':89,'web_menus':false}
        },
        views: {
            'home': {
                templateUrl: "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/report/extractReportTM1.html",
                controller: "extractReportTM1",
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: ModuleName,
                            insertBefore: '#load_plugins_before',
                            files: [
                            "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/report/extractReportTM1.js" 
                            ]
                        });
                    }]
                }
            }
        }

    })
    .state('main.whoIsClaiming', {
        url: "/whoIsClaiming",
        data: {
            pageTitle: 'Report: Who Is Claiming?',
            accessAllow: {'report_id':130,'web_menus':false}
        },
        views: {
            'home': {
                templateUrl: "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/report/whoIsClaiming.html",
                controller: "whoIsClaiming",
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: ModuleName,
                            insertBefore: '#load_plugins_before',
                            files: [
                            "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/report/whoIsClaiming.js" 
                            ]
                        });
                    }]
                }
            }
        }

    })
    .state('main.CTSBatchMaintenance', {
        url: "/CTSBatchMaintenance",
        data: {
            pageTitle: 'Report: CTS - Batch Maintenance',
            accessAllow: {'report_id':151,'web_menus':false}
        },
        views: {
            'home': {
                templateUrl: "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/report/CTSBatchMaintenance.html",
                controller: "CTSBatchMaintenance",
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: ModuleName,
                            insertBefore: '#load_plugins_before',
                            files: [
                            "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/report/CTSBatchMaintenance.js" 
                            ]
                        });
                    }]
                }
            }
        }

    })
    .state('main.CTSBatchMaintenancBatchData', {
        url: "/CTSBatchMaintenance/:id",
        data: {
            pageTitle: 'Report: CTS - Batch Maintenance',
            accessAllow: {'report_id':151,'web_menus':false}
        },
        views: {
            'home': {
                templateUrl: "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/report/CTSBatchMaintenance.html",
                controller: "CTSBatchMaintenance",
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: ModuleName,
                            insertBefore: '#load_plugins_before',
                            files: [
                            "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/report/CTSBatchMaintenance.js" 
                            ]
                        });
                    }]
                }
            }
        }

    })
    .state('main.bookingAndPaymentsCombined', {
        url: "/bookingAndPaymentsCombined",
        data: {
            pageTitle: 'Report: Booking and Payments Combined',
            accessAllow: {'report_id':1,'web_menus':false}
        },
        views: {
            'home': {
                templateUrl: "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/report/bookingAndPaymentsCombined.html",
                controller: "bookingAndPaymentsCombined",
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: ModuleName,
                            insertBefore: '#load_plugins_before',
                            files: [
                            "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/report/bookingAndPaymentsCombined.js" 
                            ]
                        });
                    }]
                }
            }
        }

    })
    .state('main.claimed-AllExcludingHotelAccount', {
        url: "/claimed-AllExcludingHotelAccount",
        data: {
            pageTitle: 'Report: Claimed - All excluding Hotel - Account',
            accessAllow: {'report_id':65,'web_menus':false}
        },
        views: {
            'home': {
                templateUrl: "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/report/claimed-AllExcludingHotelAccount.html",
                controller: "claimed-AllExcludingHotelAccount",
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: ModuleName,
                            insertBefore: '#load_plugins_before',
                            files: [
                            "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/report/claimed-AllExcludingHotelAccount.js" 
                            ]
                        });
                    }]
                }
            }
        }

    })
    .state('main.matureBookingsReportByAgent-CarsOnly', {
        url: "/matureBookingsReportByAgent-CarsOnly",
        data: {
            pageTitle: 'Report: Mature Bookings Report By Agent - Cars Only',
            accessAllow: {'report_id':61,'web_menus':false}
        },
        views: {
            'home': {
                templateUrl: "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/report/matureBookingsReportByAgent-CarsOnly.html",
                controller: "matureBookingsReportByAgent-CarsOnly",
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: ModuleName,
                            insertBefore: '#load_plugins_before',
                            files: [
                            "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/report/matureBookingsReportByAgent-CarsOnly.js" 
                            ]
                        });
                    }]
                }
            }
        }

    })
    .state('main.matureBookingsReportByAgent-HotelsOnly', {
        url: "/matureBookingsReportByAgent-HotelsOnly",
        data: {
            pageTitle: 'Report: Mature Bookings Report By Agent - Hotels Only',
            accessAllow: {'report_id':60,'web_menus':false}
        },
        views: {
            'home': {
                templateUrl: "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/report/matureBookingsReportByAgent-HotelsOnly.html",
                controller: "matureBookingsReportByAgent-HotelsOnly",
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: ModuleName,
                            insertBefore: '#load_plugins_before',
                            files: [
                            "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/report/matureBookingsReportByAgent-HotelsOnly.js" 
                            ]
                        });
                    }]
                }
            }
        }

    })
    .state('main.outstandingBookingReport-CarsOnly', {
        url: "/outstandingBookingReport-CarsOnly",
        data: {
            pageTitle: 'Report: Outstanding Booking Report - Cars Only',
            accessAllow: {'report_id':63,'web_menus':false}
        },
        views: {
            'home': {
                templateUrl: "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/report/OutstandingBookingReport-CarsOnly.html",
                controller: "outstandingBookingReport-CarsOnly",
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: ModuleName,
                            insertBefore: '#load_plugins_before',
                            files: [
                            "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/report/OutstandingBookingReport-CarsOnly.js" 
                            ]
                        });
                    }]
                }
            }
        }

    })
    .state('main.outstandingBookingReport-HotelsOnly', {
        url: "/outstandingBookingReport-HotelsOnly",
        data: {
            pageTitle: 'Report: Outstanding Booking Report - Hotels Only',
            accessAllow: {'report_id':62,'web_menus':false}
        },
        views: {
            'home': {
                templateUrl: "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/report/OutstandingBookingReport-HotelsOnly.html",
                controller: "outstandingBookingReport-HotelsOnly",
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: ModuleName,
                            insertBefore: '#load_plugins_before',
                            files: [
                            "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/report/OutstandingBookingReport-HotelsOnly.js" 
                            ]
                        });
                    }]
                }
            }
        }

    })
    .state('main.matureBookingsReport', {
        url: "/matureBookingsReport",
        data: {
            pageTitle: 'Report: Mature Bookings Report',
            accessAllow: {'report_id':19,'web_menus':false}
        },
        views: {
            'home': {
                templateUrl: "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/report/MatureBookingsReport.html",
                controller: "MatureBookingsReport",
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: ModuleName,
                            insertBefore: '#load_plugins_before',
                            files: [
                            "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/report/MatureBookingsReport.js" 
                            ]
                        });
                    }]
                }
            }
        }

    })
    .state('main.businessCalendar', {
        url: "/businessCalendar",
        data: {
            pageTitle: 'Business Calendar',
            accessAllow: {'report_id':87,'web_menus':false}
        },
        views: {
            'home': {
                templateUrl: "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/report/businessCalendar.html",
                controller: "businessCalendar",
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: ModuleName,
                            insertBefore: '#load_plugins_before',
                            files: [
                            "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/report/businessCalendar.js" 
                            ]
                        });
                    }]
                }
            }
        }

    })
    .state('main.importReassignmentReport', {
        url: "/importReassignmentReport",
        data: {
            pageTitle: 'Import Reassignment Report',
            accessAllow: {'report_id':17,'web_menus':false}
        },
        views: {
            'home': {
                templateUrl: "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/reassignment-reports/importReassignmentReport.html",
                controller: "importReassignmentReport",
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: ModuleName,
                            insertBefore: '#load_plugins_before',
                            files: [
                            "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/reassignment-reports/importReassignmentReport.js" 
                            ]
                        });
                    }]
                }
            }
        }

    })
    .state('main.userMaintenance', {
        url: "/userMaintenance",
        data: {
            pageTitle: 'User Maintenance',
            accessAllow: {'report_id':43,'web_menus':false}
        },
        views: {
            'home': {
                templateUrl: "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/users/userMaintenance.html",
                controller: "userMaintenance",
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: ModuleName,
                            insertBefore: '#load_plugins_before',
                            files: [
                            "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/users/userMaintenance.js" 
                            ]
                        });
                    }]
                }
            }
        }

    })
    .state('main.changePassword', {
        url: "/changePassword",
        data: {
            pageTitle: 'Change password',
            // accessAllow: {'report_id':44,'web_menus':false}
        },
        views: {
            'home': {
                templateUrl: "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/users/changePassword.html",
                controller: "userCtrl",
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: ModuleName,
                            insertBefore: '#load_plugins_before',
                            files: [
                            "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/users/userCtrl.js" 
                            ]
                        });
                    }]
                }
            }
        }

    })
    .state('main.accountMaintenance', {
        url: "/accountMaintenance/",
        data: {
            pageTitle: 'Account Maintenance',
            accessAllow: {'report_id':41,'web_menus':false}
        },
        views: {
            'home': {
                templateUrl: "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/account-maintenance/accountMaintenance.html",
                controller: "accountMaintenance",
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: ModuleName,
                            insertBefore: '#load_plugins_before',
                            files: [
                            "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/account-maintenance/accountMaintenance.js" 
                            ]
                        });
                    }]
                }
            }
        }

    })
    .state('main.accountMaintenanceAdd', {
        url: "/accountMaintenance/add",
        data: {
            pageTitle: 'Account Maintenance',
            accessAllow: {'report_id':41,'web_menus':false}
        },
        views: {
            'home': {
                templateUrl: "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/account-maintenance/accountMaintenance.html",
                controller: "accountMaintenance",
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: ModuleName,
                            insertBefore: '#load_plugins_before',
                            files: [
                            "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/account-maintenance/accountMaintenance.js" 
                            ]
                        });
                    }]
                }
            }
        }

    })    
    .state('main.accountMaintenanceUpdate', {
        url: "/accountMaintenance/:id",
        data: {
            pageTitle: 'Account Maintenance',
            accessAllow: {'report_id':41,'web_menus':false}
        },
        views: {
            'home': {
                templateUrl: "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/account-maintenance/accountMaintenance.html",
                controller: "accountMaintenance",
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: ModuleName,
                            insertBefore: '#load_plugins_before',
                            files: [
                            "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/account-maintenance/accountMaintenance.js" 
                            ]
                        });
                    }]
                }
            }
        }

    })
    .state('main.agentMaintenance', {
        url: "/agentMaintenance",
        data: {
            pageTitle: 'Agent Maintenance',
            accessAllow: {'report_id':40,'web_menus':false}
        },
        views: {
            'home': {
                templateUrl: "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/agent-maintenance/agentMaintenance.html",
                controller: "agentMaintenance",
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: ModuleName,
                            insertBefore: '#load_plugins_before',
                            files: [
                            "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/agent-maintenance/agentMaintenance.js" 
                            ]
                        });
                    }]
                }
            }
        }

    })    
    .state('main.agentMaintenanceAdd', {
        url: "/agentMaintenance/add",
        data: {
            pageTitle: 'Agent Maintenance',
            accessAllow: {'report_id':40,'web_menus':false}
        },
        views: {
            'home': {
                templateUrl: "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/agent-maintenance/agentMaintenance.html",
                controller: "agentMaintenance",
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: ModuleName,
                            insertBefore: '#load_plugins_before',
                            files: [
                            "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/agent-maintenance/agentMaintenance.js" 
                            ]
                        });
                    }]
                }
            }
        }

    })
    .state('main.agentMaintenanceUpdate', {
        url: "/agentMaintenanceUpdate/:id/:ta_id",
        data: {
            pageTitle: 'Agent Maintenance',
            accessAllow: {'report_id':40,'web_menus':false}
        },
        views: {
            'home': {
                templateUrl: "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/agent-maintenance/agentMaintenance.html",
                controller: "agentMaintenance",
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: ModuleName,
                            insertBefore: '#load_plugins_before',
                            files: [
                            "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/agent-maintenance/agentMaintenance.js" 
                            ]
                        });
                    }]
                }
            }
        }

    })
    .state('main.notProvidedReport', {
        url: "/notProvidedReport",
        data: {
            pageTitle: 'Not Provided Report',
            accessAllow: {'report_id':23,'web_menus':false}
        },
        views: {
            'home': {
                templateUrl: "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/commission-payments/notProvidedReport.html",
                controller: "notProvidedReportCtrl",
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: ModuleName,
                            insertBefore: '#load_plugins_before',
                            files: [
                            "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/commission-payments/notProvidedReportCtrl.js" 
                            ]
                        });
                    }]
                }
            }
        }

    })
    .state('main.unmatchedPayments', {
        url: "/unmatchedPayments",
        data: {
            pageTitle: 'Unmatched Payments',
            accessAllow: {'report_id':3,'web_menus':false}
        },
        views: {
            'home': {
                templateUrl: "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/unmatched-payments/unmatchedPayments.html",
                controller: "unmatchedPaymentsCtrl",
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: ModuleName,
                            insertBefore: '#load_plugins_before',
                            files: [
                            "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/unmatched-payments/unmatchedPaymentsCtrl.js" 
                            ]
                        });
                    }]
                }
            }
        }

    })
    .state('main.unmatchedPaymentRpt', {
        url: "/unmatchedPaymentRpt",
        data: {
            pageTitle: 'Unmatched Payment Report',
            accessAllow: {'report_id':24,'web_menus':false}
        },
        views: {
            'home': {
                templateUrl: "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/commission-payments/unmatchedPaymentReport.html",
                controller: "unmatchedPaymentRptCtrl",
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: ModuleName,
                            insertBefore: '#load_plugins_before',
                            files: [
                            "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/commission-payments/unmatchedPaymentReport.js" 
                            ]
                        });
                    }]
                }
            }
        }

    })
    .state('main.dunningActivityDetail', {
        url: "/dunningActivityDetail",
        data: {
            pageTitle: 'Dunning Activity Detail',
            accessAllow: {'report_id':138,'web_menus':false}
        },
        views: {
            'home': {
                templateUrl: "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/dunning-activity-detail/dunningActivityDetail.html",
                controller: "dunningActivityDetailCtrl",
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: ModuleName,
                            insertBefore: '#load_plugins_before',
                            files: [
                            "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/dunning-activity-detail/dunningActivityDetail.js" 
                            ]
                        });
                    }]
                }
            }
        }

    })
    .state('main.branchSummary', {
        url: "/branchSummary",
        data: {
            pageTitle: 'Branch Summary',
            accessAllow: {'report_id':39,'web_menus':false}
        },
        views: {
            'home': {
                templateUrl: "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/branch-summary/branchSummary.html",
                controller: "branchSummaryCtrl",
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: ModuleName,
                            insertBefore: '#load_plugins_before',
                            files: [
                            "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/branch-summary/branchSummary.js" 
                            ]
                        });
                    }]
                }
            }
        }

    })
    .state('main.chainCodeSummary', {
        url: "/chainCodeSummary",
        data: {
            pageTitle: 'Chain Code Summary',
            accessAllow: {'report_id':30,'web_menus':false}
        },
        views: {
            'home': {
                templateUrl: "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/chain-code-summary/chainCodeSummary.html",
                controller: "chainCodeSummaryCtrl",
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: ModuleName,
                            insertBefore: '#load_plugins_before',
                            files: [
                            "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/chain-code-summary/chainCodeSummary.js" 
                            ]
                        });
                    }]
                }
            }
        }

    })

    .state('main.agentSummary', {
        url: "/agentSummary",
        data: {
            pageTitle: 'Agent Summary',
            accessAllow: {'report_id':32,'web_menus':false}
        },
        views: {
            'home': {
                templateUrl: "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/agent-summary/agentSummary.html",
                controller: "agentSummaryCtrl",
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: ModuleName,
                            insertBefore: '#load_plugins_before',
                            files: [
                            "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/agent-summary/agentSummary.js" 
                            ]
                        });
                    }]
                }
            }
        }

    })
    .state('main.accountSummary', {
        url: "/accountSummary",
        data: {
            pageTitle: 'Account Summary',
            accessAllow: {'report_id':31,'web_menus':false}
        },
        views: {
            'home': {
                templateUrl: "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/account-summary/accountSummary.html",
                controller: "accountSummaryCtrl",
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: ModuleName,
                            insertBefore: '#load_plugins_before',
                            files: [
                            "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/account-summary/accountSummary.js" 
                            ]
                        });
                    }]
                }
            }
        }

    })
    .state('main.IATASummary', {
        url: "/IATASummary",
        data: {
            pageTitle: 'IATA Summary',
            accessAllow: {'report_id':33,'web_menus':false}
        },
        views: {
            'home': {
                templateUrl: "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/IATA-summary/IATASummary.html",
                controller: "IATASummaryCtrl",
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: ModuleName,
                            insertBefore: '#load_plugins_before',
                            files: [
                            "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/IATA-summary/IATASummary.js" 
                            ]
                        });
                    }]
                }
            }
        }

    })
    .state('main.snapShotBookingReport', {
        url: "/snapShotBookingReport",
        data: {
            pageTitle: 'SnapShot Booking Report',
            accessAllow: {'report_id':91,'web_menus':false}
        },
        views: {
            'home': {
                templateUrl: "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/snapShot-bookings-report/snapShotBookingsReport.html",
                controller: "snapShotBookingReportCtrl",
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: ModuleName,
                            insertBefore: '#load_plugins_before',
                            files: [
                            "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/snapShot-bookings-report/snapShotBookingsReport.js" 
                            ]
                        });
                    }]
                }
            }
        }

    })
    .state('main.extractReport', {
        url: "/extractReport",
        data: {
            pageTitle: 'Extract Report Xfire',
            accessAllow: {'report_id':20,'web_menus':false}
        },
        views: {
            'home': {
                templateUrl: "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/commission-payments/extractReportXfire.html",
                controller: "extractReportCtrl",
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: ModuleName,
                            insertBefore: '#load_plugins_before',
                            files: [
                            "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/commission-payments/extractReportXfire.js" 
                            ]
                        });
                    }]
                }
            }
        }

    })
    .state('main.userCreatedReportMaintenance', {
        url: "/userCreatedReportMaintenance",
        data: {
            pageTitle: 'User Created Report Maintenance',
            accessAllow: {'report_id':68,'web_menus':false}
        },
        views: {
            'home': {
                templateUrl: "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/user-created-report-maintenance/userCreatedReportMaintenance.html",
                controller: "userCreatedReportMaintenanceCtrl",
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: ModuleName,
                            insertBefore: '#load_plugins_before',
                            files: [
                            "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/user-created-report-maintenance/userCreatedReportMaintenance.js" 
                            ]
                        });
                    }]
                }
            }
        }

    })
    .state('main.agencyMaintenance', {
        url: "/agencyMaintenance",
        data: {
            pageTitle: 'Agency Maintenance',
            accessAllow: {'report_id':46,'web_menus':false}
        },
        views: {
            'home': {
                templateUrl: "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/agency-maintenance/agencyMaintenance.html",
                controller: "agencyMaintenanceCtrl",
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: ModuleName,
                            insertBefore: '#load_plugins_before',
                            files: [
                            "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/agency-maintenance/agencyMaintenance.js" 
                            ]
                        });
                    }]
                }
            }
        }

    })
    .state('main.commissionAnalysis', {
        url: "/commissionAnalysis",
        data: {
            pageTitle: 'Commission Analysis',
            accessAllow: {'report_id':4,'web_menus':false}
        },
        views: {
            'home': {
                templateUrl: "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/commission-analysis/commissionAnalysis.html",
                controller: "commissionAnalysisCtrl",
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: ModuleName,
                            insertBefore: '#load_plugins_before',
                            files: [
                            "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/commission-analysis/commissionAnalysis.js" 
                            ]
                        });
                    }]
                }
            }
        }

    })
    .state('main.commissionAnalysisById', {
        url: "/commissionAnalysis/:fromdate/:todate/:id",
        data: {
            pageTitle: 'Commission Analysis',
            accessAllow: {'report_id':4,'web_menus':false}
        },
        views: {
            'home': {
                templateUrl: "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/commission-analysis/commissionAnalysis.html",
                controller: "commissionAnalysisCtrl",
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: ModuleName,
                            insertBefore: '#load_plugins_before',
                            files: [
                            "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/commission-analysis/commissionAnalysis.js" 
                            ]
                        });
                    }]
                }
            }
        }

    })
    .state('main.chainCodeSummaryExpanded', {
        url: "/chainCodeSummaryExpanded",
        data: {
            pageTitle: 'Chain Code Summary Expanded',
            accessAllow: {'report_id':54,'web_menus':false}
        },
        views: {
            'home': {
                templateUrl: "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/chain-code-summary-expanded/chainCodeSummaryExpanded.html",
                controller: "chainCodeSummaryExpandedCtrl",
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: ModuleName,
                            insertBefore: '#load_plugins_before',
                            files: [
                            "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/chain-code-summary-expanded/chainCodeSummaryExpanded.js" 
                            ]
                        });
                    }]
                }
            }
        }

    })
    .state('main.batchReleaseUtility', {
        url: "/batchReleaseUtility",
        data: {
            pageTitle: 'Batch Release Utility',
            accessAllow: {'report_id':18,'web_menus':false}
        },
        views: {
            'home': {
                templateUrl: "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/batch-release-utility/batchReleaseUtility.html",
                controller: "batchReleaseUtilityCtrl",
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: ModuleName,
                            insertBefore: '#load_plugins_before',
                            files: [
                            "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/batch-release-utility/batchReleaseUtility.js" 
                            ]
                        });
                    }]
                }
            }
        }

    })
    .state('main.claimedAllExcludingHotel', {
        url: "/claimedAllExcludingHotel",
        data: {
            pageTitle: 'Claimed - Hotel Only - Account',
            accessAllow: {'report_id':64,'web_menus':false}
        },
        views: {
            'home': {
                templateUrl: "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/report/claimed-AllExcludingHotel.html",
                controller: "claimed-AllExcludingHotel",
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: ModuleName,
                            insertBefore: '#load_plugins_before',
                            files: [
                            "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/report/claimed-AllExcludingHotel.js" 
                            ]
                        });
                    }]
                }
            }
        }

    })
    .state('main.claimedAllTypeAccount', {
        url: "/claimedAllTypeAccount",
        data: {
            pageTitle: 'Claimed - All Types - Account',
            accessAllow: {'report_id':67,'web_menus':false}
        },
        views: {
            'home': {
                templateUrl: "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/report/claimed-AllTypeAccount.html",
                controller: "claimed-AllTypeAccount",
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: ModuleName,
                            insertBefore: '#load_plugins_before',
                            files: [
                            "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/report/claimed-AllTypeAccount.js" 
                            ]
                        });
                    }]
                }
            }
        }

    })
    .state('main.claimed-AllHotelCarAndOther', {
        url: "/claimed-AllHotelCarAndOther",
        data: {
            pageTitle: 'Claimed - All Types: Hotel, Car & Other - Account',
            accessAllow: {'report_id':66,'web_menus':false}
        },
        views: {
            'home': {
                templateUrl: "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/report/claimed-AllHotelCarAndOther.html",
                controller: "claimed-AllHotelCarAndOther",
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: ModuleName,
                            insertBefore: '#load_plugins_before',
                            files: [
                            "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/report/claimed-AllHotelCarAndOther.js" 
                            ]
                        });
                    }]
                }
            }
        }
    })
    .state('main.claimed-AllExcludingHotel-Agent', {
        url: "/claimed-AllExcludingHotel-Agent",
        data: {
            pageTitle: 'Claimed - All excluding Hotel',
            accessAllow: {'report_id':10,'web_menus':false}
        },
        views: {
            'home': {
                templateUrl: "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/report/claimed-AllExcludingHotel-Agent.html",
                controller: "claimed-AllExcludingHotel-Agent",
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: ModuleName,
                            insertBefore: '#load_plugins_before',
                            files: [
                            "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/report/claimed-AllExcludingHotel-Agent.js" 
                            ]
                        });
                    }]
                }
            }
        }
    })
    .state('main.claimed-AllTypes-Agent', {
        url: "/claimed-AllTypes-Agent",
        data: {
            pageTitle: 'Claimed - All Types',
            accessAllow: {'report_id':12,'web_menus':false}
        },
        views: {
            'home': {
                templateUrl: "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/report/claimed-AllTypes-Agent.html",
                controller: "claimed-AllTypes-Agent",
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: ModuleName,
                            insertBefore: '#load_plugins_before',
                            files: [
                            "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/report/claimed-AllTypes-Agent.js" 
                            ]
                        });
                    }]
                }
            }
        }
    })
    .state('main.claimed-AllHotelCarAndOther-Agent', {
        url: "/claimed-AllHotelCarAndOther-Agent",
        data: {
            pageTitle: 'Claimed - All Types: Hotel, Car & Other',
            accessAllow: {'report_id':11,'web_menus':false}
        },
        views: {
            'home': {
                templateUrl: "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/report/claimed-AllHotelCarAndOther-Agent.html",
                controller: "claimed-AllHotelCarAndOther-Agent",
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: ModuleName,
                            insertBefore: '#load_plugins_before',
                            files: [
                            "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/report/claimed-AllHotelCarAndOther-Agent.js" 
                            ]
                        });
                    }]
                }
            }
        }
    })
    .state('main.claimed-hotelonly-Agent', {
        url: "/claimed-hotelonly-Agent",
        data: {
            pageTitle: 'Claimed - All Hotel Only',
            accessAllow: {'report_id':6,'web_menus':false}
        },
        views: {
            'home': {
                templateUrl: "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/report/claimed-hotelonly-Agent.html",
                controller: "claimed-HotelOnly-Agent",
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: ModuleName,
                            insertBefore: '#load_plugins_before',
                            files: [
                            "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/report/claimed-hotelonly-Agent.js" 
                            ]
                        });
                    }]
                }
            }
        }
    })
    .state('main.unclaimed-AllExcludingHotel-Agent', {
        url: "/unclaimed-AllExcludingHotel-Agent",
        data: {
            pageTitle: 'Unclaimed - All Excluding Hotel',
            accessAllow: {'report_id':14,'web_menus':false}
        },
        views: {
            'home': {
                templateUrl: "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/report/unclaimed-AllExcludingHotel-Agent.html",
                controller: "unclaimed-AllExcludingHotel-Agent",
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: ModuleName,
                            insertBefore: '#load_plugins_before',
                            files: [
                            "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/report/unclaimed-AllExcludingHotel-Agent.js" 
                            ]
                        });
                    }]
                }
            }
        }
    })
    .state('main.unclaimed-AllTypes', {
        url: "/unclaimed-AllTypes",
        data: {
            pageTitle: 'Unclaimed - All Types',
            accessAllow: {'report_id':16,'web_menus':false}
        },
        views: {
            'home': {
                templateUrl: "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/report/unclaimed-AllTypes.html",
                controller: "unclaimed-AllTypes-Agent",
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: ModuleName,
                            insertBefore: '#load_plugins_before',
                            files: [
                            "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/report/unclaimed-AllTypes.js" 
                            ]
                        });
                    }]
                }
            }
        }
    })
    .state('main.unclaimed-AllTypesHotelCarAndOther-Agent', {
        url: "/unclaimed-AllTypesHotelCarAndOther-Agent",
        data: {
            pageTitle: 'Unclaimed - All Types Hotel Car and Other',
            accessAllow: {'report_id':15,'web_menus':false}
        },
        views: {
            'home': {
                templateUrl: "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/report/unclaimed-AllTypesHotelCarAndOther-Agent.html",
                controller: "unclaimed-AllHotelCarAndOther-Agent",
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: ModuleName,
                            insertBefore: '#load_plugins_before',
                            files: [
                            "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/report/unclaimed-AllTypesHotelCarAndOther-Agent.js" 
                            ]
                        });
                    }]
                }
            }
        }
    })
    .state('main.unclaimed-hotelonly-Agent', {
        url: "/unclaimed-hotelonly-Agent",
        data: {
            pageTitle: 'Unclaimed - Hotel Only',
            accessAllow: {'report_id':13,'web_menus':false}
        },
        views: {
            'home': {
                templateUrl: "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/report/unclaimed-hotelonly-Agent.html",
                controller: "unclaimed-hotelonly-Agent",
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: ModuleName,
                            insertBefore: '#load_plugins_before',
                            files: [
                            "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/report/unclaimed-hotelonly-Agent.js" 
                            ]
                        });
                    }]
                }
            }
        }
    })
    .state('main.importerFileLog', {
        url: "/importerFileLog",
        data: {
            pageTitle: 'Importer File Log',
            // accessAllow: {'report_id':41,'web_menus':false}
        },
        views: {
            'home': {
                templateUrl: "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/importer/importer_file_log.html",
                controller: "importerFileLogCtrl",
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: ModuleName,
                            insertBefore: '#load_plugins_before',
                            files: [
                            "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/importer/importer_file_log.js" 
                            ]
                        });
                    }]
                }
            }
        }

    })
    .state('main.importerRunLog', {
        url: "/importerRunLog",
        data: {
            pageTitle: 'Importer Run Log',
            // accessAllow: {'report_id':41,'web_menus':false}
        },
        views: {
            'home': {
                templateUrl: "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/importer/importer_run_log.html",
                controller: "importerRunLogCtrl",
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: ModuleName,
                            insertBefore: '#load_plugins_before',
                            files: [
                            "" + SETTINGS[GLOBALS.ENV].siteUrl + "app/components/importer/importer_run_log.js" 
                            ]
                        });
                    }]
                }
            }
        }

    })
}]);